import Swal from 'sweetalert2';  

export const DANH_SACH_CHUYEN_NGANH = 'danh sach chuyen nganh';
export const CHI_TIET_CHUYEN_NGANH_THEO_MA_SO= 'CHI_TIET_CHUYEN_NGANH_THEO_MA_SO';
export const FETCH_CHUYEN_NGANH = 'FETCH_CHUYEN_NGANH';
export const THEM_CHUYEN_NGANH ='THEM_CHUYEN_NGANH';
export const CAP_NHAT_CHUYEN_NGANH ='CAP_NHAT_CHUYEN_NGANH';
export const XOA_CHUYEN_NGANH ='XOA_CHUYEN_NGANH';
export const THONG_BA0 = "THONG BAO";

export const DANH_SACH_MON_HOC = 'danh sach mon hoc';
export const CHI_TIET_MON_HOC_THEO_MA_MH= 'CHI_TIET_MON_HOC_THEO_MA_MH';
export const THEM_MON_HOC = 'THEM_MON_HOC';
export const CAP_NHAT_MON_HOC = 'CAP_NHAT_MON_HOC';
export const XOA_MON_HOC = 'XOA_MON_HOC';
export const LAY_DSMH_THEO_TEN='LAY_DSMH_THEO_TEN';

export const FETCH_MON_HOC = 'FETCH_MON_HOC';

export const DANH_SACH_VI_PHAM = 'danh sach vi pham';
export const CHI_TIET_VI_PHAM_THEO_MA_VP= 'CHI_TIET_VI_PHAM_THEO_MA_VP';
export const FETCH_VI_PHAM = 'FETCH_VI_PHAM';
export const THEM_VI_PHAM = 'THEM_VI_PHAM';
export const CAP_NHAT_VI_PHAM = 'CAP_NHAT_VI_PHAM';
export const XOA_VI_PHAM = 'XOA_VI_PHAM';

export const DANH_SACH_GIAO_VIEN = 'danh sach giao vien';
export const CHI_TIET_GIAO_VIEN_THEO_MA_GV= 'CHI_TIET_GIAO_VIEN_THEO_MA_GV';
export const FETCH_GIAO_VIEN = 'FETCH_GIAO_VIEN';  
export const THEM_GIAO_VIEN = 'THEM_GIAO_VIEN';
export const CAP_NHAT_GIAO_VIEN = 'CAP_NHAT_GIAO_VIEN';
export const XOA_GIAO_VIEN = 'XOA_GIAO_VIEN';
export const LAY_DSGV_THEO_TEN ='LAY_DSGV_THEO_TEN';

export const DANH_SACH_KINH_PHI = 'danh sach kinh phi';
export const CHI_TIET_KINH_PHI_THEO_MA_KHOA= 'CHI_TIET_KINH_PHI_THEO_MA_KP';
export const FETCH_KINH_PHI = 'FETCH_KINH_PHI';
export const THEM_KINH_PHI='THEM_KINH_PHI';
export const CAP_NHAT_KINH_PHI='CAP_NHAT_KINH_PHI';
export const XOA_KINH_PHI='XOA_KINH_PHI';

export const DANH_SACH_KHOA = 'danh sach KHOA';
export const CHI_TIET_KHOA_THEO_MA_KHOA= 'CHI_TIET_KHOA_THEO_MA_KP';
export const FETCH_KHOA = 'FETCH_KHOA';
export const THEM_KHOA='THEM_KHOA';
export const CAP_NHAT_KHOA='CAP_NHAT_KHOA';
export const XOA_KHOA='XOA_KHOA';

export const DANH_SACH_LOP = 'danh sach LOP';
export const CHI_TIET_LOP_THEO_MA_LOP= 'CHI_TIET_LOP_THEO_MA_LOP';
export const FETCH_LOP = 'FETCH_LOP';
export const THEM_LOP='THEM_LOP';
export const CAP_NHAT_LOP='CAP_NHAT_LOP';
export const XOA_LOP='XOA_LOP';
export const DANH_SACH_SV_THEO_LOP = 'DANH_SACH_SV_THEO_LOP';
export const DANH_SACH_SINH_VIEN_THEO_LOP_HOC_PHAN = 'DANH_SACH_SINH_VIEN_THEO_LOP_HOC_PHAN';

export const DANH_SACH_LOP_HOC_PHAN = 'danh sach LOP_HOC_PHAN';
export const CHI_TIET_LOP_HOC_PHAN_THEO_MA_LOP_HOC_PHAN= 'CHI_TIET_LOP_HOC_PHAN_THEO_MA_LOP_HOC_PHAN';
export const THOI_KHOA_BIEU_LOP_HOC_PHAN_THEO_MSSV = 'THOI_KHOA_BIEU_LOP_HOC_PHAN_THEO_MSSV';
export const FETCH_LOP_HOC_PHAN = 'FETCH_LOP_HOC_PHAN';
export const THEM_LOP_HOC_PHAN='THEM_LOP_HOC_PHAN';
export const CAP_NHAT_LOP_HOC_PHAN='CAP_NHAT_LOP_HOC_PHAN';
export const XOA_LOP_HOC_PHAN='XOA_LOP_HOC_PHAN';

export const DANH_SACH_ADMIN = 'danh sach ADMIN';
export const CHI_TIET_ADMIN_THEO_MA_ADMIN= 'CHI_TIET_ADMIN_THEO_MA_ADMIN';
export const CHI_TIET_USER_THEO_MA_USER= 'CHI_TIET_USER_THEO_MA_USER';
export const FETCH_ADMIN = 'FETCH_ADMIN';
export const THEM_ADMIN='THEM_ADMIN';
export const CAP_NHAT_ADMIN='CAP_NHAT_ADMIN';
export const XOA_ADMIN='XOA_ADMIN';
export const DANG_NHAP_ADMIN= 'DANG_NHAP_ADMIN';
export const DANG_XUAT_ADMIN= 'DANG_XUAT_ADMIN';
export const XAC_THUC_TAI_KHOAN ='XAC_THUC_TAI_KHOAN';
export const KIEM_TRA_MA_OTP ='KIEM_TRA_MA_OTP';
export const CAP_NHAT_MAT_KHAU='CAP_NHAT_MAT_KHAU';
export const CAP_NHAT_AVATAR_ADMIN='CAP_NHAT_AVATAR_ADMIN';
export const DOI_MAT_KHAU ='DOI_MAT_KHAU';

export const DANH_SACH_SINH_VIEN = 'danh sach SINHVIEN';
export const CHI_TIET_SINHVIEN_THEO_MA_SINHVIEN= 'CHI_TIET_SINHVIEN_THEO_MA_SINHVIEN';
export const FETCH_SINH_VIEN = 'FETCH_SINHVIEN';
export const THEM_SINH_VIEN='THEM_SINHVIEN';
export const THEM_SINH_VIEN_TU_EXCEL_FILE = 'THEM_SINH_VIEN_TU_EXCEL_FILE';
export const LAY_DSSV_THEO_MA_SINHVIEN = 'LAY_DSSV_THEO_MA_SINHVIEN'

export const CAP_NHAT_SINH_VIEN='CAP_NHAT_SINHVIEN';
export const XOA_SINH_VIEN='XOA_SINHVIEN';
export const SINH_VIEN_CAP_NHAT_KINH_PHI = 'SINH_VIEN_CAP_NHAT_KINH_PHI';
export const THEM_VI_PHAM_SINH_VIEN = 'THEM_VI_PHAM_SINH_VIEN';

export const DANH_SACH_CTLOP = 'danh sach CTLOP';
export const CHI_TIET_CTLOP_THEO_MA_CTLOP= 'CHI_TIET_CTLOP_THEO_MA_CTLOP';
export const FETCH_CTLOP = 'FETCH_CTLOP';
export const THEM_CTLOP='THEM_CTLOP';
export const CAP_NHAT_CTLOP='CAP_NHAT_CTLOP';
export const XOA_CTLOP='XOA_CTLOP';

export const DANH_SACH_CTLOP_HOC_PHAN = 'danh sach CTLOP_HOC_PHAN';
export const CHI_TIET_CTLOP_HOC_PHAN_THEO_MA_CTLOP_HOC_PHAN= 'CHI_TIET_CTLOP_HOC_PHAN_THEO_MA_CTLOP_HOC_PHAN';
export const FETCH_CTLOP_HOC_PHAN = 'FETCH_CTLOP_HOC_PHAN';
export const THEM_CTLOP_HOC_PHAN='THEM_CTLOP_HOC_PHAN';
export const CAP_NHAT_CTLOP_HOC_PHAN='CAP_NHAT_CTLOP_HOC_PHAN';
export const XOA_CTLOP_HOC_PHAN='XOA_CTLOP_HOC_PHAN';

export const DANH_SACH_LICH_THI = 'DANH_SACH_LICH_THI';
export const CHI_TIET_LICH_THI_THEO_MA_LICH_THI= 'CHI_TIET_LICH_THI_THEO_MA_LICH_THI';
export const FETCH_LICH_THI= 'FETCH_LICH_THI';
export const THEM_LICH_THI ='THEM_LICH_THI';
export const CAP_NHAT_LICH_THI= 'CAP_NHAT_LICH_THI';
export const XOA_LICH_THI = 'XOA_LICH_THI';

export const DANH_SACH_PHU_HUYNH = 'DANH_SACH_PHU_HUYNH';
export const CHI_TIET_PHU_HUYNH_THEO_MA_PHU_HUYNH= 'CHI_TIET_PHU_HUYNH_THEO_MA_PHU_HUYNH';
export const FETCH_PHU_HUYNH= 'FETCH_PHU_HUYNH';
export const THEM_PHU_HUYNH ='THEM_PHU_HUYNH';
export const CAP_NHAT_PHU_HUYNH= 'CAP_NHAT_PHU_HUYNH';
export const XOA_PHU_HUYNH = 'XOA_PHU_HUYNH'; 
export const LAY_DSPH_THEO_TEN ='LAY_DSPH_THEO_TEN';

export const DANH_SACH_YEU_CAU = 'DANH_SACH_YEU_CAU'; 
export const DANH_SACH_PHU_HUYNH_CHO_OPTION = 'DANH_SACH_PHU_HUYNH_CHO_OPTION';
export const DANH_SACH_MON_HOC_CHO_OPTION = 'DANH_SACH_MON_HOC_CHO_OPTION';
export const DANH_SACH_SINH_VIEN_CHO_OPTION = 'DANH_SACH_SINH_VIEN_CHO_OPTION';
export const DANH_SACH_GIAO_VIEN_CHO_OPTION = 'DANH_SACH_GIAO_VIEN_CHO_OPTION';

export const XOA_YEU_CAU = 'XOA_YEU_CAU';
export const CAP_NHAT_YEU_CAU='CAP_NHAT_YEU_CAU';
export const CHI_TIET_YEU_CAU_THEO_MA_YC = 'CHI_TIET_YEU_CAU_THEO_MA_YC';
// export const server = 'http://localhost:8000/api/';
// export const serverUploadHinh = 'http://localhost:8000/uploads/avatar/';
// export const serverUploadFile = "http://localhost:8000/files/"

// export const server = 'https://apickcnmdl.herokuapp.com/api/'; 
// export const serverUploadHinh = 'https://apickcnmdl.herokuapp.com/uploads/avatar/';
// export const serverUploadFile = "https://apickcnmdl.herokuapp.com/files/"

export const server = 'https://appckc.phamthang.dev/api/'; 
export const serverUploadHinh = 'https://appckc.phamthang.dev/uploads/avatar/';
export const serverUploadFile = "https://appckc.phamthang.dev/files/";
export const CHINH_SUA_KINH_PHI='CHINH_SUA_KINH_PHI';
export function ThongBaoThanhCong(){
    Swal.fire({
        type: 'success',
        title: 'Thành Công!',
        showConfirmButton: false,
        timer: 1000
    })
}
export function ThongBaoKhongTimThay(){
    Swal.fire({
        type: 'warning',
        title: 'Không tìm thấy, mời bạn kiểm tra lại!',
        showConfirmButton: false,
        timer: 1000
    })
}
export function ThongBaoThatBai(){ 
    Swal.fire({
        type: 'warning',
        title: 'Thất bại!',
        showConfirmButton: false,
        timer: 1000
    })
}
export function ThongBaoMaLoi(chuoi){
    Swal.fire({
        type: 'warning',
        title: chuoi,
        showConfirmButton: false,
        timer: 1000
    })
    
}
export function ThongBaoDuLieuKhongTonTai(){ 
    Swal.fire({
        type: 'warning',
        title: 'Dữ liệu này không tồn tại!',
        showConfirmButton: false,
        timer: 1000
    })
} 