import $ from 'jquery';
import Swal from 'sweetalert2';  
export let dem = 0;
export function KiemTraNhapDuLieu(e){   
    var target = e.target;
    var tenThe = target.name;
    var giaTri = target.value.trim(); 
    if(giaTri.length<1 || giaTri ==="" ){
       $("#"+tenThe).removeClass("none")  
        dem++;
    }
    else{ 
        $("#"+tenThe).addClass("none") 
        dem = 0;
    }  
} 
export function KiemTraDuLieuNhapLaMatKhau(e){  
    var target = e.target;
    var tenThe = target.name;
    var giaTri = target.value.trim(); 
    if(giaTri.length<7 || giaTri ==="" ){
       $("#"+tenThe).removeClass("none") 
       dem++;
    }
    else{ 
        $("#"+tenThe).addClass("none")
        dem = 0
    }  
}
export function KiemTraDuLieuNhapLaSoDT(e){  
    var target = e.target;
    var tenThe = target.name;
    var giaTri = target.value.trim(); 
    const regexp = /^\d{10,11}$/;  
    if(regexp.exec(giaTri) ===null ){
       $("#"+tenThe).removeClass("none") 
    }
    else{ 
        $("#"+tenThe).addClass("none")
    }  
}
export function KiemTraDuLieuNhapLaDiem(e){  
    var target = e.target;
    var tenThe = target.name;
    var giaTri = target.value.trim();   
    const regexp = /^[0-9]{1}\.[0-9]{1,2}$/gm;   
    if(giaTri.length<1 || regexp.exec(giaTri) ===null ){
       $("#"+tenThe).removeClass("none") 
    }
    else{ 
        $("#"+tenThe).addClass("none")
    }  
}
export function KiemTraDuLieuNhapLaSo(e){  
    var target = e.target;
    var tenThe = target.name;
    var giaTri = target.value.trim(); 
    console.log(giaTri ) 
    const regexp = /^[0-9]{1,}$/gm;   
    if(giaTri.length<1 || regexp.exec(giaTri) ===null ){
       $("#"+tenThe).removeClass("none") 
    }
    else{ 
        $("#"+tenThe).addClass("none")
    }  
}
export function KiemTraDuLieuNhapLaKhoa(e){  
    var target = e.target;
    var tenThe = target.name;
    var giaTri = target.value.trim();  
    const regexp = /^[0-9]{1,3}$/gm;   
    if(giaTri.length<1 || regexp.exec(giaTri) ===null ){
       $("#"+tenThe).removeClass("none") 
    }
    else{ 
        $("#"+tenThe).addClass("none")
    }  
}
export function KiemTraDuLieuNhapLaChu(e){  
    var target = e.target;
    var tenThe = target.name;
    var giaTri = target.value.trim();  
    const regexp = /^[a-zA-Z]{1,}$/;   
    if(giaTri.length<1 || regexp.exec(giaTri) ===null ){
       $("#"+tenThe).removeClass("none") 
    }
    else{ 
        $("#"+tenThe).addClass("none")
    }  
}

export function KiemTraDuLieuNhapLaEmail(e){ 
    var target = e.target;
    var tenThe = target.name;
    var giaTri = target.value.trim(); 
    const regexp=/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/; 
    if(giaTri.length<1 || regexp.exec(giaTri) ===null ){
       $("#"+tenThe).removeClass("none")
    }
    else{ 
        $("#"+tenThe).addClass("none")
    }  
}

export function ThongBaoThanhCong(){
    Swal.fire({
        type: 'success',
        title: 'Thành Công!',
        showConfirmButton: true, 
    })
}
export function ThongBaoKhongTimThay(){
    Swal.fire({
        type: 'warning',
        title: 'Không tìm thấy, mời bạn kiểm tra lại!',
        showConfirmButton: true, 
    })
}
export function ThongBaoThatBai(){ 
    Swal.fire({
        type: 'warning',
        title: 'Thất bại!',
        showConfirmButton: false,
        timer: 1000
    })
}
export function ThongBaoMaLoi(chuoi){
    Swal.fire({
        type: 'warning',
        title: chuoi,
        showConfirmButton: false,
        timer: 1000
    })
    
}
export function ThongBaoDuLieuKhongTonTai(){ 
    Swal.fire({
        type: 'warning',
        title: 'Dữ liệu này không tồn tại!',
        showConfirmButton: false,
        timer: 1000
    })
} 