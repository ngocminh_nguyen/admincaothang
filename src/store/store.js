import {createStore, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
import reducers from '../reducers';
import { composeWithDevTools } from 'redux-devtools-extension';
import { loadingBarMiddleware } from 'react-redux-loading-bar'
// import rootReducer from 'reducers'
const initialState = {};
const Middleware = [thunk];
const store = createStore(
    reducers,
    initialState,
    composeWithDevTools(
     applyMiddleware(...Middleware,
        // loadingBarMiddleware()
        )
    )
);

export default store;