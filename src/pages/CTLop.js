import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import Header from "../components/header";
import CTLop from "../components/chi-tiet-lop";
class ChiTietLop extends Component {
    render() {
        return (
            <div className="wrapper">
                <Header />
                <CTLop  id={this.props.match.params.id} />
            </div>
        );
    }
}

export default ChiTietLop;
