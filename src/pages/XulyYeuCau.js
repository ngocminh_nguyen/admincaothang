import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';  
import Header from "../components/header";
import XLYC from "../components/xu-ly-yeu-cau-sinh-vien";
import XLYCPH from "../components/xu-ly-yeu-cau-phu-huynh";
class XLYeuCau extends Component {
  render() {
    return (
        <div className="wrapper">
            <Header/> 
            {this.props.match.params.loai==="1"?<XLYC id={this.props.match.params.id} loai={this.props.match.params.loai}/>:<XLYCPH id={this.props.match.params.id} loai={this.props.match.params.loai}/>} 
        </div>
    );
  }
}

export default XLYeuCau;
