import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';  
import Header from "../components/header";
import KP from "../components/kinh-phi";
class KinhPhi extends Component {
  render() {
    return (
        <div className="wrapper">
            <Header/>
            <KP />
        </div>
    );
  }
}

export default KinhPhi;
