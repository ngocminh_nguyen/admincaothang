import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';  
import Header from "../components/header";
import PH from "../components/phu-huynh";
class PhuHuynh extends Component {
  render() {
    return (
        <div className="wrapper">
            <Header/>
            <PH/>
        </div>
    );
  }
}

export default PhuHuynh;
