import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import Header from "../components/header";
import LichThi from './../components/ChucNangThem_CapNhat/them-lich-thi';
class ThemLichThi extends Component {
    render() {
        return (
            <div className="wrapper">
                <Header />
                <LichThi id={this.props.match.params.id} loai={this.props.match.params.loai}/>
            </div>
        );
    }
}

export default ThemLichThi;
