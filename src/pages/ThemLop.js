import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import Header from "../components/header";
import Lop from './../components/ChucNangThem_CapNhat/them-lop';
class ThemLop extends Component {
    render() {
        return (
            <div className="wrapper">
                <Header />
                <Lop id={this.props.match.params.id} />
            </div>
        );
    }
}

export default ThemLop;
