import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import Header from "../components/header";
import ProAd from "../components/profile-admin";
class ProfileAdmin extends Component {
    render() {
        return (
            <div className="wrapper">
                <Header />
                <ProAd  id={this.props.match.params.id}/>
            </div>
        );
    }
}

export default ProfileAdmin;
