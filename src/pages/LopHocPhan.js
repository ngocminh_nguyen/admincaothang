import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import Header from "../components/header";
import LHocPhan from "../components/lop-hoc-phan";
class LopHocPhan extends Component {
    render() {
        return (
            <div className="wrapper">
                <Header />
                <LHocPhan id={this.props.match.params.id}/>
            </div>
        );
    }
}

export default LopHocPhan;
