import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';  
import Header from "../components/header";
import PhuHuynh from './../components/ChucNangThem_CapNhat/them-phu-huynh';
class ThemPhuHuynh extends Component {
  render() {
    return (
        <div className="wrapper">
            <Header/>
            <PhuHuynh id={this.props.match.params.id}/>
        </div>
    );
  }
}

export default ThemPhuHuynh;
