import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import Header from "../components/header";
import CTLop from '../components/ChucNangThem_CapNhat/them-chi-tiet-lop';
class ThemCTLop extends Component {
    render() {
        return (
            <div className="wrapper">
                <Header />
                <CTLop id={this.props.match.params.id} />
            </div>
        );
    }
}

export default ThemCTLop;
