import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';  
import Header from "../components/header";
import VP from "../components/vi-pham";
class ViPham extends Component {
  render() {
    return (
        <div className="wrapper">
            <Header/>
            <VP id={this.props.match.params.id}/>
        </div>
    );
  }
}

export default ViPham;
