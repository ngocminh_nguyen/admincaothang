import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import Header from "../components/header";
import Khoa from "../components/khoa";
class KhoaHoc extends Component {
    render() {
        return (
            <div className="wrapper">
                <Header />
                <Khoa/>
            </div>
        );
    }
}

export default KhoaHoc;
