import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';  
import Header from "../components/header";
import ViPham from './../components/ChucNangThem_CapNhat/them-vi-pham';
class ThemViPham extends Component {
  render() {
    return (
        <div className="wrapper">
            <Header/>
            <ViPham id={this.props.match.params.id}/>
        </div>
    );
  }
}

export default ThemViPham;
