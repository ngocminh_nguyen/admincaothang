import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import Header from "../components/header";
import LopHoc from "../components/lop";
class Lop extends Component {
    render() {
        return (
            <div className="wrapper">
                <Header />
                <LopHoc id={this.props.match.params.id}/>
            </div>
        );
    }
}

export default Lop;
