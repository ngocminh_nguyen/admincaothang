import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';  
import Header from "../components/header";
import CN from "../components/chuyen-nganh";
class ChuyenNganh extends Component {
  render() {
    return (
        <div className="wrapper">
            <Header/>
            <CN/>
        </div>
    );
  }
}

export default ChuyenNganh;
