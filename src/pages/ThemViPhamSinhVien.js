import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';  
import Header from "../components/header";
import ViPhamSV from './../components/ChucNangThem_CapNhat/them-vi-pham-sinh-vien';
class ThemViPhamSV extends Component {
  render() {
    return (
        <div className="wrapper">
            <Header/>
            <ViPhamSV id={this.props.match.params.id}/>
        </div>
    );
  }
}

export default ThemViPhamSV;
