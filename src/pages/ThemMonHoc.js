import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';  
import Header from "../components/header";
import MonHoc from './../components/ChucNangThem_CapNhat/them-mon-hoc';
class ThemMonHoc extends Component {
  render() {
    return (
        <div className="wrapper">
            <Header/>
            <MonHoc id={this.props.match.params.id}/>
        </div>
    );
  }
}

export default ThemMonHoc;
