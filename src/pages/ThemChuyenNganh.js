import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import Header from "../components/header";
import ChuyenNganh from './../components/ChucNangThem_CapNhat/them-chuyen-nganh';
class ThemChuyenNganh extends Component {
  render() {
    return (
      <div className="wrapper">
        <Header />
        <ChuyenNganh id={this.props.match.params.id} />
      </div>
    );
  }
}

export default ThemChuyenNganh;
