import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';  
import Header from "../components/header";
import SinhVien from '../components/ChucNangThem_CapNhat/them-sinh-vien';
class ThemSinhVien extends Component {
  render() {
    return (
        <div className="wrapper">
            <Header/>
            <SinhVien id={this.props.match.params.id}/>
        </div>
    );
  }
}

export default ThemSinhVien;
