import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import Header from "../components/header";
import Admin from '../components/ChucNangThem_CapNhat/them-admin';
class ThemAdmin extends Component {
    render() {
        return (
            <div className="wrapper">
                <Header />
                <Admin id={this.props.match.params.id} />
            </div>
        );
    }
}

export default ThemAdmin;
