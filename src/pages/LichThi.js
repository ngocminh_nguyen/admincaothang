import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';  
import Header from "../components/header";
import LT from "../components/lich-thi";
class LichThi extends Component {
  render() {
    return (
        <div className="wrapper">
            <Header/>
            <LT id={this.props.match.params.id}/>
        </div>
    );
  }
}

export default LichThi;
