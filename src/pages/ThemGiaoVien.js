import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import Header from "../components/header";
import GiaoVien from './../components/ChucNangThem_CapNhat/them-giao-vien';
class ThemGiaoVien extends Component {
  render() {
    return (
      <div className="wrapper">
        <Header />
        <GiaoVien id={this.props.match.params.id} />
      </div>
    );
  }
}

export default ThemGiaoVien;
