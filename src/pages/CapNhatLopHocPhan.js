import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import Header from "../components/header";
import CNLopHocPhan from './../components/ChucNangThem_CapNhat/cap-nhat-lop-hoc-phan';
class CapNhatLopHocPhan extends Component {
    render() {
        return (
            <div className="wrapper">
                <Header />
                <CNLopHocPhan id={this.props.match.params.id} />
            </div>
        );
    }
}

export default CapNhatLopHocPhan;
