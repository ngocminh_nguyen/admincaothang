import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import Header from "../components/header";
import LopHocPhan from './../components/ChucNangThem_CapNhat/them-lop-hoc-phan';
class ThemLopHocPhan extends Component {
    render() {
        return (
            <div className="wrapper">
                <Header />
                <LopHocPhan id={this.props.match.params.id} />
            </div>
        );
    }
}

export default ThemLopHocPhan;
