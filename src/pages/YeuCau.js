import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';  
import Header from "../components/header";
import YC from "../components/yeu-cau";
class YeuCau extends Component {
  render() {
    return (
        <div className="wrapper">
            <Header/>
            <YC id={this.props.match.params.id}/>
        </div>
    );
  }
}

export default YeuCau;
