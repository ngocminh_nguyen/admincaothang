import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';  
import Header from "../components/header";
import MH from "../components/mon-hoc";
class MonHoc extends Component {
  render() {
    return (
        <div className="wrapper">
            <Header/>
            <MH/>
        </div>
    );
  }
}

export default MonHoc;
