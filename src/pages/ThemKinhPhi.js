import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';  
import Header from "../components/header";
import KinhPhi from './../components/ChucNangThem_CapNhat/them-kinh-phi';
class ThemKinhPhi extends Component {
  render() {
    return (
        <div className="wrapper">
            <Header/>
            <KinhPhi id={this.props.match.params.id}/>
        </div>
    );
  }
}

export default ThemKinhPhi;
