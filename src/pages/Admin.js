import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import Header from "../components/header";
import AD from "../components/admin";
class Admin extends Component {
    render() {
        return (
            <div className="wrapper">
                <Header />
                <AD />
            </div>
        );
    }
}

export default Admin;
