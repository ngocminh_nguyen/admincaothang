import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';  
import Header from "../components/header";
import GV from "../components/giao-vien";
class GiaoVien extends Component {
  render() {
    return (
        <div className="wrapper">
            <Header/>
            <GV/>
        </div>
    );
  }
}

export default GiaoVien;
