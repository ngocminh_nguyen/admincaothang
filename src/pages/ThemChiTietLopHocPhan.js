import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import Header from "../components/header";
import CTLopHocPhan from '../components/ChucNangThem_CapNhat/them-chi-tiet-lop-hoc-phan';
class ThemCTLopHocPhan extends Component {
    render() {
        return (
            <div className="wrapper">
                <Header /> 
                <CTLopHocPhan id={this.props.match.params.id} mssv={this.props.match.params.mssv}/>
            </div>
        );
    }
}

export default ThemCTLopHocPhan;
