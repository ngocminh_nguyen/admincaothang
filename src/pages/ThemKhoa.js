import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import Header from "../components/header";
import Khoa from './../components/ChucNangThem_CapNhat/them-khoa';
class ThemKhoa extends Component {
    render() {
        return (
            <div className="wrapper">
                <Header />
                <Khoa id={this.props.match.params.id} />
            </div>
        );
    }
}

export default ThemKhoa;
