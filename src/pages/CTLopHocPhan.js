import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import Header from "../components/header";
import CTLopHocPhan from "../components/chi-tiet-lop-hoc-phan";
class ChiTietLopHocPhan extends Component {
    render() {
        return (
            <div className="wrapper">
                <Header />
                <CTLopHocPhan id={this.props.match.params.id} />
            </div>
        );
    }
}

export default ChiTietLopHocPhan;
