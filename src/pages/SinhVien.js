import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import Header from "../components/header";
import SV from "../components/sinh-vien";
class SinhVien extends Component {
    render() {
        return (
            <div className="wrapper">
                <Header />
                <SV />
            </div>
        );
    }
}

export default SinhVien;
