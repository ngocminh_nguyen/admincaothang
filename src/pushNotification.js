import firebase from "firebase/app";
import "firebase/messaging";

const config = {
  apiKey: "AIzaSyBWC0lUSDZxwZn5x5jIRpLfXxtDImufUL0",
  authDomain: "appckc-4105e.firebaseapp.com",
  databaseURL: "https://appckc-4105e.firebaseio.com",
  projectId: "appckc-4105e",
  storageBucket: "appckc-4105e.appspot.com",
  messagingSenderId: "966589584312"
};

export const initFirebase = () => {
  firebase.initializeApp(config);
};

export const permissionToReceiveNotification = async () => {
  try {
    const messaging = firebase.messaging();
    await messaging.requestPermission();
    const token = await messaging.getToken();
    console.log("Token của bạn : ", token);
    return token;
  } catch (error) {
    console.error(error);
  }
};