import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css'; 
import propTypes from 'prop-types';
import khung from "../assets/img/khung.png";
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import DSLopHocPhan from './DanhSach/ds-chi-tiet-lop-hoc-phan';
import LopHocPhan from './Item/chi-tiet-lop-hoc-phan-item';
import {DanhSachSinhVienTheoLopHocPhan  } from './../actions/lop_hoc_phan';
class ctlophocphan extends Component {
    constructor(props) {
        super(props);
        this.state = {  
            DSLopHocPhan:[],  
            ctLopHocPhanItem:[],
        }  
        this.HienThiChiTietLopHocPhan = this.HienThiChiTietLopHocPhan.bind(this);
    }
    componentDidMount() {
        this.props.DanhSachSinhVienTheoLopHocPhan(this.props.id);
    }
    componentWillReceiveProps(nextProps) {
        this.setState({ DSLopHocPhan: nextProps.DSLopHocPhan}); 
    } 
    render() {   
        return (
            <div id="body-content">
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-md-12">
                            <div className="content-title">
                                <h1 className="text-center h1-title-style">Danh Sách Sinh Viên Lớp Học Phần {this.props.id}</h1>
                                <span className="kitu-dacbiet">&#9884;</span>
                            </div>
                        </div>
                        <div className="col-md-12">
                            <Link to={`/lophocphan/${this.props.id}/themsinhvien`} className="button-them--style button-default--style button-thaotac--style">Thêm Chi Tiết Lớp Học Phần</Link>
                            <Link to={`/lophocphan`} className="button-them--style button-default--style button-thaotac--style">Quay Lại</Link>
                        </div> 
                        <div className="col-md-12"> 
                            <div className="tableTKB">
                                <div className="chuc-nang-them" background={khung}>
                                    <input className="search-data-table pl-1" name="seart-data-table" placeholder="Search..."></input>
                                </div>
                            <DSLopHocPhan>
                                {()=>this.HienThiChiTietLopHocPhan(this.state.DSLopHocPhan)}
                            </DSLopHocPhan>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
    HienThiChiTietLopHocPhan=(ctLopHocPhanItem)=>{
        var result = null;
        if(ctLopHocPhanItem.length>0){
            result = ctLopHocPhanItem.map((item, key)=>{
                return <LopHocPhan 
                key={key}
                ctLopHocPhanItem={item}
                index = {key}
                maLop={this.props.id} 
                />
            })
        } 
        return result;
    }
}
const mapStateToProps=state=>({
    DSLopHocPhan: state.LayDSLopHocPhan.DSLopHocPhan,
    a:console.log(state.LayDSLopHocPhan.DSLopHocPhan)
}); 
ctlophocphan.propTypes = {
    DanhSachSinhVienTheoLopHocPhan  : propTypes.func.isRequired,
}
export default connect(mapStateToProps, {DanhSachSinhVienTheoLopHocPhan})(ctlophocphan);
