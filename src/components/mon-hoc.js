import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';  
import khung from "../assets/img/khung.png";
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import DSMonHoc from './DanhSach/ds-mon-hoc';
import MonHocItem from './Item/mon-hoc-item';
import propTypes from 'prop-types';
import {DanhSachMonHoc, TimMonHocTheoTen} from './../actions/mon_hoc';
import Pagination from "react-js-pagination";
class monhoc extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            trangthai:1,
            DSMonHoc:[], 
            monHocItem:[],
            activePage: '', 
            current_page:'',
            total:'',
            per_page:'', 
        }  
        this.HienThiMonHocItem = this.HienThiMonHocItem.bind(this);
    }
    componentDidMount() {
        this.props.DanhSachMonHoc();
    }
    componentWillReceiveProps(nextProps) {
        this.setState({ DSMonHoc: nextProps.DSMonHoc.data,
                        current_page:nextProps.DSMonHoc.current_page,
                        total:nextProps.DSMonHoc.total,
                        per_page:nextProps.DSMonHoc.per_page}); 
    } 
    handlePageChange=(pageNumber)=> {
        this.state.current_page = pageNumber;
        console.log(`active page is ${pageNumber}`);
        this.setState({current_page: this.state.current_page});
        this.props.DanhSachMonHoc(this.state.current_page);
    } 
    TimMonHocTheoTen=(e)=>{
        var target = e.target; 
        this.props.TimMonHocTheoTen(target.value);
        // alert('1')
    }
    render() {  
        return (
            <div id="body-content">
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-md-12">
                        <div className="content-title"> 
                            <h1 className="text-center h1-title-style">Môn Học</h1>
                            <span className="kitu-dacbiet">&#9884;</span>
                        </div>
                        </div>
                        <div className="col-md-12"> 
                            <Link to="/monhoc/them"  className="button-them--style button-default--style button-thaotac--style">Thêm Môn Học</Link>
                        </div>
                        <div className="col-md-12"> 
                            <div className="tableTKB">
                                <div className="chuc-nang-them" background={khung}>
                                    <input className="search-data-table pl-1" name="seart-data-table" placeholder="Search..." onChange={this.TimMonHocTheoTen}></input>
                                </div>
                            <DSMonHoc>
                                {()=>this.HienThiMonHocItem(this.state.DSMonHoc)}
                                </DSMonHoc>
                                <div className="locDLTheoTrang">
                                    <Pagination
                                        activePage={this.state.current_page}
                                        itemsCountPerPage={this.state.per_page}
                                        totalItemsCount={this.state.total}
                                        pageRangeDisplayed={8}
                                        onChange={this.handlePageChange}
                                        itemClass="page-item"
                                        linkClass='page-link'
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
    HienThiMonHocItem=(monHocItem)=>{
        var result = null;
        if(monHocItem.length>0){
            result = monHocItem.map((item, key)=>{
                return <MonHocItem 
                key={key}
                monHocItem={item}
                index = {key}
                />
            })
        } 
        return result;
    }
}
const mapStateToProps=state=>({
    DSMonHoc: state.LayDSMonHoc.DSMonHoc,
    a:console.log(state.LayDSMonHoc.DSMonHoc)
}); 
monhoc.propTypes = {
    DanhSachMonHoc: propTypes.func.isRequired,
    TimMonHocTheoTen: propTypes.func.isRequired,
}
export default connect(mapStateToProps, {DanhSachMonHoc,TimMonHocTheoTen})(monhoc);
