import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { connect } from 'react-redux';
import {Link} from 'react-router-dom';
import propTypes from 'prop-types';
import {XoaViPham} from './../../actions/vi_pham';
class viphamitem extends Component {
    constructor(props) {
        super(props);
        this.state = { 
        }
    }
    componentDidMount() {
        
    } 
    XoaViPham=()=>{
        var maVP=this.props.viPhamItem.ma_vi_pham;
        var xoa = !this.props.viPhamItem.xoa?1:0;
        this.props.XoaViPham(maVP, xoa);
    }
    render() {
        var {viPhamItem, index } = this.props;
        var ngaytao = this.props.viPhamItem.ngay_tao.slice(0, 10);
        var ngaycapnhat = this.props.viPhamItem.ngay_cap_nhat.slice(0, 10);
        var trangthai = this.props.viPhamItem.xoa ? false : true;
        return (
            <tr>
                <td>{index+1}</td>
                <td>{viPhamItem.ma_vi_pham}</td>
                <td>{viPhamItem.noi_dung}</td>
                <td>{viPhamItem.hinh_thuc_ky_luat}</td>
                <td>{reformatDate(ngaytao)}</td>
                <td>{reformatDate(ngaycapnhat)}</td> 
                <td className="button-dieu-chinh">
                    <Link to={`/vipham/${viPhamItem.ma_vi_pham}/capnhat`} className="btn-update button--style"  ><i className="fa fa-pencil"></i></Link> 
                </td>
                <td>
                    <label className="switch ">
                        <input type="checkbox" defaultChecked={trangthai}  onChange={this.XoaViPham} className="default" />
                        <span className="slider round"></span>
                    </label>
                </td>
            </tr>

        );
    }
}
function reformatDate(dateStr) {
    var dArr = dateStr.split("-");  // ex input "2010-01-18"
    return dArr[2] + "/" + dArr[1] + "/" + dArr[0].substring(2); //ex out: "18/01/10"
}; 
viphamitem.propTypes={
    XoaViPham: propTypes.func.isRequired,
}
export default connect(null,{XoaViPham} )(viphamitem);