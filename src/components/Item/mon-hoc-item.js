import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { connect } from 'react-redux';
import {Link} from 'react-router-dom';
import propTypes from 'prop-types';
import {XoaMonHoc} from './../../actions/mon_hoc';
class monhocitem extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            xoa:''
        } 
    }
    componentDidMount() { 
    }  
    onChange = (e) => {
        var target = e.target;  
        this.setState({xoa:e.target.checked}) 
        var maMH=this.props.monHocItem.ma_mon_hoc;
        this.props.XoaMonHoc(maMH, target.checked?0:1);
    }
    
    render() {
        var {monHocItem, index } = this.props; 
        var ngaytao = this.props.monHocItem.ngay_tao.slice(0, 10);
        var ngaycapnhat = this.props.monHocItem.ngay_cap_nhat.slice(0, 10);
        if(this.state.xoa!==""){
            this.props.monHocItem.xoa = !this.state.xoa
        }
        var trangthai = this.props.monHocItem.xoa ? false : true;
        return (
            <tr>
                <td>{index+1}</td>
                <td>{monHocItem.ten_chuyen_nganh}</td>
                <td>{monHocItem.ma_mon_hoc}</td>
                <td>{monHocItem.ten_mon_hoc}</td>
                <td>{monHocItem.dv_hoc_phan}</td>
                <td>{reformatDate(ngaytao)}</td>
                <td>{reformatDate(ngaycapnhat)}</td> 
                <td className="button-dieu-chinh">
                    <Link to={`/monhoc/${monHocItem.ma_mon_hoc}/capnhat`} className={trangthai?"btn-update button--style":"btn-update button--style not-active"}  ><i className="fa fa-pencil"></i></Link> 
                </td>
                <td>
                    <label className="switch ">
                        <input type="checkbox" defaultChecked={trangthai}  onChange={this.onChange} className="default" />
                        <span className="slider round"></span>
                    </label>
                </td>
            </tr>

        );
    }
}
function reformatDate(dateStr) {
    var dArr = dateStr.split("-");  // ex input "2010-01-18"
    return dArr[2] + "/" + dArr[1] + "/" + dArr[0].substring(2); //ex out: "18/01/10"
}; 
monhocitem.propTypes={
    XoaMonHoc: propTypes.func.isRequired,
}
export default connect(null,{XoaMonHoc} )(monhocitem);
