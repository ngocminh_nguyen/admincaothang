import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { connect } from 'react-redux';
import {Link} from 'react-router-dom';
import propTypes from 'prop-types';
import {XoaGiaoVien} from './../../actions/giao_vien';
class giaovienitem extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            xoa:''
        } 
    }
    componentDidMount() { 
    }  
    onChange = (e) => {
        var target = e.target;  
        this.setState({xoa:e.target.checked}) 
        var maGV=this.props.giaoVienItem.ma_giao_vien;
        this.props.XoaGiaoVien(maGV, target.checked?0:1);
    } 
    render() {
        var {giaoVienItem, index } = this.props; 
        if(this.state.xoa!==""){
            this.props.giaoVienItem.xoa = !this.state.xoa
        }
        var trangthai = this.props.giaoVienItem.xoa ? false : true;
        var gioitinh=this.props.giaoVienItem.gioi_tinh?'nam':'nữ';
        var ngaysinh= this.props.giaoVienItem.ngay_sinh.slice(0, 10);
        return (
            <tr>
                <td>{index+1}</td>
                <td>{giaoVienItem.ten_khoa}</td>
                <td>{giaoVienItem.ma_giao_vien}</td>
                <td>{giaoVienItem.ho_ten}</td>
                <td>{reformatDate(ngaysinh)}</td>
                <td>{gioitinh}</td>
                <td>{giaoVienItem.dien_thoai}</td>
                <td>{giaoVienItem.email}</td>
                <td>{giaoVienItem.dia_chi}</td> 
                <td className="button-dieu-chinh">
                    <Link to={`/giaovien/${giaoVienItem.ma_giao_vien}/capnhat`} className={trangthai?"btn-update button--style":"btn-update button--style not-active"} ><i className="fa fa-pencil"></i></Link>
                </td>
                <td>
                    <label className="switch ">
                        <input type="checkbox" defaultChecked={trangthai}  onChange={this.onChange} className="default" />
                        <span className="slider round"></span>
                    </label>
                </td>
            </tr>

        );
    }
}
function reformatDate(dateStr) {
    var dArr = dateStr.split("-");  // ex input "2010-01-18"
    return dArr[2] + "/" + dArr[1] + "/" + dArr[0].substring(2); //ex out: "18/01/10"
};
giaovienitem.propTypes={
    XoaGiaoVien: propTypes.func.isRequired,
}
export default connect(null,{XoaGiaoVien} )(giaovienitem);
