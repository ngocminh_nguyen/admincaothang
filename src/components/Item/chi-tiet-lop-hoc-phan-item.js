import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { connect } from 'react-redux';
import {Link} from 'react-router-dom';
import propTypes from 'prop-types';
import {XoaCTLopHocPhan} from './../../actions/chi_tiet_lop_hoc_phan';
class chitietctLopHocPhanItem extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            xoa:'',
            key:''
        } 
    }
    componentDidMount() { 
    }   
    XoaSVKhoiLopHocPhan=()=>{  
        this.setState({key: this.props.index+1})
        this.props.XoaCTLopHocPhan(this.props.maLop ,this.props.ctLopHocPhanItem.mssv);
    }
    render() { 
        var {ctLopHocPhanItem, index, maLop } = this.props;
        if(this.state.xoa!==""){
            this.props.ctLopHocPhanItem.xoa = !this.state.xoa
        } 
        var gioitinh=this.props.ctLopHocPhanItem.gioi_tinh?'nam':'nữ';
        var ngaysinh= this.props.ctLopHocPhanItem.ngay_sinh.slice(0, 10);
        return (
            <tr class={index+1 ===this.state.key?"none":''}>
               <td>{index+1}</td>
                <td><img src={ctLopHocPhanItem.hinh_dai_dien} alt="hinh sinh vien" className="hinhAdmin--style"/></td>
                <td>{ctLopHocPhanItem.mssv}</td>  
                <td>{ctLopHocPhanItem.ho_ten}</td>  
                <td>{reformatDate(ngaysinh)}</td>
                <td>{gioitinh}</td>
                <td>{ctLopHocPhanItem.noi_sinh}</td> 
                <td>{ctLopHocPhanItem.ma_phu_huynh}</td>
                <td>{ctLopHocPhanItem.diem}</td> 
                <td className="button-dieu-chinh">
                    <Link to={`/ctlophocphan/${ctLopHocPhanItem.ma_lop}/${ctLopHocPhanItem.mssv}`} className="btn-update button--style" ><i className="fa fa-pencil"></i></Link>
                </td>   
                <td  style={{textAlign:"center"}}>
                    <button type="button" className="button--xoa" onClick={this.XoaSVKhoiLopHocPhan}>Xoá</button>
                </td>
            </tr>

        );
    }
} 
function reformatDate(dateStr) {
    var dArr = dateStr.split("-");  // ex input "2010-01-18"
    return dArr[2] + "/" + dArr[1] + "/" + dArr[0].substring(2); //ex out: "18/01/10"
}; 
chitietctLopHocPhanItem.propTypes={
    XoaCTLopHocPhan: propTypes.func.isRequired,
}
export default connect(null,{XoaCTLopHocPhan} )(chitietctLopHocPhanItem);
