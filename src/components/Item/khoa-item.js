import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { connect } from 'react-redux';
import {Link} from 'react-router-dom';
import propTypes from 'prop-types';
import {XoaKhoa} from './../../actions/khoa';
class khoaitem extends Component {
    constructor(props) {
        super(props);
        this.state = {
            xoa: ''
        }
    }
    componentDidMount() {
    }
    onChange = (e) => {
        var target = e.target;
        this.setState({ xoa: e.target.checked })
        var ma_khoa = this.props.khoaItem.ma_khoa;
        this.props.XoaKhoa(ma_khoa, target.checked ? 0 : 1);
    }  
    render() {
        var {khoaItem, index } = this.props;
        if(this.state.xoa!==""){
            this.props.khoaItem.xoa = !this.state.xoa
        }
        var trangthai = this.props.khoaItem.xoa ? false : true;  
        return (
            <tr>
                <td>{index+1}</td>
                <td>{khoaItem.ma_khoa}</td>
                <td>{khoaItem.ten_khoa}</td>     
                <td className="button-dieu-chinh">
                    <Link to={`/khoa/${khoaItem.ma_khoa}/capnhat`} className={trangthai?"btn-update button--style":"btn-update button--style not-active"} ><i className="fa fa-pencil"></i></Link>
                </td>
                <td>
                    <label className="switch ">
                        <input type="checkbox" defaultChecked={trangthai}  onChange={this.onChange} className="default" />
                        <span className="slider round"></span>
                    </label>
                </td>
            </tr>

        );
    }
} 

khoaitem.propTypes={
    XoaKhoa: propTypes.func.isRequired,
}
export default connect(null,{XoaKhoa} )(khoaitem);
