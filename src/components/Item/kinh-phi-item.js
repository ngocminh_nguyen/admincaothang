import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css'; 
import { connect } from 'react-redux'; 
import {Link} from 'react-router-dom';
import {XoaKinhPhi} from './../../actions/kinh_phi';
import propTypes from 'prop-types';
class kinhphiitem extends Component {
    constructor(props) {
        super(props);
        this.state = {
            trangthai: 1,
            xoa: ''
        }
    }
    componentDidMount() {
    }
    onChange = (e) => {
        var target = e.target;
        this.setState({ xoa: e.target.checked })
        var makhoa = this.props.kinhPhiItem.khoa;
        this.props.XoaKinhPhi(makhoa, target.checked ? 0 : 1);
    } 
    render() {
        var { kinhPhiItem, index } = this.props; 
        var thoigianbd = this.props.kinhPhiItem.thoi_gian_bat_dau.slice(0, 10);
        var thoigiankt = this.props.kinhPhiItem.thoi_gian_ket_thuc.slice(0, 10);
        if(this.state.xoa!==""){
            this.props.kinhPhiItem.xoa = !this.state.xoa
        }
        var trangthai = this.props.kinhPhiItem.xoa ? false : true; 
        
        return (
            <tr>
                <td>{index + 1}</td>
                <td>{kinhPhiItem.khoa}</td>
                <td>{kinhPhiItem.hoc_ky}</td>
                <td>{kinhPhiItem.loai}</td>
                <td>{formatNumber(kinhPhiItem.so_tien)+ ' VNĐ'} </td>
                <td>{reformatDate(thoigianbd)} - {reformatDate(thoigiankt)}</td> 
                <td className="button-dieu-chinh">
                    <Link to={`/kinhphi/${kinhPhiItem.khoa}/capnhat`} className={trangthai?"btn-update button--style":"btn-update button--style not-active"} ><i className="fa fa-pencil"></i></Link>    
                </td> 
                <td>
                    <label className="switch ">
                        <input type="checkbox" defaultChecked={trangthai}  onChange={this.onChange} className="default" />
                        <span className="slider round"></span>
                    </label>
                </td>
            </tr>

        );
    }
}
function formatNumber(tien){ 
    tien += ''; 
    var x = tien.split(".");
        var rgx= /(\d)(\d{3})/;
    if(tien.length==5){
        rgx = /(\d{2})(\d{3})/;  
    }
    else if(tien.length==6){
        rgx = /(\d{3})(\d{3})/;  
    }
    else if(tien.length>7){
        rgx = /(\d{2})(\d{3})/; 
    }
    else if(tien.length==7)
        rgx = /(\d{1})(\d{3})/;   
    var x1 = x[0];
    var x2 = x.length >1? '.'+x[1]:''; 
    while (rgx.test(x1)){
        x1= x1.replace(rgx, '$1'+ "." +"$2."); 
    } 
    return x1+x2;
}
function reformatDate(dateStr) {
    var dArr = dateStr.split("-");  // ex input "2010-01-18"
    return dArr[2] + "/" + dArr[1] + "/" + dArr[0].substring(2); //ex out: "18/01/10"
};
const mapStateToProps = (state) =>({ 
    
});
kinhphiitem.propTypes={
    XoaKinhPhi: propTypes.func.isRequired,
}
export default connect(mapStateToProps, {XoaKinhPhi})(kinhphiitem);
