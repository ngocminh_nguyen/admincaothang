import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { connect } from 'react-redux';
import {Link} from 'react-router-dom';
import propTypes from 'prop-types';
import {XoaCTLop} from './../../actions/chi_tiet_lop';
import $ from 'jquery'
class chitietlophocitem extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            xoa:'', 
            key:''
        } 
    }
    componentDidMount() { 
    }   
    XoaSVKhoiLop=()=>{   
        this.setState({key: this.props.index+1})
        this.props.XoaCTLop(this.props.maLop, this.props.ctLopHocItem.mssv) 
    }
    render() {  
        var {ctLopHocItem, index, maLop, DSLop } = this.props;  
        if(this.state.xoa!==""){
            this.props.ctLopHocItem.xoa = !this.state.xoa
        }   
        var gioitinh=this.props.ctLopHocItem.gioi_tinh?'nam':'nữ';
        var ngaysinh= this.props.ctLopHocItem.ngay_sinh.slice(0, 10);
        return (
            <tr class={index+1 ===this.state.key?"none":''}>
                <td>{index+1}</td>
                <td><img src={ctLopHocItem.hinh_dai_dien} alt="hinh sinh vien" className="hinhAdmin--style"/></td>
                <td>{ctLopHocItem.mssv}</td>  
                <td>{ctLopHocItem.ho_ten}</td>  
                <td>{reformatDate(ngaysinh)}</td>
                <td>{gioitinh}</td> 
                <td>{ctLopHocItem.noi_sinh}</td> 
                <td>{ctLopHocItem.ma_phu_huynh}</td> 
                <td style={{textAlign:"center"}}>
                    <button type="button" className="button--xoa" onClick={this.XoaSVKhoiLop}>Xoá</button>
                </td> 
            </tr> 
        );
    }
} 
function reformatDate(dateStr) {
    var dArr = dateStr.split("-");  // ex input "2010-01-18"
    return dArr[2] + "/" + dArr[1] + "/" + dArr[0].substring(2); //ex out: "18/01/10"
}; 
chitietlophocitem.propTypes={
    XoaCTLop: propTypes.func.isRequired,
}
export default connect(null,{XoaCTLop} )(chitietlophocitem);
