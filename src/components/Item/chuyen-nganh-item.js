import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { connect } from 'react-redux';
import {Link} from 'react-router-dom';
import propTypes from 'prop-types';
import {XoaChuyenNganh} from './../../actions/chuyen_nganh';
class chuyennganhitem extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            xoa:''
        } 
    }
    componentDidMount() { 
    }  
    onChange = (e) => {
        var target = e.target;  
        this.setState({xoa:e.target.checked}) 
        var maCN=this.props.chuyenNganhItem.ma_so; 
        this.props.XoaChuyenNganh(maCN, target.checked?0:1);
    } 
    render() {
        var {chuyenNganhItem, index } = this.props; 
        var ngaytao = this.props.chuyenNganhItem.ngay_tao.slice(0, 10); 
        var ngaycapnhat = this.props.chuyenNganhItem.ngay_cap_nhat.slice(0, 10);
        if(this.state.xoa!==""){
            this.props.chuyenNganhItem.xoa = !this.state.xoa
        }
        var trangthai = this.props.chuyenNganhItem.xoa ? false : true;
        return (
            <tr>
                <td>{index}</td>
                <td>{chuyenNganhItem.ma_so}</td>
                <td>{chuyenNganhItem.ma_chu}</td>
                <td>{chuyenNganhItem.ten_chuyen_nganh}</td>
                <td>{chuyenNganhItem.ten_khoa}</td>
                <td>{reformatDate(ngaytao)}</td>
                <td>{reformatDate(ngaycapnhat)}</td> 
                <td className="button-dieu-chinh">
                    <Link to={`/chuyennganh/${chuyenNganhItem.ma_so}/capnhat`} className={trangthai?"btn-update button--style":"btn-update button--style not-active"} ><i className="fa fa-pencil"></i></Link>
                </td>
                <td>
                    <label className="switch ">
                        <input type="checkbox" defaultChecked={trangthai}  onChange={this.onChange} className="default" />
                        <span className="slider round"></span>
                    </label>
                </td>
            </tr>

        );
    }
}
function reformatDate(dateStr) {
    var dArr = dateStr.split("-");  // ex input "2010-01-18"
    return dArr[2] + "/" + dArr[1] + "/" + dArr[0].substring(2); //ex out: "18/01/10"
}; 
chuyennganhitem.propTypes={
    XoaChuyenNganh: propTypes.func.isRequired,
}
export default connect(null,{XoaChuyenNganh} )(chuyennganhitem);
