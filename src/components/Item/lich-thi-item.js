import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { connect } from 'react-redux';
import {Link} from 'react-router-dom';
import propTypes from 'prop-types';
import {XoaLichThi} from '../../actions/lich_thi';
import moment from 'moment';
class lichThiItem extends Component {
    constructor(props) {
        super(props);
        this.state = {
            trangthai: 1,
            xoa: ''
        }
    }
    componentDidMount() {
    }
    onChange = (e) => {
        var target = e.target;
        this.setState({ xoa: e.target.checked })
        var ma_lich_thi = this.props.lichThiItem.ma_lich_thi;
        this.props.XoaLichThi(ma_lich_thi, target.checked ? 0 : 1);
    } 
    render() {
        var {lichThiItem } = this.props; 
        if(this.state.xoa!==""){
            this.props.lichThiItem.xoa = !this.state.xoa
        }
        var trangthai = this.props.lichThiItem.xoa ? false : true;
        return (
            <tr>  
                <td>{lichThiItem.ma_lop }</td>
                <td>{moment(lichThiItem.ngay_thi).format("MM/DD/YYYY")}</td>
                <td>{lichThiItem.gio_thi.slice(0,5)}</td>
                <td> 
                    <p>Nửa lớp đầu: {JSON.parse(lichThiItem.phong_thi)[0].nualopdau}</p>
                    <p>Nửa lớp cuối: {JSON.parse(lichThiItem.phong_thi)[0].nualopcuoi}</p>
                </td>
                <td>{lichThiItem.loai===0?"Thi lần 1":"Thi lần 2"}</td> 
                <td className="button-dieu-chinh">
                    <Link to={`/lichthi/${lichThiItem.ma_lich_thi}/${lichThiItem.loai}/capnhat`} className={trangthai?"btn-update button--style":"btn-update button--style not-active"} ><i className="fa fa-pencil"></i></Link>
                </td>
                <td>
                    <label className="switch ">
                        <input type="checkbox" defaultChecked={trangthai}  onChange={this.onChange} className="default" />
                        <span className="slider round"></span>
                    </label>
                </td>
            </tr>

        );
    }
} 
// function reformatDate(dateStr) {
//     var dArr = dateStr.split("-");  // ex input "2010-01-18"
//     return dArr[2] + "/" + dArr[1] + "/" + dArr[0].substring(2); //ex out: "18/01/10"
// }; 
lichThiItem.propTypes={
    XoaLichThi: propTypes.func.isRequired,
}
export default connect(null,{XoaLichThi} )(lichThiItem);
