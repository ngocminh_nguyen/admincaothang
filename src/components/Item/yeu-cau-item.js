import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { connect } from 'react-redux';
import {Link} from 'react-router-dom';
import propTypes from 'prop-types';
import {XoaYeuCau} from './../../actions/yeu_cau';
class yeucauitem extends Component {
    constructor(props) {
        super(props);
        this.state = { 
        }
    }
    componentDidMount() {
        
    } 
    XoaYeuCau=()=>{
        var maVP=this.props.yeuCauItem.ma_vi_pham;
        var xoa = !this.props.yeuCauItem.xoa?1:0;
        this.props.XoaYeuCau(maVP, xoa);
    }
    render() {
        var {yeuCauItem, index } = this.props;
        // var ngay_sinh = this.props.yeuCauItem.ngay_sinh.slice(0, 10); 
        var trangthai = this.props.yeuCauItem.xoa ? false : true;
        return (
            <tr>
                <td>{index+1}</td>
                <td>{yeuCauItem.ma_yeu_cau }</td>
                <td>{yeuCauItem.mssv }</td>
                <td>{JSON.parse(yeuCauItem.noi_dung_yeu_cau).ho_ten }</td>
                <td>{JSON.parse(yeuCauItem.noi_dung_yeu_cau).ngay_sinh}</td>
                <td>{JSON.parse(yeuCauItem.noi_dung_yeu_cau).noi_sinh}</td> 
                <td className="button-dieu-chinh">
                    <Link to={`/vipham/${yeuCauItem.ma_vi_pham}/capnhat`} className="btn-update button--style"  ><i className="fa fa-pencil"></i></Link> 
                </td>
                <td>
                    <label className="switch ">
                        <input type="checkbox" defaultChecked={trangthai}  onChange={this.XoaYeuCau} className="default" />
                        <span className="slider round"></span>
                    </label>
                </td>
            </tr>

        );
    }
}
function reformatDate(dateStr) {
    var dArr = dateStr.split("-");  // ex input "2010-01-18"
    return dArr[2] + "/" + dArr[1] + "/" + dArr[0].substring(2); //ex out: "18/01/10"
}; 
yeucauitem.propTypes={
    XoaYeuCau: propTypes.func.isRequired,
}
export default connect(null,{XoaYeuCau} )(yeucauitem);