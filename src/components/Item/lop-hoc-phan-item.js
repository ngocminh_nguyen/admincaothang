import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { connect } from 'react-redux';
import {Link} from 'react-router-dom';
import propTypes from 'prop-types';
import {XoaLopHocPhan} from './../../actions/lop_hoc_phan';
import moment from 'moment';
class lophocphanitem extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            xoa:''
        } 
    }
    componentDidMount() { 
    }  
    onChange = (e) => {
        var target = e.target;  
        this.setState({xoa:e.target.checked}) 
        var ma_lop=this.props.lopHocPhanItem.ma_lop;
        this.props.XoaLopHocPhan(ma_lop, target.checked?0:1);
    } 
    render() {
        var {lopHocPhanItem, index } = this.props;
        var mang1=(JSON.parse(this.props.lopHocPhanItem.thoi_gian)) 
        if(this.state.xoa!==""){
            this.props.lopHocPhanItem.xoa = !this.state.xoa
        }
        var trangthai = this.props.lopHocPhanItem.xoa ? false : true;  
        return (
            <tr>
                <td>{index+1}</td>
                <td>{lopHocPhanItem.ma_lop }</td>
                <td>{lopHocPhanItem.ho_ten }</td>     
                <td>{lopHocPhanItem.ten_mon_hoc}</td>
                <td>{lopHocPhanItem.ten_chuyen_nganh}</td>
                <td>{lopHocPhanItem.hoc_ky}</td> 
                {/* <td>{mang1.map(function(a, key){return( <p key={key}>{a.phong}</p>)})} </td> */}
                <td>{mang1.map(function(a, key){return( <p key={key}>T{a.thu}</p>)})} </td>
                <td>{mang1.map(function(a, key){return( <p key={key}>Từ  tiết  {a.tiet_bat_dau}</p>)})} </td>
                <td>{mang1.map(function(a, key){return( <p key={key}>đến  tiết  {a.tiet_ket_thuc}</p>)})} </td> 
                <td>{moment(lopHocPhanItem.thoi_gian_ket_thuc).format("MM/DD/YYYY")}</td>
                <td> <Link to={`/lophocphan/${lopHocPhanItem.ma_lop}/danhsach`} className='' >Xem Thêm>></Link></td>
                <td> <Link to={`/lichthi/${lopHocPhanItem.ma_lop}/them`} className='' >Thêm Lịch Thi>></Link></td>
                <td className="button-dieu-chinh">
                    <Link to={`/lophocphan/${lopHocPhanItem.ma_lop}/capnhat`} className={trangthai?"btn-update button--style":"btn-update button--style not-active"} ><i className="fa fa-pencil"></i></Link>
                </td>
                <td>
                    <label className="switch ">
                        <input type="checkbox" defaultChecked={trangthai}  onChange={this.onChange} className="default" />
                        <span className="slider round"></span>
                    </label>
                </td>
            </tr>

        );
    }
} 
// function reformatDate(dateStr) {
//     var dArr = dateStr.split("-");  // ex input "2010-01-18"
//     return dArr[2] + "/" + dArr[1] + "/" + dArr[0].substring(2); //ex out: "18/01/10"
// }; 
lophocphanitem.propTypes={
    XoaLopHocPhan: propTypes.func.isRequired,
}
export default connect(null,{XoaLopHocPhan} )(lophocphanitem);
