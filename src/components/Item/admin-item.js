import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { connect } from 'react-redux';
import {Link} from 'react-router-dom';
import propTypes from 'prop-types';
import {XoaAdmin} from './../../actions/admin';
class adminitem extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            xoa:''
        } 
    }
    componentDidMount() { 
    }  
    onChange = (e) => { 
        var target = e.target;  
        this.setState({xoa:e.target.checked}) 
        var ma_admin=this.props.adminItem.ma_admin;
        this.props.XoaAdmin(ma_admin, target.checked?0:1);
    } 
    render() {
        var {adminItem, index } = this.props;
        if(this.state.xoa!==""){
            this.props.adminItem.xoa = !this.state.xoa
        }
        var trangthai = this.props.adminItem.xoa ? false : true; 
        return (
            <tr>
                <td>{index+1}</td>
                <td>{adminItem.ma_admin}</td>
                <td>{adminItem.ten_dang_nhap }</td>  
                <td><img src={adminItem.hinh_dai_dien } alt="hinh admin" className="hinhAdmin--style"/></td>
                <td>{adminItem.ho_ten }</td>
                <td>{adminItem.email }</td>  
                <td className="button-dieu-chinh">
                    <Link to={`/admin/${adminItem.ma_admin}/capnhat`} className={trangthai?"btn-update button--style":"btn-update button--style not-active"} ><i className="fa fa-pencil"></i></Link>
                </td>
                <td>
                    <label className="switch ">
                        <input type="checkbox" defaultChecked={trangthai}  onChange={this.onChange} className="default" />
                        <span className="slider round"></span>
                    </label>
                </td>
            </tr>

        );
    }
} 
adminitem.propTypes={
    XoaAdmin: propTypes.func.isRequired,
}
export default connect(null,{XoaAdmin} )(adminitem);
