import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { connect } from 'react-redux';
import {Link} from 'react-router-dom';
import propTypes from 'prop-types';
import {XoaPhuHuynh} from './../../actions/phu_huynh';
class phuhuynhitem extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            xoa:''
        } 
    }
    componentDidMount() { 
    }  
    onChange = (e) => {
        var target = e.target;  
        this.setState({xoa:e.target.checked}) 
        var maPH=this.props.phuhuynhItem.ma_phu_huynh;
        this.props.XoaPhuHuynh(maPH, target.checked?0:1);
    }
    
    render() {
        var {phuhuynhItem, index } = this.props; 
        var ngaytao = this.props.phuhuynhItem.ngay_tao.slice(0, 10);
        var ngaycapnhat = this.props.phuhuynhItem.ngay_cap_nhat.slice(0, 10); 
        if(this.state.xoa!==""){
            this.props.phuhuynhItem.xoa = !this.state.xoa
        }
        var trangthai = this.props.phuhuynhItem.xoa ? false : true;
        return (
            <tr>
                <td>{index+1}</td>
                <td>{phuhuynhItem.ma_phu_huynh }</td>
                <td>{phuhuynhItem.ho_ten_cha }</td>
                <td>{phuhuynhItem.ho_ten_me }</td>
                <td>{phuhuynhItem.so_dien_thoai }</td>
                <td>{phuhuynhItem.dia_chi }</td>
                <td>{reformatDate(ngaytao)}</td>
                <td>{reformatDate(ngaycapnhat)}</td> 
                <td className="button-dieu-chinh">
                    <Link to={`/phuhuynh/${phuhuynhItem.ma_phu_huynh}/capnhat`} className={trangthai?"btn-update button--style":"btn-update button--style not-active"}  ><i className="fa fa-pencil"></i></Link> 
                </td>
                <td>
                    <label className="switch ">
                        <input type="checkbox" defaultChecked={trangthai}  onChange={this.onChange} className="default" />
                        <span className="slider round"></span>
                    </label>
                </td>
            </tr>

        );
    }
}
function reformatDate(dateStr) {
    var dArr = dateStr.split("-");  // ex input "2010-01-18"
    return dArr[2] + "/" + dArr[1] + "/" + dArr[0].substring(2); //ex out: "18/01/10"
}; 
phuhuynhitem.propTypes={
    XoaPhuHuynh: propTypes.func.isRequired,
}
export default connect(null,{XoaPhuHuynh} )(phuhuynhitem);
