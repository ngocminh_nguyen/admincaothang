import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { connect } from 'react-redux';
import {Link} from 'react-router-dom';
import propTypes from 'prop-types';
import {XoaLop} from './../../actions/lop';
class lopitem extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            xoa:''
        } 
    }
    componentDidMount() { 
    }  
    onChange = (e) => {
        var target = e.target;  
        this.setState({xoa:e.target.checked}) 
        var ma_lop=this.props.lopItem.ma_lop;
        this.props.XoaLop(ma_lop, target.checked?0:1);
       
    } 
    render() {
        var {lopItem, index } = this.props;
        if(this.state.xoa!==""){
            this.props.lopItem.xoa = !this.state.xoa
        }
        var trangthai = this.props.lopItem.xoa ? false : true;  
        return (
            <tr>
                <td>{index+1}</td> 
                <th>{lopItem.ten_chuyen_nganh}</th> 
                <td>{lopItem.ma_lop}</td> 
                <td>{lopItem.ten_lop }</td> 
                <td> <Link to={`/lop/${lopItem.ma_lop}/danhsach`} className='' >Xem Thêm>></Link></td> 
                <td className="button-dieu-chinh">
                    <Link to={`/lop/${lopItem.ma_lop}/capnhat`} className={trangthai?"btn-update button--style":"btn-update button--style not-active"} ><i className="fa fa-pencil"></i></Link>
                </td>
                <td>
                    <label className="switch ">
                        <input type="checkbox" defaultChecked={trangthai}  onChange={this.onChange} className="default" />
                        <span className="slider round"></span>
                    </label>
                </td>
            </tr>

        );
    }
} 
 
lopitem.propTypes={
    XoaLop: propTypes.func.isRequired,
}
export default connect(null,{XoaLop} )(lopitem);
