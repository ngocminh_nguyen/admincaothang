import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { connect } from 'react-redux';
import {Link} from 'react-router-dom';
import propTypes from 'prop-types';
import {XoaSinhVien, SinhVienDongKinhPhi} from './../../actions/sinh_vien';
import moment from 'moment';
import * as $ from 'jquery';
class sinhvienitem extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            trangthaiKP:true,
            trangthaiVP:true,
            xoa:'',
        } 
    }
    componentDidMount() {
    } 
    onChange = (e) => {
        var target = e.target; 
        this.setState({xoa:e.target.checked}) 
        var mssv=this.props.sinhVienItem.mssv; 
        this.props.XoaSinhVien(mssv, target.checked?0:1);
    }
    XemKinhPhi=(mssv)=>{
        var kp = !this.state.trangthaiKP;
        $(".table-vipham").addClass("none");
        if(kp){
            $("#"+mssv).addClass("none");
            this.setState({trangthaiKP:kp})
        }
        else{
            $("#"+mssv).removeClass("none"); 
            this.setState({trangthaiKP:kp})
        } 
    }
    XemViPham=(mssv)=>{
        $(".table-kinhphi").addClass("none");
        var kp = !this.state.trangthaiVP;
        if(kp){
            $("#VP"+mssv).addClass("none");
            this.setState({trangthaiVP:kp})
        }
        else{
            $("#VP"+mssv).removeClass("none"); 
            this.setState({trangthaiVP:kp})
        } 
    }
    DongKinhPhi=(mssv,hk, loai)=>{
        this.props.SinhVienDongKinhPhi(mssv, hk, loai);
        
    }
    render() {
        var {sinhVienItem, index } = this.props; 
        var mangKP =sinhVienItem.kinh_phi!==""?JSON.parse(sinhVienItem.kinh_phi).kinhphi:""; 
        var mangVP =sinhVienItem.vi_pham!==""?JSON.parse(sinhVienItem.vi_pham).vipham:""; 
        const gobal = this;  
        if(this.state.xoa!==""){
            this.props.sinhVienItem.xoa = !this.state.xoa
        }
        var trangthai = this.props.sinhVienItem.xoa ? false : true;  
        var gioitinh=this.props.sinhVienItem.gioi_tinh?'nam':'nữ';
        var ngaysinh= this.props.sinhVienItem.ngay_sinh.slice(0, 10);
        // $(".table-responsive ").click(function(){
            
        //     $(".table-kinhphi").addClass("none");
        //     // $(".table-vipham").addClass("none");
        // }) 
        window.onclick = function(event) {
            if (event.target.className == 'table-kinhphi' || event.target.className == 'table-vipham') {
                $(".table-kinhphi").addClass("none"); 
                $(".table-vipham").addClass("none"); 
            } 
          }
        return (
            <tr>
                <td>{index+1}</td>
                <td>{sinhVienItem.mssv}</td>  
                <td>{sinhVienItem.ho_ten}</td>  
                <td>{gioitinh}</td>
                <td>{reformatDate(ngaysinh)}</td>
                <td><img src={sinhVienItem.hinh_dai_dien} alt="hinh sinh vien" className="hinhAdmin--style"/></td>
                <td>{sinhVienItem.noi_sinh}</td> 
                <td>{sinhVienItem.ma_phu_huynh}</td>
                <td  style={{position:"relative"}}>
                     <button type="button" className="btn-xemthem" onClick={()=>this.XemKinhPhi(sinhVienItem.mssv)}>Xem</button>
                     <div className="table-kinhphi none" id={sinhVienItem.mssv}>
                     <table className="table table-dark table-striped">
                        <thead>
                            <tr className=""> 
                                <th>Khóa</th>
                                <th>Học Kỳ</th>
                                <th>Loại</th>
                                <th>Kinh Phí</th>
                                <th>Thời Hạn</th>
                                <th className="th-button--style">Đã Đóng</th> 
                            </tr>
                        </thead>
                        <tbody>   
                        {mangKP!==""?Object.values(mangKP).map(function(kp, key){return(
                                <tr key={key+1}>
                                    <td>{kp.khoa}</td>
                                    <td>{kp.hoc_ky}</td>
                                    <td>{kp.loai}</td>
                                    <td>{kp.so_tien}</td>
                                    <td>{moment(kp.thoi_gian_bat_dau).format("DD/MM/YY")} - {moment(kp.thoi_gian_ket_thuc).format("DD/MM/YY")}</td> 
                                    <td>
                                    <label className="switch ">
                                        <input type="checkbox" defaultChecked={kp.trang_thai ? true : false} onChange={()=>gobal.DongKinhPhi(sinhVienItem.mssv, kp.hoc_ky, kp.loai)}   className="default" />
                                        <span className="slider round"></span>
                                    </label>
                                    </td>
                                </tr>
                               )
                            }):<tr><td col={6}>Rỗng</td></tr>}
                        </tbody>
                    </table>
                    </div>
                </td>
                <td  style={{position:"relative"}}>
                     <button type="button" className="btn-xemthem" onClick={()=>this.XemViPham(sinhVienItem.mssv)}>Xem</button>
                     <div className="table-vipham none" id={"VP"+sinhVienItem.mssv}>
                     <table className="table table-dark table-striped">
                        <thead>
                            <tr className=""> 
                                <th>Nội Dung Vi Phạm</th>
                                <th>Thời Gian</th>  
                            </tr>
                        </thead>
                        <tbody>   
                        {mangVP!==""?Object.values(mangVP).map(function(vp, key){return(
                                <tr key={key+1}>
                                    <td>{vp.noi_dung_vi_pham}</td> 
                                    <td>{moment(vp.thoi_gian.date).format("DD/MM/YY")}</td>  
                                </tr>
                               )
                            }):<tr><td col={2}>Rỗng</td></tr>}
                        </tbody>
                    </table>
                    </div>
                </td>
                <td className="button-dieu-chinh">
                    <Link to={`/sinhvien/${sinhVienItem.mssv}/capnhat`} className={trangthai?"btn-update button--style":"btn-update button--style not-active"}  ><i className="fa fa-pencil"></i></Link>
                </td> 
                <td>
                    <label className="switch ">
                        <input type="checkbox" defaultChecked={trangthai}  onChange={this.onChange} className="default" />
                        <span className="slider round"></span>
                    </label>
                </td> 
            </tr> 
            
            
        );
        
    }
} 
function reformatDate(dateStr) {
    var dArr = dateStr.split("-");  // ex input "2010-01-18"
    return dArr[2] + "/" + dArr[1] + "/" + dArr[0].substring(2); //ex out: "18/01/10"
}; 
sinhvienitem.propTypes={
    XoaSinhVien: propTypes.func.isRequired,
    SinhVienDongKinhPhi: propTypes.func.isRequired
}
export default connect(null,{XoaSinhVien, SinhVienDongKinhPhi} )(sinhvienitem);
