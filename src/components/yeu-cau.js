import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';  
import khung from "../assets/img/khung.png";
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import DSYeuCau from './DanhSach/ds-yeu-cau';
import YeuCauItem from './Item/yeu-cau-item';
import propTypes from 'prop-types';
import {DanhSachYeuCau, CapNhatYeuCau, XoaYeuCau} from './../actions/yeu_cau';
class yeucau extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            trangthai:1,
            huy_yeu_cau :'',

            DSYeuCau:[], 
            yeuCauItem:[],
        }   
    }
    componentDidMount() {
        this.props.DanhSachYeuCau();
    }
    componentWillReceiveProps(nextProps) {
        this.setState({ DSYeuCau: nextProps.DSYeuCau}); 
    } 
    ChapNhanCapNhatYeuCau=(yc)=>{
        console.log(yc)
        var mayc = yc.ma_yeu_cau;
        var mssv = yc.mssv;
        var hoten = JSON.parse(yc.noi_dung_yeu_cau).ho_ten;
        var ngaysinh = JSON.parse(yc.noi_dung_yeu_cau).ngay_sinh;
        var noisinh =JSON.parse(yc.noi_dung_yeu_cau).noi_sinh ;
        var gioitinh = JSON.parse(yc.noi_dung_yeu_cau).gioi_tinh;
        var sdt = JSON.parse(yc.noi_dung_yeu_cau).sdt;
        this.props.CapNhatYeuCau(mayc, mssv, hoten, ngaysinh, gioitinh, noisinh, sdt)
    }
    ChapNhanXoaYeuCau=(mayc, xoa)=>{
        this.setState({huy_yeu_cau:mayc})
    }
    render() { 
        console.log(this.props.id)
        var { huy_yeu_cau }= this.state;
        // console.log(huy_yeu_cau, this.state.huy_yeu_cau)
        const gobal = this;
        return (
            <div id="body-content">
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-md-12">
                        <div className="content-title"> 
                            <h1 className="text-center h1-title-style">Lịch Sử Cập Nhật Của Sinh Viên</h1>
                            <span className="kitu-dacbiet">&#9884;</span>
                        </div>
                        </div>
                        <div className="col-md-12"> 
                            {/* <Link to="/vipham/them"  className="button-them--style button-default--style button-thaotac--style">Thêm Vi Phạm</Link> */}
                        </div>
                        <div className="chuc-nang-them" background={khung}> 
                            <ul>
                                <li className="nav-link">
                                    <input className="search-data-table pl-1" name="seart-data-table" placeholder="Search..."></input>
                                </li>
                                <li className="nav-link">
                                    <Link to="#" data-toggle="tooltip" title="phân trang">
                                        <i className="fa fa-caret-square-o-down"></i>
                                    </Link>
                                </li>
                                <li className="nav-link">
                                    <button className="btn-an-cot--style" data-toggle="tooltip" title="Ẩn cột">
                                        <i className="fa fa-th"></i>
                                    </button>
                                </li> 
                            </ul>
                        </div>
                        <div className="col-md-12">
                        <div className="row">
                            <div className="col-lg-12">
                                    {this.state.DSYeuCau.map(function(yc, key){
                                        return(
                                        <div className="yeucau-data"> 
                                        <div className="col-lg-5 m-auto"> 
                                            <div className="itemYeuCau">
                                            <h4>Người cập nhật: {yc.loai==="1"?"Sinh Viên":"Phụ Huynh"}</h4>
                                                        <h6>Mã số: {yc.nguoi_yeu_cau}</h6>
                                                        <h6>Ngày tạo: {yc.ngay_tao}</h6>
                                                        <h6>Ngày cập nhật:{yc.ngay_cap_nhat}</h6>
                                                        <Link to={`/yeucau/${yc.nguoi_yeu_cau}/${yc.loai}/chitiet`}>Xem Thêm...  </Link> 
                                            </div>
                                        </div>
                                        <div className={gobal.huy_yeu_cau == yeucau.nguoi_yeu_cau?"col-lg-12 col-md-12 none":"col-lg-12 col-md-12"} >
                                            <button className='button-default--style button-thaotac--style' type="button" onClick={()=>gobal.ChapNhanCapNhatYeuCau(yc)}>Chấp Nhận</button>
                                            <button className='button-default--style button-thoat--style' type="button" onClick={()=>gobal.ChapNhanXoaYeuCau(yc.nguoi_yeu_cau , yc.xoa)}>Hủy</button>
                                        </div> 
                                        </div>
                                        )
                                    })}
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
    // HienThiYeuCau=(yeuCauItem)=>{
    //     var result = null;
    //     if(yeuCauItem.length>0){
    //         result = yeuCauItem.map((item, key)=>{
    //             return <YeuCauItem 
    //             key={key}
    //             yeuCauItem={item}
    //             index = {key}
    //             />
    //         })
    //     } 
    //     return result;
    // }
}
const mapStateToProps=state=>({
    DSYeuCau: state.LayDSYeuCau.DSYeuCau,
    a: console.log(state.LayDSYeuCau.DSYeuCau)
}); 
yeucau.propTypes = {
    DanhSachYeuCau: propTypes.func.isRequired,
    CapNhatYeuCau: propTypes.func.isRequired,
    XoaYeuCau: propTypes.func.isRequired
}
export default connect(mapStateToProps, {DanhSachYeuCau, CapNhatYeuCau,XoaYeuCau})(yeucau);
