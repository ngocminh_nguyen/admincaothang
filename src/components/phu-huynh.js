import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';  
import khung from "../assets/img/khung.png";
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import DSPhuHuynh from './DanhSach/ds-phu-huynh';
import PhuHuynhItem from './Item/phu-huynh-item';
import propTypes from 'prop-types';
import {DanhSachPhuHuynh, TimPhuHuynhTheoTen} from './../actions/phu_huynh';
import Pagination from "react-js-pagination";
class phuhuynh extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            trangthai:1,
            DSPhuHuynh:[], 
            phuhuynhItem:[],
            activePage: '', 
            current_page:'',
            total:'',
            per_page:'',
        }  
        this.HienThiPhuHuynhItem = this.HienThiPhuHuynhItem.bind(this);
    }
    componentDidMount() {
        this.props.DanhSachPhuHuynh();
    }
    componentWillReceiveProps(nextProps) {
        this.setState({ DSPhuHuynh: nextProps.DSPhuHuynh.data,
                        current_page:nextProps.DSPhuHuynh.current_page,
                        total:nextProps.DSPhuHuynh.total,
                        per_page:nextProps.DSPhuHuynh.per_page}); 
    } 
    TimPhuHuynhTheoTen=(e)=>{
        var target = e.target; 
        this.props.TimPhuHuynhTheoTen(target.value); 
    }
    handlePageChange=(pageNumber)=> {
        console.log(pageNumber)
        this.state.current_page = pageNumber;
        console.log(`active page is ${pageNumber}`);
        this.setState({current_page: this.state.current_page});
        this.props.DanhSachPhuHuynh(this.state.current_page);
    } 
    render() {  
        return (
            <div id="body-content">
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-md-12">
                        <div className="content-title"> 
                            <h1 className="text-center h1-title-style">Phụ Huynh</h1>
                            <span className="kitu-dacbiet">&#9884;</span>
                        </div>
                        </div>
                        <div className="col-md-12"> 
                            <Link to="/phuhuynh/them"  className="button-them--style button-default--style button-thaotac--style">Thêm Phụ Huynh</Link>
                        </div>
                        <div className="col-md-12">
                            <div className="tableTKB">
                                <div className="chuc-nang-them" background={khung}>
                                    <input className="search-data-table pl-1" name="seart-data-table" placeholder="Search..." onChange={this.TimPhuHuynhTheoTen}></input>
                                </div>
                                <DSPhuHuynh>
                                    {() => this.HienThiPhuHuynhItem(this.state.DSPhuHuynh)}
                                </DSPhuHuynh>
                                <div className="locDLTheoTrang">
                                    <Pagination
                                        activePage={this.state.current_page}
                                        itemsCountPerPage={this.state.per_page}
                                        totalItemsCount={this.state.total}
                                        pageRangeDisplayed={8}
                                        onChange={this.handlePageChange}
                                        itemClass="page-item"
                                        linkClass='page-link'
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
    HienThiPhuHuynhItem=(phuhuynhItem)=>{
        var result = null;
        if(phuhuynhItem.length>0){
            result = phuhuynhItem.map((item, key)=>{
                return <PhuHuynhItem 
                key={key}
                phuhuynhItem={item}
                index = {key}
                />
            })
        } 
        return result;
    }
}
const mapStateToProps=state=>({
    DSPhuHuynh: state.LayDSPhuHuynh.DSPhuHuynh,
    a: console.log( state.LayDSPhuHuynh.DSPhuHuynh)
}); 
phuhuynh.propTypes = {
    DanhSachPhuHuynh: propTypes.func.isRequired,
    TimPhuHuynhTheoTen: propTypes.func.isRequired,
}
export default connect(mapStateToProps, {DanhSachPhuHuynh, TimPhuHuynhTheoTen})(phuhuynh);
