import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { connect } from 'react-redux';
import propTypes from 'prop-types';
import { Link } from 'react-router-dom'; 
import { DangNhapAdmin } from './../actions/admin';
class dangnhap extends Component {
    constructor(props) {
        super(props);
        this.state = {
            ten_tai_khoan: '',
            mat_khau: '',
            isLogin: false,
            notLogin: false,
            admin: this.props.admin
        };
        this.onChange = this.onChange.bind(this);
        this.DangNhap = this.DangNhap.bind(this);
    }
    onChange(event) {
        var target = event.target;
        var name = target.name;
        var value = target.value;
        this.setState({ [name]: value })
    }
    DangNhap() {
        // this.props.DangNhapAdmin(this.state.ten_tai_khoan, this.state.mat_khau);
    }
    // componentWillReceiveProps(nextProps) {
    //     console.log(nextProps.isDangNhap);
    //     if (nextProps.isDangNhap) {
    //         this.setState({
    //             isLogin: true,
    //             admin: nextProps.admin
    //         })
    //         console.log(this.state.admin);
    //         this.props.history.push("/")
    //     }
    //     else {
    //         this.setState({ notLogin: true })
    //     }
    // }
    render() {
        return (
            <div>
                <div className="input-group">
                    <div className="input-group-prepend">
                        <span className="input-group-text input-group-text-custom"><i className="mdi mdi-account" /></span>
                    </div>
                    <input className="form-control" value={this.state.ten_tai_khoan} type="text" required placeholder="ten_tai_khoan" name="ten_tai_khoan" onChange={(value) => this.onChange(value)} />
                </div>
                <div className="input-group">
                    <div className="input-group-prepend">
                        <span className="input-group-text input-group-text-custom"><i className="mdi mdi-key" /></span>
                    </div>
                    <input className="form-control" type="mat_khau" required placeholder="mat_khau" name="mat_khau" onChange={(value) => this.onChange(value)} />
                </div>
                <button onClick={this.DangNhap} className="btn btn-primary btn-custom w-md waves-effect waves-light" type="button"  >Log In
                  </button>
            </div>
        );
    }
}
const mapStateToProps = state => ({
    // isLogin: state.DangNhapAdmin.isLogin,
    // a:console.log(state.DangNhapAdmin.isLogin),
    // admin: state.DangNhapAdmin.admin,
    // b:console.log(state.DangNhapAdmin.admin),
});
dangnhap.propTypes = {
    DangNhapAdmin: propTypes.func.isRequired,
}
export default connect(mapStateToProps,{DangNhapAdmin})(DangNhapAdmin(dangnhap));
