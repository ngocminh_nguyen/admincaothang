import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css'; 
import propTypes from 'prop-types';
import khung from "../assets/img/khung.png";
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import DSKhoa from './DanhSach/ds-khoa';
import KhoaItem from './Item/khoa-item';
import {DanhSachKhoa} from './../actions/khoa';
class khoa extends Component {
    constructor(props) {
        super(props);
        this.state = {  
            DSKhoa:[], 
            khoaItem:[], 
        }  
        this.HienThiKhoa = this.HienThiKhoa.bind(this);
    }
    componentDidMount() {
        this.props.DanhSachKhoa();
    }
    componentWillReceiveProps(nextProps) {
        this.setState({ DSKhoa: nextProps.DSKhoa}); 
    } 
    render() {   
        return (
            <div id="body-content">
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-md-12">
                            <div className="content-title"> 
                                <h1 className="text-center h1-title-style">Khoa</h1>
                                <span className="kitu-dacbiet">&#9884;</span>
                            </div>
                        </div>
                        <div className="col-md-12">
                            <Link to="/khoa/them" className="button-them--style button-default--style button-thaotac--style">Thêm Khoa</Link>
                        </div>
                        <div className="col-md-12"> 
                            <div className="tableTKB">
                                <div className="chuc-nang-them" background={khung}>
                                    <input className="search-data-table pl-1" name="seart-data-table" placeholder="Search..."></input>
                                </div>
                            <DSKhoa>
                                {()=>this.HienThiKhoa(this.state.DSKhoa)}
                            </DSKhoa>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
    HienThiKhoa=(khoaItem)=>{
        var result = null;
        if(khoaItem.length>0){
            result = khoaItem.map((item, key)=>{
                return <KhoaItem 
                key={key}
                khoaItem={item}
                index = {key}
                />
            })
        } 
        return result;
    }
}
const mapStateToProps=state=>({
    DSKhoa: state.LayDSKhoa.DSKhoa,
}); 
khoa.propTypes = {
    DanhSachKhoa: propTypes.func.isRequired,
}
export default connect(mapStateToProps, {DanhSachKhoa})(khoa);
