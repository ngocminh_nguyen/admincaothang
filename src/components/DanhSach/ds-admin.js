import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css'; 
import { connect } from 'react-redux'; 
class dsadmin extends Component {
    constructor(props) {
        super(props);
        this.state = {  
        } 
    }  
    render() { 
        return (
            <div className="table-responsive  ">
                <table className="table table-striped">
                    <thead>
                        <tr className="table-header--style">
                            <th>STT</th>
                            <th>Mã Admin</th>
                            <th>Tên Đăng Nhập</th> 
                            <th>Hình Đại Diện</th>
                            <th>Họ Tên</th>
                            <th>Email</th> 
                            <th className="th-button--style">Cập Nhật</th>
                            <th className="th-button--style text-center">Xóa</th>

                        </tr>
                    </thead>
                    <tbody>
                       {this.props.children()}
                    </tbody>
                </table>
            </div>

        );
    }
}

const mapStateToProps = (state) => ({
}); 
export default connect(mapStateToProps, null)(dsadmin);
