import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { connect } from 'react-redux';
class dslophocphan extends Component {
    constructor(props) {
        super(props);
        this.state = {
        }
    }
    render() {
        return (
            <div className="table-responsive  ">
                <table className="table table-striped">
                    <thead>
                        <tr className="table-header--style">
                            <th>STT</th>
                            <th>Mã Lớp</th> 
                            <th>Giáo Viên</th>
                            <th>Môn Học</th>
                            <th>Chuyên Ngành</th>
                            <th>Học Kỳ</th>
                            {/* <th>Phòng</th> */}
                            <th>Thứ</th>
                            <th>Tiết Bắt Đầu</th>
                            <th>Tiết Kết Thúc</th> 
                            <th>Thời Gian Dự Kiến Kết Thúc</th> 
                            <th>Danh Sách Sinh Viên</th>
                            <th>Lịch Thi</th>
                            <th className="th-button--style">Cập Nhật</th>
                            <th className="th-button--style text-center">Xóa</th>

                        </tr>
                    </thead>
                    <tbody>
                        {this.props.children()}
                    </tbody>
                </table>
            </div>

        );
    }
}

const mapStateToProps = (state) => ({
});
export default connect(mapStateToProps, null)(dslophocphan);
