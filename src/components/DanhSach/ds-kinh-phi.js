import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css'; 
import { connect } from 'react-redux'; 
class dskinhphi extends Component {
    constructor(props) {
        super(props);
        this.state = {
            trangthai: 1,
            dsKinhPhi: [], 
        } 
    }  
    render() { 
        return (
            <div className="table-responsive">
                <table className="table table-striped">
                    <thead>
                        <tr className="table-header--style">
                            <th>STT</th>
                            <th>Khóa</th>
                            <th>Học Kỳ</th>
                            <th>Loại</th>
                            <th>Kinh Phí</th>
                            <th>Thời Hạn</th>
                            <th className="th-button--style">Cập Nhật</th>
                            <th className="th-button--style text-center">Xóa</th>

                        </tr>
                    </thead>
                    <tbody>
                       {this.props.children()}
                    </tbody>
                </table>
            </div>

        );
    }
}
const mapStateToProps = (state) => ({
}); 
export default connect(mapStateToProps, null)(dskinhphi);
