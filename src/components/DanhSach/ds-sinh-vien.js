import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { connect } from 'react-redux';   

class dssinhvien extends Component {
    constructor(props) {
        super(props);
        this.state = {
            
        }
    }
    
    inTaiLieu = () => {  
    }
    
    render() {
        return (
            <div className="table-responsive "> 
                <table className="table table-striped">
                    <thead>
                        <tr className="table-header--style">
                            <th>STT</th>
                            <th>Mã Sinh Viên</th>
                            <th>Họ Tên</th>
                            <th>Ngày Sinh</th>
                            <th>Giới Tính</th>
                            <th>Hình Đại Diện</th>
                            <th>Nơi Sinh</th>
                            <th>Phụ Huynh</th>
                            <th>Kinh Phí</th>
                            <th>Vi Phạm</th>
                            <th className="th-button--style">Cập Nhật</th>
                            <th className="th-button--style text-center">Xóa</th>

                        </tr>
                    </thead>
                    <tbody>
                        {this.props.children()}
                    </tbody>
                </table>  
            </div>

        );
    }
}

const mapStateToProps = (state) => ({
});
export default connect(mapStateToProps, null)(dssinhvien);
