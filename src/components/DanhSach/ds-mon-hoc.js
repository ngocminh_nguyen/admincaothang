import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css'; 
import { connect } from 'react-redux'; 
class dsmonhoc extends Component {
    constructor(props) {
        super(props);
        this.state = {
            trangthai: 1,
            dsMonHoc: [], 
        } 
    }  
    render() { 
        return (
            <div className="table-responsive  ">
                <table className="table table-striped">
                    <thead>
                        <tr className="table-header--style">
                            <th>STT</th>
                            <th>Chuyên Ngành</th>
                            <th>Mã Môn Học</th>
                            <th>Tên Môn Học</th>
                            <th>Đơn Vị Học Phần</th>
                            <th>Ngày Tạo</th>
                            <th>Ngày Cập Nhật</th>
                            <th className="th-button--style">Cập Nhật</th>
                            <th className="th-button--style text-center">Xóa</th> 
                        </tr>
                    </thead>
                    <tbody>
                        {this.props.children()}
                    </tbody>
                </table>
            </div>

        );
    }
}
const mapStateToProps = (state) => ({
}); 
export default connect(mapStateToProps, null)(dsmonhoc);
