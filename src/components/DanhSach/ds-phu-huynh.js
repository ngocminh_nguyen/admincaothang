import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css'; 
import { connect } from 'react-redux'; 
class dsphuhuynh extends Component {
    constructor(props) {
        super(props);
        this.state = { 
        } 
    }  
    render() { 
        return (
            <div className="table-responsive  ">
                <table className="table table-striped">
                    <thead>
                        <tr className="table-header--style">
                            <th>STT</th>
                            <th>Mã Phụ Huynh</th>
                            <th>Họ Tên Cha</th>
                            <th>Họ Tên Mẹ</th>
                            <th>Số Điện Thoại</th>
                            <th>Địa Chỉ</th>
                            <th>Ngày Tạo</th>
                            <th>Ngày Cập Nhật</th>
                            <th className="th-button--style">Cập Nhật</th>
                            <th className="th-button--style text-center">Xóa</th>

                        </tr>
                    </thead>
                    <tbody>
                        {this.props.children()}
                    </tbody>
                </table>
            </div>

        );
    }
}
const mapStateToProps = (state) => ({
}); 
export default connect(mapStateToProps, null)(dsphuhuynh);
