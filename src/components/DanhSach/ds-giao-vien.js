import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css'; 
import { connect } from 'react-redux'; 
class dsgiaovien extends Component {
    constructor(props) {
        super(props);
        this.state = {
            trangthai: 1,
            dsGiaoVien: [], 
        } 
    }  
    render() { 
        return (
            <div className="table-responsive  ">
                <table className="table table-striped">
                    <thead>
                        <tr className="table-header--style">
                            <th>STT</th>
                            <th>Khoa</th>
                            <th>Mã Giáo viên</th>
                            <th>Họ Tên</th>
                            <th>Ngày Sinh</th>
                            <th>Giới Tính</th>
                            <th>Điện Thoại</th>
                            <th>Email</th>
                            <th>Địa Chỉ</th>
                            <th className="th-button--style">Cập Nhật</th>
                            <th className="th-button--style text-center">Xóa</th>

                        </tr>
                    </thead>
                    <tbody>
                       {this.props.children()}
                    </tbody>
                </table>
            </div>

        );
    }
}

const mapStateToProps = (state) => ({
}); 
export default connect(mapStateToProps, null)(dsgiaovien);
