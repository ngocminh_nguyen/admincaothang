import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css'; 
import { connect } from 'react-redux'; 
class dschuyennganh extends Component {
    constructor(props) {
        super(props);
        this.state = {
            trangthai: 1, 
        } 
    } 
    render() { 
        return (
            <div className="table-responsive  ">
                <table className="table table-striped">
                    <thead>
                        <tr className="table-header--style">
                            <th>STT</th>
                            <th>Mã Số</th> 
                            <th>Mã Chữ</th>
                            <th>Chuyên Ngành</th>
                            <th>Mã Khoa</th>
                            <th>Ngày Tạo</th>
                            <th>Ngày Cập Nhật</th>
                            <th className="th-button--style">Cập Nhật</th>
                            <th className="th-button--style text-center">Xóa</th>

                        </tr>
                    </thead>
                    <tbody>
                       {this.props.children()}
                    </tbody>
                </table>
            </div>

        );
    }
}
const mapStateToProps = (state) => ({
}); 
export default connect(mapStateToProps, null)(dschuyennganh);
