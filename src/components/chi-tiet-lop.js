import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css'; 
import propTypes from 'prop-types';
import khung from "../assets/img/khung.png";
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import DSLop from './DanhSach/ds-chi-tiet-lop';
import LopHocPhan from './Item/chi-tiet-lop-item';
import {DanhSachSinhVienTheoLop  } from './../actions/lop';
class ctlophoc extends Component {
    constructor(props) {
        super(props);
        this.state = {  
            DSLop:[],  
            ctLopHocItem:[],
        }  
        this.HienThiChiTietLop = this.HienThiChiTietLop.bind(this);
    }
    componentDidMount() {
        this.props.DanhSachSinhVienTheoLop(this.props.id);
    }
    componentWillReceiveProps(nextProps) { 
        this.setState({ DSLop: nextProps.DSLop}); 
    } 
    render() {   
        console.log(this.props.id)
        return (
            <div id="body-content">
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-md-12">
                            <div className="content-title"> 
                                <h1 className="text-center h1-title-style">Danh Sách Sinh Viên Lớp {this.props.id}</h1> 
                                <span className="kitu-dacbiet">&#9884;</span>
                            </div>
                        </div>
                        <div className="col-md-12">
                            <Link to={`/lop/${this.props.id}/themsinhvien`} className="button-them--style button-default--style button-thaotac--style">Thêm Chi Tiết Lớp Học</Link>
                            <Link to={`/lop`} className="button-them--style button-default--style button-thaotac--style">Quay Lại</Link>
                        </div> 
                        <div className="chuc-nang-them" background={khung}>
                            {/* <span className="title-caption">Ẩn thêm cột</span> */}
                            <ul>
                                <li className="nav-link">
                                    <input className="search-data-table pl-1" name="seart-data-table" placeholder="Search..."></input>
                                </li>
                                <li className="nav-link">
                                    <Link to="#" data-toggle="tooltip" title="phân trang">
                                        <i className="fa fa-caret-square-o-down"></i>
                                    </Link>
                                </li>
                                </ul>
                        </div>
                        <div className="col-md-12"> 
                            <div className="tableTKB">
                                <div className="chuc-nang-them" background={khung}>
                                    <input className="search-data-table pl-1" name="seart-data-table" placeholder="Search..."></input>
                                </div>
                            <DSLop>
                                {()=>this.HienThiChiTietLop(this.state.DSLop)}
                            </DSLop>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
    HienThiChiTietLop=(ctLopHocItem)=>{
        var ds=ctLopHocItem;
        var result = null;
        if(ctLopHocItem.length>0){
            result = ctLopHocItem.map((item, key)=>{
                return <LopHocPhan 
                key={key}
                ctLopHocItem={item}
                index = {key}
                maLop={this.props.id}  
                />
            })
        } 
        return result;
    }
}
const mapStateToProps=state=>({
    DSLop: state.LayDSLop.DSLop, 
    CTCuaChiTietLop: state.LayCTCuaChiTietLop.CTCuaChiTietLop, 
}); 
ctlophoc.propTypes = {
    DanhSachSinhVienTheoLop  : propTypes.func.isRequired,
}
export default connect(mapStateToProps, {DanhSachSinhVienTheoLop})(ctlophoc);
