import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';  
import khung from "../assets/img/khung.png";
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import DSKinhPhi1 from './DanhSach/ds-kinh-phi';
import KinhPhiItem from './Item/kinh-phi-item';
import propTypes from 'prop-types';
import {DanhSachKinhPhi} from './../actions/kinh_phi';
import FcmToken from './FcmToken';
class kinhphi extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            trangthai:1,
            DSachKinhPhi:[], 
            kinhPhiItem:[],
        }  
        this.HienThiKinhPhiItem = this.HienThiKinhPhiItem.bind(this);
    }
    componentDidMount() {
        this.props.DanhSachKinhPhi();
    }
    componentWillReceiveProps(nextProps) {
        this.setState({ DSachKinhPhi: nextProps.DSKinhPhi}); 
    } 
    render() {    
        return (
            <div id="body-content">
                <div className="container-fluid">
                    <div className="row">
                    {/* <FcmToken/> */}
                        <div className="col-md-12">
                            <div className="content-title"> 
                                <h1 className="text-center h1-title-style">Kinh Phí</h1>
                                <span className="kitu-dacbiet">&#9884;</span>
                            </div>
                        </div>
                        <div className="col-md-12"> 
                            <Link to='/kinhphi/them' className="button-them--style button-default--style button-thaotac--style">Thêm Kinh Phí</Link>
                        </div>
                        <div className="col-md-12"> 
                            <div className="tableTKB">
                                <div className="chuc-nang-them" background={khung}>
                                    <input className="search-data-table pl-1" name="seart-data-table" placeholder="Search..."></input>
                                </div>
                            <DSKinhPhi1>
                                {()=>this.HienThiKinhPhiItem(this.state.DSachKinhPhi)}
                            </DSKinhPhi1>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
    HienThiKinhPhiItem=(kinhPhiItem)=>{
        var result = null;
        if(kinhPhiItem.length>0){
            result = kinhPhiItem.map((item, key)=>{
                return <KinhPhiItem 
                key={key}
                kinhPhiItem={item}
                index = {key}
                />
            })
        } 
        return result;
    }
}
const mapStateToProps=state=>({
    DSKinhPhi: state.LayDSKinhPhi.DSKinhPhi, 
}); 
kinhphi.propTypes = {
    DanhSachKinhPhi: propTypes.func.isRequired,
}
export default connect(mapStateToProps, {DanhSachKinhPhi})(kinhphi);
