import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { connect } from 'react-redux';
import propTypes from 'prop-types';
// import { ThemKinhPhi } from './../../actions/them_action';
import { CapNhatKinhPhi, ThemKinhPhi, ChiTietKinhPhiTheoMaKhoa } from './../../actions/kinh_phi';
import { withRouter } from 'react-router';
import moment from 'moment';
import * as func from '../../constants/FunctionsKTDuLieu_ThongBao';
import Calendar from 'react-calendar';
import Swal from 'sweetalert2';
import * as $ from 'jquery'
const initialState = {
    id: '',
    khoa: '',
    hoc_ky: '1',
    loai: '1',
    so_tien: '',
    thoi_gian_bat_dau: new Date(),
    thoi_gian_ket_thuc: new Date(),
    ngay_tao: '',
    ngay_cap_nhat: ''
}
class themkinhphi extends Component {
    constructor(props) {
        super(props);
        this.state = initialState;
        this.Thoat = this.Thoat.bind(this);
        this.ThemKinhPhi = this.ThemKinhPhi.bind(this);
    }
    componentWillMount() {
        if (this.props.id) {
            this.props.ChiTietKinhPhiTheoMaKhoa(this.props.id);
        }
    }
    async componentWillReceiveProps(nextProps) {
        if (nextProps.CTKinhPhi !== undefined && Object.keys(nextProps.CTKinhPhi).length > 0)
            this.setState({
                id: nextProps.CTKinhPhi.id,
                khoa: nextProps.CTKinhPhi.khoa,
                hoc_ky: nextProps.CTKinhPhi.hoc_ky,
                loai: nextProps.CTKinhPhi.loai,
                so_tien: nextProps.CTKinhPhi.so_tien,
                thoi_gian_bat_dau: nextProps.CTKinhPhi.thoi_gian_bat_dau,
                thoi_gian_ket_thuc: nextProps.CTKinhPhi.thoi_gian_ket_thuc,
                ngay_cap_nhat: nextProps.CTKinhPhi.ngay_cap_nhat,
                ngay_tao: nextProps.CTKinhPhi.ngay_tao
            });
        this.setState({ status: nextProps.status })
        if (nextProps.status === "Thành Công") {
            await Swal.fire({
                type: 'success',
                title: 'Thành Công!',
                showConfirmButton: true,
                confirmButtonText: 'Đồng ý'
            })
            this.props.history.push("/kinhphi")
        }
    }
    onChange = (e) => {
        var target = e.target;
        var name = target.name;
        this.setState({
            [name]: target.value
        });
    }
    onChangeTGKT = (e) => {
        this.setState({ thoi_gian_ket_thuc: e })
    }
    onChangeTGBD = (e) => {
        this.setState({ thoi_gian_bat_dau: e })
    }
    ThemKinhPhi = (e) => {
        var soTheKT = $("p.mess-err").length;
        var soTheHopLe = $("p.mess-err.none").length;
        if (this.state.khoa !== "" && this.state.loai !== "" && this.state.so_tien && this.state.hoc_ky !== "") {
            if (soTheHopLe === soTheKT) {
                if (this.props.id === undefined) {
                    this.state.thoi_gian_bat_dau = moment(this.state.thoi_gian_bat_dau).format("YYYY-MM-DD");
                    this.state.thoi_gian_ket_thuc = moment(this.state.thoi_gian_ket_thuc).format("YYYY-MM-DD");
                    if( this.state.thoi_gian_bat_dau<this.state.thoi_gian_ket_thuc === true){
                        this.props.ThemKinhPhi(this.state);
                        this.setState(initialState);
                    }
                    else{
                        func.ThongBaoMaLoi("Ngày kết thúc luôn lớn hơn ngày bắt đầu")
                    }
                }
                else {
                    this.state.thoi_gian_bat_dau = moment(this.state.thoi_gian_bat_dau).format("YYYY-MM-DD");
                    this.state.thoi_gian_ket_thuc = moment(this.state.thoi_gian_ket_thuc).format("YYYY-MM-DD");
                    if( this.state.thoi_gian_bat_dau<this.state.thoi_gian_ket_thuc === true){
                    this.props.CapNhatKinhPhi(this.state);
                }
                    else{
                        func.ThongBaoMaLoi("Ngày kết thúc luôn lớn hơn ngày bắt đầu")
                    }
                }
            }
            else {
                func.ThongBaoMaLoi("Bạn phải nhập thông tin hợp lệ")
            }
        }
        else {
            func.ThongBaoMaLoi("Bạn phải nhập đủ thông tin ")
        }
    }
    Thoat() {
        this.props.history.push('/kinhphi');
    }
    render() {
        var { khoa, so_tien, hoc_ky, loai, thoi_gian_bat_dau, thoi_gian_ket_thuc } = this.state;
        return (
            <div id="body-content">
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-md-12">
                            <div className="content-title">
                                <h1 className="text-center h1-title-style">{this.props.id ? "Cập Nhật Học Phí" : "Thêm Kinh Phí"}</h1>
                                <span className="kitu-dacbiet">&#9884;</span>
                            </div>
                        </div>
                        <div className="col-md-12 text-center">
                            <div className="form-content">
                                <div className="row">
                                    <div className="col-lg-5 m-auto col-md-12">
                                        <div className='form-group'>
                                            <label>Khóa:</label>
                                            <input
                                                name="khoa" type='text'
                                                className="input-nhap--style"
                                                maxLength="2"
                                                value={khoa}
                                                onChange={this.onChange}
                                                onKeyUp={func.KiemTraDuLieuNhapLaSo}
                                                onFocus={func.KiemTraNhapDuLieu}
                                            />
                                            <p className="mess-err none" id="khoa">Khóa phải là số và không được để trống</p>
                                        </div>
                                    </div>
                                    <div className="col-lg-5 m-auto col-md-12">
                                        <div className='form-group'>
                                            <label >Học Kỳ:</label>
                                            <select
                                                name="hoc_ky"
                                                className="input-nhap--style"
                                                value={hoc_ky}
                                                onChange={this.onChange}
                                            >
                                                <option value="1">Học Kì 1</option>
                                                <option value="2">Học Kì 2</option>
                                                <option value="3">Học Kì 3</option>
                                                <option value="4">Học Kì 4</option>
                                                <option value="5">Học Kì 5</option>
                                                <option value="6">Học Kì 6</option>

                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-lg-5 m-auto col-md-12">
                                        <div className='form-group'>
                                            <label>Loại</label>
                                            <select
                                                name="loai"
                                                className="input-nhap--style"
                                                value={loai}
                                                onChange={this.onChange}
                                            >
                                                <option value="1">Học Phí</option>
                                                <option value="2">Bảo Hiểm</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div className="col-lg-5 m-auto col-md-12">
                                        <div className='form-group'>
                                            <label>Số Tiền:</label>
                                            <input
                                                name="so_tien" type="text"
                                                maxLength="8"
                                                className="input-nhap--style"
                                                value={so_tien}
                                                onChange={this.onChange}
                                                onKeyUp={func.KiemTraDuLieuNhapLaSo}
                                                onFocus={func.KiemTraNhapDuLieu}
                                            />
                                            <p className="mess-err none" id="so_tien">Tiền phải là số không được để trống và nhiều hơn 7 ký tự</p>
                                        </div>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-lg-5 m-auto col-md-12">
                                        <div className='form-group'>
                                            <label>Thời Gian Bắt Đầu:</label>
                                            <div>
                                                <Calendar
                                                    onChange={this.onChangeTGBD}
                                                    value={moment(thoi_gian_bat_dau).toDate()}
                                                />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-lg-5 m-auto col-md-12">
                                        <div className='form-group'>
                                            <label >Thời Gian Kết Thúc:</label>
                                            <div>
                                                <Calendar
                                                    onChange={this.onChangeTGKT}
                                                    value={moment(thoi_gian_ket_thuc).toDate()}
                                                />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-lg-12 col-md-12">
                                    <button className='button-default--style button-thaotac--style' type="button" onClick={this.ThemKinhPhi}>{this.props.id === undefined ? "Thêm" : "Lưu"}</button>
                                    <button className='button-default--style button-thoat--style' type="button" onClick={this.Thoat}>Thoát</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
const mapStateToProps = state => ({
    CTKinhPhi: state.LayCTKinhPhi.CTKinhPhi,
    status: state.LayCTKinhPhi.status
});
themkinhphi.propTypes = {
    ThemKinhPhi: propTypes.func.isRequired,
    CapNhatKinhPhi: propTypes.func.isRequired,
    ChiTietKinhPhiTheoMaKhoa: propTypes.func.isRequired,
}
export default connect(mapStateToProps, { ThemKinhPhi, CapNhatKinhPhi, ChiTietKinhPhiTheoMaKhoa })(withRouter(themkinhphi));
