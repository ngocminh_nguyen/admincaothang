import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { connect } from 'react-redux';
import propTypes from 'prop-types';
import { ChiTietLopTheoMaLop, ThemLop, CapNhatLop } from '../../actions/lop';
import { DanhSachChuyenNganh } from '../../actions/chuyen_nganh';
import { withRouter } from 'react-router';
import * as func from '../../constants/FunctionsKTDuLieu_ThongBao'; 
import Swal from 'sweetalert2';  
import * as $ from 'jquery'
const initialState = {
    id: '',
    ma_lop: '',
    bac_dao_tao: 'TC',
    chuyen_nganh: '',
    khoa: '',
    ky_tu_phan_biet: '',
    ten_lop: '',
    ngay_cap_nhat: '',
    ngay_tao: '',
    DSChuyenNganh: []
}
class themlop extends Component {
    constructor(props) {
        super(props);
        this.state = initialState;
        this.ThemLop = this.ThemLop.bind(this);
        this.Thoat = this.Thoat.bind(this)
    }
    componentWillMount() {
        if (this.props.id)
            this.props.ChiTietLopTheoMaLop(this.props.id);
        this.props.DanhSachChuyenNganh();
    }
    async componentWillReceiveProps(nextProps) {
        var mangtam=[];
        if (nextProps.DSChuyenNganh !== undefined && nextProps.DSChuyenNganh.length > 0) {
            nextProps.DSChuyenNganh.map((item, key)=>{
                if(item.xoa==0){
                    mangtam.push(item);
                }
            })
            this.setState({ DSChuyenNganh: mangtam, chuyen_nganh: mangtam[0].id }) 
        } 
        if (nextProps.CTLop !== undefined && Object.keys(nextProps.CTLop).length > 0) {
            this.setState({
                id: nextProps.CTLop.id,
                ma_lop: nextProps.CTLop.ma_lop,
                bac_dao_tao: nextProps.CTLop.bac_dao_tao,
                chuyen_nganh: nextProps.CTLop.chuyen_nganh,
                khoa: nextProps.CTLop.khoa,
                ky_tu_phan_biet: nextProps.CTLop.ky_tu_phan_biet,
                ten_lop: nextProps.CTLop.ten_lop,
                ngay_cap_nhat: nextProps.CTLop.ngay_cap_nhat,
                ngay_tao: nextProps.CTLop.ngay_tao
            });
        } 
        if (nextProps.status === "Thành Công") {
            await Swal.fire({
                type: 'success',
                title: 'Thành Công!',
                showConfirmButton: true,
                confirmButtonText: 'Đồng ý'
            })
            this.props.history.push("/lop")
        }
    }
    onChange = (e) => {
        var target = e.target;
        var name = target.name;
        this.setState({
            [name]: target.value,
        });
    }
    ThemLop = (e) => {  
        var soTheKT = $("p.mess-err").length; 
        var soTheHopLe = $("p.mess-err.none").length;
        if(this.state.khoa !== "" && this.state.ky_tu_phan_biet !== "" && this.state.ten_lop !== "" && func.dem ===0){
            if(soTheHopLe  === soTheKT){
                if (this.props.id === undefined) {
                    this.props.ThemLop(this.state);
                    initialState.chuyen_nganh = this.state.chuyen_nganh;
                    initialState.DSChuyenNganh = this.state.DSChuyenNganh;
                    this.setState(initialState);
                }
                else {  
                    this.props.CapNhatLop(this.state); 
                }   
            }
            else{
                func.ThongBaoMaLoi("Bạn phải nhập thông tin hợp lệ")
            } 
        }
        else{ 
            func.ThongBaoMaLoi("Bạn phải nhập đủ thông tin ") 
        } 
    }
    Thoat() {
        // var history = this.props.id;
        this.props.history.push('/lop');
    }
    render() {
        var { bac_dao_tao, khoa, ky_tu_phan_biet, ten_lop } = this.state;
        return (
            <div id="body-content">
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-md-12">
                            <div className="content-title">
                                <h1 className="text-center h1-title-style">{this.props.id ? "Cập Nhật Lớp" : "Thêm Lớp"}</h1>
                                <span className="kitu-dacbiet">&#9884;</span>
                            </div>
                        </div>
                        <div className="col-md-12 text-center">
                            <div className="form-content">
                                <div className="row">
                                    <div className="col-lg-5 m-auto col-md-12">
                                        <div className={this.props.id !== undefined ? 'none' : 'form-group'} >
                                            <label  >Bậc Đào Tạo:</label>
                                            <select
                                                name="bac_dao_tao"
                                                className="input-nhap--style"
                                                value={bac_dao_tao}
                                                onChange={this.onChange}
                                            >
                                                <option value="ĐH">Đại Học</option>
                                                <option value="CĐN">Cao Đẳng Nghề</option>
                                                <option value="CĐ">Cao Đẳng</option>
                                                <option value="TC">Trung Cấp Chuyên Nghiệp</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div className="col-lg-5 m-auto col-md-12">
                                        <div className={this.props.id !== undefined ? 'none' : 'form-group'}>
                                            <label >Chuyên Ngành:</label>
                                            <select
                                                name="chuyen_nganh"
                                                className="input-nhap--style"
                                                onChange={this.onChange}
                                            >
                                                {this.state.DSChuyenNganh.map((item, key) =>
                                                    <option key={key} value={item.id} className="input-nhap--style">{item.ten_chuyen_nganh}</option>
                                                )}
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-lg-5 m-auto col-md-12">
                                        <div className={this.props.id !== undefined ? 'none' : 'form-group'}>
                                            <label >Khóa</label>
                                            <input
                                                name="khoa" type="text"
                                                className="input-nhap--style"
                                                maxLength="2"
                                                value={khoa}
                                                onChange={this.onChange}
                                                onKeyUp={func.KiemTraDuLieuNhapLaSo} 
                                                onFocus={func.KiemTraNhapDuLieu}
                                            />
                                            <p className="mess-err none" id="khoa">Khóa phải là số và không được để trống </p>
                                        </div>
                                    </div>
                                    <div className="col-lg-5 m-auto col-md-12">
                                        <div className={this.props.id !== undefined ? 'none' : 'form-group'}>
                                            <label >Ký Tự Phân Biệt:</label>
                                            <input
                                                name="ky_tu_phan_biet" type="text"
                                                className="input-nhap--style"
                                                value={ky_tu_phan_biet}
                                                onChange={this.onChange}
                                                maxLength="5"
                                                onKeyUp={func.KiemTraDuLieuNhapLaChu} 
                                                onFocus={func.KiemTraNhapDuLieu}
                                                style={{textTransform:"uppercase"}}
                                            />
                                            <p className="mess-err none" id="ky_tu_phan_biet">Ký tự phân biệt phải là chữ và không được để trống</p>
                                        </div>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-lg-5 m-auto col-md-12">
                                        <div className='form-group '>
                                            <label >Tên Lớp:</label>
                                            <input
                                                name="ten_lop" type="text"
                                                className="input-nhap--style"
                                                value={ten_lop}
                                                onChange={this.onChange}
                                                onKeyUp={func.KiemTraNhapDuLieu} 
                                                onFocus={func.KiemTraNhapDuLieu}
                                            />
                                            <p className="mess-err none" id="ten_lop">Tên lớp không được để trống</p>
                                        </div>
                                    </div>
                                    <div className={this.props.id?"none":"col-lg-5 m-auto col-md-12"}></div>
                                </div>
                                <div className="col-lg-12 col-md-12">
                                    <button className='button-default--style button-thaotac--style' type="button" onClick={this.ThemLop}>{this.props.id === undefined ? "Thêm" : "Lưu"}</button>
                                    <button className='button-default--style button-thoat--style' type="button" onClick={this.Thoat}>Thoát</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
const mapStateToProps = state => ({
    CTLop: state.LayCTLop.CTLop,
    DSChuyenNganh: state.LayDSChuyenNganh.DSChuyenNganh,
    status: state.LayCTLop.status
});
themlop.propTypes = {
    ThemLop: propTypes.func.isRequired,
    CapNhatLop: propTypes.func.isRequired,
    ChiTietLopTheoMaLop: propTypes.func.isRequired,
    DanhSachChuyenNganh: propTypes.func.isRequired
}
export default connect(mapStateToProps, { ThemLop, CapNhatLop, ChiTietLopTheoMaLop, DanhSachChuyenNganh })(withRouter(themlop));
