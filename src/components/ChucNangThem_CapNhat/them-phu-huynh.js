import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { connect } from 'react-redux';
import propTypes from 'prop-types';
import { ThemPhuHuynh, CapNhatPhuHuynh, ChiTietPhuHuynhTheoMAPH } from './../../actions/phu_huynh'; 
import { withRouter } from 'react-router';
import * as func from '../../constants/FunctionsKTDuLieu_ThongBao'; 
import Swal from 'sweetalert2';  
import * as $ from 'jquery'
const initialState = {
    id:'',
    ma_phu_huynh:'',
    ho_ten_cha : '',
    ho_ten_me :'',
    so_dien_thoai :'',
    ngay_cap_nhat:'',
    dia_chi: '',
    ngay_tao:'' 
}
class themphuhuynh extends Component {
    constructor(props) {
        super(props);
        this.state = initialState;
        this.Thoat = this.Thoat.bind(this);
        this.ThemPhuHuynh = this.ThemPhuHuynh.bind(this);
    }
    componentWillMount(){
        if(this.props.id)
        this.props.ChiTietPhuHuynhTheoMAPH(this.props.id); 
    }
    async componentWillReceiveProps(nextProps) {
        if(this.props.id && nextProps.CTPhuHuynh!== undefined && Object.keys(nextProps.CTPhuHuynh).length > 0)
            this.setState({
                ma_phu_huynh: nextProps.CTPhuHuynh.ma_phu_huynh,
                ho_ten_cha: nextProps.CTPhuHuynh.ho_ten_cha,
                ho_ten_me: nextProps.CTPhuHuynh.ho_ten_me,
                so_dien_thoai: nextProps.CTPhuHuynh.so_dien_thoai,
                dia_chi: nextProps.CTPhuHuynh.dia_chi,
                ngay_cap_nhat: nextProps.CTPhuHuynh.ngay_cap_nhat,
                ngay_tao: nextProps.CTPhuHuynh.ngay_tao
            });
        if (nextProps.status === "Thành Công") {
            await Swal.fire({
                type: 'success',
                title: 'Thành Công!',
                showConfirmButton: true,
                confirmButtonText: 'Đồng ý'
            })
            this.props.history.push("/phuhuynh")
        }
    }
    onChange = (e) => {
        var target = e.target;
        var name = target.name;
        this.setState({
            [name]: target.value
        });
    }
    ThemPhuHuynh = (e) => {  
        var soTheKT = $("p.mess-err").length; 
        var soTheHopLe = $("p.mess-err.none").length;
        if(this.state.so_dien_thoai!==""){
            if(soTheHopLe  === soTheKT){
                if (this.props.id === undefined) {
                    this.props.ThemPhuHuynh(this.state);
                    this.setState(initialState)
                }
                else {  
                    this.props.CapNhatPhuHuynh(this.state);  
                }   
            }
            else{
                func.ThongBaoMaLoi("Bạn phải nhập thông tin hợp lệ")
            } 
        }
        else{ 
            func.ThongBaoMaLoi("Bạn phải nhập đủ thông tin ") 
        } 
    }
    Thoat() { 
        this.props.history.push('/phuhuynh');
    }
    render() {
        var { ho_ten_cha , ho_ten_me , so_dien_thoai, dia_chi } = this.state; 
        return (
            <div id="body-content">
                <div className="container-fluid">
                    <div className="row"> 
                        <div className="col-md-12"> 
                            <div className="content-title">
                                <h1 className="text-center h1-title-style">{this.props.id?"Cập Nhật Phụ Huynh":"Thêm Phụ Huynh"}</h1>
                                <span className="kitu-dacbiet">&#9884;</span>
                            </div>
                        </div>
                        <div className="col-md-12 text-center">
                            <div className="form-content">
                                <div className="row"> 
                                    <div className="col-lg-5 m-auto col-md-12">
                                        <div className='form-group'>
                                            <label >Họ Tên Cha:</label>
                                            <input 
                                                name="ho_ten_cha" type="text"
                                                className="input-nhap--style"
                                                value={ho_ten_cha}
                                                onChange={this.onChange} 
                                            />
                                            {/* <p className="mess-err none" id="ho_ten_cha">Họ tên cha không được để trống</p> */}
                                        </div>
                                    </div>
                                    <div className="col-lg-5 m-auto col-md-12">
                                        <div className='form-group'>
                                            <label >Họ Tên Mẹ:</label>
                                            <input 
                                                name="ho_ten_me" type="text"
                                                className="input-nhap--style"
                                                value={ho_ten_me}
                                                onChange={this.onChange} 
                                            />
                                            {/* <p className="mess-err none" id="ho_ten_me">Họ tên mẹ không được để trống</p> */}
                                        </div>
                                    </div>
                                    </div>
                                    <div className="row"> 
                                    <div className="col-lg-5 m-auto col-md-12">
                                        <div className='form-group'>
                                            <label >Số điện thoại:</label>
                                            <input 
                                                name="so_dien_thoai" type=""
                                                className="input-nhap--style"
                                                value={so_dien_thoai}
                                                onChange={this.onChange}
                                                onKeyUp={func.KiemTraDuLieuNhapLaSoDT} 
                                                onFocus={func.KiemTraNhapDuLieu}
                                                maxLength="11"
                                            />
                                            <p className="mess-err none" id="so_dien_thoai">Số điện thoại không hợp lệ</p>
                                        </div>
                                    </div>
                                    <div className="col-lg-5 m-auto col-md-12">
                                        <div className='form-group'>
                                            <label>Địa chỉ:</label>
                                            <input 
                                                name="dia_chi" type="text"
                                                className="input-nhap--style"
                                                value={dia_chi}
                                                onChange={this.onChange}  
                                                onKeyUp={func.KiemTraNhapDuLieu} 
                                                onFocus={func.KiemTraNhapDuLieu}
                                            />
                                            {/* <p className="mess-err none" id="ho_ten_cha">Họ tên cha không được để trống</p> */}
                                        </div>
                                    </div>
                                    </div>
                                    <div className="col-lg-12 col-md-12">
                                        <button className='button-default--style button-thaotac--style' type="button" onClick={this.ThemPhuHuynh}>{this.props.id === undefined? "Thêm":"Lưu"}</button>
                                        <button className='button-default--style button-thoat--style' type="button" onClick={this.Thoat}>Thoát</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
const mapStateToProps=state=>({
    CTPhuHuynh: state.LayCTPhuHuynh.CTPhuHuynh,
    status: state.LayCTPhuHuynh.status 
});
themphuhuynh.propTypes = {
    ThemPhuHuynh: propTypes.func.isRequired,
    CapNhatPhuHuynh: propTypes.func.isRequired,
    ChiTietPhuHuynhTheoMAPH: propTypes.func.isRequired,
}
export default connect(mapStateToProps, { ThemPhuHuynh, CapNhatPhuHuynh, ChiTietPhuHuynhTheoMAPH })(withRouter(themphuhuynh)); 
