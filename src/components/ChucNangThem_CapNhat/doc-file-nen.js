import React, {useCallback} from 'react'
import {useDropzone} from 'react-dropzone'
import readXlsxFile from 'read-excel-file'; 
import { ThemSinhVienTuFileNen, ThemHinhSinhVien } from '../../actions/sinh_vien' 
import * as types from '../../constants/constants'; 
import axios from "axios";
let data = new FormData()  

export const MyDropzone=()=> {
  const onDrop = useCallback(acceptedFiles => {
    const reader = new FileReader()

    reader.onabort = () => console.log('file reading was aborted')
    reader.onerror = () => console.log('file reading has failed')
    reader.onload = () => {
      // Do whatever you want with the file contents
      const binaryStr = reader.result

      console.log(binaryStr)
    }

    acceptedFiles.forEach(tenfile => (   
          data.append('up_load_file',tenfile , tenfile.name), 
          axios.post(types.server +"sinh_vien/upload-file", data) 
    ))

  },  [])
  const {getRootProps, getInputProps, isDragActive} = useDropzone({onDrop})

  return (
    <div {...getRootProps()} className="pt-5 pb-5">
      <input {...getInputProps()} />
      <p>Kéo thả tệp nén (.zip) vào đây hoặc click chuột vào để chọn files</p>
    </div>
  )
}