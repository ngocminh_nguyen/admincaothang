import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { connect } from 'react-redux';
import propTypes from 'prop-types';
import { ThemMonHoc, CapNhatMonHoc, ChiTietMonHocTheoMaMH } from './../../actions/mon_hoc'; 
import { DanhSachChuyenNganh } from '../../actions/chuyen_nganh';
import { withRouter } from 'react-router';
import * as func from '../../constants/FunctionsKTDuLieu_ThongBao'; 
import Swal from 'sweetalert2';  
import * as $ from 'jquery'
const initialState = {
    id:'',
    ma_mon_hoc: '',
    ten_mon_hoc:'',
    dv_hoc_phan:'',
    ngay_cap_nhat:'',
    ma_chuyen_nganh: '',
    DSChuyenNganh: [],
    ngay_tao:'' 
}
class themmonhoc extends Component {
    constructor(props) {
        super(props);
        this.state = initialState;
        this.Thoat = this.Thoat.bind(this);
        this.ThemMonHoc = this.ThemMonHoc.bind(this);
    }
    componentWillMount(){
        if(this.props.id)
        this.props.ChiTietMonHocTheoMaMH(this.props.id); 
        this.props.DanhSachChuyenNganh();
    }
    async componentWillReceiveProps(nextProps) { 
        var mangtam=[];
        if (nextProps.DSChuyenNganh !== undefined && nextProps.DSChuyenNganh.length > 0) {
            nextProps.DSChuyenNganh.map((item, key)=>{
                if(item.xoa==0){
                    mangtam.push(item);
                }
            })
            this.setState({ DSChuyenNganh: mangtam, ma_chuyen_nganh: mangtam[0].id }) 
        } 
        if(this.props.id && nextProps.CTMonHoc!== undefined && Object.keys(nextProps.CTMonHoc).length > 0)
        this.setState({ id: nextProps.CTMonHoc.id,
            ma_mon_hoc: nextProps.CTMonHoc.ma_mon_hoc,
            ten_mon_hoc: nextProps.CTMonHoc.ten_mon_hoc,
            ma_chuyen_nganh: nextProps.CTMonHoc.ma_chuyen_nganh,
            dv_hoc_phan: nextProps.CTMonHoc.dv_hoc_phan,
            ngay_cap_nhat: nextProps.CTMonHoc.ngay_cap_nhat,
            ngay_tao: nextProps.CTMonHoc.ngay_tao
        });
        if (nextProps.status === "Thành Công") {
            await Swal.fire({
                type: 'success',
                title: 'Thành Công!',
                showConfirmButton: true,
                confirmButtonText: 'Đồng ý'
            })
            this.props.history.push("/monhoc")
        }  
    }
    onChange = (e) => {
        var target = e.target;
        var name = target.name;
        this.setState({
            [name]: target.value
        });
    }
    ThemMonHoc = (e) => {  
        var soTheKT = $("p.mess-err").length; 
        var soTheHopLe = $("p.mess-err.none").length;
        if(this.state.ten_mon_hoc !== "" && this.state.dv_hoc_phan!==""){
            if(soTheHopLe  === soTheKT){
                if (this.props.id === undefined) {
                    this.props.ThemMonHoc(this.state);
                    initialState.ma_chuyen_nganh = this.state.ma_chuyen_nganh;
                    initialState.DSChuyenNganh = this.state.DSChuyenNganh;
                    this.setState(initialState)
                }
                else {  
                    this.props.CapNhatMonHoc(this.state); 
                }   
            }
            else{
                func.ThongBaoMaLoi("Bạn phải nhập thông tin hợp lệ")
            } 
        }
        else{ 
            func.ThongBaoMaLoi("Bạn phải nhập đủ thông tin ") 
        } 
    }
    Thoat() { 
        this.props.history.push('/monhoc');
    }
    render() {
        var {  ten_mon_hoc, dv_hoc_phan, ma_chuyen_nganh } = this.state; 
        return (
            <div id="body-content">
                <div className="container-fluid">
                    <div className="row"> 
                        <div className="col-md-12"> 
                            <div className="content-title">
                                <h1 className="text-center h1-title-style">{this.props.id?"Cập Nhật Môn Học":"Thêm Môn Học"}</h1>
                                <span className="kitu-dacbiet">&#9884;</span>
                            </div>
                        </div>
                        <div className="col-md-12 text-center">
                            <div className="form-content">
                                <div className="row"> 
                                    <div className="col-lg-5 m-auto col-md-12">
                                        <div className='form-group'>
                                            <label >Tên Môn Học</label>
                                            <input 
                                                name="ten_mon_hoc" type="text"
                                                className="input-nhap--style"
                                                value={ten_mon_hoc}
                                                onChange={this.onChange}
                                                onKeyUp={func.KiemTraNhapDuLieu} 
                                                onFocus={func.KiemTraNhapDuLieu}
                                            />
                                            <p className="mess-err none" id="ten_mon_hoc">Tên môn học  không được để trống</p>
                                        </div>
                                    </div>
                                    <div className="col-lg-5 m-auto col-md-12">
                                        <div className='form-group'>
                                            <label >Đơn Vị Học Phần:</label>
                                            <input 
                                                name="dv_hoc_phan" type="text"
                                                className="input-nhap--style"
                                                maxLength="2"
                                                value={dv_hoc_phan}
                                                onChange={this.onChange}
                                                onKeyUp={func.KiemTraDuLieuNhapLaSo} 
                                                onFocus={func.KiemTraNhapDuLieu}
                                            />
                                            <p className="mess-err none" id="dv_hoc_phan">Đơn vị học phần phải là số và không được để trống</p>
                                        </div>
                                    </div>
                                    <div className="col-lg-5 m-auto col-md-12">
                                        <div className='form-group'>
                                            <label >Chuyên Ngành:</label>
                                            <select
                                                name="ma_chuyen_nganh"
                                                className="input-nhap--style"
                                                value={ma_chuyen_nganh}
                                                onChange={this.onChange}
                                            >
                                                {this.state.DSChuyenNganh.map((item, key) =>
                                                    <option key={key} value={item.id} className="input-nhap--style">{item.ten_chuyen_nganh}</option>
                                                )}
                                            </select>
                                        </div>
                                    </div>
                                    <div className="col-lg-5 m-auto col-md-12"></div>
                                    <div className="col-lg-12 col-md-12">
                                        <button className='button-default--style button-thaotac--style' type="button" onClick={this.ThemMonHoc}>{this.props.id === undefined? "Thêm":"Lưu"}</button>
                                        <button className='button-default--style button-thoat--style' type="button" onClick={this.Thoat}>Thoát</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
const mapStateToProps=state=>({
    CTMonHoc: state.LayCTMonHoc.CTMonHoc,
    DSChuyenNganh: state.LayDSChuyenNganh.DSChuyenNganh,
    status: state.LayCTMonHoc.status,
});
themmonhoc.propTypes = {
    ThemMonHoc: propTypes.func.isRequired,
    CapNhatMonHoc: propTypes.func.isRequired,
    ChiTietMonHocTheoMaMH: propTypes.func.isRequired,
    DanhSachChuyenNganh: propTypes.func.isRequired,
}
export default connect(mapStateToProps, { ThemMonHoc, CapNhatMonHoc, ChiTietMonHocTheoMaMH, DanhSachChuyenNganh })(withRouter(themmonhoc)); 
