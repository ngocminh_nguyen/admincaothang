import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { connect } from 'react-redux';
import propTypes from 'prop-types';
import moment from 'moment';
import { ChiTietLichThiTheoMaLichThi, ThemLichThi, CapNhatLichThi } from '../../actions/lich_thi';
// import {DanhSachGiaoVien} from '../../actions/giao_vien';
import { DanhSachMonHoc } from '../../actions/mon_hoc';
import { DanhSachLopHocPhan } from '../../actions/lop_hoc_phan';
import { withRouter } from 'react-router';
import * as func from '../../constants/FunctionsKTDuLieu_ThongBao'; 
import TimeKeeper from 'react-timekeeper'; 
import Calendar from 'react-calendar';
import Swal from 'sweetalert2';  
import * as $ from 'jquery'
const initialState = {
    id: '',
    ma_lich_thi: '',
    ma_mon_hoc: '',
    hoc_ky: '1',
    ngay_thi:new Date(),
    gio_thi: '6:00',
    phong_thi: [],
    phong1:'',
    phong2:'',
    loai: '0',
    ghi_chu: '',
    ma_lop: '',
    DSLopHocPhan: [],
    time: '6:50 am',
    displayTimepicker: true,
    displayTimepicker2: true,
    date : '2015-06-26',
}
class themlichthi extends Component {
    constructor(props) {
        super(props);
        this.state = initialState;
        this.ThemLichThi = this.ThemLichThi.bind(this);
        this.Thoat = this.Thoat.bind(this)
    }
    componentWillMount() { 
        if (this.props.id) {
            this.props.ChiTietLichThiTheoMaLichThi(this.props.id, this.props.loai);
        } 
        this.props.DanhSachLopHocPhan(); 
    }
    async componentWillReceiveProps(nextProps) { 
        var mangtam=[];
        if (nextProps.DSLopHocPhan !== undefined && nextProps.DSLopHocPhan.length > 0) {
            nextProps.DSLopHocPhan.map((item, key)=>{
                if(item.xoa==0){
                    mangtam.push(item);
                }
            })
            this.setState({ DSLopHocPhan: mangtam, ma_lop: mangtam[0].ma_lop }) 
        } 
        if (this.props.id && this.props.loai && nextProps.CTLichThi !== undefined && Object.keys(nextProps.CTLichThi).length > 0) {
            this.setState({
                id: nextProps.CTLichThi.id,
                ma_lich_thi: nextProps.CTLichThi.ma_lich_thi,
                ma_lop: nextProps.CTLichThi.ma_lop,
                ngay_thi: nextProps.CTLichThi.ngay_thi,
                gio_thi: nextProps.CTLichThi.gio_thi.slice(0,5),
                phong1: JSON.parse(nextProps.CTLichThi.phong_thi)[0].nualopdau,
                phong2: JSON.parse(nextProps.CTLichThi.phong_thi)[0].nualopcuoi,
                loai: nextProps.CTLichThi.loai,
            });
            console.log(this.state.ma_lop)
        }
        if(this.props.id && this.props.loai ===undefined){
            this.setState({ma_lop:this.props.id})
        }
        this.setState({ status: nextProps.status })
        if (nextProps.status === "Thành Công") {
            await Swal.fire({
                type: 'success',
                title: 'Thành Công!',
                showConfirmButton: true,
                confirmButtonText: 'Đồng ý'
            })
            this.props.history.push("/lichthi")
        }
    }
    onChange = (e) => {
        var target = e.target;
        var name = target.name;
        this.setState({
            [name]: target.value,
        }); 
    }
    ThemLichThi = (e) => {  
        // var newdata = { 
        //     "nualopdau":this.state.phong1,
        //     "nualopcuoi":this.state.phong2,
        // }; 
        // this.setState({ phong_thi:[{
        //     nualopdau:this.state.phong1,
        //     nualopcuoi:this.state.phong2,
        // }]);  
        this.state.phong_thi = [{"nualopdau":this.state.phong1, "nualopcuoi":this.state.phong2,}] 
        var soTheKT = $("p.mess-err").length; 
        var soTheHopLe = $("p.mess-err.none").length;
        if(this.state.phong_thi !=="" ){
            if(soTheHopLe  === soTheKT){ 
                if(this.props.id && this.props.loai){  
                    console.log(this.state)     
                    this.state.ngay_thi=moment(this.state.ngay_thi).format("YYYY-MM-DD")
                    this.props.CapNhatLichThi(this.state); 
                }   
                else{
                    this.state.ngay_thi=moment(this.state.ngay_thi).format("YYYY-MM-DD")
                        this.props.ThemLichThi(this.state);
                        initialState.ma_lop = this.state.ma_lop;
                        initialState.DSLopHocPhan = this.state.DSLopHocPhan;  
                        this.setState(initialState); 
                }
            }
            else{
                func.ThongBaoMaLoi("Bạn phải nhập thông tin hợp lệ")
            } 
        }
        else{ 
            func.ThongBaoMaLoi("Bạn phải nhập đủ thông tin ") 
        } 
    }
    Thoat() { 
        this.props.history.push('/lichthi');
    }
    handleTimeChangeGioThi=(newTime)=>{
        
        this.setState({ gio_thi: newTime.formatted24})
    } 
    toggleTimekeeper=(val)=>{
        this.setState({displayTimepicker: val}) 
    }
    onChangeDate=(e)=>{ 
        this.setState({ngay_thi:e}) 
    }
    render() {
        console.log(this.props.id, this.props.loai)
        var {  ngay_thi, gio_thi , loai, phong1, phong2, ma_lop } = this.state; 
        return (
            <div id="body-content">
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-md-12">
                            <div className="content-title">
                                <h1 className="text-center h1-title-style">{this.props.id ? "Cập Nhật Lịch Thi" : "Thêm Lịch Thi"}</h1>
                                <span className="kitu-dacbiet">&#9884;</span>
                            </div>
                        </div>
                        <div className="col-md-12 text-center">
                            <div className="form-content">
                                <div className="row"> 
                                    <div className="col-lg-5 m-auto col-md-12">
                                        <div className='form-group'>
                                            <label >Lớp Học:</label>
                                            <select
                                                name="ma_lop"
                                                className="input-nhap--style"
                                                value={ma_lop}
                                                onChange={this.onChange}
                                            >
                                                {this.state.DSLopHocPhan.map((lophoc, key) =>
                                                    <option key={key} value={lophoc.ma_lop} className="input-nhap--style">{lophoc.ma_lop}</option>
                                                )}
                                            </select>
                                        </div>
                                    </div>
                                    <div className="col-lg-5 m-auto col-md-12"></div>
                                </div>
                                <div className="row">
                                    <div className="col-lg-5 m-auto col-md-12">
                                        <div className='form-group'>
                                            <label >Loại:</label>
                                            <select
                                                name="loai"
                                                className="input-nhap--style"
                                                value={loai}
                                                onChange={this.onChange}
                                            >
                                                <option value="0">Thi Lần 1</option>
                                                <option value="1">Thi Lần 2</option>
                                            </select> 
                                        </div>
                                    </div>
                                    <div className="col-lg-5 m-auto col-md-12">
                                        <div className="row">
                                            <div className="col-lg-6">
                                                <div className='form-group'>
                                                    <label>Nửa lớp đầu:</label>
                                                    <input
                                                        name="phong1" type="text"
                                                        className="input-nhap--style"
                                                        value={phong1}
                                                        onChange={this.onChange}
                                                        maxLength="4"
                                                        onKeyUp={func.KiemTraDuLieuNhapLaSo} 
                                                        onFocus={func.KiemTraNhapDuLieu}
                                                    />
                                                    <p className="mess-err none" id="phong_thi">Phòng phải là số và không được nhiều hơn 4 ký tự</p>
                                                </div>
                                            </div>
                                            <div className="col-lg-6">
                                                <div className='form-group'>
                                                    <label>Nửa lớp cuối:</label>
                                                    <input
                                                        name="phong2" type="text"
                                                        className="input-nhap--style"
                                                        value={phong2}
                                                        onChange={this.onChange}
                                                        maxLength="4"
                                                        onKeyUp={func.KiemTraDuLieuNhapLaSo} 
                                                        onFocus={func.KiemTraNhapDuLieu}
                                                    />
                                                    <p className="mess-err none" id="phong_thi">Phòng phải là số và không được nhiều hơn 4 ký tự</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div> 
                                </div>
                                <div className="row">
                                    <div className="col-lg-5 m-auto col-md-12">
                                        <div className='form-group'>
                                            <label >Giờ Thi:</label> 
                                            <div>
                                                {this.state.displayTimepicker ?
                                                    <TimeKeeper
                                                        time={gio_thi}
                                                        onChange={this.handleTimeChangeGioThi}
                                                        onDoneClick={() => {
                                                            this.toggleTimekeeper(false)
                                                        }}
                                                        switchToMinuteOnHourSelect={true}
                                                    />
                                                    :
                                                    false
                                                } 
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-lg-5  col-md-12 mt-0" style={{margin:"0 auto"}}>
                                        <div className='form-group'>
                                            <label >Ngày Thi:</label> 
                                            <div>
                                                <Calendar
                                                onChange={this.onChangeDate}
                                                value={moment(ngay_thi).toDate()}
                                                />
                                            </div>
                                        </div>
                                    </div> 
                                </div> 
                                <div className="col-lg-12 col-md-12">
                                    <button className='button-default--style button-thaotac--style' type="button" onClick={this.ThemLichThi}>{this.props.id === undefined ? "Thêm" : "Lưu"}</button>
                                    <button className='button-default--style button-thoat--style' type="button" onClick={this.Thoat}>Thoát</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
const mapStateToProps = state => ({
    CTLichThi: state.LayCTLichThi.CTLichThi,
    status: state.LayCTLichThi.status,
    DSLopHocPhan: state.LayDSLopHocPhan.DSLopHocPhan,
    a:console.log(state.LayCTLichThi.CTLichThi)
});
themlichthi.propTypes = {
    ThemLichThi: propTypes.func.isRequired,
    CapNhatLichThi: propTypes.func.isRequired,
    ChiTietLichThiTheoMaLichThi: propTypes.func.isRequired,
    DanhSachLopHocPhan: propTypes.func.isRequired,
}
export default connect(mapStateToProps, { ThemLichThi, CapNhatLichThi, ChiTietLichThiTheoMaLichThi, DanhSachLopHocPhan })(withRouter(themlichthi));
