import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { connect } from 'react-redux';
import propTypes from 'prop-types'; 
import { CapNhatKhoa, ThemKhoa, ChiTietKhoaTheoMaKhoa } from './../../actions/khoa';
import { withRouter } from 'react-router'; 
import * as func from '../../constants/FunctionsKTDuLieu_ThongBao'; 
import Swal from 'sweetalert2';  
import * as $ from 'jquery'
const initialState = {
    id:'',
    ma_khoa: '',
    ten_khoa: '', 
    ngay_tao: '',
    ngay_cap_nhat: '',
}
class themkhoa extends Component {
    constructor(props) {
        super(props);
        this.state = initialState;
        this.Thoat = this.Thoat.bind(this)
    }
    componentWillMount(){
        if(this.props.id){
            this.props.ChiTietKhoaTheoMaKhoa(this.props.id); 
        }
    }
    async componentWillReceiveProps(nextProps) {
        if(this.props.id && nextProps.CTKhoa!== undefined && Object.keys(nextProps.CTKhoa).length > 0)
        this.setState({ id:nextProps.CTKhoa.id,
                        ma_khoa:nextProps.CTKhoa.ma_khoa, 
                        ten_khoa:nextProps.CTKhoa.ten_khoa,  
                        ngay_cap_nhat: nextProps.CTKhoa.ngay_cap_nhat,
                        ngay_tao: nextProps.CTKhoa.ngay_tao, 
        }); 
        this.setState({ status: nextProps.status })
        if (nextProps.status === "Thành Công") {
            await Swal.fire({
                type: 'success',
                title: 'Thành Công!',
                showConfirmButton: true,
                confirmButtonText: 'Đồng ý'
            })
            this.props.history.push("/khoa")
        }
    }
    onChange = (e) => {
        var target = e.target;
        var name = target.name;
        this.setState({
            [name]: target.value
        });
    }
    ThemKhoa = (e) => {    
        var soTheKT = $("p.mess-err").length; 
        var soTheHopLe = $("p.mess-err.none").length;
        if(this.state.ten_khoa.trim() ){
            if(soTheHopLe  === soTheKT){
                if (this.props.id === undefined) {
                
                    this.props.ThemKhoa(this.state);
                    this.setState(initialState);
                }
                else {  
                    this.props.CapNhatKhoa(this.state); 
                }   
            }
            else{
                func.ThongBaoMaLoi("Bạn phải nhập thông tin hợp lệ")
            } 
        }
        else{ 
            func.ThongBaoMaLoi("Bạn phải nhập đủ thông tin ") 
        } 
    }
    Thoat() { 
        this.props.history.push('/khoa');
    }
    render() { 
        var {ten_khoa } = this.state;
        return (
            <div id="body-content"> 
                <div className="container-fluid">
                    <div className="row"> 
                        <div className="col-md-12"> 
                            <div className="content-title">
                                <h1 className="text-center h1-title-style">{this.props.id?"Cập nhật khoa":"Thêm Khoa"}</h1>
                                <span className="kitu-dacbiet">&#9884;</span>
                            </div>
                        </div>
                        <div className="col-md-12 text-center">
                            <div className="form-content">
                                <div className="row"> 
                                    <div className="col-lg-5 col-md-12 m-auto">
                                        <div className='from-group '>
                                            <label>Tên Khoa:</label>
                                            <input 
                                                name="ten_khoa" type="text"
                                                className="input-nhap--style "
                                                value={ten_khoa}
                                                onChange={this.onChange}
                                                onKeyUp={func.KiemTraNhapDuLieu} 
                                                onFocus={func.KiemTraNhapDuLieu}
                                            />
                                            <p className="mess-err none" id="ten_khoa">Tên khoa không được để trống</p>
                                        </div>
                                    </div> 
                                    <div className="col-lg-12 col-md-12 mt-4">
                                        <button className='button-default--style button-thaotac--style' type="button" onClick={this.ThemKhoa}>{this.props.id === undefined?"Thêm":"Lưu"}</button>
                                        <button className='button-default--style button-thoat--style' type="button" onClick={this.Thoat}>Thoát</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
const mapStateToProps=state=>({
    CTKhoa: state.LayCTKhoa.CTKhoa, 
    status: state.LayCTKhoa.status
});
themkhoa.propTypes = {
    ThemKhoa: propTypes.func.isRequired,
    CapNhatKhoa: propTypes.func.isRequired,
    ChiTietKhoaTheoMaKhoa:propTypes.func.isRequired,
}
export default connect(mapStateToProps, { ThemKhoa, CapNhatKhoa, ChiTietKhoaTheoMaKhoa })(withRouter(themkhoa)); 
