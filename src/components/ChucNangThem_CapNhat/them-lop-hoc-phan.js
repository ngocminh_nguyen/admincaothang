import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { connect } from 'react-redux';
import propTypes from 'prop-types'; 
import {Link} from 'react-router-dom';
import {ChiTietLopHocPhanTheoMaLopHocPhan, ThemLopHocPhan, CapNhatLopHocPhan} from '../../actions/lop_hoc_phan';
import {DanhSachGiaoVienChoOption} from '../../actions/giao_vien';
import {DanhSachMonHocChoOption} from '../../actions/mon_hoc';
import moment from 'moment';
import { withRouter } from 'react-router'; 
import Swal from 'sweetalert2';  
import TimeKeeper from 'react-timekeeper'; 
import Calendar from 'react-calendar';
const initialState = { 
    id: '',
    ma_lop  : '',
    ma_giao_vien   : '', 
    ma_mon_hoc :'', 
    hoc_ky   : '1', 
    thu   : '2', 
    tiet_bat_dau : '1', 
    tiet_ket_thuc : '1', 
    ngay_cap_nhat:'',
    ngay_tao:'',
    phong:'',
    DSGiaoVien:[],
    DSMonHoc: [],
    tableTKB: [], 
    mangThoi_Gian:[],
    thoi_gian_ket_thuc :new Date(), 
}
class themlophocphan extends Component {
    constructor(props) {
        super(props);
        this.state = initialState;
        this.ThemLopHocPhan = this.ThemLopHocPhan.bind(this);
        this.Thoat = this.Thoat.bind(this)
    }
    componentWillMount(){
        if(this.props.id){
            this.props.ChiTietLopHocPhanTheoMaLopHocPhan(this.props.id); 
        }
        this.props.DanhSachMonHocChoOption(); 
        this.props.DanhSachGiaoVienChoOption();
    }
    async componentWillReceiveProps(nextProps) { 
        var mangtam=[];
        if (nextProps.DSGiaoVien !== undefined && nextProps.DSGiaoVien.length > 0) {
            nextProps.DSGiaoVien.map((item, key)=>{
                if(item.xoa==0){
                    mangtam.push(item);
                }
            })
            this.setState({ DSGiaoVien: mangtam, ma_giao_vien: mangtam[0].ma_giao_vien }) 
        } 
        var mangDSMH=[];
        if (nextProps.DSMonHoc !== undefined && nextProps.DSMonHoc.length > 0) {
            nextProps.DSMonHoc.map((item, key)=>{
                if(item.xoa==0){
                    mangDSMH.push(item);
                }
            }) 
            this.setState({ DSMonHoc: mangDSMH, ma_mon_hoc: mangDSMH[0].ma_mon_hoc }) 
        } 
        if (this.props.id && nextProps.CTLopHocPhan !== undefined && Object.keys(nextProps.CTLopHocPhan).length > 0 ) {
            this.setState({
                id: nextProps.CTLopHocPhan.id,
                ma_lop: nextProps.CTLopHocPhan.ma_lop,
                ma_giao_vien: nextProps.CTLopHocPhan.ma_giao_vien,
                ma_mon_hoc: nextProps.CTLopHocPhan.ma_mon_hoc,
                thoi_gian_ket_thuc:  nextProps.CTLopHocPhan.thoi_gian_ket_thuc,
                hoc_ky: nextProps.CTLopHocPhan.nextProps.CTLopHocPhan.hoc_ky, 
                mangThoi_Gian: JSON.parse(nextProps.CTLopHocPhan.thoi_gian), 
                ngay_cap_nhat: nextProps.CTLopHocPhan.ngay_cap_nhat,
                ngay_tao: nextProps.CTLopHocPhan.ngay_tao
            });
        }   
    }
    onChange = (e) => {
        var target = e.target;
        var name = target.name;
        this.setState({
            [name]: target.value,
        });
    }
    ThemLopHocPhan = (e) => {  
        if (this.props.id === undefined) {
            this.state.thoi_gian_ket_thuc=moment(this.state.thoi_gian_ket_thuc).format("YYYY-MM-DD") 
            this.props.ThemLopHocPhan(this.state); 
            initialState.ma_giao_vien = this.state.ma_giao_vien;
            initialState.DSGiaoVien = this.state.DSGiaoVien;
            initialState.ma_mon_hoc = this.state.ma_mon_hoc;
            initialState.DSMonHoc = this.state.DSMonHoc; 
            this.setState(initialState);
        } 
    }
    Thoat() {
        // var history = this.props.id;
        
        this.props.history.push('/lophocphan');
    }
    LuuDuLieuVaThemDong=()=>{  
        var newdata = { thu:this.state.thu,
                        tiet_bat_dau:this.state.tiet_bat_dau,
                        tiet_ket_thuc:this.state.tiet_ket_thuc,
                        phong: this.state.phong}; 
        this.setState({ tableTKB: this.state.tableTKB.concat(newdata) });  
    }
    XoaDL(thu){
        var mangtam = this.state.tableTKB;
        mangtam.splice(thu,1);
        this.setState({tableTKB: mangtam}); 
    }
    onChangeDate=(e)=>{ 
        this.setState({thoi_gian_ket_thuc:e}) 
    }
    render() {  
        const gobal = this;
        var {ma_giao_vien, ma_mon_hoc , hoc_ky , tiet_ket_thuc, tiet_bat_dau, thu, thoi_gian_ket_thuc, phong} = this.state;  

        return (
            <div id="body-content">
                <div className="container-fluid">
                    <div className="row"> 
                        <div className="col-md-12"> 
                            <div className="content-title">
                                <h1 className="text-center h1-title-style">{this.props.id?"Cập Nhật Lớp Học Phần":"Thêm Lớp Học Phần"}</h1>
                                <span className="kitu-dacbiet">&#9884;</span>
                            </div>
                        </div>
                        <div className="col-md-12 text-center">
                            <div className="form-content table-responsive tableTKB text-center">
                                <div className=" p-4"> 
                                    <div className="col-lg-5 col-md-12 m-auto ">
                                        <div className='form-group'>
                                            <label >Môn Học:</label>
                                            <select
                                                name="ma_mon_hoc"
                                                className="input-nhap--style"
                                                value={ma_mon_hoc}
                                                onChange={this.onChange}
                                            >
                                                {this.state.DSMonHoc.map((monhoc, key) =>
                                                    <option key={key} value={monhoc.ma_mon_hoc} className="input-nhap--style">{monhoc.ten_mon_hoc}({monhoc.ten_chuyen_nganh})</option>
                                                )}
                                            </select>
                                        </div>
                                    </div>  
                                    <div className="col-lg-5 col-md-12 m-auto ">
                                        <div className='form-group'>
                                            <label >Giáo Viên:</label>
                                            <select
                                                name="ma_giao_vien"
                                                className="input-nhap--style"
                                                value={ma_giao_vien}
                                                onChange={this.onChange}
                                            >
                                                {this.state.DSGiaoVien.map((item, key) =>
                                                    <option key={key} value={item.ma_giao_vien} className="input-nhap--style">{item.ho_ten}</option>
                                                )}
                                            </select>
                                        </div>
                                    </div> 
                                    <div className="col-lg-5 col-md-12 m-auto ">
                                        <div className='form-group'>
                                            <label >Học Kỳ:</label>
                                            <select
                                                name="hoc_ky"
                                                className="input-nhap--style"
                                                value={hoc_ky}
                                                onChange={this.onChange}
                                            >
                                                <option value="1">Học Kỳ 1</option>
                                                <option value="2">Học Kỳ 2</option>
                                                <option value="3">Học Kỳ 3</option>
                                                <option value="4">Học Kỳ 4</option>
                                                <option value="5">Học Kỳ 5</option>
                                                <option value="6">Học Kỳ 6</option>
                                            </select>
                                        </div>
                                    </div> 
                                    <div className="col-lg-5 col-md-12 m-auto">
                                        <div className='from-group '>
                                            <label>Phòng Học:</label>
                                            <input 
                                                name="phong" type="text"
                                                className="input-nhap--style "
                                                value={phong}
                                                onChange={this.onChange}
                                                // onKeyUp={func.KiemTraNhapDuLieu} 
                                                // onFocus={func.KiemTraNhapDuLieu}
                                            />
                                            <p className="mess-err none" id="ten_khoa">Phòng không được để trống</p>
                                        </div>
                                    </div> 
                                    <div className="col-lg-5 m-auto col-md-12">
                                        <div className='form-group'>
                                            <label >Ngày Dự Kiến Kết Thúc:</label> 
                                            <div>
                                                <Calendar
                                                onChange={this.onChangeDate}
                                                value={moment(thoi_gian_ket_thuc).toDate()}
                                                />
                                            </div>
                                        </div>
                                    </div>
                                    <table className="table table-striped mt-4 " style={{ border: "1px solid #dee2e6" }}>  
                                        <thead >
                                            <tr>
                                                <th className="text-center" colSpan={1}> Thứ </th>
                                                <th> Tiết Bắt Đầu </th>
                                                <th colSpan={1}>
                                                     Tiết Kết Thúc 
                                                </th> 
                                                <th colSpan={1} >Thao Tác </th>
                                            </tr>
                                            </thead>
                                        <tbody className="DuLieuLHP">  
                                            <tr>
                                                <td style={{ width: 0.001 }}>
                                                    <select 
                                                        name="thu"
                                                        className="input-nhap--style ml-3"
                                                        value={thu}
                                                        onChange={this.onChange}
                                                    >
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4">4</option>
                                                        <option value="5">5</option>
                                                        <option value="6">6</option>
                                                        <option value="7">7</option>
                                                    </select>
                                                </td>  
                                                <td style={{ width: 0.1 }}>
                                                    <select
                                                        name="tiet_bat_dau"  
                                                        className="input-nhap--style"
                                                        value={tiet_bat_dau}
                                                        onChange={this.onChange}
                                                    >
                                                        <option value="1">Tiết 1</option>
                                                        <option value="2">Tiết 2</option>
                                                        <option value="3">Tiết 3</option>
                                                        <option value="4">Tiết 4</option>
                                                        <option value="5">Tiết 5</option>
                                                        <option value="6">Tiết 6</option> 
                                                    </select>                               
                                                </td>
                                                <td style={{ width: 0.1 }}>
                                                    <select
                                                        name="tiet_ket_thuc"  
                                                        className="input-nhap--style"
                                                        value={tiet_ket_thuc}
                                                        onChange={this.onChange}
                                                    >
                                                        <option value="1">Tiết 1</option>
                                                        <option value="2">Tiết 2</option>
                                                        <option value="3">Tiết 3</option>
                                                        <option value="4">Tiết 4</option>
                                                        <option value="5">Tiết 5</option>
                                                        <option value="6">Tiết 6</option> 
                                                    </select>                               
                                                </td>
                                                
                                                <td style={{ width: 0.01 }}  className="button-dieu-chinh">
                                                {/* <Link to="#" className="btn-update button--style" style={{left:"0" }} onClick={()=>this.LuuDuLieuVaThemDong()}><i className="fa fa-pencil"></i></Link>  */} 
                                                    <button type="button" className="btn-taodong" onClick={()=>this.LuuDuLieuVaThemDong()}>Thêm</button>
                                                </td> 
                                            </tr>  
                                            {/* {rows.map((r) => (  
                                                <td>{r}</td>  
                                            ))}  */}
                                            {this.state.tableTKB.map(function (row, i) {
                                                return (<tr key={i}>
                                                    <td style={{ width: 0.001 }}>T{row.thu}</td>
                                                    <td style={{ width: 0.001 }}>Từ tiết {row.tiet_bat_dau}</td>
                                                    <td style={{ width: 0.001 }}>đến tiết  {row.tiet_ket_thuc}</td> 
                                                    <td>
                                                    <i id="Xoa" onClick={() => gobal.XoaDL(i)} className="fa fa-times" style={{ color: "red" }} ></i>
                                                </td>
                                                </tr>);
                                            })}
                                        </tbody>
                                    </table> 
                                    <div className="col-lg-12 col-md-12">
                                        <button className='button-default--style button-thaotac--style' type="button" onClick={this.ThemLopHocPhan}>{this.props.id===undefined?"Thêm":"Lưu"}</button>
                                        
                                        <button className='button-default--style button-thoat--style' type="button" onClick={this.Thoat}>Thoát</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
} 
const mapStateToProps=state=>({
    CTLopHocPhan: state.LayCTLopHocPhan.CTLopHocPhan, 
    DSGiaoVien: state.LayDSGiaoVien.DSGiaoVienChoOption,
    DSMonHoc: state.LayDSMonHoc.DSMonHocChoOption, 
    status: state.LayCTLopHocPhan.status, 
});
themlophocphan.propTypes = {
    ThemLopHocPhan: propTypes.func.isRequired,
    CapNhatLopHocPhan: propTypes.func.isRequired,
    ChiTietLopHocPhanTheoMaLopHocPhan: propTypes.func.isRequired,
    DanhSachGiaoVienChoOption: propTypes.func.isRequired,
    DanhSachMonHocChoOption: propTypes.func.isRequired,
}
export default connect(mapStateToProps, { ThemLopHocPhan, CapNhatLopHocPhan, ChiTietLopHocPhanTheoMaLopHocPhan, DanhSachGiaoVienChoOption, DanhSachMonHocChoOption})(withRouter(themlophocphan));
