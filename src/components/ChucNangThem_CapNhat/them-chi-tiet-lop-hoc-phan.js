import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { connect } from 'react-redux';
import propTypes from 'prop-types'; 
import {ChiTietCTLopHocPhanTheoMaCTLopHocPhan, ThemCTLopHocPhan, CapNhatCTLopHocPhan} from '../../actions/chi_tiet_lop_hoc_phan';
import {DanhSachSinhVienChoOption} from '../../actions/sinh_vien';
import { withRouter } from 'react-router';
import * as func from '../../constants/FunctionsKTDuLieu_ThongBao'; 
import Select from 'react-select';
import Swal from 'sweetalert2';  
import $ from "jquery"
const initialState = { 
    id: '', 
    ma_lop   : '',
    masv :'',
    diem   : '', 
    ngay_cap_nhat:'',
    ngay_tao:'',
    DSLopHocPhan:[], 
    CTCuaChiTietLopHocPhan:[],
    options:[],
    status:''
}
class themchitietlophocphan extends Component {
    constructor(props) {
        super(props);
        this.state = {initialState, selectedOption: ''};
        this.Thoat = this.Thoat.bind(this)
    }
    componentWillMount(){ 
        if(this.props.id&&this.props.mssv){
            this.props.ChiTietCTLopHocPhanTheoMaCTLopHocPhan(this.props.id, this.props.mssv);
        }
            this.props.DanhSachSinhVienChoOption();

    }
    async componentWillReceiveProps(nextProps) {
        if(this.props.id&&this.props.mssv){
            if (nextProps.CTCuaChiTietLopHocPhan !== undefined && Object.keys(nextProps.CTCuaChiTietLopHocPhan).length>0) {
                this.setState({
                    ma_lop: nextProps.CTCuaChiTietLopHocPhan.ma_lop,
                    masv: nextProps.CTCuaChiTietLopHocPhan.masv,
                    diem: nextProps.CTCuaChiTietLopHocPhan.diem
                })
            } 
        }
        this.state.status = nextProps.status
        if (this.state.status == "Thành Công" && this.props.mssv) {
            await Swal.fire({
                type: 'success',
                title: 'Thành Công!',
                showConfirmButton: true,
                confirmButtonText: 'Đồng ý'
            })
            this.props.history.push(`/lophocphan/${this.props.id}/danhsach`)
        }
        var opt = [];
        nextProps.DSSinhVienChoOption.map(function (item, index) {
            return (
                opt.push({ "label": item.mssv + ": " + item.ho_ten, "value": item.mssv })
            )
        })
        this.setState({ options: opt })
    }
    onChange = (e) => {
        var target = e.target;
        var name = target.name;
        this.setState({
            [name]: target.value,
        });
    }
    handleChange = selectedOption => {
        this.setState({selectedOption }); 
        this.setState({masv: selectedOption.value }); 
    }; 
    ThemCTLopHocPhan = (e) => { 
        var soTheKT = $("p.mess-err").length; 
        var soTheHopLe = $("p.mess-err.none").length;
        if(this.state.diem!== undefined &&this.state.masv!==undefined){
            if (soTheHopLe === soTheKT) {
                if (this.props.id && this.props.mssv) {
                    this.props.CapNhatCTLopHocPhan(this.state)
                } else {
                    this.state.ma_lop = this.props.id;
                    this.props.ThemCTLopHocPhan(this.state);
                    initialState.DSLopHocPhan = this.state.DSLopHocPhan;
                    initialState.options = this.state.options;
                    this.setState(initialState);
                }
            }   
            else{ 
                func.ThongBaoMaLoi("Bạn phải nhập đúng thông tin ") 
            } 
        }else{ 
            func.ThongBaoMaLoi("Bạn phải nhập đủ thông tin ") 
        } 
    }
    Thoat() {
        this.props.history.push(`/lophocphan/${this.props.id}/danhsach`);
    }
    render() { 
        console.log(this.props.id, this.props.mssv)
        var { masv, diem, masv} = this.state;

        return (
            <div id="body-content">
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-md-12">
                            <div className="content-title">
                                <h1 className="text-center h1-title-style">{this.props.mssv?"Cập Nhật Điểm Sinh Viên":"Thêm Sinh Viên Vào Lớp Học Phần"}</h1>
                                <span className="kitu-dacbiet">&#9884;</span>
                            </div>
                        </div>
                        <div className="col-md-12 text-center">
                            <div className="form-content">
                                <div className="row">  
                                    <div className="col-lg-5 m-auto col-md-12">
                                        <div className='form-group'>
                                            <label >Mã Số Sinh Viên</label> 
                                            <Select  
                                            name="masv"
                                                value={this.props.mssv?({label: this.props.mssv, value: this.props.mssv}):this.state.selectedOption}
                                                onChange={this.handleChange}
                                                options={this.state.options}  
                                            />
                                        </div>
                                    </div> 
                                    <div className="col-lg-5 m-auto col-md-12">
                                        <div className='form-group'>
                                            <label >Điểm:</label>
                                            <input
                                                name="diem" type="text"
                                                className="input-nhap--style"
                                                value={diem}
                                                maxLength="4"
                                                onChange={this.onChange}
                                                onKeyUp={func.KiemTraDuLieuNhapLaDiem}
                                                onFocus={func.KiemTraNhapDuLieu}  
                                            />
                                            <p className="mess-err none" id="diem">Dữ liệu nhập phải là số và theo định dạng 0.00 hoặc 0.0</p>
                                        </div>
                                    </div>  
                                    </div> 
                                    <div className="col-lg-12 col-md-12">
                                        <button className='button-default--style button-thaotac--style' type="button" onClick={this.ThemCTLopHocPhan}>{this.props.id&&this.props.mssv?"Cập Nhật":"Thêm"}</button>
                                        <button className='button-default--style button-thoat--style' type="button" onClick={this.Thoat}>Thoát</button>
                                    </div> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
} 
const mapStateToProps=state=>({ 
    CTCuaChiTietLopHocPhan: state.LayCTCuaChiTietLopHocPhan.CTCuaChiTietLopHocPhan,
    DSSinhVienChoOption: state.LayDSSinhVien.DSSinhVienChoOption,
    status: state.LayDSCuaChiTietLopHocPhan.status, 
    a:console.log(state.LayDSCuaChiTietLopHocPhan.status)
});
themchitietlophocphan.propTypes = {
    ThemCTLopHocPhan: propTypes.func.isRequired, 
    ChiTietCTLopHocPhanTheoMaCTLopHocPhan: propTypes.func.isRequired, 
    CapNhatCTLopHocPhan: propTypes.func.isRequired,
    DanhSachSinhVienChoOption:  propTypes.func.isRequired,
}
export default connect(mapStateToProps, {DanhSachSinhVienChoOption, ThemCTLopHocPhan, ChiTietCTLopHocPhanTheoMaCTLopHocPhan, CapNhatCTLopHocPhan })(withRouter(themchitietlophocphan));
