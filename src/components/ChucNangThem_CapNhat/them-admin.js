import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { connect } from 'react-redux';
import propTypes from 'prop-types';
import { ChiTietAdminTheoMaAdmin, ThemAdmin, CapNhatAdmin } from '../../actions/admin'
import { withRouter } from 'react-router';
import * as func from '../../constants/FunctionsKTDuLieu_ThongBao';   
import LoadingBar from 'react-redux-loading-bar';
import Swal from 'sweetalert2';  
import $ from 'jquery';
import firebase from "firebase/app";
import "firebase/messaging";
const config = {
    apiKey: "AIzaSyBWC0lUSDZxwZn5x5jIRpLfXxtDImufUL0",
    authDomain: "appckc-4105e.firebaseapp.com",
    databaseURL: "https://appckc-4105e.firebaseio.com",
    projectId: "appckc-4105e",
    storageBucket: "appckc-4105e.appspot.com",
    messagingSenderId: "966589584312"
};

export const initFirebase = () => {
    firebase.initializeApp(config);
}; 
let initialState = {
    id: '',
    ma_admin: '',
    ten_dang_nhap: '',
    mat_khau: '',
    hinh_dai_dien: '',
    ho_ten: '',
    email: '',
    sdt: '',
    ngay_cap_nhat: '',
    ngay_tao: '',
    hinh_anh: '',
    dachonhinh: false, 
    ktDieuKien: 0,
    token:'',
} 
class themadmin extends Component {
    constructor(props) {
        super(props);
        this.state = initialState;
        this.ThemAdmin = this.ThemAdmin.bind(this);
        this.Thoat = this.Thoat.bind(this)  
    }
    permissionToReceiveNotification = async () => { 
        const messaging = firebase.messaging();
        await messaging.requestPermission();
        var token = await messaging.getToken();
        this.setState({ token: token })  
    }
    componentWillMount() { 
        this.permissionToReceiveNotification();
        if (this.props.id !== undefined)
            this.props.ChiTietAdminTheoMaAdmin(this.props.id);
    } 
    async componentWillReceiveProps(nextProps) {
        if (nextProps.CTAdmin !== undefined)
            this.setState({
                id: nextProps.CTAdmin.id,
                ma_admin: nextProps.CTAdmin.ma_admin,
                ten_dang_nhap: nextProps.CTAdmin.ten_dang_nhap,
                mat_khau: nextProps.CTAdmin.mat_khau,
                hinh_dai_dien: nextProps.CTAdmin.hinh_dai_dien,
                ho_ten: nextProps.CTAdmin.ho_ten,
                sdt: nextProps.CTAdmin.sdt,
                email: nextProps.CTAdmin.email,
                ngay_cap_nhat: nextProps.CTAdmin.ngay_cap_nhat,
                ngay_tao: nextProps.CTAdmin.ngay_tao
            }); 
        if (nextProps.status === "Thành Công") {
            await Swal.fire({
                type: 'success',
                title: 'Thành Công!',
                showConfirmButton: true,
                confirmButtonText: 'Đồng ý'
            })
            this.props.history.push("/admin")
        }
    }
    onChange = (e) => { 
        var target = e.target;
        var name = target.name; 
        this.setState({
            [name]: target.value,
        });   
    }
    fileHinhAnh = (e) => {
        initialState.hinh_dai_dien = e.target.files[0];
        // this.state.hinh_anh
        if (this.props.id) {
            this.setState({ hinh_anh: initialState.hinh_dai_dien });
        }
        if (this.state.hinh_anh !== this.state.hinh_dai_dien) {
            this.setState({ dachonhinh: true })
        }
        this.setState({ hinh_dai_dien: initialState.hinh_dai_dien })
    }
    ThemAdmin = (e) => {  
        var soTheKT = $("p.mess-err").length; 
        var soTheHopLe = $("p.mess-err.none").length;
        if(this.state.ten_dang_nhap.trim() !=="" && this.state.ho_ten.trim() !== "" && this.state.sdt.trim()!== "" && this.state.email.trim() !== ""){
            if(soTheHopLe  === soTheKT){
                if (this.props.id === undefined) {
                
                    this.props.ThemAdmin(this.state);
                    this.setState(initialState);
                }
                else {  
                    this.props.CapNhatAdmin(this.state);
                }   
            }
            else{
                func.ThongBaoMaLoi("Bạn phải nhập thông tin hợp lệ")
            } 
        }
        else{ 
            func.ThongBaoMaLoi("Bạn phải nhập đủ thông tin ") 
        } 
    }
    Thoat() {  
        this.props.history.push('/admin');
    }   
    render() { 
        console.log(this.state.token)
        var { ho_ten, ten_dang_nhap, mat_khau, email, sdt } = this.state; 
        return (
            <div id="body-content">
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-md-12">
                            <div className="content-title">
                                <h1 className="text-center h1-title-style">{this.props.id ? "Cập Nhật Admin" : "Thêm Admin"}</h1>
                                <span className="kitu-dacbiet">&#9884;</span>
                            </div>
                        </div>
                        <div className="row">
                            <LoadingBar/>
                        </div>

                        <div className="col-md-12 text-center">
                            <div className="form-content">
                                <div className="row">
                                    <div className="col-lg-5 m-auto col-md-12">
                                        <div className='form-group' >
                                            <label >Tên Đăng Nhập:</label>
                                            <input
                                                name="ten_dang_nhap" type="text"
                                                className="input-nhap--style"
                                                value={ten_dang_nhap}
                                                onChange={this.onChange}
                                                onKeyUp={func.KiemTraNhapDuLieu} 
                                                onFocus={func.KiemTraNhapDuLieu}
                                                // onKeyUp={func.KiemTraNhapDuLieu("ten_dang_nhap", ten_dang_nhap)} méo gọi cũng chạy
                                                // onKeyUp={()=>func.KiemTraNhapDuLieu("ten_dang_nhap", ten_dang_nhap)} gọi hàm mới chạy
                                            />
                                            <p  className="mess-err none" id="ten_dang_nhap">Tên đăng nhập không được để trống</p>
                                        </div>
                                    </div> 
                                    <div className="col-lg-5 m-auto  col-md-12">
                                        <div className='form-group'>
                                            <label>Họ Tên:</label>
                                            <input
                                                name="ho_ten" type="text"
                                                className="input-nhap--style"
                                                value={ho_ten}
                                                onChange={this.onChange}
                                                onKeyUp={func.KiemTraNhapDuLieu} 
                                                onFocus={func.KiemTraNhapDuLieu}  
                                            />
                                            <p className="mess-err none" id="ho_ten">Họ tên không được để trống</p>
                                        </div>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-lg-5 m-auto  col-md-12">
                                        <div className='form-group'>
                                            <label>Hình Đại Diện:</label>
                                            <input
                                                name="hinh_dai_dien" type="file"
                                                className="input-nhap--style"
                                                // value={hinh_dai_dien}
                                                onChange={this.fileHinhAnh}
                                            /> 
                                        </div>
                                    </div>
                                    <div className="col-lg-5 m-auto  col-md-12">
                                        <div className='form-group'>
                                            <label>Email:</label>
                                            <input
                                                name="email" type="text"
                                                className="input-nhap--style"
                                                value={email}
                                                onChange={this.onChange}
                                                onKeyUp={func.KiemTraDuLieuNhapLaEmail} 
                                                onFocus={func.KiemTraNhapDuLieu}
                                            />
                                            <p className="mess-err none" id="email">Email phải nhập đúng định dạng.</p>
                                        </div>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-lg-5 m-auto  col-md-12">
                                        <div className='form-group'>
                                            <label>Số Điện Thoại:</label>
                                            <input
                                                name="sdt" type='text'
                                                className="input-nhap--style"
                                                value={sdt}
                                                maxLength="11"
                                                onChange={this.onChange}
                                                onKeyUp={func.KiemTraDuLieuNhapLaSoDT} 
                                                onFocus={func.KiemTraNhapDuLieu}
                                            />
                                            <p className="mess-err none" id="sdt">Số điện thoại không hợp lệ</p>
                                        </div>
                                    </div>
                                    <div className="col-lg-5 m-auto  col-md-12">
                                        
                                        <div className={this.props.id ? "none" : 'form-group'}>
                                            <label>Mật Khẩu:</label>
                                            <input 
                                                name="mat_khau" type="password"
                                                className="input-nhap--style"
                                                value={mat_khau}
                                                onChange={this.onChange}
                                                onKeyUp={func.KiemTraDuLieuNhapLaMatKhau}   
                                                onFocus={func.KiemTraNhapDuLieu}
                                            />
                                            <p className="mess-err none" id="mat_khau">Mật khẩu phải trên 7 ký tự và không được để trống</p>
                                        </div>
                                    </div>
                                    <div className="col-lg-12 col-md-12">
                                        <button className='button-default--style button-thaotac--style' type="button" onClick={this.ThemAdmin}>{this.props.id === undefined ? "Thêm" : "Lưu"}</button>
                                        <button className='button-default--style button-thoat--style' type="button" onClick={this.Thoat}>Thoát</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
const mapStateToProps = state => ({
    status: state.LayCTAdmin.status,
    CTAdmin: state.LayCTAdmin.CTAdmin,
});
themadmin.propTypes = {
    ThemAdmin: propTypes.func.isRequired,
    CapNhatAdmin: propTypes.func.isRequired,
    ChiTietAdminTheoMaAdmin: propTypes.func.isRequired,
}
export default connect(mapStateToProps, { ThemAdmin, CapNhatAdmin, ChiTietAdminTheoMaAdmin })(withRouter(themadmin));
