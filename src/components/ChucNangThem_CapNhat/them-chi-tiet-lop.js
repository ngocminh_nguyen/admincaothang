import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { connect } from 'react-redux';
import propTypes from 'prop-types';
import { ThemCTLop} from '../../actions/chi_tiet_lop'
import { withRouter } from 'react-router';
import { DanhSachLop } from '../../actions/lop';
import {DanhSachSinhVienChoOption} from '../../actions/sinh_vien';
import Swal from 'sweetalert2';   
import Select from 'react-select';
const initialState = {
    id: '',
    ma_lop: '',
    masv: '',
    ngay_cap_nhat: '',
    ngay_tao: '',
    DSLop: [],
    DSSinhVienChoOption:[],
    options:[]
}  
class themchitietlop extends Component {
    constructor(props) {
        super(props);
        this.state = {initialState, selectedOption: "0306161002"};
        this.ThemCTLop = this.ThemCTLop.bind(this);
        this.Thoat = this.Thoat.bind(this);
        
    }
    componentWillMount() { 
        this.props.DanhSachSinhVienChoOption();
    }
    async componentWillReceiveProps(nextProps) {  
        this.setState({DSSinhVienChoOption:nextProps.DSSinhVienChoOption}) 
        var opt = [];
        nextProps.DSSinhVienChoOption.map(function (item, index) {
            return (
                opt.push({"label":item.mssv+": "+item.ho_ten,"value":item.mssv})
                ) 
            }) 
        this.setState({options:opt})
    }
    onChange = (e) => {
        var target = e.target;
        var name = target.name;
        this.setState({
            [name]: target.value,
        });
    }
    handleChange = selectedOption => {
        this.setState({selectedOption }); 
        this.setState({masv: selectedOption.value }); 
    };
    ThemCTLop = (e) => {
        this.state.ma_lop = this.props.id;
        this.props.ThemCTLop(this.state);  
        initialState.options = this.state.options;
        this.setState(initialState);
    }
    Thoat() {
        // var history = this.props.id;
        this.props.history.push(`/lop/${this.props.id}/danhsach`);
    }
    render() {
        console.log(this.state.DSSinhVienChoOption)
        var { masv } = this.state;
        return (
            <div id="body-content">
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-md-12">
                            <div className="content-title">
                                <h1 className="text-center h1-title-style">Thêm Sinh Viên Vào Lớp </h1>
                                <span className="kitu-dacbiet">&#9884;</span>
                            </div>
                        </div>
                        <div className="col-md-12 text-center">
                            <div className="form-content">
                                <div className="row"> 
                                    <div className="col-lg-5 m-auto col-md-12">
                                        <div className='form-group'>
                                            <label >Mã Số Sinh Viên</label>
                                            {/* <input
                                                name="masv" type="text"
                                                className="input-nhap--style"
                                                value={masv}
                                                onChange={this.onChange}
                                            /> */}
                                            <Select  
                                                value={this.state.selectedOption}
                                                onChange={this.handleChange}
                                                options={this.state.options} 
                                            />
                                        </div>
                                    </div> 
                                    <div className="col-lg-12 col-md-12">
                                        <button className='button-default--style button-thaotac--style' type="button" onClick={this.ThemCTLop}>Thêm</button>
                                        <button className='button-default--style button-thoat--style' type="button" onClick={this.Thoat}>Thoát</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
const mapStateToProps = state => ({  
    status: state.LayCTCuaChiTietLop.status,
    DSSinhVienChoOption: state.LayDSSinhVien.DSSinhVienChoOption
});
themchitietlop.propTypes = {
    ThemCTLop: propTypes.func.isRequired, 
    DanhSachSinhVienChoOption: propTypes.func.isRequired,
}
export default connect(mapStateToProps, { ThemCTLop, DanhSachSinhVienChoOption})(withRouter(themchitietlop));
