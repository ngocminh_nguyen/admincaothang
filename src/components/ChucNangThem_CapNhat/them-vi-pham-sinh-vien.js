import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { connect } from 'react-redux';
import propTypes from 'prop-types'; 
import {ThemViPhamSinhVien,  DanhSachSinhVienChoOption} from './../../actions/sinh_vien';
import { withRouter } from 'react-router';
import * as func from '../../constants/FunctionsKTDuLieu_ThongBao'; 
import Calendar from 'react-calendar';
import moment from 'moment';
import Select from 'react-select';
import Swal from 'sweetalert2';  
import $ from "jquery"
const initialState = {
    id: '',
    mssv: '',
    thong_bao : '', 
    options:[],
    thoi_gian_vi_pham :new Date(), 
}
class themviphamsv extends Component {
    constructor(props) {
        super(props);
        this.state ={initialState, selectedOption: ''};
        this.Thoat = this.Thoat.bind(this)
    }
    componentWillMount() {
        this.props.DanhSachSinhVienChoOption()
    }
    async componentWillReceiveProps(nextProps) { 
        var opt = [];
        nextProps.DSSinhVien.map(function (item, index) {
            return (
                opt.push({ "label": item.mssv + ": " + item.ho_ten, "value": item.mssv })
            )
        })
        this.setState({ options: opt })
    }
    onChange = (e) => {
        var target = e.target;
        var name = target.name;
        this.setState({
            [name]: target.value
        });
    }
    handleChange = selectedOption => {
        this.setState({selectedOption }); 
        this.setState({mssv: selectedOption.value }); 
    }; 
    ThemViPhamSinhVien = (e) => {  
        var soTheKT = $("p.mess-err").length; 
        var soTheHopLe = $("p.mess-err.none").length;
        if(this.state.mssv !==undefined && this.state.thong_bao !==undefined){
            if(soTheHopLe  === soTheKT){
                if (this.props.id === undefined) {
                    this.state.thoi_gian_vi_pham=moment(this.state.thoi_gian_vi_pham).format("YYYY-MM-DD") 
                    this.props.ThemViPhamSinhVien(this.state.mssv, this.state.thong_bao, this.state.thoi_gian_vi_pham);
                    initialState.options = this.state.options; 
                    this.setState(initialState);
                } 
            }
            else{
                func.ThongBaoMaLoi("Bạn phải nhập thông tin hợp lệ")
            } 
        }
        else{ 
            func.ThongBaoMaLoi("Bạn phải nhập đủ thông tin ") 
        } 
    }
    onChangeDate=(e)=>{ 
        this.setState({thoi_gian_vi_pham:e}) 
    }
    Thoat() {
        this.props.history.push('/sinhvien');
    }
    render() {
        var {  thong_bao , mssv, thoi_gian_vi_pham } = this.state; 
        return (
            <div id="body-content">
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-md-12">
                            <div className="content-title">
                                <h1 className="text-center h1-title-style"> Thêm Vi Phạm Sinh Viên</h1>
                                <span className="kitu-dacbiet">&#9884;</span>
                            </div>
                        </div>
                        <div className="col-md-12 text-center">
                            <div className="form-content">
                                <div className="row"> 
                                    <div className="col-lg-5 m-auto col-md-12">
                                        <div className='form-group'>
                                            <label style={{ margin: 10 }}>Mã Sinh Viên:</label>
                                            {/* <input
                                                style={{ margin: 10, borderRadius: 3 }}
                                                name="mssv" type="text"
                                                className="input-nhap--style"
                                                value={mssv}
                                                onChange={this.onChange}
                                                onKeyUp={func.KiemTraNhapDuLieu} 
                                                onFocus={func.KiemTraNhapDuLieu}  
                                            /> */}
                                            <Select  
                                            name="mssv"
                                                value={this.state.selectedOption}
                                                onChange={this.handleChange}
                                                options={this.state.options}  
                                            /> 
                                        </div>
                                    </div>
                                    <div className="col-lg-5 m-auto col-md-12">
                                        <div className='form-group'>
                                            <label style={{ margin: 10 }}>Thông Báo:</label>
                                            <input
                                                style={{ margin: 10, borderRadius: 3 }}
                                                name="thong_bao" type="text"
                                                className="input-nhap--style"
                                                value={thong_bao}
                                                onChange={this.onChange}
                                                onKeyUp={func.KiemTraNhapDuLieu} 
                                                onFocus={func.KiemTraNhapDuLieu}  
                                            />
                                            <p className="mess-err none" id="thong_bao">Mã sinh viên không được để trống</p>
                                        </div>
                                    </div>
                                    <div className="col-lg-5 m-auto col-md-12">
                                        <div className='form-group'>
                                            <label >Ngày Vi Phạm:</label> 
                                            <div>
                                                <Calendar
                                                onChange={this.onChangeDate}
                                                value={moment(thoi_gian_vi_pham).toDate()}
                                                />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-lg-12 col-md-12">
                                    <button className='button-default--style button-thaotac--style' type="button" onClick={this.ThemViPhamSinhVien}>{this.props.id === undefined ? "Thêm" : "Lưu"}</button>
                                    <button className='button-default--style button-thoat--style' type="button" onClick={this.Thoat}>Thoát</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
const mapStateToProps = state => ({
    DSSinhVien: state.LayDSSinhVien.DSSinhVienChoOption,
});
themviphamsv.propTypes = {
    ThemViPhamSinhVien: propTypes.func.isRequired,
    DanhSachSinhVienChoOption: propTypes.func.isRequired,
}
export default connect(mapStateToProps, {ThemViPhamSinhVien, DanhSachSinhVienChoOption})(withRouter(themviphamsv)); 
