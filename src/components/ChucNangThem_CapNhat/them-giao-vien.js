import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { connect } from 'react-redux';
import propTypes from 'prop-types';
import moment from 'moment'; 
import {ChiTietGiaoVienTheoMAGV, ThemGiaoVien, CapNhatGiaoVien} from '../../actions/giao_vien'
import { withRouter } from 'react-router';
import * as func from '../../constants/FunctionsKTDuLieu_ThongBao'; 
import { DanhSachKhoa } from '../../actions/khoa';
import Calendar from 'react-calendar';
import Swal from 'sweetalert2';  
import * as $ from 'jquery'
const initialState = { 
    id: '',
    ma_giao_vien: '',
    ho_ten: '',
    rdGioiTinh: '',
    ma_khoa: '',
    ngay_sinh: new Date(2019,0,1),
    dien_thoai: '',
    email: '',
    dia_chi: '',
    ngay_cap_nhat:'',
    ngay_tao:'',
    DSKhoa: [], 
}
class themgiaovien extends Component {
    constructor(props) {
        super(props);
        this.state = initialState;
        this.ThemGiaoVien = this.ThemGiaoVien.bind(this);
        this.Thoat = this.Thoat.bind(this)
    }
    componentWillMount(){
        if(this.props.id)
        this.props.ChiTietGiaoVienTheoMAGV(this.props.id); 
        this.props.DanhSachKhoa();
    }
    async componentWillReceiveProps(nextProps) {
        var mangtam=[];
        if (nextProps.DSKhoa !== undefined && nextProps.DSKhoa.length > 0) {
            nextProps.DSKhoa.map((item, key)=>{
                if(item.xoa==0){
                    mangtam.push(item);
                }
            })
            this.setState({ DSKhoa: mangtam, ma_khoa: mangtam[0].ma_khoa }) 
        }  
        if(nextProps.CTGiaoVien!== undefined && Object.keys(nextProps.CTGiaoVien).length > 0)
            this.setState({
                id: nextProps.CTGiaoVien.id,
                ma_khoa:  nextProps.CTGiaoVien.ma_khoa,
                ma_giao_vien: nextProps.CTGiaoVien.ma_giao_vien,
                ho_ten: nextProps.CTGiaoVien.ho_ten,
                rdGioiTinh: nextProps.CTGiaoVien.gioi_tinh,
                ngay_sinh: nextProps.CTGiaoVien.ngay_sinh,
                dien_thoai: nextProps.CTGiaoVien.dien_thoai,
                email: nextProps.CTGiaoVien.email,
                dia_chi: nextProps.CTGiaoVien.dia_chi,
                ngay_cap_nhat: nextProps.CTGiaoVien.ngay_cap_nhat,
                ngay_tao: nextProps.CTGiaoVien.ngay_tao
            });
        this.setState({ status: nextProps.status })
        if (nextProps.status === "Thành Công") {
            await Swal.fire({
                type: 'success',
                title: 'Thành Công!',
                showConfirmButton: true,
                confirmButtonText: 'Đồng ý'
            })
            this.props.history.push("/giaovien")
        } 
    }
    onChange = (e) => {
        var target = e.target;
        var name = target.name;
        this.setState({
            [name]: target.value,
        });
    }
    ThemGiaoVien = (e) => {   
        var soTheKT = $("p.mess-err").length; 
        var soTheHopLe = $("p.mess-err.none").length;
        if(this.state.ho_ten !=="" && this.state.email !=="" && this.state.ngay_sinh !=="" && this.state.rdGioiTinh !=="" && this.state.dien_thoai !=="" &&  this.state.dia_chi !==""){
            if(soTheHopLe  === soTheKT){
                if (this.props.id === undefined) {
                    this.state.ngay_sinh=moment(this.state.ngay_sinh).format("YYYY-MM-DD")
                    this.props.ThemGiaoVien(this.state);
                    initialState.ma_khoa = this.state.ma_khoa;
                    initialState.DSKhoa = this.state.DSKhoa;
                    this.setState(initialState);
                    this.setState(initialState);
                }
                else {  
                    this.state.ngay_sinh=moment(this.state.ngay_sinh).format("YYYY-MM-DD")
                    this.props.CapNhatGiaoVien(this.state);  
                }   
            }
            else{
                func.ThongBaoMaLoi("Bạn phải nhập thông tin hợp lệ")
            } 
        }
        else{ 
            func.ThongBaoMaLoi("Bạn phải nhập đủ thông tin ") 
        } 
    }
    Thoat() {
        // var history = this.props.id;
        this.props.history.push('/giaovien');
    }
    onChangeDate=(e)=>{ 
        this.setState({ngay_sinh:e}) 
    }
    render() { 
        var { ho_ten , ngay_sinh , dien_thoai , email , dia_chi, ma_khoa  } = this.state;
        return (
            <div id="body-content">
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-md-12"> 
                            <div className="content-title">
                                <h1 className="text-center h1-title-style">{this.props.id?"Cập Nhật Giáo Viên ":"Thêm Giáo Viên"}</h1>
                                <span className="kitu-dacbiet">&#9884;</span>
                            </div>
                        </div>
                        <div className="col-md-12 text-center">
                            <div className="form-content">
                                <div className="row"> 
                                    
                                    <div className="col-lg-5 m-auto col-md-12">
                                        <div className='form-group'>
                                            <label >Họ Tên</label>
                                            <input 
                                                style={{textTransform:"capitalize"}}
                                                name="ho_ten" type="text"
                                                className="input-nhap--style"
                                                value={ho_ten}
                                                onChange={this.onChange}
                                                onKeyUp={func.KiemTraNhapDuLieu} 
                                                onFocus={func.KiemTraNhapDuLieu}  
                                            />
                                            <p className="mess-err none" id="ho_ten">Họ tên không được để trống</p>
                                        </div>
                                    </div>
                                    <div className="col-lg-5 m-auto col-md-12">
                                        <div className='form-group'>
                                            <label  >Điện Thoại:</label>
                                            <input 
                                                name="dien_thoai" type="text"
                                                className="input-nhap--style"
                                                maxLength="11"
                                                value={dien_thoai}
                                                onChange={this.onChange}
                                                onKeyUp={func.KiemTraDuLieuNhapLaSoDT} 
                                                onFocus={func.KiemTraNhapDuLieu}
                                            />
                                            <p className="mess-err none" id="dien_thoai">Số điện thoại không hợp lệ</p>
                                        </div>
                                    </div>
                                    </div>
                                    <div className="row"> 
                                    <div className="col-lg-5 m-auto col-md-12">
                                        <div className='form-group'>
                                            <label >Email:</label>
                                            <input
                                                name="email" type="text"
                                                className="input-nhap--style"
                                                value={email}
                                                onChange={this.onChange}
                                                onKeyUp={func.KiemTraDuLieuNhapLaEmail} 
                                                onFocus={func.KiemTraNhapDuLieu}
                                            />
                                            <p className="mess-err none" id="email">Email phải nhập đúng định dạng và không được để trống</p>
                                        </div>
                                    </div>
                                    <div className="col-lg-5 m-auto col-md-12">
                                        <div className='form-group'>
                                            <label>Địa Chỉ:</label>
                                            <input
                                                name="dia_chi" type="text"
                                                className="input-nhap--style"
                                                value={dia_chi}
                                                onChange={this.onChange}
                                                onKeyUp={func.KiemTraNhapDuLieu} 
                                                onFocus={func.KiemTraNhapDuLieu}
                                            />
                                            <p className="mess-err none" id="dia_chi">Địa chỉ không được để trống</p>
                                        </div>
                                    </div>
                                    </div>
                                    <div className="row"> 
                                    <div className="col-lg-5 m-auto col-md-12">
                                        <div className='form-group'>
                                            <label className="label-input--style">Khoa:</label>
                                            <select name="ma_khoa"
                                                value={this.state.ma_khoa}
                                                onChange={this.onChange}
                                                className="input-nhap--style"

                                            >
                                                {/* <option defaultChecked>Mời bạn chọn khoa</option> */}
                                                {this.state.DSKhoa.map((item, key) => <option key={key} value={item.ma_khoa} className="input-nhap--style">{item.ten_khoa}</option>
                                                )}
                                            </select>
                                        </div>
                                    </div> 
                                    <div className="col-lg-5 m-auto col-md-12">
                                        <div className='form-group'>
                                            <label style={{ margin: 10 }}>Giới Tính:</label>
                                            <input
                                                style={{ margin: 10 }}
                                                name="rdGioiTinh" type='radio'
                                                checked={this.state.rdGioiTinh==='1'}
                                                value='1'
                                                onChange={this.onChange}
                                            />Nam
                                            <input
                                                    style={{ margin: 10 }}
                                                    name="rdGioiTinh" type='radio'
                                                    checked={this.state.rdGioiTinh==='0'}
                                                    value='0'
                                                    onChange={this.onChange}
                                                />Nữ
                                    </div> 
                                    </div>
                                    <div className="col-lg-5 m-auto col-md-12">
                                        <div className='form-group'>
                                            <label >Ngày Sinh</label>
                                            <div>
                                                <Calendar
                                                    onChange={this.onChangeDate}
                                                    value={moment(ngay_sinh).toDate()}
                                                />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-lg-12 col-md-12">
                                        <button className='button-default--style button-thaotac--style' type="button" onClick={this.ThemGiaoVien}>{this.props.id===undefined?"Thêm":"Lưu"}</button>
                                        <button className='button-default--style button-thoat--style' type="button" onClick={this.Thoat}>Thoát</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
} 
const mapStateToProps=state=>({
    CTGiaoVien: state.LayCTGiaoVien.CTGiaoVien, 
    status: state.LayCTGiaoVien.status,
    DSKhoa: state.LayDSKhoa.DSKhoa,
});
themgiaovien.propTypes = {
    ThemGiaoVien: propTypes.func.isRequired,
    CapNhatGiaoVien: propTypes.func.isRequired,
    ChiTietGiaoVienTheoMAGV: propTypes.func.isRequired,
    DanhSachKhoa: propTypes.func.isRequired,
}
export default connect(mapStateToProps, { ThemGiaoVien, CapNhatGiaoVien, ChiTietGiaoVienTheoMAGV, DanhSachKhoa })(withRouter(themgiaovien));
