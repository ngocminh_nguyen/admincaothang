import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { connect } from 'react-redux';
import propTypes from 'prop-types';
import moment from 'moment';
import { ChiTietSinhVienTheoMaSV, ThemSinhVien, CapNhatSinhVien, ThemSinhVienTuFileNen, ThemHinhSinhVien, UpLoadFileNen } from '../../actions/sinh_vien';
import { DanhSachPhuHuynhChoOption } from '../../actions/phu_huynh';
import { DanhSachLop } from '../../actions/lop'
import { withRouter } from 'react-router';
import { DanhSachChuyenNganh } from '../../actions/chuyen_nganh';
import * as func from '../../constants/FunctionsKTDuLieu_ThongBao';
import Dropzone from 'react-dropzone'
import $ from 'jquery';
import Swal from 'sweetalert2';
import Select from 'react-select';
import { MyDropzone } from './doc-file-nen';
import Calendar from 'react-calendar';
let initialState = {
    id: '',
    mssv: '',
    ma_lop: '',
    bac_dao_tao: '1',
    ma_chuyen_nganh: '',
    khoa: '',
    loai_hinh_dao_tao: '1',
    ho_ten: '',
    ngay_sinh: new Date(2019, 0, 1),
    rdGioiTinh: '',
    hinh_dai_dien: '',
    noi_sinh: '',
    ngay_cap_nhat: '',
    ngay_tao: '',
    hinh_anh: '',
    ma_phu_huynh: '',
    so_dien_thoai: '',
    DSChuyenNganh: [],
    dachonhinh: false,
    rdbThemSi_Le: '',
    loHinhAnh: [],
    file: '',
    status: '',
    accept: 0,
    DSLop: [],
    options: [],
}

class themsinhvien extends Component {

    constructor(props) {

        super(props);
        this.state = initialState;
        this.ThemSinhVien = this.ThemSinhVien.bind(this);
        this.Thoat = this.Thoat.bind(this)
    }
    componentWillMount() {
        if (this.props.id) {
            this.props.ChiTietSinhVienTheoMaSV(this.props.id);
        }
        this.props.DanhSachChuyenNganh();
        this.props.DanhSachPhuHuynhChoOption();
        this.props.DanhSachLop();
    }
    async  componentWillReceiveProps(nextProps) {
        var mangtam = [];
        if (nextProps.DSChuyenNganh !== undefined && nextProps.DSChuyenNganh.length > 0) {
            nextProps.DSChuyenNganh.map((item, key) => {
                if (item.xoa == 0) {
                    mangtam.push(item);
                }
            })
            this.setState({ DSChuyenNganh: mangtam, ma_chuyen_nganh: mangtam[0].ma_so })
        }
        var mangDSLop = [];
        if (nextProps.DSLop !== undefined && nextProps.DSLop.length > 0) {
            nextProps.DSLop.map((item, key) => {
                if (item.xoa == 0) {
                    mangDSLop.push(item);
                }
            })
            this.setState({ DSLop: mangDSLop, ma_lop: mangDSLop[0].ma_lop })
        }
        if (nextProps.CTSinhVien !== undefined && Object.keys(nextProps.CTSinhVien).length > 0)
            this.setState({
                id: nextProps.CTSinhVien.id,
                mssv: nextProps.CTSinhVien.mssv,
                ho_ten: nextProps.CTSinhVien.ho_ten,
                khoa: nextProps.CTSinhVien.khoa,
                ngay_sinh: nextProps.CTSinhVien.ngay_sinh,
                rdGioiTinh: nextProps.CTSinhVien.gioi_tinh,
                hinh_dai_dien: nextProps.CTSinhVien.hinh_dai_dien,
                noi_sinh: nextProps.CTSinhVien.noi_sinh,
                ma_phu_huynh: nextProps.CTSinhVien.ma_phu_huynh,
                ngay_cap_nhat: nextProps.CTSinhVien.ngay_cap_nhat,
                so_dien_thoai: nextProps.CTSinhVien.so_dien_thoai,
                ngay_tao: nextProps.CTSinhVien.ngay_tao,
                selectedOption: nextProps.CTSinhVien.ma_phu_huynh

            });

        this.setState({ status: nextProps.status })
        if (nextProps.status === "Thành Công") {
            await Swal.fire({
                type: 'success',
                title: 'Thành Công!',
                showConfirmButton: true,
                confirmButtonText: 'Đồng ý'
            })
            this.props.history.push("/sinhvien")
        }
        var opt = [];
        console.log(nextProps.DSPhuHuynh)
        if(nextProps.DSPhuHuynh !== undefined && Object.keys(nextProps.DSPhuHuynh).length > 0){
            nextProps.DSPhuHuynh.map(function (item, index) {
                return (
                    opt.push({ "label": item.ho_ten_cha + ": " + item.so_dien_thoai, "value": item.ma_phu_huynh  })
                )
            })
            this.setState({ options: opt })
        }
    }

    onChange = (e) => {
        var target = e.target;
        var name = target.name;
        this.setState({
            [name]: target.value,
        });
    }
    handleChange = selectedOption => {
        this.setState({selectedOption }); 
        this.setState({ma_phu_huynh: selectedOption.value }); 
    }; 
    onDropSinhVien = (acceptedFiles) => {
        acceptedFiles.map((file, key) => {
            this.props.UpLoadFileNen(file)
            this.props.ThemSinhVienTuFileNen(file.name, this.state.ma_lop)
        })
    }
    onDropHinhAnh = (acceptedFiles) => {
        acceptedFiles.map((file, key) => {
            this.props.ThemHinhSinhVien(file)
        })
    }
    fileHinhAnh = (e) => {
        initialState.hinh_dai_dien = e.target.files[0];
        if (this.props.id) {
            this.setState({ hinh_anh: initialState.hinh_dai_dien });
        }
        if (this.state.hinh_anh !== this.state.hinh_dai_dien) {
            this.setState({ dachonhinh: true })
        }
        this.setState({ hinh_dai_dien: initialState.hinh_dai_dien })
    }
    fileExcel = (e) => {
        this.state.file = e.target.files[0];
    }
    ThemSinhVien = (e) => {

        var soTheKT = $("p.mess-err").length;
        var soTheHopLe = $("p.mess-err.none").length;
        if (this.state.khoa !== "" && this.state.ho_ten !== "" && this.state.so_dien_thoai !== "" && this.state.noi_sinh !== "") {
            if (soTheHopLe === soTheKT) {
                if (this.props.id === undefined) {
                    this.state.ngay_sinh = moment(this.state.ngay_sinh).format("YYYY-MM-DD")
                    console.log(this.state)
                    this.props.ThemSinhVien(this.state);
                    initialState.chuyen_nganh = this.state.chuyen_nganh;
                    initialState.DSChuyenNganh = this.state.DSChuyenNganh;
                    this.setState(initialState);
                }
                else {
                    this.state.ngay_sinh = moment(this.state.ngay_sinh).format("YYYY-MM-DD")
                    this.props.CapNhatSinhVien(this.state);
                }
            }
            else {
                func.ThongBaoMaLoi("Bạn phải nhập thông tin hợp lệ")
            }
        }
        else {
            func.ThongBaoMaLoi("Bạn phải nhập đủ thông tin ")
        }
    }
    Thoat() {
        this.props.history.push('/sinhvien');
    }
    onChangeDate = (e) => {
        this.setState({ ngay_sinh: e })
    }
    ThemLoSinhVien = () => {
        this.props.ThemHinhSinhVien(this.state.loHinhAnh)
    }
    HienThiThemLe = () => {
        $("#menu-themle").addClass("menu-active");
        $(".themLe").removeClass("none");
        $(".themSi").addClass("none");
        $("#menu-themsi").removeClass("menu-active");
    }
    HienThiThemSi = () => {
        $("#menu-themsi").addClass("menu-active");
        $(".themSi").removeClass("none");
        $(".themLe").addClass("none");
        $("#menu-themle").removeClass("menu-active");
    }
    render() {
        var { bac_dao_tao, ma_chuyen_nganh, khoa, loai_hinh_dao_tao, ho_ten, ngay_sinh, noi_sinh, ma_phu_huynh, so_dien_thoai, ma_lop } = this.state;
        return (
            <div id="body-content">
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-md-12">
                            <div className="content-title">
                                <h1 className="text-center h1-title-style">{this.props.id ? "Cập Nhật Sinh Viên" : "Thêm Sinh Viên"}</h1>
                                <span className="kitu-dacbiet">&#9884;</span>
                            </div>
                        </div>
                        <div className="col-md-12 text-center">
                            <div className="form-content">
                                <div className="navTabs">
                                    <div className="">
                                        <span className="menu" id="menu-themsi" onClick={() => this.HienThiThemSi()}>
                                            Thêm Sinh Viên Từ Tập Tin
                                        </span>
                                        <span className="menu  menu-active" id="menu-themle" onClick={() => this.HienThiThemLe()}>
                                            Thêm Sinh Viên
                                        </span>
                                        <div className="themSi none" >
                                            <div className="col-lg-5 m-auto col-md-12">
                                                <div className='form-group'>
                                                    <label >Chọn lớp:</label>
                                                    <select
                                                        name="ma_lop"
                                                        className="input-nhap--style"
                                                        value={ma_lop}
                                                        onChange={this.onChange}
                                                    >
                                                        {this.state.DSLop.map((item, key) =>
                                                            <option key={key} value={item.ma_lop} className="input-nhap--style">{item.ten_lop} ({item.ma_lop})</option>
                                                        )}
                                                    </select>
                                                </div>
                                            </div>
                                            <div className="row m-5">
                                                <div className="col-lg-5 m-auto ">
                                                    <label className="text-center">Chọn files thêm sinh viên:</label>
                                                    <Dropzone onDrop={this.onDropSinhVien}>
                                                        {({ getRootProps, getInputProps }) => (
                                                            <div {...getRootProps()}>
                                                                <input {...getInputProps()} />
                                                                Click me to upload a file!
                                                        </div>
                                                        )}
                                                    </Dropzone>

                                                </div>
                                                <div className="col-lg-5 m-auto ">
                                                    <label className="text-center">Chọn files thêm hình ảnh:</label>
                                                    <Dropzone onDrop={this.onDropHinhAnh}>
                                                        {({ getRootProps, getInputProps }) => (
                                                            <div {...getRootProps()}>
                                                                <input {...getInputProps()} />
                                                                Click me to upload a file!
                                                        </div>
                                                        )}
                                                    </Dropzone>
                                                    {/* <input name="" type="file" onChange={this.fileLoHinhAnh}></input> */}
                                                </div>
                                            </div>
                                            <div className="col-lg-12 col-md-12">
                                                <button className='button-default--style button-thoat--style' type="button" onClick={this.Thoat}>Thoát</button>
                                            </div>
                                        </div>
                                        <div className="themLe ">
                                            <div className="row">
                                                <div className={this.props.id ? "none" : "col-lg-5 m-auto col-md-12"}>
                                                    <div className='form-group'>
                                                        <label  >Bậc Đào Tạo:</label>
                                                        <select
                                                            name="bac_dao_tao"
                                                            className="input-nhap--style"
                                                            value={bac_dao_tao}
                                                            onChange={this.onChange}
                                                        >
                                                            <option value="5">Đại Học</option>
                                                            <option value="4">Cao Đẳng Nghề</option>
                                                            <option value="3">Cao Đẳng</option>
                                                            <option value="2">Trung Cấp Chuyên Nghiệp</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div className={this.props.id ? "none" : "col-lg-5 m-auto col-md-12"}>
                                                    <div className='form-group'>
                                                        <label >Chuyên Ngành:</label>
                                                        <select
                                                            name="ma_chuyen_nganh"
                                                            className="input-nhap--style"
                                                            value={ma_chuyen_nganh}
                                                            onChange={this.onChange}
                                                        >
                                                            {this.state.DSChuyenNganh.map((item, key) =>
                                                                <option key={key} value={item.ma_so} className="input-nhap--style">{item.ten_chuyen_nganh}</option>
                                                            )}
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className={this.props.id ? "none" : "col-lg-5 m-auto col-md-12"}>
                                                    <div className='form-group'>
                                                        <label >Khóa</label>
                                                        <input
                                                            name="khoa" type="text"
                                                            className="input-nhap--style"
                                                            value={khoa}
                                                            onChange={this.onChange}
                                                            maxLength="2"
                                                            onChange={this.onChange}
                                                            onKeyUp={func.KiemTraDuLieuNhapLaSo}
                                                            onFocus={func.KiemTraNhapDuLieu}
                                                        />
                                                        <p className="mess-err none" id="khoa">Khóa phải là số và không được để trống</p>
                                                    </div>
                                                </div>
                                                <div className={this.props.id ? "none" : "col-lg-5 m-auto col-md-12"}>
                                                    <div className='form-group'>
                                                        <label >Loại Hình Đào Tạo:</label>
                                                        <select
                                                            name="loai_hinh_dao_tao"
                                                            className="input-nhap--style"
                                                            value={loai_hinh_dao_tao}
                                                            onChange={this.onChange}
                                                        >
                                                            <option value="1">Chinh Qui</option>
                                                            <option value="2">Liên Thông</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-lg-5 m-auto col-md-12">
                                                    <div className='form-group'>
                                                        <label style={{ margin: 10 }}>Họ Tên:</label>
                                                        <input
                                                            name="ho_ten" type="text"
                                                            className="input-nhap--style"
                                                            value={ho_ten}
                                                            onChange={this.onChange}
                                                            onKeyUp={func.KiemTraNhapDuLieu}
                                                            onFocus={func.KiemTraNhapDuLieu}
                                                        />
                                                        <p className="mess-err none" id="ho_ten">Họ tên không được để trống</p>
                                                    </div>
                                                </div>
                                                <div className="col-lg-5 m-auto col-md-12">
                                                    <div className='form-group'>
                                                        <label >Số Điện Thoại</label>
                                                        <input
                                                            name="so_dien_thoai" type="text"
                                                            className="input-nhap--style"
                                                            maxLength="11"
                                                            value={so_dien_thoai}
                                                            onChange={this.onChange}
                                                            onKeyUp={func.KiemTraDuLieuNhapLaSoDT}
                                                            onFocus={func.KiemTraNhapDuLieu}
                                                        />
                                                        <p className="mess-err none" id="so_dien_thoai">Số điện thoại phải có 10 - 11 chữ số và không được để trống</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-lg-5 m-auto col-md-12">
                                                    <div className='form-group'>
                                                        <label>Nơi Sinh:</label>
                                                        <input
                                                            name="noi_sinh" type="text"
                                                            className="input-nhap--style"
                                                            value={noi_sinh}
                                                            onChange={this.onChange}
                                                            onKeyUp={func.KiemTraNhapDuLieu}
                                                            onFocus={func.KiemTraNhapDuLieu}
                                                        />
                                                        <p className="mess-err none" id="noi_sinh">Nơi sinh không được để trống</p>
                                                    </div>
                                                </div>
                                                <div className="col-lg-5 m-auto col-md-12">
                                                    <div className='form-group'>
                                                        <label>Hình Đại Diện:</label>
                                                        <input
                                                            name="hinh_dai_dien" type="file"
                                                            className="input-nhap--style"
                                                            // value={hinh_dai_dien}
                                                            onChange={this.fileHinhAnh}
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-lg-5 m-auto col-md-12">
                                                    <div className='form-group'>
                                                        <label >Phụ Huynh</label>
                                                        <Select   
                                                            value={this.state.selectedOption}
                                                            onChange={this.handleChange}
                                                            options={this.state.options}  
                                                        /> 
                                                    </div>
                                                </div>
                                                <div className="col-lg-5 m-auto col-md-12">
                                                    <div className='form-group'>
                                                        <label style={{ margin: 10 }}>Giới Tính:</label>
                                                        <input
                                                            style={{ margin: 10 }}
                                                            name="rdGioiTinh" type='radio'
                                                            value='1'
                                                            checked={this.state.rdGioiTinh === '1'}
                                                            onChange={this.onChange}
                                                        />Nam
                                                        <input
                                                            style={{ margin: 10 }}
                                                            name="rdGioiTinh" type='radio'
                                                            value='0'
                                                            checked={this.state.rdGioiTinh === '0'}
                                                            onChange={this.onChange}
                                                        />Nữ
                                                    </div>
                                                </div>
                                            </div>

                                            <div className="col-lg-5 m-auto col-md-12">
                                                <div className='form-group'>
                                                    <label >Ngày Sinh:</label>
                                                    <div>
                                                        <Calendar
                                                            onChange={this.onChangeDate}
                                                            value={moment(ngay_sinh).toDate()}
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col-lg-12 col-md-12">
                                                <button className='button-default--style button-thaotac--style' type="button" onClick={this.ThemSinhVien}>{this.props.id === undefined ? "Thêm" : "Lưu"}</button>
                                                <button className='button-default--style button-thoat--style' type="button" onClick={this.Thoat}>Thoát</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
const mapStateToProps = state => ({
    CTSinhVien: state.LayCTSinhVien.CTSinhVien,
    DSPhuHuynh: state.LayDSPhuHuynh.DSPhuHuynhChoOption,
    DSChuyenNganh: state.LayDSChuyenNganh.DSChuyenNganh,
    DSLop: state.LayDSLop.DSLop,
    status: state.LayDSSinhVien.status,
});
themsinhvien.propTypes = {
    DanhSachPhuHuynhChoOption: propTypes.func.isRequired,
    ThemSinhVien: propTypes.func.isRequired,
    CapNhatSinhVien: propTypes.func.isRequired,
    DanhSachChuyenNganh: propTypes.func.isRequired,
    ChiTietSinhVienTheoMaSV: propTypes.func.isRequired,
    ThemSinhVienTuFileNen: propTypes.func.isRequired,
    ThemHinhSinhVien: propTypes.func.isRequired,
    UpLoadFileNen: propTypes.func.isRequired,
    DanhSachLop: propTypes.func.isRequired,
}
export default connect(mapStateToProps, { ThemSinhVien, CapNhatSinhVien, ChiTietSinhVienTheoMaSV, DanhSachChuyenNganh, ThemSinhVienTuFileNen, ThemHinhSinhVien, UpLoadFileNen, DanhSachPhuHuynhChoOption, DanhSachLop })(withRouter(themsinhvien));
