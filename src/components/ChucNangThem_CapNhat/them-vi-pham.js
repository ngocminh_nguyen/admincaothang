import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { connect } from 'react-redux';
import propTypes from 'prop-types';
// import {  } from './../../actions/them_action';
import { CapNhatViPham, ThemViPham, ChiTietViPhamTheoMaViPham } from './../../actions/vi_pham';
import { withRouter } from 'react-router';
import * as func from '../../constants/FunctionsKTDuLieu_ThongBao'; 
import Swal from 'sweetalert2';  
import * as $ from 'jquery';
const initialState = {
    id: '',
    ma_vi_pham: '',
    noi_dung: '',
    hinh_thuc_ky_luat: '',
    ngay_tao: '',
    ngay_cap_nhat: ''
}
class themvipham extends Component {
    constructor(props) {
        super(props);
        this.state = initialState;
        this.Thoat = this.Thoat.bind(this)
    }
    componentWillMount() {
        if (this.props.id) {
            this.props.ChiTietViPhamTheoMaViPham(this.props.id);
        }
    }
    async componentWillReceiveProps(nextProps) {
        if (nextProps.CTViPham !== undefined)
            this.setState({
                id: nextProps.CTViPham.id,
                ma_vi_pham: nextProps.CTViPham.ma_vi_pham,
                noi_dung: nextProps.CTViPham.noi_dung,
                hinh_thuc_ky_luat: nextProps.CTViPham.hinh_thuc_ky_luat,
                ngay_cap_nhat: nextProps.CTViPham.ngay_cap_nhat,
                ngay_tao: nextProps.CTViPham.ngay_tao,
            });
        if (nextProps.status === "Thành Công") {
            await Swal.fire({
                type: 'success',
                title: 'Thành Công!',
                showConfirmButton: true,
                confirmButtonText: 'Đồng ý'
            })
            this.props.history.push("/vipham")
        }
    }
    onChange = (e) => {
        var target = e.target;
        var name = target.name;
        this.setState({
            [name]: target.value
        });
    }
    ThemViPham = (e) => {  
        var soTheKT = $("p.mess-err").length; 
        var soTheHopLe = $("p.mess-err.none").length;
        if(this.state.noi_dung !=="" && this.state.hinh_thuc_ky_luat !==""){
            if(soTheHopLe  === soTheKT){
                if (this.props.id === undefined) {
                    this.props.ThemViPham(this.state);
                    this.setState(initialState);
                }
                else {  
                    this.props.CapNhatViPham(this.state);  
                }   
            }
            else{
                func.ThongBaoMaLoi("Bạn phải nhập thông tin hợp lệ")
            } 
        }
        else{ 
            func.ThongBaoMaLoi("Bạn phải nhập đủ thông tin ") 
        } 
    }
    Thoat() {
        this.props.history.push('/vipham');
    }
    render() {
        var { ma_vi_pham, noi_dung, hinh_thuc_ky_luat } = this.state;
        return (
            <div id="body-content">
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-md-12">
                            <div className="content-title">
                                <h1 className="text-center h1-title-style">{this.props.id ? "Cập Nhật Vi Phạm" : "Thêm Vi Phạm"}</h1>
                                <span className="kitu-dacbiet">&#9884;</span>
                            </div>
                        </div>
                        <div className="col-md-12 text-center">
                            <div className="form-content">
                                <div className="row"> 
                                    <div className="col-lg-5 m-auto col-md-12">
                                        <div className='form-group'>
                                            <label style={{ margin: 10 }}>Nội Dung:</label>
                                            <input
                                                style={{ margin: 10, borderRadius: 3 }}
                                                name="noi_dung" type="text"
                                                className="input-nhap--style"
                                                value={noi_dung}
                                                onChange={this.onChange}
                                                onKeyUp={func.KiemTraNhapDuLieu} 
                                                onFocus={func.KiemTraNhapDuLieu}  
                                            />
                                            <p className="mess-err none" id="noi_dung">Nội dung vi phạm không được để trống</p>
                                        </div>
                                    </div>  
                                    <div className="col-lg-5 m-auto col-md-12">
                                        <div className='form-group'>
                                            <label style={{ margin: 10 }}>Hình Thức Kỷ Luật:</label>
                                            <input
                                                style={{ margin: 10, borderRadius: 3 }}
                                                name="hinh_thuc_ky_luat" type="text"
                                                className="input-nhap--style"
                                                value={hinh_thuc_ky_luat}
                                                onChange={this.onChange}
                                                onKeyUp={func.KiemTraNhapDuLieu} 
                                                onFocus={func.KiemTraNhapDuLieu}  
                                            />
                                            <p className="mess-err none" id="hinh_thuc_ky_luat">Hình thức kỷ luật không được để trống</p>
                                        </div>
                                    </div>
                                    <div className="col-lg-5 m-auto col-md-12"></div>
                                </div>
                                <div className="col-lg-12 col-md-12">
                                    <button className='button-default--style button-thaotac--style' type="button" onClick={this.ThemViPham}>{this.props.id === undefined ? "Thêm" : "Lưu"}</button>
                                    <button className='button-default--style button-thoat--style' type="button" onClick={this.Thoat}>Thoát</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
const mapStateToProps = state => ({
    CTViPham: state.LayCTViPham.CTViPham,
    status: state.LayCTViPham.status
});
themvipham.propTypes = {
    ThemViPham: propTypes.func.isRequired,
    CapNhatViPham: propTypes.func.isRequired,
    ChiTietViPhamTheoMaViPham: propTypes.func.isRequired,
}
export default connect(mapStateToProps, { ThemViPham, CapNhatViPham, ChiTietViPhamTheoMaViPham })(withRouter(themvipham)); 
