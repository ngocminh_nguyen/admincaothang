import React, { Component } from 'react';
import propTypes from 'prop-types';
import { connect } from 'react-redux';
import { CapNhatChuyenNganh, ThemChuyenNganhVaoDS, ChiTietChuyenNganhTheoMaSo } from '../../actions/chuyen_nganh';
import { DanhSachKhoa } from '../../actions/khoa';
import { withRouter } from 'react-router';
import * as func from '../../constants/FunctionsKTDuLieu_ThongBao'; 
import Swal from 'sweetalert2';  
import * as $ from "jquery"
// CommonJS  
let initialState = {
    id: '',
    ma_so: '',
    ma_chu:'',
    ten_chuyen_nganh: '',
    ma_khoa: '',
    ngay_cap_nhat: '',
    ngay_tao: '',
    DSKhoa: [], 
}
class themchuyennganh extends Component {
    constructor(props) {
        super(props);
        this.state = initialState;
        this.ThemChuyenNganh = this.ThemChuyenNganh.bind(this)
        this.Thoat = this.Thoat.bind(this)
    }
    componentWillMount() {
        if (this.props.id) {
            this.props.ChiTietChuyenNganhTheoMaSo(this.props.id);
        }
        this.props.DanhSachKhoa();
    }
    async componentWillReceiveProps(nextProps) {
        var mangtam=[];
        if (nextProps.DSKhoa !== undefined && nextProps.DSKhoa.length > 0) {
            nextProps.DSKhoa.map((item, key)=>{
                if(item.xoa==0){
                    mangtam.push(item);
                }
            })
            this.setState({ DSKhoa: mangtam, ma_khoa: mangtam[0].ma_khoa }) 
        }  
        if (this.props.id && nextProps.CTChuyenNganh !== undefined && Object.keys(nextProps.CTChuyenNganh).length > 0) {
            this.setState({
                // id: nextProps.CTChuyenNganh.id,
                ma_so: nextProps.CTChuyenNganh.ma_so,
                ma_chu: nextProps.CTChuyenNganh.ma_chu,
                ten_chuyen_nganh: nextProps.CTChuyenNganh.ten_chuyen_nganh,
                ma_khoa: nextProps.CTChuyenNganh.ma_khoa,
                ngay_cap_nhat: nextProps.CTChuyenNganh.ngay_cap_nhat,
                ngay_tao: nextProps.CTChuyenNganh.ngay_tao
            });
        }  
        if (nextProps.status === "Thành Công") {
            await Swal.fire({
                type: 'success',
                title: 'Thành Công!',
                showConfirmButton: true,
                confirmButtonText: 'Đồng ý'
            })
            this.props.history.push("/chuyennganh")
        }   
    }
    
    onChange = (event, e) => {
        var target = event.target;
        var name = target.name;
        var value = target.value;
        this.setState({ [name]: value }) 
    }
    ThemChuyenNganh() { 
        var soTheKT = $("p.mess-err").length; 
        var soTheHopLe = $("p.mess-err.none").length;
        if(this.state.ma_chu.trim() !=="" && this.state.ma_so.trim() !=="" && this.state.ten_chuyen_nganh.trim() !== ""){
            if(soTheHopLe  === soTheKT){
                if (this.props.id === undefined) {
                    this.props.ThemChuyenNganhVaoDS(this.state);
                    initialState.ma_khoa = this.state.ma_khoa;
                    initialState.DSKhoa = this.state.DSKhoa;
                    this.setState(initialState);
                }
                else {  
                    this.props.CapNhatChuyenNganh(this.state); 
                }   
            }
            else{
                func.ThongBaoMaLoi("Bạn phải nhập thông tin hợp lệ")
            } 
        }
        else{ 
            func.ThongBaoMaLoi("Bạn phải nhập đủ thông tin ") 
        } 
    }
    Thoat() {
        this.props.history.push('/chuyennganh');

    }
    render() { 
        return (
            <div id="body-content">
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-md-12">
                            <div className="content-title">
                                <h1 className="text-center h1-title-style">{this.props.id ? "Cập Nhật Chuyên Ngành" : "Thêm Chuyên Ngành"}</h1>
                                <span className="kitu-dacbiet">&#9884;</span>
                            </div>
                        </div>
                        <div className="col-md-12">
                            <div className="form-content">
                                <div className="row" >
                                    <div className={this.props.id?"none":"col-lg-5 m-auto col-md-12"}>
                                        <div className='form-group'>
                                            <label className="label-input--style" >Mã Số:</label>
                                            <input className="input-nhap--style"
                                                name="ma_so" type="text"
                                                maxLength="4"
                                                value={this.state.ma_so}
                                                onChange={this.onChange}
                                                onKeyUp={func.KiemTraDuLieuNhapLaSo}
                                                onFocus={func.KiemTraNhapDuLieu}
                                            />
                                            <p className="mess-err none" id="ma_so">Không được nhập chữ hoặc để trống</p>
                                        </div>
                                    </div>
                                    <div className={this.props.id?"none":"col-lg-5 m-auto col-md-12"}>
                                        <div className='form-group'>
                                            <label className="label-input--style">Mã Chữ</label>
                                            <input className="input-nhap--style"
                                                // style={{textTransform:"uppercase"}}
                                                name="ma_chu" type='text'
                                                maxLength="10"
                                                value={this.state.ma_chu}
                                                onChange={this.onChange}
                                                onKeyUp={func.KiemTraDuLieuNhapLaChu}
                                                onFocus={func.KiemTraNhapDuLieu}
                                            />
                                            <p className="mess-err none" id="ma_chu">Không được nhập số hoặc để trống</p>
                                        </div>
                                    </div>
                                    </div>
                                    <div className="row"> 
                                    <div className="col-lg-5 m-auto col-md-12">
                                        <div className='form-group'>
                                            <label className="label-input--style">Tên Chuyên Ngành:</label>
                                            <input className="input-nhap--style"
                                                name="ten_chuyen_nganh" type="text"
                                                value={this.state.ten_chuyen_nganh}
                                                onChange={this.onChange}
                                                onKeyUp={func.KiemTraNhapDuLieu} 
                                                onFocus={func.KiemTraNhapDuLieu}    
                                            />
                                            <p className="mess-err none" id="ten_chuyen_nganh">Tên chuyên ngành không được để trống</p>
                                        </div>
                                    </div>
                                    <div className="col-lg-5 m-auto col-md-12">
                                        <div className='form-group'>
                                            <label className="label-input--style">Khoa:</label>
                                            <select name="ma_khoa"
                                                value={this.state.ma_khoa}
                                                onChange={this.onChange}
                                                className="input-nhap--style"

                                            >
                                                {/* <option defaultChecked>Mời bạn chọn khoa</option> */}
                                                {this.state.DSKhoa.map((item, key) => <option key={key} value={item.ma_khoa} className="input-nhap--style">{item.ten_khoa}</option>
                                                )}
                                            </select>
                                        </div>
                                    </div>
                                    <div className="col-lg-12 col-md-12">
                                        <button className='button-default--style button-thaotac--style' type="button" onClick={this.ThemChuyenNganh}>{this.props.id === undefined ? "Thêm" : "Lưu"}</button>
                                        <button className='button-default--style button-thoat--style' type="button" onClick={this.Thoat}>Thoát</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
const mapStateToProps = state => ({
    CTChuyenNganh: state.LayCTChuyenNganh.CTChuyenNganh,
    DSKhoa: state.LayDSKhoa.DSKhoa,
    status: state.LayDSChuyenNganh.status
});
themchuyennganh.propTypes = {
    ThemChuyenNganhVaoDS: propTypes.func.isRequired,
    CapNhatChuyenNganh: propTypes.func.isRequired,
    ChiTietChuyenNganhTheoMaSo: propTypes.func.isRequired,
    DanhSachKhoa: propTypes.func.isRequired,
}
export default connect(mapStateToProps, { ThemChuyenNganhVaoDS, CapNhatChuyenNganh, ChiTietChuyenNganhTheoMaSo, DanhSachKhoa })(withRouter(themchuyennganh));
