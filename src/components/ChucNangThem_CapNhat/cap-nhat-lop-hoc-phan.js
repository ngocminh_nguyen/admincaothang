import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { connect } from 'react-redux';
import propTypes from 'prop-types';
import Calendar from 'react-calendar';
import { ChiTietLopHocPhanTheoMaLopHocPhan, ThemLopHocPhan, CapNhatLopHocPhan } from '../../actions/lop_hoc_phan';
import { DanhSachGiaoVienChoOption } from '../../actions/giao_vien';
import { DanhSachMonHocChoOption } from '../../actions/mon_hoc'; 
import Swal from 'sweetalert2';  
import moment from 'moment';
import { withRouter } from 'react-router';
const initialState = {
    id: '',
    ma_lop: '',
    ma_giao_vien: '',
    ma_mon_hoc: '',
    hoc_ky: '1', 
    thu:'2',
    tiet_bat_dau:'1',
    tiet_ket_thuc:'1',
    phong:'',
    thoi_gian_ket_thuc:new Date(2019,0,1),
    ngay_tao: '',
    DSGiaoVien: [],
    DSMonHoc: [],
    tableTKB: [], 
    mangThoi_Gian: []
}  
class capnhatlophocphan extends Component {
    constructor(props) {
        super(props);
        this.state = initialState;
        this.ThemLopHocPhan = this.ThemLopHocPhan.bind(this);
        this.Thoat = this.Thoat.bind(this) 
    }
    componentWillMount() {
        if (this.props.id) {
            this.props.ChiTietLopHocPhanTheoMaLopHocPhan(this.props.id);
        }
        this.props.DanhSachMonHocChoOption();

        this.props.DanhSachGiaoVienChoOption();
    }
    async componentWillReceiveProps(nextProps) {
        var mangtam=[];
        if (nextProps.DSGiaoVien !== undefined && nextProps.DSGiaoVien.length > 0) {
            nextProps.DSGiaoVien.map((item, key)=>{
                if(item.xoa==0){
                    mangtam.push(item);
                }
            })
            this.setState({ DSGiaoVien: mangtam, ma_giao_vien: mangtam[0].ma_giao_vien }) 
        } 
        var mangDSMH=[];
        if (nextProps.DSMonHoc !== undefined && nextProps.DSMonHoc.length > 0) {
            nextProps.DSMonHoc.map((item, key)=>{
                if(item.xoa==0){
                    mangDSMH.push(item);
                }
            })
            this.setState({ DSMonHoc: mangDSMH, ma_mon_hoc: mangDSMH[0].ma_mon_hoc }) 
        } 
        if (this.props.id && nextProps.CTLopHocPhan !== undefined && Object.keys(nextProps.CTLopHocPhan).length > 0) {
            this.setState({
                id: nextProps.CTLopHocPhan.id,
                ma_lop: nextProps.CTLopHocPhan.ma_lop,
                ma_giao_vien: nextProps.CTLopHocPhan.ma_giao_vien,
                ma_mon_hoc: nextProps.CTLopHocPhan.ma_mon_hoc,
                hoc_ky: nextProps.CTLopHocPhan.hoc_ky,
                mangThoi_Gian: JSON.parse(nextProps.CTLopHocPhan.thoi_gian),
                thoi_gian_ket_thuc: nextProps.CTLopHocPhan.thoi_gian_ket_thuc,
                ngay_tao: nextProps.CTLopHocPhan.ngay_tao
            });
        }
        if (nextProps.status === "Thành Công") {
            await Swal.fire({
                type: 'success',
                title: 'Thành Công!',
                showConfirmButton: true,
                confirmButtonText: 'Đồng ý'
            })
            this.props.history.push("/lophocphan")
        }
    }
    onChange = (e) => {
        var target = e.target;
        var name = target.name;
        this.setState({
            [name]: target.value,
        });
    }
    ThemLopHocPhan = (e) => { 
            if(this.state.tableTKB.length == 0) 
                this.state.tableTKB = this.state.mangThoi_Gian;
                this.state.thoi_gian_ket_thuc=moment(this.state.thoi_gian_ket_thuc).format("YYYY-MM-DD") 
            this.props.CapNhatLopHocPhan(this.state);  
    }
    Thoat() {
        // var history = this.props.id;

        this.props.history.push('/lophocphan');
    }
    LuuDuLieuVaThemDong = () => { 
        var newdata = {
            "thu": this.state.thu,
            "tiet_bat_dau": this.state.tiet_bat_dau,
            "tiet_ket_thuc": this.state.tiet_ket_thuc, 
            "phong": this.state.phong,
        }; 
        this.setState({ tableTKB: this.state.tableTKB.concat(newdata) });  
    } 
    XoaDL(thu){
        var mangtam = this.state.tableTKB;
        mangtam.splice(thu,1);
        this.setState({tableTKB: mangtam}); 
        console.log( this.state.tableTKB)
    }
    onChangeDate=(e)=>{ 
        this.setState({thoi_gian_ket_thuc:e}) 
    }
    render() {
        const gobal = this;
        var { ma_giao_vien, ma_mon_hoc, hoc_ky, tiet_ket_thuc, tiet_bat_dau, thu, thoi_gian_ket_thuc,phong } = this.state; 
        console.log(moment(thoi_gian_ket_thuc).toDate())
      
        return (
            <div id="body-content">
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-md-12">
                            <div className="content-title">
                                <h1 className="text-center h1-title-style">Cập Nhật Lớp Học Phần</h1>
                                <span className="kitu-dacbiet">&#9884;</span>
                            </div>
                        </div>
                        <div className="col-md-12 ">
                            <div className="form-content table-responsive tableTKB text-left p-0">
                                <div className="row p-4">
                                    <div className="col-lg-2 col-md-2 ">
                                        <div className='margin-top'>
                                            <label >Môn Học: </label>
                                        </div>
                                    </div>
                                    <div className="col-lg-10 col-md-10 ">
                                        <div className="margin-top">
                                            {this.state.DSMonHoc.map((monhoc, key) =>
                                                <p key={key} value={monhoc.ma_mon_hoc} className={monhoc.ma_mon_hoc === ma_mon_hoc ? " p--style-label" : "none"}>{monhoc.ten_mon_hoc}</p>
                                            )}
                                        </div>
                                    </div>
                                    <div className="col-lg-2 col-md-2 ">
                                        <div className=''>
                                            <label >Giáo Viên: </label>
                                        </div>
                                    </div>
                                    <div className="col-lg-10 col-md-10 ">
                                        <div className="" >
                                            <select style={{ width: 300 }}
                                                name="ma_giao_vien"
                                                className="input-nhap--style"
                                                value={ma_giao_vien}
                                                onChange={this.onChange}
                                            >
                                                {this.state.DSGiaoVien.map((gv, key) =>
                                                    <option key={key} value={gv.ma_giao_vien} className="input-nhap--style">{gv.ho_ten}</option>
                                                )}
                                            </select>
                                        </div>
                                    </div> 
                                    <div className="col-lg-2 col-md-2 ">
                                        <div className='margin-top'>
                                            <label >Học Kỳ: </label>
                                        </div>
                                    </div>
                                    <div className="col-lg-10 col-md-10  mb-4">
                                        <div className="margin-top">
                                            <select style={{ width: 300 }}
                                                name="hoc_ky"
                                                className="input-nhap--style"
                                                value={hoc_ky}
                                                onChange={this.onChange}
                                            >
                                                <option value="1">Học Kỳ 1</option>
                                                <option value="2">Học Kỳ 2</option>
                                                <option value="3">Học Kỳ 3</option>
                                                <option value="4">Học Kỳ 4</option>
                                                <option value="5">Học Kỳ 5</option>
                                                <option value="6">Học Kỳ 6</option>
                                            </select>
                                        </div>
                                    </div> 
                                    <div className="col-lg-2 col-md-2 ">
                                        <div className='margin-top'>
                                            <label >Phòng Học: </label>
                                        </div>
                                    </div>
                                    <div className="col-lg-10 col-md-10  mb-4">
                                        <div className="margin-top"> 
                                            <input style={{ width: 300 }}
                                                name="phong" type="text"
                                                className="input-nhap--style "
                                                value={phong}
                                                onChange={this.onChange} 
                                            />
                                        </div>
                                    </div> 
                                    {/* <div className="col-lg-6 col-md-12 m-auto">
                                        <div className='from-group '>
                                            <label>Phòng Học:</label>
                                            <input 
                                                name="phong" type="text"
                                                className="input-nhap--style "
                                                value={phong}
                                                onChange={this.onChange}
                                                // onKeyUp={func.KiemTraNhapDuLieu} 
                                                // onFocus={func.KiemTraNhapDuLieu}
                                            />
                                            <p className="mess-err none" id="ten_khoa">Phòng không được để trống</p>
                                        </div>
                                    </div> */}
                                    <div className="col-lg-6 col-md-12 text-center">
                                        <div className="" style={{boxShadow: "0 0 6px #ccc",  height: "397px"}}>
                                        <table className="table table-striped" style={{ border: "1px solid #dee2e6" }}>
                                            <thead>
                                                <tr>
                                                    <th style={{ borderBottom: "1px solid white" }} colSpan={3} className="text-center">Thời Gian Chưa Cập Nhật</th>
                                                </tr>
                                                <tr>
                                                    <th>Thứ</th>
                                                    <th>Tiết Bắt Đầu</th>
                                                    <th>Tiết Kết Thúc</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {this.state.mangThoi_Gian.map((item, key) =>
                                                    <tr key={key}>
                                                        <td>T{item.thu}</td>
                                                        <td>Từ tiết {item.tiet_bat_dau}</td>
                                                        <td> đến tiết  {item.tiet_ket_thuc}</td>
                                                    </tr>
                                                )}
                                                      </tbody>
                                        </table>
                                  
                                        </div>
                                    </div>
                                    <div className="col-lg-6 col-md-12 text-center">
                                        <div className="" style={{boxShadow: "0 0 6px #ccc",  height: "397px"}}>
                                            <table className="table table-striped" style={{ border: "1px solid #dee2e6"}}>
                                                <thead>
                                                    <tr>
                                                        <th style={{ borderBottom: "1px solid white" }} colSpan={4} className="text-center">Thời Gian Sau Khi Cập Nhật</th>
                                                    </tr>
                                                    <tr>
                                                        <th>Thứ</th>
                                                        <th>Tiết Bắt Đầu</th>
                                                        <th colSpan={2}>Tiết Kết Thúc</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            <select
                                                                name="thu"
                                                                className="input-nhap--style ml-3"
                                                                value={thu}
                                                                onChange={this.onChange}
                                                            >
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4">4</option>
                                                                <option value="5">5</option>
                                                                <option value="6">6</option>
                                                                <option value="7">7</option>
                                                            </select>
                                                        </td>
                                                        <td>
                                                            <select
                                                                name="tiet_bat_dau"
                                                                className="input-nhap--style"
                                                                value={tiet_bat_dau}
                                                                onChange={this.onChange}
                                                            >
                                                                <option value="1">Tiết 1</option>
                                                                <option value="2">Tiết 2</option>
                                                                <option value="3">Tiết 3</option>
                                                                <option value="4">Tiết 4</option>
                                                                <option value="5">Tiết 5</option>
                                                                <option value="6">Tiết 6</option>
                                                            </select>
                                                        </td>
                                                        <td >
                                                            <select
                                                                name="tiet_ket_thuc"
                                                                className="input-nhap--style"
                                                                value={tiet_ket_thuc}
                                                                onChange={this.onChange}
                                                            >
                                                                <option value="1">Tiết 1</option>
                                                                <option value="2">Tiết 2</option>
                                                                <option value="3">Tiết 3</option>
                                                                <option value="4">Tiết 4</option>
                                                                <option value="5">Tiết 5</option>
                                                                <option value="6">Tiết 6</option>
                                                            </select>
                                                            
                                                        </td>
                                                        <td><button type="button" className="btn-taodong" onClick={()=>this.LuuDuLieuVaThemDong()}>Thêm</button></td>
                                                    </tr>
                                                    {this.state.tableTKB.map(function (row, index) {
                                                        return (<tr key={index}>
                                                            <td >T{row.thu}</td>
                                                            <td >Từ tiết {row.tiet_bat_dau}</td>
                                                            <td >đến tiết {row.tiet_ket_thuc}</td> 
                                                            <td>
                                                                  <i id="Xoa" onClick={()=>gobal.XoaDL(index)} className="fa fa-times" style={{color:"red"}} ></i>  
                                                            </td>
                                                        </tr>);
                                                    })}
                                                    
                                                </tbody>
                                            </table>
                                        </div>
                                    </div> 
                                    <div className="col-lg-6 col-md-12 text-center">
                                        <div className='form-group'>
                                            <label >Ngày Dự Kiến Kết Thúc:</label> 
                                            <div>
                                                <Calendar
                                                onChange={this.onChangeDate}
                                                value={moment(thoi_gian_ket_thuc).toDate()}
                                                />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-lg-5 col-md-12 m-auto text-center">
                                        <button className='button-default--style button-thaotac--style' type="button" onClick={this.ThemLopHocPhan}>{this.props.id === undefined ? "Thêm" : "Lưu"}</button>
                                        <button className='button-default--style button-thoat--style' type="button" onClick={this.Thoat}>Thoát</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
const mapStateToProps = state => ({
    CTLopHocPhan: state.LayCTLopHocPhan.CTLopHocPhan,
    a:console.log(state.LayCTLopHocPhan.CTLopHocPhan),
    DSGiaoVien: state.LayDSGiaoVien.DSGiaoVienChoOption,
    DSMonHoc: state.LayDSMonHoc.DSMonHocChoOption,
    status: state.LayCTLopHocPhan.status
});
capnhatlophocphan.propTypes = { 
    CapNhatLopHocPhan: propTypes.func.isRequired,
    ChiTietLopHocPhanTheoMaLopHocPhan: propTypes.func.isRequired,
    DanhSachGiaoVienChoOption: propTypes.func.isRequired,
    DanhSachMonHocChoOption: propTypes.func.isRequired,
}
export default connect(mapStateToProps, { ThemLopHocPhan, CapNhatLopHocPhan, ChiTietLopHocPhanTheoMaLopHocPhan, DanhSachGiaoVienChoOption, DanhSachMonHocChoOption })(withRouter(capnhatlophocphan));
