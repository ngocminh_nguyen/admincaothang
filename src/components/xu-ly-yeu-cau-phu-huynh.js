import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import khung from "../assets/img/khung.png";
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import propTypes from 'prop-types'; 
import Swal from 'sweetalert2';
import {ChiTietPhuHuynhTheoMAPH, CapNhatPhuHuynh} from '../actions/phu_huynh'
import { ChiTietYeuCauTheoMaYeuCau, XoaYeuCau } from './../actions/yeu_cau';
class xlyeucauph extends Component {
    constructor(props) {
        super(props);
        this.state = {
            trangthai: 1,
            huy_yeu_cau: '',
            nguoi_yeu_cau:'',
            ho_ten_cha :'',
            ho_ten_me :'',
            so_dien_thoai :'',
            dia_chi :'',
            xoa:'',
            status:'',
            CTYeuCau: [],
            CTPhuHuynh:[],
            yeuCauItem: [],
        }
    }
    componentDidMount() { 
        if (this.props.id) {
            this.props.ChiTietYeuCauTheoMaYeuCau(this.props.id);
            this.props.ChiTietPhuHuynhTheoMAPH(this.props.id)
        }
    }
    async componentWillReceiveProps(nextProps) { 
        if(nextProps.CTYeuCau !== undefined && Object.keys(nextProps.CTYeuCau).length > 0 ){ 
            if(this.props.loai==="2"){
                this.setState({CTYeuCau: nextProps.CTYeuCau,  
                    nguoi_yeu_cau: nextProps.CTYeuCau.nguoi_yeu_cau,
                    ho_ten_cha: JSON.parse(nextProps.CTYeuCau.noi_dung_yeu_cau).ho_ten_cha,
                    ho_ten_me: JSON.parse(nextProps.CTYeuCau.noi_dung_yeu_cau).ho_ten_me,
                    so_dien_thoai: JSON.parse(nextProps.CTYeuCau.noi_dung_yeu_cau).so_dien_thoai,
                    dia_chi: JSON.parse(nextProps.CTYeuCau.noi_dung_yeu_cau).dia_chi,
                    xoa:  nextProps.CTYeuCau.xoa,
                    })
            }
        }  
        if (nextProps.CTPhuHuynh !== undefined && Object.keys(nextProps.CTPhuHuynh).length > 0) {
            this.setState({ CTPhuHuynh: nextProps.CTPhuHuynh });
        }
        this.setState({ status: nextProps.status })
        if (nextProps.status === "Thành Công") {
            await Swal.fire({
                type: 'success',
                title: 'Thành Công!',
                showConfirmButton: true,
                confirmButtonText: 'Đồng ý'
            }) 
            this.props.history.push("/yeucau")
        }
    }
    
    ChapNhanXoaYeuCau = () => {
        this.props.XoaYeuCau(this.props.id, 1)
        this.setState({xoa:1})
    }
    render() {
        var {nguoi_yeu_cau, ho_ten_cha, ho_ten_me, so_dien_thoai, dia_chi, CTPhuHuynh } = this.state;
        var {CTYeuCau} = this.state;
        return (
            <div id="body-content">
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-md-12">
                            <div className="content-title">
                                <h1 className="text-center h1-title-style">Xử Lý Yêu Cầu Của Phụ Huynh</h1>
                                <span className="kitu-dacbiet">&#9884;</span>
                            </div>
                        </div>
                        <div className="col-md-12">
                        </div>
                        <div className="chuc-nang-them" background={khung}>
                            <ul>
                                <li className="nav-link">
                                    <input className="search-data-table pl-1" name="seart-data-table" placeholder="Search..."></input>
                                </li>
                                <li className="nav-link">
                                    <Link to="#" data-toggle="tooltip" title="phân trang">
                                        <i className="fa fa-caret-square-o-down"></i>
                                    </Link>
                                </li>
                                <li className="nav-link">
                                    <button className="btn-an-cot--style" data-toggle="tooltip" title="Ẩn cột">
                                        <i className="fa fa-th"></i>
                                    </button>
                                </li>
                            </ul>
                        </div>
                        <div className="col-md-12">
                            <div className="row">
                                <div className="col-lg-6">
                                    <div className="yeucau-data"> 
                                        <h3 className="m-auto">Thông Tin Vừa Cập Nhật</h3> 
                                        <table className="tableSV" style={{ border: "1px solid #dee2e6" }}>
                                            <tr>
                                                <th style={{ borderBottom: "1px solid white" }} colSpan={4} className="text-center"><label >Mã Phụ Huynh:</label></th>
                                                <td><label>{nguoi_yeu_cau}</label></td>
                                            </tr>

                                            <tr>
                                                <th style={{ borderBottom: "1px solid white" }} colSpan={4} className="text-center"> <label >Họ Tên Cha:</label></th>
                                                <td><label>{ho_ten_cha}</label></td>
                                            </tr>
                                            <tr>
                                                <th style={{ borderBottom: "1px solid white" }} colSpan={4} className="text-center"><label >Họ Tên Mẹ:</label></th>
                                                <td><label>{ho_ten_me}</label></td>
                                            </tr>
                                            <tr>
                                                <th style={{ borderBottom: "1px solid white" }} colSpan={4} className="text-center"><label >Số Điện Thoại:</label></th>
                                                <td><label>{so_dien_thoai}</label></td>
                                            </tr>
                                            <tr>
                                                <th  colSpan={4} className="text-center"><label >Địa Chỉ:</label></th>
                                                <td><label>{dia_chi}</label></td>
                                            </tr> 
                                        </table>  
                                    </div>
                                </div>
                                <div className="col-lg-6">
                                    <div className="yeucau-data">
                                        <h3 className="m-auto">Thông Tin Phụ Huynh Hiện Tại</h3> 
                                        <table className="tableSV" style={{ border: "1px solid #dee2e6" }}>
                                            <tr>
                                                <th style={{ borderBottom: "1px solid white" }} colSpan={4} className="text-center"><label >Mã Phụ Huynh:</label></th>
                                                <td><label>{CTPhuHuynh.ma_phu_huynh}</label></td>
                                            </tr>

                                            <tr>
                                                <th style={{ borderBottom: "1px solid white" }} colSpan={4} className="text-center"> <label >Họ Tên Cha:</label></th>
                                                <td><label>{CTPhuHuynh.ho_ten_cha}</label></td>
                                            </tr>
                                            <tr>
                                                <th style={{ borderBottom: "1px solid white" }} colSpan={4} className="text-center"><label >Họ Tên Mẹ:</label></th>
                                                <td><label>{CTPhuHuynh.ho_ten_me}</label></td>
                                            </tr>
                                            <tr>
                                                <th style={{ borderBottom: "1px solid white" }} colSpan={4} className="text-center"><label >Số Điện Thoại:</label></th>
                                                <td><label>{CTPhuHuynh.so_dien_thoai}</label></td>
                                            </tr>
                                            <tr>
                                                <th   colSpan={4} className="text-center"><label >Địa Chỉ:</label></th>
                                                <td><label>{CTPhuHuynh.dia_chi}</label></td>
                                            </tr> 
                                        </table> 
                                    </div> 
                                </div>
                                <div className={this.state.xoa === 1 ? "none" : "col-lg-12 col-md-12 text-center" }>
                                    {/* <button className={this.state.status === "Thành Công" || this.state.xoa === 1? "none" : 'button-default--style button-thaotac--style'} type="button" onClick={() => this.ChapNhanCapNhatPhuHuynh()}>Chấp Nhận</button> */}
                                    <button className='button-default--style button-thoat--style' type="button" onClick={() => this.ChapNhanXoaYeuCau()}>Hủy</button>
                                    <Link class="quaylai" to="/yeucau">Quay lại...</Link> 
                                </div>
                                <div className={this.state.xoa === 1 ?"col-lg-12 col-md-12 text-center":"none"} >
                                    <button className={this.state.status === "Thành Công" ? "none" : 'button-default--style button-thaotac--style'} type="button">Yêu Cầu Đã Được Xử Lý</button> 
                                    <Link class="quaylai" to="/yeucau">Quay lại...</Link> 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    } 
}
const mapStateToProps = state => ({
    CTYeuCau: state.LayDSYeuCau.CTYeuCau,
    CTPhuHuynh: state.LayCTPhuHuynh.CTPhuHuynh, 
    status: state.LayCTPhuHuynh.status ,  
});
xlyeucauph.propTypes = {
    ChiTietYeuCauTheoMaYeuCau: propTypes.func.isRequired,
    ChiTietPhuHuynhTheoMAPH: propTypes.func.isRequired,
    CapNhatPhuHuynh: propTypes.func.isRequired,
    XoaYeuCau: propTypes.func.isRequired
}
export default connect(mapStateToProps, { ChiTietYeuCauTheoMaYeuCau, CapNhatPhuHuynh, XoaYeuCau, ChiTietPhuHuynhTheoMAPH })(xlyeucauph);
