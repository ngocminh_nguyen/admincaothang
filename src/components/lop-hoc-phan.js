import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css'; 
import propTypes from 'prop-types';
import khung from "../assets/img/khung.png";
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import DSLopHocPhan from './DanhSach/ds-lop-hoc-phan';
import LopHocPhan from './Item/lop-hoc-phan-item';
import {DanhSachLopHocPhan  } from './../actions/lop_hoc_phan';
class lophocphan extends Component {
    constructor(props) {
        super(props);
        this.state = {  
            DSLopHocPhan:[],  
            lopHocPhanItem:[],
        }  
        this.HienThiLopHocPhan = this.HienThiLopHocPhan.bind(this);
    }
    componentDidMount() {
        this.props.DanhSachLopHocPhan();
    }
    componentWillReceiveProps(nextProps) {
        this.setState({ DSLopHocPhan: nextProps.DSLopHocPhan}); 
    } 
    render() {   
        return (
            <div id="body-content">
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-md-12">
                        <div className="content-title"> 
                            <h1 className="text-center h1-title-style">Lớp Học Phần</h1>
                            <span className="kitu-dacbiet">&#9884;</span>
                        </div>
                        </div>
                        <div className="col-md-12">
                            <Link to="/lophocphan/them" className="button-them--style button-default--style button-thaotac--style">Thêm Lớp Học Phần</Link>
                        </div>
                        <div className="col-md-12"> 
                            <div className="tableTKB">
                                <div className="chuc-nang-them" background={khung}>
                                    <input className="search-data-table pl-1" name="seart-data-table" placeholder="Search..."></input>
                                </div>
                            <DSLopHocPhan>
                                {()=>this.HienThiLopHocPhan(this.state.DSLopHocPhan)}
                            </DSLopHocPhan>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
    HienThiLopHocPhan=(lopHocPhanItem)=>{
        var result = null;
        if(lopHocPhanItem.length>0){
            result = lopHocPhanItem.map((item, key)=>{
                return <LopHocPhan 
                key={key}
                lopHocPhanItem={item}
                index = {key}
                />
            })
        } 
        return result;
    }
}
const mapStateToProps=state=>({
    DSLopHocPhan: state.LayDSLopHocPhan.DSLopHocPhan,
}); 
lophocphan.propTypes = {
    DanhSachLopHocPhan  : propTypes.func.isRequired,
}
export default connect(mapStateToProps, {DanhSachLopHocPhan  })(lophocphan);
