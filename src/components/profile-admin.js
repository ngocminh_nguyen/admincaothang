import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { connect } from 'react-redux';
import propTypes from 'prop-types'; 
import { Link } from 'react-router-dom';
import {ChiTietAdminTheoMaAdmin, CapNhatAdmin, DoiAvatarAdmin, DoiMatKhau} from '../actions/admin'
import { withRouter } from 'react-router';
import * as func from '../constants/FunctionsKTDuLieu_ThongBao';  
import Swal from 'sweetalert2';   
import $ from 'jquery'
let initialState = { 
    id: '',
    ma_admin : '',
    ten_dang_nhap : '',
    mat_khau: '',
    hinh_dai_dien : '',
    ho_ten : '',
    email: '', 
    sdt: '',
    ngay_cap_nhat:'',
    ngay_tao:'',
    hinh_anh:'',
    dachonhinh:false, 
    ten_admin:'',
    email_admin:'',
    mat_khau_cu:'',
    mat_khau_moi:'',
    nhap_lai_mat_khau_moi:''
}
class profileadmin extends Component {
    constructor(props) {
        super(props);
        this.state = initialState;
        this.CapNhatAdmin = this.CapNhatAdmin.bind(this);
        this.Thoat = this.Thoat.bind(this)
    }
    componentWillMount(){
        if(this.props.id)
        this.props.ChiTietAdminTheoMaAdmin(this.props.id); 
    }
    async componentWillReceiveProps(nextProps) {
        if(nextProps.CTAdmin!== undefined)
        this.setState({ id: nextProps.CTAdmin.id,
                        ma_admin :nextProps.CTAdmin.ma_admin,
                        ten_dang_nhap: nextProps.CTAdmin.ten_dang_nhap,
                        mat_khau: nextProps.CTAdmin.mat_khau,
                        hinh_dai_dien: nextProps.CTAdmin.hinh_dai_dien,
                        ho_ten: nextProps.CTAdmin.ho_ten,
                        ten_admin: nextProps.CTAdmin.ho_ten,
                        sdt: nextProps.CTAdmin.sdt,
                        email: nextProps.CTAdmin.email, 
                        email_admin: nextProps.CTAdmin.email, 
                        ngay_cap_nhat: nextProps.CTAdmin.ngay_cap_nhat,
                        ngay_tao:nextProps.CTAdmin.ngay_tao
                });   
    }
    onChange = (e) => {
        var target = e.target;
        var name = target.name;
        this.setState({
            [name]: target.value,
        });
    }
    fileHinhAnh = (e) =>{   
        initialState.hinh_dai_dien= e.target.files[0]; 
        this.props.DoiAvatarAdmin(this.props.id, initialState.hinh_dai_dien)
    }
    ChoPhepDoiMatKhau=()=>{
        $("#DoiMatKhau").css("display","block")
        $("#DoiMatKhau").css("opacity","1")
        $("input").attr("readOnly", false); 
    }
    ChoPhepCapNhatAdmin=()=>{  
        $("input").attr("readOnly", false); 
        $("input").addClass("input-nhap--style");
        $("input").removeClass("input-profile--style");
        $("#cho-phep-hien").removeClass("none");
    }
    CapNhatAdmin = async(e) => {  
        console.log(this.state);
        if(this.state.ten_dang_nhap.trim() !=="" && this.state.ho_ten.trim() !== "" && this.state.sdt.trim()!== "" && this.state.email.trim() !== ""){
            this.props.CapNhatAdmin(this.state);  
            // this.props.history.push('/admin/'+(this.props.id)+'/profile'); 
            $("input").removeClass("input-nhap--style");
            $("input").addClass("input-profile--style");
            await func.ThongBaoThanhCong();
            $("input").attr("readOnly", true); 
            $("#cho-phep-hien").addClass("none");
        }
        else{
            func.ThongBaoThatBai();
        }
    }
    DoiMatKhau=()=>{ 
        if(this.state.mat_khau_moi === this.state.nhap_lai_mat_khau_moi){
            this.props.DoiMatKhau(this.props.id, this.state.mat_khau_cu, this.state.mat_khau_moi)
        }
        else{
            func.ThongBaoMaLoi("Mật khẩu nhập lại mới không đúng")
        }
    }
    Thoat() { 
        this.props.history.push('/admin/'+(this.props.id)+'/profile');
        $("input").removeClass("input-nhap--style");
        $("input").addClass("input-profile--style");
        $("#cho-phep-hien").addClass("none"); 
        $("#DoiMatKhau").css("display","none");
        $("input").attr("readOnly", true); 
    }
    
    render() {    
        console.log(this.props.id)
        console.log(this.state.hinh_dai_dien)
        var { ho_ten , ten_dang_nhap  , email , hinh_dai_dien, sdt, mat_khau_cu, mat_khau_moi} = this.state; 
        return (
            <div id="body-content">
                <div className="container-fluid">
                    <div className="row" style={{marginRight: "0", marginLeft: "0"}}>
                        <div className="col-md-12"> 
                            <div className="content-title">
                                <h1 className="text-center h1-title-style">{this.props.id ? "Profile Admin" : "Profile Admin"}</h1>
                                <span className="kitu-dacbiet">&#9884;</span>
                            </div>
                        </div>
                        
                        <div className="col-md-12 text-center admin-profile mb-3"> 
                            <div className="form-content container-fluid">
                                <div className="row">
                                    <div className="col-lg-4"> 
                                        <div className="profile-img">
                                            <img src={hinh_dai_dien} alt=""/> 
                                            <div className="upload-btn-wrapper">
                                                <button className="sua-anh"> Đổi ảnh</button>
                                                <input type="file" name="hinh_dai_dien" onChange={this.fileHinhAnh}/>
                                            </div> 
                                        </div>
                                    </div>
                                    <div className="col-lg-8"> 
                                        <div className="profile-info">
                                            <p className="info-name">{this.state.ten_admin}</p>
                                            <Link to="#" className="email">{this.state.email_admin} </Link><span>- Administrator</span>
                                            <p className="mt-2">" Người làm việc chăm chỉ sẽ đánh bại người tài năng khi người tài năng không làm việc<br/>
                                            Làm nhiều lên nha admin, không chăm chỉ sẽ bị đuổi việc đó. "</p>
                                            <button className="sua-profile" onClick={()=>this.ChoPhepCapNhatAdmin()}><i className="fa fa-edit"></i>Sửa thông tin</button> <Link to="#"  className="doi-mat-khau"  onClick={()=>this.ChoPhepDoiMatKhau()}>Bạn có muốn đổi mật khẩu không?</Link>
                                        </div>
                                    </div> 
                                    <div className="col-lg-12"> 
                                        <p className="account-profile">Thông tin tài khoản</p>
                                    </div>
                                    <div className="container ">
                                        <div className="row ml-5 mt-4">
                                        <div className="col-lg-4 col-md-12">
                                        <div className='from-group'>
                                            <label >Tên Đăng Nhập:</label> 
                                        </div>
                                    </div>
                                    <div className="col-lg-8 col-md-12">
                                        <div className='from-group'> 
                                            <input  
                                                name="ten_dang_nhap" type="text"
                                                className="input-profile--style"
                                                value={ten_dang_nhap} 
                                                onChange={this.onChange}
                                                readOnly="readOnly"
                                            />
                                        </div>
                                    </div> 
                                    <div className="col-lg-4 col-md-12">
                                        <div className='from-group'>
                                            <label>Họ Tên:</label> 
                                        </div>
                                    </div>
                                    <div className="col-lg-8 col-md-12">
                                        <div className='from-group'>
                                            <input
                                                name="ho_ten" type="text"
                                                className="input-profile--style"
                                                value={ho_ten}
                                                onChange={this.onChange}
                                                readOnly="readOnly"
                                            />
                                        </div>
                                    </div> 
                                    <div className="col-lg-4 col-md-12">
                                        <div className='from-group'>
                                            <label>Email:</label> 
                                        </div>
                                    </div>
                                    <div className="col-lg-8 col-md-12">
                                        <div className='from-group'>
                                            <input
                                                name="email" type="text"
                                                className="input-profile--style"
                                                value={email}
                                                onChange={this.onChange}
                                                readOnly="readOnly"
                                            />
                                        </div>
                                    </div>
                                    <div className="col-lg-4 col-md-12">
                                        <div className='from-group'>
                                            <label>Số Điện Thoại:</label> 
                                        </div>
                                    </div>
                                    <div className="col-lg-8 col-md-12">
                                        <div className='from-group'>
                                            <input 
                                                name="sdt" type='text'
                                                className="input-profile--style"
                                                value={sdt}
                                                onChange={this.onChange}
                                                readOnly="readOnly"
                                            /> 
                                        </div>
                                    </div>
                                   
                                        </div>
                                    </div>
                                    <div className="col-lg-12 col-md-12 none" id="cho-phep-hien">
                                        <button className='button-default--style button-thaotac--style' type="button" onClick={this.CapNhatAdmin}>{this.props.id===undefined?"Thêm":"Lưu"}</button>
                                        <button className='button-default--style button-thoat--style' type="button" onClick={this.Thoat}>Thoát</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> 
                <div id="DoiMatKhau" className="modal fade" role="dialog">
                    <div className="modal-dialog">
                        <div className="modal-content">
                            <div className="modal-header text-center">
                                <h2 className="modal-title" style={{"color":"#4220cc"}}>Đổi Mật Khẩu</h2>
                            </div>
                            <div className="modal-body text-center">
                                <div className="button-dangnhap ">
                                    <div className="input--style mb-5">
                                    <span className="spanMatKhau">
                                        Mật Khẩu Cũ
                                            </span>
                                    <input className="inputMatKhau mb-0" type="password" required name="mat_khau_cu"
                                    onKeyUp={func.KiemTraNhapDuLieu}   
                                    onFocus={func.KiemTraNhapDuLieu}
                                    onChange={(value) => this.onChange(value)} value={this.state.mat_khau_cu} />
                                    <p className="mess-err none" id="mat_khau_cu">Mật khẩu phải lớn hơn 7 ký tự</p>
                                    </div>
                                    <div className="input--style mb-5">
                                        <span className="spanMatKhauMoi">
                                            Mật Khẩu Mới
                                                </span>
                                        <input className="inputMatKhauMoi mb-0" type="password" required name="mat_khau_moi"
                                            onKeyUp={func.KiemTraDuLieuNhapLaMatKhau}
                                            onFocus={func.KiemTraNhapDuLieu}
                                            onChange={(value) => this.onChange(value)} value={this.state.mat_khau_moi} />
                                        <p className="mess-err none" id="mat_khau_moi">Mật khẩu phải lớn hơn 7 ký tự</p>
                                    </div>
                                    <div className="input--style">
                                        <span className="spanNhapLaiMatKhau mb-4">
                                            Nhập Lại Mật Khẩu Mới
                                                </span>
                                        <input className="inputNhapLaiMatKhau mb-0" type="password" required name="nhap_lai_mat_khau_moi" 
                                        onKeyUp={func.KiemTraDuLieuNhapLaMatKhau}   
                                        onFocus={func.KiemTraNhapDuLieu}
                                        onChange={(value) => this.onChange(value)} value={this.state.nhap_lai_mat_khau_moi} />
                                        <p className="mess-err none" id="nhap_lai_mat_khau_moi">Mật khẩu phải lớn hơn 7 ký tự</p>
                                    </div>
                                </div>
                            </div>
                            <div className=" p-0 text-center">
                                <div className="col-lg-12 col-md-12" id="cho-phep-hien">
                                    <button className='button-default--style button-thaotac--style' type="button" onClick={this.DoiMatKhau}>Lưu</button>
                                    <button className='button-default--style button-thoat--style' type="button" onClick={this.Thoat}>Thoát</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
} 
const mapStateToProps=state=>({
    CTAdmin: state.LayCTAdmin.CTAdmin, 
    status: state.LayCTAdmin.status
});
profileadmin.propTypes = {
    CapNhatAdmin: propTypes.func.isRequired,
    ChiTietAdminTheoMaAdmin: propTypes.func.isRequired,
    DoiAvatarAdmin: propTypes.func.isRequired,
    DoiMatKhau: propTypes.func.isRequired,
}
export default connect(mapStateToProps, { CapNhatAdmin, ChiTietAdminTheoMaAdmin, DoiAvatarAdmin, DoiMatKhau })(withRouter(profileadmin));
