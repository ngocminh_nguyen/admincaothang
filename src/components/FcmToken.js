import React, { Component } from "react";
import { permissionToReceiveNotification } from "./../pushNotification";
class FcmToken extends Component {
  render() {
    return (
      <button onClick={permissionToReceiveNotification}>
        Clique aqui para receber notificações
      </button>
    );
  }
}
export default FcmToken;
