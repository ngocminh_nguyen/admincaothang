import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import khung from "../assets/img/khung.png";
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import propTypes from 'prop-types';
import Swal from 'sweetalert2';  
import { ChiTietSinhVienTheoMaSV, CapNhatSinhVien} from '../actions/sinh_vien';
import {ChiTietPhuHuynhTheoMAPH} from '../actions/phu_huynh'
import { ChiTietYeuCauTheoMaYeuCau, CapNhatYeuCau, XoaYeuCau } from './../actions/yeu_cau';
class xlyeucausv extends Component {
    constructor(props) {
        super(props);
        this.state = {
            trangthai: 1,
            huy_yeu_cau: '',
            nguoi_yeu_cau:'',
            ho_ten:'',
            ngay_sinh:'',
            noi_sinh:'',
            gioi_tinh:'',
            sdt:'', 
            xoa:'',
            CTYeuCau: [],
            CTSinhVien: [],
            CTPhuHuynh:[],
            yeuCauItem: [],
            status:''
        }
    }
    componentDidMount() {
        if(this.props.id){
            this.props.ChiTietYeuCauTheoMaYeuCau(this.props.id);
            if(this.props.loai ==="1"){
                this.props.ChiTietSinhVienTheoMaSV(this.props.id);
            } 
        }
        
    }
    async componentWillReceiveProps(nextProps) {
        if(nextProps.CTYeuCau !== undefined && Object.keys(nextProps.CTYeuCau).length > 0 && this.props.loai==="1"){   
            this.setState({ CTYeuCau: nextProps.CTYeuCau,  
                nguoi_yeu_cau: nextProps.CTYeuCau.nguoi_yeu_cau,
                ho_ten: JSON.parse(nextProps.CTYeuCau.noi_dung_yeu_cau).ho_ten,
                ngay_sinh: JSON.parse(nextProps.CTYeuCau.noi_dung_yeu_cau).ngay_sinh,
                noi_sinh: JSON.parse(nextProps.CTYeuCau.noi_dung_yeu_cau).noi_sinh,
                gioi_tinh: JSON.parse(nextProps.CTYeuCau.noi_dung_yeu_cau).gioi_tinh,
                sdt: JSON.parse(nextProps.CTYeuCau.noi_dung_yeu_cau).sdt,
                xoa:nextProps.CTYeuCau.xoa
                }); 
        } 
        if(nextProps.CTSinhVien !== undefined && Object.keys(nextProps.CTSinhVien).length > 0 &&this.props.loai==="1"){
            this.setState({ CTSinhVien: nextProps.CTSinhVien}); 
        }   
        this.setState({ status: nextProps.status })
        if (nextProps.status === "Thành Công") {
            await Swal.fire({
                type: 'success',
                title: 'Thành Công!',
                showConfirmButton: true,
                confirmButtonText: 'Đồng ý'
            })
            this.props.history.push("/yeucau")
        }
    }
    ChapNhanXoaYeuCau = () => {
        this.props.XoaYeuCau(this.props.id, 1)
        this.setState({xoa:1})
    }
    render() {
        console.log(this.props.id, this.props.loai)
        var {nguoi_yeu_cau, ho_ten, ngay_sinh, noi_sinh, gioi_tinh, sdt, CTSinhVien, ho_ten_cha, ho_ten_me, so_dien_thoai, dia_chi, CTPhuHuynh } = this.state;
        var {CTYeuCau} = this.state;
        // console.log(huy_yeu_cau, this.state.huy_yeu_cau)
        const gobal = this;
        return (
            <div id="body-content">
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-md-12">
                            <div className="content-title">
                                <h1 className="text-center h1-title-style">Xử Lý Yêu Cầu Của Sinh Viên</h1>
                                <span className="kitu-dacbiet">&#9884;</span>
                            </div>
                        </div>
                        <div className="col-md-12">
                            {/* <Link to="/vipham/them"  className="button-them--style button-default--style button-thaotac--style">Thêm Vi Phạm</Link> */}
                        </div>
                        <div className="chuc-nang-them" background={khung}>
                            <ul>
                                <li className="nav-link">
                                    <input className="search-data-table pl-1" name="seart-data-table" placeholder="Search..."></input>
                                </li>
                                <li className="nav-link">
                                    <Link to="#" data-toggle="tooltip" title="phân trang">
                                        <i className="fa fa-caret-square-o-down"></i>
                                    </Link>
                                </li>
                                <li className="nav-link">
                                    <button className="btn-an-cot--style" data-toggle="tooltip" title="Ẩn cột">
                                        <i className="fa fa-th"></i>
                                    </button>
                                </li>
                            </ul>
                        </div>
                        <div className="col-md-12">
                            <div className="row">
                                <div className="col-lg-6">
                                    <div className="yeucau-data"> 
                                        <h3 className="m-auto">Thông Tin Vừa Cập Nhật</h3>
                                        <table className={this.props.loai==="1"?"tableSV":"none"} style={{ border: "1px solid #dee2e6" }}>
                                            <tr>
                                                <th style={{ borderBottom: "1px solid white" }} colSpan={4} className="text-center"><label >Mã Sinh Viên:</label></th>
                                                <td><label>{nguoi_yeu_cau}</label></td>
                                            </tr>

                                            <tr>
                                                <th style={{ borderBottom: "1px solid white" }} colSpan={4} className="text-center"> <label >Họ Tên:</label></th>
                                                <td><label>{ho_ten}</label></td>
                                            </tr>
                                            <tr>
                                                <th style={{ borderBottom: "1px solid white" }} colSpan={4} className="text-center"><label >Ngày Sinh:</label></th>
                                                <td><label>{ngay_sinh}</label></td>
                                            </tr>
                                            <tr>
                                                <th style={{ borderBottom: "1px solid white" }} colSpan={4} className="text-center"><label >Giới Tính:</label></th>
                                                <td><label>{gioi_tinh ?"Nam" : "Nữ"}</label></td>
                                            </tr>
                                            <tr>
                                                <th style={{ borderBottom: "1px solid white" }} colSpan={4} className="text-center"><label >Nơi Sinh:</label></th>
                                                <td><label>{noi_sinh}</label></td>
                                            </tr>
                                            <tr>
                                                <th colSpan={4} className="text-center"><label >Điện THoại:</label></th>
                                                <td><label>{sdt}</label></td>
                                            </tr>
                                        </table>  
                                    </div>
                                </div>
                                <div className="col-lg-6">
                                    <div className="yeucau-data">
                                        <h3 className="m-auto">Thông Tin Sinh Viên Hiện Tại</h3>
                                        <table className="tableSV" style={{ border: "1px solid #dee2e6" }}>
                                            <tr>
                                                <th style={{ borderBottom: "1px solid white" }} colSpan={4} className="text-center"><label >Mã Sinh Viên:</label></th>
                                                <td><label>{CTSinhVien.mssv}</label></td>
                                            </tr>

                                            <tr>
                                                <th style={{ borderBottom: "1px solid white" }} colSpan={4} className="text-center"> <label >Họ Tên:</label></th>
                                                <td><label>{CTSinhVien.ho_ten}</label></td>
                                            </tr>
                                            <tr>
                                                <th style={{ borderBottom: "1px solid white" }} colSpan={4} className="text-center"><label >Ngày Sinh:</label></th>
                                                <td><label>{CTSinhVien.ngay_sinh}</label></td>
                                            </tr>
                                            <tr>
                                                <th style={{ borderBottom: "1px solid white" }} colSpan={4} className="text-center"><label >Giới Tính:</label></th>
                                                <td><label>{CTSinhVien.gioi_tinh ? "Nam" : "Nữ"}</label></td>
                                            </tr>
                                            <tr>
                                                <th style={{ borderBottom: "1px solid white" }} colSpan={4} className="text-center"><label >Nơi Sinh:</label></th>
                                                <td><label>{CTSinhVien.noi_sinh}</label></td>
                                            </tr>
                                            <tr>
                                                <th colSpan={4} className="text-center"><label >Điện THoại:</label></th>
                                                <td><label>{CTSinhVien.so_dien_thoai}</label></td>
                                            </tr>
                                        </table> 
                                    </div> 
                                </div>
                                <div className="col-lg-12 col-md-12 text-center"> 
                                    <button className='button-default--style button-thoat--style' type="button" onClick={() => this.ChapNhanXoaYeuCau()}>Hủy Thông Báo</button>
                                    <Link class="quaylai" to="/yeucau">Quay lại...</Link> 
                                </div> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    } 
}
const mapStateToProps = state => ({
    CTYeuCau: state.LayDSYeuCau.CTYeuCau,
    CTSinhVien: state.LayCTSinhVien.CTSinhVien,
    status:state.LayDSSinhVien.status,
    a:console.log(state.LayDSSinhVien.status)
});
xlyeucausv.propTypes = {
    ChiTietYeuCauTheoMaYeuCau: propTypes.func.isRequired,
    ChiTietSinhVienTheoMaSV: propTypes.func.isRequired, 
    XoaYeuCau: propTypes.func.isRequired
}
export default connect(mapStateToProps, { ChiTietYeuCauTheoMaYeuCau, XoaYeuCau, ChiTietSinhVienTheoMaSV })(xlyeucausv);
