import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css'; 
import propTypes from 'prop-types';
import khung from "../assets/img/khung.png";
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import DSAdmin from './DanhSach/ds-admin'
import AdminItem from './Item/admin-item';
import {DanhSachAdmin} from './../actions/admin';
import Pagination from "react-js-pagination";
class admin extends Component {
    constructor(props) {
        super(props);
        this.state = {  
            DSAdmin:[], 
            adminItem:[],
            activePage: '',
            current_page: '',
            total: '',
            per_page: '',
        }  
        this.HienThiAdminItem = this.HienThiAdminItem.bind(this);
    }
    componentDidMount() {
        this.props.DanhSachAdmin();
    }
    componentWillReceiveProps(nextProps) {
        this.setState({ DSAdmin: nextProps.DSAdmin.data,
            current_page: nextProps.DSAdmin.current_page,
            total: nextProps.DSAdmin.total,
            per_page: nextProps.DSAdmin.per_page}); 
    }
    handlePageChange = (pageNumber) => {
        console.log(pageNumber)
        this.state.current_page = pageNumber;
        console.log(`active page is ${pageNumber}`);
        this.setState({ current_page: this.state.current_page });
        this.props.DanhSachAdmin(this.state.current_page);
    } 
    render() {   
        console.log(this.state.DSAdmin)
        return (
            <div id="body-content">
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-md-12">
                            <div className="content-title"> 
                                <h1 className="text-center h1-title-style">Admin</h1>
                                <span className="kitu-dacbiet">&#9884;</span>
                            </div>
                        </div>
                        <div className="col-md-12">
                            <Link to="/admin/them" className="button-them--style button-default--style button-thaotac--style">Thêm Admin</Link>
                        </div>
                        <div className="col-md-12"> 
                            <div className="tableTKB">
                                <div className="chuc-nang-them" background={khung}>
                                    <input className="search-data-table pl-1" name="seart-data-table" placeholder="Search..."></input>
                                </div>
                            <DSAdmin>
                                {()=>this.HienThiAdminItem(this.state.DSAdmin)}
                            </DSAdmin>
                                <div className="locDLTheoTrang">
                                    <Pagination
                                        activePage={this.state.current_page}
                                        itemsCountPerPage={this.state.per_page}
                                        totalItemsCount={this.state.total}
                                        pageRangeDisplayed={8}
                                        onChange={this.handlePageChange}
                                        itemClass="page-item"
                                        linkClass='page-link'
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
    HienThiAdminItem=(adminItem)=>{
        var result = null;
        if(adminItem.length>0){
            result = adminItem.map((item, key)=>{
                return <AdminItem 
                key={key}
                adminItem={item}
                index = {key}
                />
            })
        } 
        return result;
    }
}
const mapStateToProps=state=>({
    DSAdmin: state.LayDSAdmin.DSAdmin,
}); 
admin.propTypes = {
    DanhSachAdmin: propTypes.func.isRequired,
}
export default connect(mapStateToProps, {DanhSachAdmin})(admin);
