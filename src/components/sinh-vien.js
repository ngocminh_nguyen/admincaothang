import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css'; 
import propTypes from 'prop-types';
import khung from "../assets/img/khung.png";
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import DSSinhVien from './DanhSach/ds-sinh-vien';
import SinhVienItem from './Item/sinh-vien-item';
import {DanhSachSinhVien, LayDanhSachSinhVienTheoMSSV} from './../actions/sinh_vien';  
import Pagination from "react-js-pagination";
import $ from 'jquery'
class sinhvien extends Component {
    constructor(props) {
        super(props);
        this.state = {  
            DSSinhVien:[],  
            sinhVienItem:[],
            file:'',
            activePage: '', 
            current_page:'',
            total:'',
            per_page:'', 
        }  
        this.HienThiSinhVienItem = this.HienThiSinhVienItem.bind(this); 
    }
    componentWillMount() {
        this.props.DanhSachSinhVien(0);
    }
    componentWillReceiveProps(nextProps) {
        this.setState({ DSSinhVien: nextProps.DSSinhVien.data, 
                        current_page:nextProps.DSSinhVien.current_page,
                        total:nextProps.DSSinhVien.total,
                        per_page:nextProps.DSSinhVien.per_page
                    }); 
    } 
    handlePageChange=(pageNumber)=> {
        this.state.current_page = pageNumber;
        console.log(`active page is ${pageNumber}`);
        this.setState({current_page: this.state.current_page});
        this.props.DanhSachSinhVien(this.state.current_page);
    } 
    TimKiemTheoMSSV=(e)=>{
        var target = e.target;
        console.log(target.value)
        this.props.LayDanhSachSinhVienTheoMSSV(target.value)
    }
    render() {   
        return ( 
            <div id="body-content">
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-md-12">
                        <div className="content-title"> 
                            <h1 className="text-center h1-title-style">Sinh Viên</h1>
                            <span className="kitu-dacbiet">&#9884;</span>
                        </div>
                        </div> 
                        <div className="col-md-12">
                            <Link to="/sinhvien/them" className="button-them--style button-default--style button-thaotac--style">Thêm Sinh Viên</Link>
                            <Link to="/vipham/sinhvien"  className="button-them--style button-default--style button-thaotac--style">Thêm Vi Phạm Sinh Viên</Link>

                        </div>
                        <div className="col-md-6"> 
                        </div> 
                        {/* <div className="chuc-nang-them" background={khung}> 
                            <ul>
                                <li className="nav-link">
                                    <input className="search-data-table pl-1" name="seart-data-table" placeholder="Search..."></input>
                                </li> 
                            </ul>
                        </div> */}
                        <div className="col-md-12"> 
                            <div className="tableTKB">
                                <div className="chuc-nang-them" background={khung}>
                                    <input className="search-data-table pl-1" name="seart-data-table" placeholder="Search..." onChange={this.TimKiemTheoMSSV}></input>
                                </div>
                                <DSSinhVien>
                                    {() => this.HienThiSinhVienItem(this.state.DSSinhVien)}
                                </DSSinhVien>
                                <div className="locDLTheoTrang">
                                    <Pagination
                                        activePage={this.state.current_page}
                                        itemsCountPerPage={this.state.per_page}
                                        totalItemsCount={this.state.total}
                                        pageRangeDisplayed={8}
                                        onChange={this.handlePageChange}
                                        itemClass="page-item"
                                        linkClass='page-link'
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
        );
    }
    HienThiSinhVienItem=(sinhVienItem)=>{
        var result = null;
        if(sinhVienItem.length>0){
            result = sinhVienItem.map((item, key)=>{
                return <SinhVienItem 
                key={key}
                sinhVienItem={item}
                index = {key}
                />
            })
        } 
        return result;
    }
}
const mapStateToProps=state=>({
    DSSinhVien: state.LayDSSinhVien.DSSinhVien,

    a: console.log(state.LayDSSinhVien.DSSinhVien)
}); 
sinhvien.propTypes = {
    DanhSachSinhVien : propTypes.func.isRequired, 
    LayDanhSachSinhVienTheoMSSV: propTypes.func.isRequired
}
export default connect(mapStateToProps, {DanhSachSinhVien, LayDanhSachSinhVienTheoMSSV })(sinhvien);
