import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
// import logo from "../assets/img/logo.png";
import { Link } from 'react-router-dom';
import { DangXuatAdmin, ChiTietUserTheoMaUser } from './../actions/admin';
import { DanhSachYeuCau, ChiTietYeuCauTheoMaYeuCau } from './../actions/yeu_cau';
import { connect } from 'react-redux';
import propTypes from 'prop-types';
import { withRouter } from "react-router";
import $ from 'jquery'
class header extends Component {
  constructor(props) {
    super(props);
    this.state = { 
      admin: this.props.admin,
      trang_thai_admin: true,
      trang_thai_bring: true,
      trang_thai_menu:true,
      DSYeuCau:[],
      hinh_dai_dien:'',
    };
  }
  componentWillMount(){
    this.props.DanhSachYeuCau();
    this.props.ChiTietUserTheoMaUser(this.props.admin.ma_admin); 
  }
  componentWillReceiveProps(nextProps) { 
    // this.setState({admin: nextProps.admin})
    if(nextProps.DSYeuCau!==undefined)
      this.setState({DSYeuCau:nextProps.DSYeuCau})
    if(nextProps.user!== undefined)
      this.setState({ hinh_dai_dien:nextProps.user.hinh_dai_dien, 
              });   
     
  }
  DangXuat = () => {
    this.props.DangXuatAdmin();
    this.props.history.push("/dangnhap");
  }
  HienThiCaiDat = (classAnHien, classMauActive) => {
    $("." + classAnHien).addClass("none");
    $("." + classMauActive).css("background", "transparent");

    if (classAnHien === "bring-header") {
      $(".admin-header").addClass("none");
      $(".admin-setting").css("background", "transparent");
      $("." + classAnHien).removeClass("none");
      $("." + classMauActive).css("background", "rgba(0,0,0,.2)")
      // $("."+classAnHien).css("transition","0.5s"); 
      var a = !this.state.trang_thai_bring;
      this.setState({ trang_thai_bring: a });
      this.setState({ trang_thai_admin: true });
      if (!this.state.trang_thai_bring) {
        $("." + classAnHien).addClass("none");
        $("." + classMauActive).css("background", "transparent")
      }
    }
    else if (classAnHien === "admin-header") {
      this.setState({ trang_thai_bring: true });
      $(".bring-header").addClass("none");
      $(".bring-setting").css("background", "transparent");
      $("." + classAnHien).removeClass("none");
      $("." + classMauActive).css("background", "rgba(0,0,0,.2)");
      var ab = !this.state.trang_thai_admin
      this.setState({ trang_thai_admin: ab })

      if (!this.state.trang_thai_admin) {
        $("." + classAnHien).addClass("none");
        $("." + classMauActive).css("background", "transparent")
      }
    }

  }
  HienThiMenu=()=>{
    let a = !this.state.trang_thai_menu;
  // console.log(this.state.trang_thai_menu)

    $("#menu-left-mini").css("display","block") 
    $("#menu-left").css("display","block")
    $(".menu-top--fix").css({"left":"250px","transition":"all 0.5s"})
    $("#body-content").css({"left":"250px","transition":"all 0.5s"})
    if(a){
      $("#menu-left").css("animation", "left_to_right 0.5s")
      $("#menu-left-mini").css("display","none") 
      this.setState({trang_thai_menu:true})
    }
    else{ 
      $("#menu-left").css("display","none")
      $("#menu-left-mini").css("animation", "left_to_right 0.5s")
      $(".menu-top--fix").css({"left":"70px","transition":"all 0.5s"})
      $("#body-content").css({"left":"70px","transition":"all 0.5s"})
      this.setState({trang_thai_menu:false})
    }  
  }
  XuLyYeuCau=(ma,loai)=>{
    this.props.history.push(`/yeucau/${ma}/${loai}/chitiet`);
    this.props.ChiTietYeuCauTheoMaYeuCau(ma);
  }
  render() {
    const gobal = this; 
    $("#root").ready(function () {
      $(".wrapper").click(function(){
        $(".admin-header").addClass("none");
      $(".admin-setting").css("background", "transparent");
      $(".bring-header").addClass("none");
      $(".bring-setting").css("background", "transparent");
      })
      var dem =0;
      $("#bangbieu").click(function(){ 
        $(".bangbieu-menu").css("height","135px")
        $(".sinhvien-menu").css("height","0")
        $(".lophoc-menu").css("height","0")
        $(".admin-menu").css("height","0") 
      })
      $("#lophoc").click(function(){ 
        $(".bangbieu-menu").css("height","0")
        $(".sinhvien-menu").css("height","0")
        $(".lophoc-menu").css("height","100px")
        $(".admin-menu").css("height","0") 
      })
      $("#sinhvien").click(function(){ 
        $(".bangbieu-menu").css("height","0")
        $(".sinhvien-menu").css("height","100")
        $(".lophoc-menu").css("height","0")
        $(".admin-menu").css("height","0") 
      })
      $("#admin").click(function(){ 
        $(".bangbieu-menu").css("height","0")
        $(".admin-menu").css("height","100")
        $(".lophoc-menu").css("height","0")
        $(".sinhvien-menu").css("height","0") 
      }) 
      if(dem==1){
        $(".bangbieu-menu").css("height","0")
      }
    }) 
    return (
      <div className="wrapper">
        <div className="sidebarMenu">
          <div id="menu-left" className=" ">
            <div className="menu-left__admin-img">
              <Link to={`/admin/${this.state.admin.ma_admin}/profile`} className="admin-img--style">
                <img src="../assets/img/logo.png" alt="" />
              </Link>
              <h4 className="admin-name--style">Cao Thắng</h4>
              <h6 className="quyen">Technical colleges</h6>
            </div>
            <div id="sidebar-menu" className="main_menu_side hidden-print main_menu text-left">
              <div className="menu_section active">
                <ul className="nav side-menu">
                  <li className="active title" id="bangbieu">
                    <p >
                      <i className="fa fa-home" /> Dữ Liệu Chung
                          <span className="fa fa-chevron-down" />
                    </p>
                    <ul className="nav child_menu bangbieu-menu none" >
                      <li>
                        <Link to="/khoa">Khoa</Link>
                      </li>
                      <li className="current-page">
                        <Link to="/chuyennganh">Chuyên ngành</Link>
                      </li>
                      <li>
                        <Link to="/monhoc">Môn học</Link>
                      </li>
                      <li>
                        <Link to="/giaovien">Giáo Viên</Link>
                      </li>  
                    </ul>
                  </li>
                  <li className="title" id="sinhvien">
                    <p>
                    <i className="fa fa-user"></i> Sinh Viên
                          <span className="fa fa-chevron-down" />
                    </p>
                    <ul className="nav child_menu sinhvien-menu none">
                      <li>
                        <Link to="/phuhuynh">Phụ Huynh</Link>
                      </li>
                      <li>
                        <Link to="/sinhvien">Sinh Viên</Link>
                      </li>
                      {/* <li className="current-page">
                        <Link to="/vipham">Vi phạm</Link>
                      </li> */}
                      <li>
                        <Link to="/kinhphi">Kinh phí</Link>
                      </li>
                    </ul>
                  </li>
                  <li className="title" id="lophoc">
                    <p>
                      <i className="fa fa-home" /> Lớp Học
                          <span className="fa fa-chevron-down" />
                    </p>
                    <ul className="nav child_menu lophoc-menu show none">

                      <li>
                        <Link to="/lop">Lớp Học</Link>
                      </li>
                      <li>
                        <Link to="/lophocphan">Lớp Học Phần</Link>
                      </li> 
                      <li>
                        <Link to="/lichthi">Lịch Thi</Link>
                      </li>
                    </ul>
                  </li> 
                  <li className="title"  id="admin">
                    <p>
                    <i className="fa fa-user"></i> Quản Lý
                          <span className="fa fa-chevron-down" />
                    </p>
                    <ul className="nav child_menu admin-menu show none">
                    <li className="title">
                      <Link to="/admin"> Admin </Link>
                    </li>
                    <li className="title" id="admin">
                      <Link to="/yeucau"> Lịch sử cập nhật </Link>
                    </li>
                    <li className="title" id="admin">
                      <Link to={`/admin/${this.state.admin.ma_admin}/profile`}> Thông Tin </Link>
                    </li>
                  </ul>
                </li>
                </ul>
              </div>
            </div>
          </div>
          <div id="menu-left-mini">
            <ul>
              <li><Link to="">
                <div className="img-admin-logo">
                <img src="../assets/img/logo.png" alt="" />
                </div>
              </Link></li>
              <li className="active"><Link to="/khoa">
                <div className="icon-menu">
                  <i className="fa fa-align-justify"></i>
                  <p>Khoa</p>
                </div>
              </Link></li>
              <li className=""><Link to="/chuyennganh">
                <div className="icon-menu">
                  <i className="fa fa-align-justify"></i>
                  <p>Chuyên Ngành</p>
                </div>
              </Link></li>
              <li className="active"><Link to="/monhoc">
                <div className="icon-menu">
                  <i className="fa fa-align-justify"></i>
                  <p>Môn Học</p>
                </div>
              </Link></li>
              <li><Link to="/giaovien">
                <div className="icon-menu">
                  <i className="fa fa-align-justify"></i>
                  <p>Giáo Viên</p>
                </div>
              </Link></li>
              <li><Link to="/kinhphi">
                <div className="icon-menu">
                  <i className="fa fa-align-justify"></i>
                  <p>Kinh Phí</p>
                </div>
              </Link></li>
              <li><Link to="/lichthi">
                <div className="icon-menu">
                  <i className="fa fa-align-justify"></i>
                  <p>Lịch Thi</p>
                </div>
              </Link></li>
              <li><Link to="/lop">
                <div className="icon-menu">
                  <i className="fa fa-align-justify"></i>
                  <p>Lớp Học</p>
                </div>
              </Link></li>
              <li><Link to="/lophocphan">
                <div className="icon-menu">
                  <i className="fa fa-align-justify"></i>
                  <p>Lớp Học Phần</p>
                </div>
              </Link></li>  
              <li><Link to="/sinhvien">
                <div className="icon-menu">
                  <i className="fa fa-align-justify"></i>
                  <p>Sinh Viên</p>
                </div>
              </Link></li>
              <li><Link to="/vipham">
                <div className="icon-menu">
                  <i className="fa fa-align-justify"></i>
                  <p>Vi Phạm</p>
                </div>
              </Link></li>
              <li><Link to="/phuhuynh">
                <div className="icon-menu">
                  <i className="fa fa-align-justify"></i>
                  <p>Phụ Huynh</p>
                </div>
              </Link></li>
              <li><Link to="/admin">
                <div className="icon-menu">
                  <i className="fa fa-align-justify"></i>
                  <p>Admin</p>
                </div>
              </Link></li>
              {/* <li><Link to="/yeucau">
                <div className="icon-menu">
                  <i className="fa fa-align-justify"></i>
                  <p>Lịch sử cập nhật</p>
                </div>
              </Link></li> */}
            </ul>
          </div>
        </div>
        <div className="content-inner-all">
          {/* <!-- -------------start menu top-------------- --> */}
          <div className="menu-top">
            <div className="menu-top--fix">
              <div className="top-menu">
                <div className="container-fluid">
                  <div className="row">
                    <div className="col-sm-4 col-md-4">
                      <div className="menu-bars input-search--style" id="toggle-menu" onClick={this.HienThiMenu}>
                        <i className="fa fa-bars"></i>
                      </div>
                      {/* <button className="btn btn-light btn-submit--style" type="submit"><i className="fa fa-search"></i></button> */}

                    </div>
                    <div className="col-sm-8 col-md-8">
                      <div className="menu-top__contact">
                        <ul>
                          <li className="nav-link bring-setting">
                            <Link to="#" onClick={() => this.HienThiCaiDat("bring-header", "bring-setting")}>
                              <i className="fa fa-bell-o"></i>
                              <span className="bring">{this.state.DSYeuCau.length}</span>
                            </Link>
                            <div className="bring-header none">
                              {this.state.DSYeuCau.map(function (yeucau, key) {
                                return (
                                  <Link onClick={()=>gobal.XuLyYeuCau(yeucau.nguoi_yeu_cau, yeucau.loai)}
                                  // to={`/yeucau/${yeucau.nguoi_yeu_cau}/${yeucau.loai}/chitiet`}
                                   className="thongbao" key={key}>
                                    <div className="thongbao-left">
                                      <i className="fa fa-bell-o"></i>
                                    </div>
                                    <div className="thongbao-right">
                                      <span>{yeucau.loai==="2"?"Phụ Huynh":"Sinh Viên"}:{yeucau.nguoi_yeu_cau }</span>
                                      <span> Vừa cập nhật thông tin</span>
                                    </div>
                                  </Link>
                                )
                              })} 
                            </div>
                          </li>
                          {/* <li className="nav-link">
                                        <Link to=" ">
                                          <i className="fa fa-envelope-o"></i>
                                          <span className="bring">3</span>
                                        </Link>
                                      </li> */}
                          <li className="nav-link admin-setting">
                            <Link to="#" onClick={() => this.HienThiCaiDat("admin-header", "admin-setting")}>
                              <img src={this.state.hinh_dai_dien} alt=""></img> 
                            </Link>
                            <div className="admin-header none">
                              {/* <ul>
                                <li className="nav-link"><Link to={`/admin/${this.state.admin.ma_admin}/profile`}><i className="fa fa-cog"></i>Profile</Link></li>
                                <li className="nav-link"><Link to="#" onClick={() => this.DangXuat()} style={{ marginTop: -30 }}><i className="fa fa-reply-all"></i>Đăng Xuất</Link></li>
                              </ul> */}
                                <Link to={`/admin/${this.state.admin.ma_admin}/profile`} className="thongbao" >
                                  <div className="thongbao-left">
                                    <i className="fa fa-cog"></i>
                                  </div>
                                  <div className="thongbao-right">
                                    <span>Thông Tin</span>
                                  </div>
                                </Link>
                                <Link to="#" onClick={() => this.DangXuat()} className="thongbao">
                                  <div className="thongbao-left">
                                  <i className="fa fa-reply-all"></i>
                                  </div>
                                  <div className="thongbao-right">
                                    <span>Đăng Xuất</span>
                                  </div>
                                </Link>
                            </div>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          {/* <!-- -------------end menu top-------------- --> */}


        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  isLogin: state.DangNhapAdmin.isLogin,
  user: state.LayCTUser.user,
  admin: state.DangNhapAdmin.admin,  
  DSYeuCau: state.LayDSYeuCau.DSYeuCau, 
});
header.propTypes = {
  DangXuatAdmin: propTypes.func.isRequired,
  ChiTietUserTheoMaUser: propTypes.func.isRequired,
  ChiTietYeuCauTheoMaYeuCau:propTypes.func.isRequired,
}
export default connect(mapStateToProps, { DangXuatAdmin, DanhSachYeuCau, ChiTietUserTheoMaUser, ChiTietYeuCauTheoMaYeuCau})(withRouter(header));
