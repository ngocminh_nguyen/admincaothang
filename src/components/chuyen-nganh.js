import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css'; 
import propTypes from 'prop-types'; 
import khung from "../assets/img/khung.png";
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import DSChuyenNganh from './DanhSach/ds-chuyen-nganh';
import ChuyenNganhItem from './Item/chuyen-nganh-item';
import {DanhSachChuyenNganh} from '../actions/chuyen_nganh';
class chuyennganh extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            trangthai:1,
            DSChuyenNganh:[],
            tasks:'',
            chuyenNganhItem:[],
        }  
        this.HienThiChuyenNganhItem = this.HienThiChuyenNganhItem.bind(this);
    }
    componentDidMount() {
        this.props.DanhSachChuyenNganh();
    }
    componentWillReceiveProps(nextProps) {
        if(nextProps.DSChuyenNganh !== null && nextProps.DSChuyenNganh !== '[]' && nextProps.DSChuyenNganh !== '' && nextProps.DSChuyenNganh !== undefined ){
            this.setState({ DSChuyenNganh: nextProps.DSChuyenNganh});  
        }
    } 
    render() {    
        return (
            <div id="body-content">
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-md-12">
                            <div className="content-title">
                                <h1 className="text-center h1-title-style">Chuyên ngành</h1>
                                <span className="kitu-dacbiet">&#9884;</span>
                            </div>
                        </div>
                        <div className="col-md-12">
                            <Link to="/chuyennganh/them" className="button-them--style button-default--style button-thaotac--style">Thêm Chuyên Ngành</Link>
                        </div>
                        <div className="col-md-12"> 
                            <div className="tableTKB">
                                <div className="chuc-nang-them" background={khung}>
                                    <input className="search-data-table pl-1" name="seart-data-table" placeholder="Search..."></input>
                                </div>
                            <DSChuyenNganh>
                                {()=>this.HienThiChuyenNganhItem(this.state.DSChuyenNganh)}
                            </DSChuyenNganh>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
    HienThiChuyenNganhItem=(chuyenNganhItem)=>{
        var result = null;
        if(chuyenNganhItem.length>0){
            result = chuyenNganhItem.map((item, key)=>{
                return <ChuyenNganhItem 
                key={key}
                chuyenNganhItem={item}
                index = {key}
                />
            })
        } 
        return result;
    }
}
const mapStateToProps=state=>({
    DSChuyenNganh: state.LayDSChuyenNganh.DSChuyenNganh,  
}); 
chuyennganh.propTypes = {
    DanhSachChuyenNganh: propTypes.func.isRequired,
}
export default connect(mapStateToProps, {DanhSachChuyenNganh})(chuyennganh);
