import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css'; 
import propTypes from 'prop-types';
import khung from "../assets/img/khung.png";
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import DSLopHoc from './DanhSach/ds-lop';
import LopHocItem from './Item/lop-item';
import {DanhSachLop } from './../actions/lop';
class lophoc extends Component {
    constructor(props) {
        super(props);
        this.state = {  
            DSLopHoc:[],  
            lopItem:[],
        }  
        this.HienThiLopHocItem = this.HienThiLopHocItem.bind(this);
    }
    componentDidMount() {
        this.props.DanhSachLop();
    }
    componentWillReceiveProps(nextProps) {
        this.setState({ DSLopHoc: nextProps.DSLop}); 
    } 
    render() {   
        return (
            <div id="body-content">
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-md-12">
                        <div className="content-title"> 
                            <h1 className="text-center h1-title-style">Lớp Học</h1>
                            <span className="kitu-dacbiet">&#9884;</span>
                        </div>
                        </div>
                        <div className="col-md-12">
                            <Link to="/lop/them" className="button-them--style button-default--style button-thaotac--style">Thêm Lớp Học</Link>
                        </div>
                        <div className="col-md-12"> 
                            <div className="tableTKB">
                                <div className="chuc-nang-them" background={khung}>
                                    <input className="search-data-table pl-1" name="seart-data-table" placeholder="Search..."></input>
                                </div>
                            <DSLopHoc>
                                {()=>this.HienThiLopHocItem(this.state.DSLopHoc)}
                            </DSLopHoc>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
    HienThiLopHocItem=(lopItem)=>{
        var result = null;
        if(lopItem.length>0){
            result = lopItem.map((item, key)=>{
                return <LopHocItem 
                key={key}
                lopItem={item}
                index = {key} 
                />
            })
        } 
        return result;
    }
}
const mapStateToProps=state=>({
    DSLop: state.LayDSLop.DSLop,
}); 
lophoc.propTypes = {
    DanhSachLop : propTypes.func.isRequired,
}
export default connect(mapStateToProps, {DanhSachLop })(lophoc);
