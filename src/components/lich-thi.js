import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import propTypes from 'prop-types'; 
import {Link} from 'react-router-dom';
import {connect} from 'react-redux'; 
import khung from "../assets/img/khung.png";
import DSLichThi from './DanhSach/ds-lich-thi';
import LichThi from './Item/lich-thi-item';
import {DanhSachLichThi  } from './../actions/lich_thi';
class lichthi extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            trangthai:1,
            DSLichThi:[]
        }  
    }
    componentDidMount() {
        this.props.DanhSachLichThi();
    }
    componentWillReceiveProps(nextProps) {
        this.setState({ DSLichThi: nextProps.DSLichThi}); 
    } 
    render() { 
        return (
            <div id="body-content">
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-md-12">
                            <div className="content-title">
                                <h1 className="text-center h1-title-style">Lịch Thi</h1>
                                <span className="kitu-dacbiet">&#9884;</span>
                            </div>
                        </div>
                        <div className="col-md-12">
                            <Link to='/lichthi/them' className="button-them--style button-default--style button-thaotac--style">Thêm Lịch Thi</Link>
                        </div>
                        <div className="chuc-nang-them" background={khung}>
                            {/* <span className="title-caption">Ẩn thêm cột</span> */}
                            <ul>
                                <li className="nav-link">
                                    <input className="search-data-table pl-1" name="seart-data-table" placeholder="Search..."></input>
                                </li>
                                <li className="nav-link">
                                    <Link to="#" data-toggle="tooltip" title="phân trang">
                                        <i className="fa fa-caret-square-o-down"></i>
                                    </Link>
                                </li>
                                <li className="nav-link">
                                    <button className="btn-an-cot--style" data-toggle="tooltip" title="Ẩn cột">
                                        <i className="fa fa-th"></i>
                                    </button>
                                    <ul className="dropdown-menu" role="menu">
                                        <li role="menuitem">
                                            <label>
                                            <input
                                                type="checkbox"
                                                data-field="id"
                                                defaultValue={1}
                                                defaultChecked="checked"
                                            />{" "}
                                            ID
                                            </label>
                                        </li>
                                        <li role="menuitem">
                                            <label>
                                            <input
                                                type="checkbox"
                                                data-field="name"
                                                defaultValue={2}
                                                defaultChecked="checked"
                                            />{" "}
                                            Project
                                            </label>
                                        </li>
                                        <li role="menuitem">
                                            <label>
                                            <input
                                                type="checkbox"
                                                data-field="email"
                                                defaultValue={3}
                                                defaultChecked="checked"
                                            />{" "}
                                            Email
                                            </label>
                                        </li>
                                         <li role="menuitem">
                                            <label>
                                            <input
                                                type="checkbox"
                                                data-field="email"
                                                defaultValue={3}
                                                defaultChecked="checked"
                                            />{" "}
                                            Email
                                            </label>
                                        </li>
                                    </ul>
                                </li> 
                            </ul>
                        </div>
                        <div className="col-md-12"> 
                            <div className="tableTKB">
                                <div className="chuc-nang-them" background={khung}>
                                    <input className="search-data-table pl-1" name="seart-data-table" placeholder="Search..."></input>
                                </div>
                                <DSLichThi>
                                    {()=>this.HienThiLichThi(this.state.DSLichThi)}
                                </DSLichThi>
                            </div>
                        </div> 
                    </div>
                </div>
            </div>
        );
    }
    HienThiLichThi=(lichThiItem)=>{
        var result = null;
        if(lichThiItem.length>0){
            result = lichThiItem.map((item, key)=>{
                return <LichThi 
                key={key}
                lichThiItem={item}
                index = {key}
                />
            })
        } 
        return result;
    }
}
const mapStateToProps=state=>({
    DSLichThi: state.LayDSLichThi.DSLichThi,
    A:console.log(state.LayDSLichThi.DSLichThi)
}); 
lichthi.propTypes = {
    DanhSachLichThi  : propTypes.func.isRequired,
}
export default connect(mapStateToProps, {DanhSachLichThi  })(lichthi);
