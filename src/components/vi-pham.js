import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';  
import khung from "../assets/img/khung.png";
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import DSViPham from './DanhSach/ds-vi-pham';
import ViPhamItem from './Item/vi-pham-item';
import propTypes from 'prop-types';
import {DanhSachViPham} from './../actions/vi_pham';
class vipham extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            trangthai:1,
            DSViPham:[], 
            viPhamItem:[],
        }  
        this.HienThiViPhamItem = this.HienThiViPhamItem.bind(this);
    }
    componentDidMount() {
        this.props.DanhSachViPham();
    }
    componentWillReceiveProps(nextProps) {
        this.setState({ DSViPham: nextProps.DSViPham}); 
    } 
    render() { 
        return (
            <div id="body-content">
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-md-12">
                        <div className="content-title"> 
                            <h1 className="text-center h1-title-style">Vi Phạm</h1>
                            <span className="kitu-dacbiet">&#9884;</span>
                        </div>
                        </div>
                        <div className="col-md-3"> 
                            <Link to="/vipham/them"  className="button-them--style button-default--style button-thaotac--style">Thêm Vi Phạm</Link>
                        </div> 
                        <div className="col-md-9"> 
                            <Link to="/vipham/sinhvien"  className="button-them--style button-default--style button-thaotac--style">Thêm Vi Phạm Sinh Viên</Link>
                        </div> 
                        <div className="col-md-12"> 
                            <div className="tableTKB">
                                <div className="chuc-nang-them" background={khung}>
                                    <input className="search-data-table pl-1" name="seart-data-table" placeholder="Search..."></input>
                                </div>
                            <DSViPham>
                                {()=>this.HienThiViPhamItem(this.state.DSViPham)}
                            </DSViPham>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
    HienThiViPhamItem=(viPhamItem)=>{
        var result = null;
        if(viPhamItem.length>0){
            result = viPhamItem.map((item, key)=>{
                return <ViPhamItem 
                key={key}
                viPhamItem={item}
                index = {key}
                />
            })
        } 
        return result;
    }
}
const mapStateToProps=state=>({
    DSViPham: state.LayDSViPham.DSViPham,
}); 
vipham.propTypes = {
    DanhSachViPham: propTypes.func.isRequired,
}
export default connect(mapStateToProps, {DanhSachViPham})(vipham);
