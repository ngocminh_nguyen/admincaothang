import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { connect } from 'react-redux';
import propTypes from 'prop-types';
import { withRouter } from "react-router";
import { Link } from 'react-router-dom';
import { DangNhapAdmin, XacThucTaiKhoan, KiemTraMaOTP, CapNhatMatKhau } from './../actions/admin';
import firebase from "firebase/app";
import "firebase/messaging";
import Swal from 'sweetalert2'; 
import $ from 'jquery';
import * as func from '../constants/FunctionsKTDuLieu_ThongBao';   
const config = {
    apiKey: "AIzaSyBWC0lUSDZxwZn5x5jIRpLfXxtDImufUL0",
    authDomain: "appckc-4105e.firebaseapp.com",
    databaseURL: "https://appckc-4105e.firebaseio.com",
    projectId: "appckc-4105e",
    storageBucket: "appckc-4105e.appspot.com",
    messagingSenderId: "966589584312"
};

export const initFirebase = () => {
    firebase.initializeApp(config);
}; 
class dangnhap extends Component {
    constructor(props) {
        super(props);
        this.state = {
            ten_tai_khoan: '', 
            isLogin: false,
            notLogin: false,
            admin: this.props.admin,
            token:'',
            ten_dang_nhap:'',
            email:'',
            dulieutrave:'', 
            ten_dang_nhap_bienTam:'',
            otp:'',
            mat_khau_cu:'',
            mat_khau_moi:'',
            nhap_lai_mat_khau_moi:'',
            hienthi_doiMK:false,
            hienthi_ktTaikhoan:false,
            hienthi_ktOTP:false,
            
        };
        this.onChange = this.onChange.bind(this);
        this.DangNhap = this.DangNhap.bind(this);
        this.permissionToReceiveNotification = this.permissionToReceiveNotification.bind(this)
    }
    permissionToReceiveNotification = async () => { 
            const messaging = firebase.messaging();
            await messaging.requestPermission();
            var token = await messaging.getToken();
            this.setState({ token: token })  
    }
    componentWillMount=()=>{
        this.permissionToReceiveNotification();
    }
    async componentWillReceiveProps(nextProps) { 
        if (nextProps.isLogin) {
            this.setState({
                isLogin: true,
                admin: nextProps.admin
            }) 
            await Swal.fire({
                type: 'success',
                title: 'Thành Công!',
                showConfirmButton: true,
                confirmButtonText: 'Đồng ý'
            })
            this.props.history.push("/admin")
        }
        else {
            this.setState({ notLogin: true })
        }   
        console.log(nextProps.XacThucTK )
        if (nextProps.XacThucTK !== "" && nextProps.XacThucTK !== undefined) {
            // $(".TrangKiemTraMaOTP").removeClass("none");
            // $(".TrangXacThucTaiKhoan").addClass("none");
            this.setState({hienthi_ktOTP:true})
            if(this.state.hienthi_ktOTP == true){
                this.setState({hienthi_ktOTP:false})
            }
        }
        if (nextProps.XacThucOTP === "Thành Công") {
            // $(".TrangKiemTraMaOTP").addClass("none");
            // $(".TrangDoiMatKhau").removeClass("none");
            this.setState({hienthi_doiMK:true})
        } 
    }
    onChange(event) {
        var target = event.target;
        var name = target.name;
        var value = target.value;
        this.setState({ [name]: value })
    }
    DangNhap() { 
      
        // console.log(a, this.state.token1)
        this.props.DangNhapAdmin(this.state.ten_tai_khoan, this.state.mat_khau, this.state.token);
       
    }
    TroVe=(dulieu)=>{
        
        console.log()
        if(dulieu == "dangnhap"){
            this.props.history.push("/admin");
             $(".TrangXacThucTaiKhoan").removeClass("none"); 
            this.state.ten_tai_khoan="";
            this.setState({ten_tai_khoan:this.state.ten_tai_khoan})
        }
        if(dulieu == "ktTaiKhoan"){
            // $(".TrangKiemTraMaOTP").addClass("none");
            // $(".TrangXacThucTaiKhoan").removeClass("none"); 
            this.setState({hienthi_ktOTP:false})
            // alert("otp")
        }
        else if(dulieu === 'ktOTP'){
            // $(".TrangDangNhapAdmin").addClass("none");
            // $(".TrangKiemTraMaOTP").romoveClass("none");
            // $(".TrangXacThucTaiKhoan").addClass("none");
            // alert("taikhoan")
        }
        else{
            // this.props.history.push("/admin");
        }
    } 
    QuenMatKhau=()=>{ 
        // $(".TrangDangNhapAdmin").addClass("none");
        // $(".TrangXacThucTaiKhoan").removeClass("none")
        this.setState({hienthi_ktTaikhoan: true})
    }
    XacThucTaiKhoan=async()=>{  
        this.props.XacThucTaiKhoan(this.state.ten_dang_nhap); 
        await this.setState({ten_dang_nhap_bienTam:this.state.ten_dang_nhap})  
    } 
    KiemTraMaOTP=()=>{ 
        console.log(this.state.otp)
        this.props.KiemTraMaOTP(this.state.ten_dang_nhap_bienTam, this.state.otp) 
    }
    CapNhatMatKhau=async()=>{
        if(this.state.mat_khau_moi === this.state.nhap_lai_mat_khau_moi){
            this.props.CapNhatMatKhau(this.state.ten_dang_nhap_bienTam, this.state.otp, this.state.mat_khau_moi)
            await this.props.DangNhapAdmin(this.state.ten_dang_nhap_bienTam, this.state.mat_khau_moi, this.state.token); 
        }
        else{
            func.ThongBaoMaLoi("Mật khẩu không trùng khớp")
        } 
    }
    render() { 
        // console.log(this.state.dulieutrave) 
        console.log(this.state.token) 
        return (
            <div className="wrapper-admin">
                <div className="background-admin"> 
                    <div className="trangdangnhap TrangDangNhapAdmin">
                        <div className="admin-img">
                            <img src="../assets/img/DN.jpg"/>
                        </div>
                        <div className="button-dangnhap">
                            <div className="input--style"> 
                                <span className="spanTenTK">
                                    Tên tài khoản
                                </span>
                                <input className="inputTenTK" id="KTTenTK" value={this.state.ten_tai_khoan} type="text" required   name="ten_tai_khoan" onChange={(value) => this.onChange(value)} />
                            </div>
                            <div className="input--style">
                                <span className="spanMatKhau">
                                    Mật Khẩu
                                </span>
                                <input className="inputMatKhau" type="password" required  name="mat_khau" onChange={(value) => this.onChange(value)} />
                            </div>
                            <br />
                            <Link to="#" className="quen-mat-khau--style"  onClick={()=>this.QuenMatKhau()}>Bạn quên mật khẩu?</Link>
                            <button style={{float:"right"}} onClick={()=>this.DangNhap() } className="button-login--style" type="button"  >Đăng Nhập
                            </button>
                        </div>
                    </div>
                    <div className={this.state.hienthi_ktTaikhoan?"trangdangnhap TrangXacThucTaiKhoan ":"trangdangnhap TrangXacThucTaiKhoan none"}>
                        <div className="admin-img"> 
                            <img  src="../assets/img/XNTK.jpg" alt="a"/>
                        </div>
                        <div className="button-dangnhap mt-4">
                            <div className="input--style">
                                <span className="spanTenTK">
                                    Tên tài khoản
                                </span>
                                <input className="inputTenTK" id="KTTenTK" value={this.state.ten_dang_nhap} type="text" required name="ten_dang_nhap" onChange={(value) => this.onChange(value)} />
                            </div>  
                            <Link to="#" className="quen-mat-khau--style"  onClick={()=>this.TroVe("dangnhap")}>Trở về</Link>
                            <button style={{float:"right"}}  onClick={()=>this.XacThucTaiKhoan() } className="button-login--style" type="button" id="btnXacThucTK" >Gửi
                            </button> 
                        </div>
                    </div> 
                    <div className={this.state.hienthi_ktOTP?"trangdangnhap TrangKiemTraMaOTP":"trangdangnhap TrangKiemTraMaOTP none"}>
                        <div className="admin-img"> 
                            <img  src="../assets/img/XNOTP.jpg" alt="a" width="67%"/>
                        </div>
                        <div className="button-dangnhap">
                            <div className="input--style"> 
                               <h2>{this.state.ten_dang_nhap_bienTam}</h2>
                            </div>
                            <div className="input--style">
                                <span className="spanMatKhau">
                                    Nhập Mã OTP Của Bạn
                                </span>
                                <input className="inputMatKhau" value={this.state.otp} type="text" required  name="otp" onChange={(value) => this.onChange(value)} />
                            </div>
                            <br /> 
                            <Link to="#" className="quen-mat-khau--style"  onClick={()=>this.TroVe("ktTaiKhoan")}>Trở về</Link>
                            <button style={{float:"right"}}  onClick={()=>this.KiemTraMaOTP() } className="button-login--style" type="button"  >Xác thực
                            </button>
                           
                        </div>
                    </div>
                    <div className={this.state.hienthi_doiMK?"trangdangnhap TrangDoiMatKhau":"trangdangnhap TrangDoiMatKhau none"}>
                        <div className="admin-img">
                            <img  src="../assets/img/DMK.jpg" alt="a" width="65%"/>
                        </div>
                        <div className="button-dangnhap pt-0"> 
                            <div className="input--style mb-4">
                                <span className="spanMatKhauMoi">
                                    Mật Khẩu Mới
                                        </span>
                                <input className="inputMatKhauMoi mb-0" type="password"   name="mat_khau_moi"
                                  onKeyUp={func.KiemTraDuLieuNhapLaMatKhau}
                                            onFocus={func.KiemTraNhapDuLieu}  onChange={(value) => this.onChange(value)} value={this.state.mat_khau_moi} />
                                <p className="mess-err none" id="mat_khau_moi">Mật khẩu phải lớn hơn 7 ký tự</p>
                            </div>
                            <div className="input--style">
                                <span className="spanNhapLaiMatKhau">
                                    Nhập Lại Mật Khẩu Mới
                                        </span>
                                <input className="inputNhapLaiMatKhau mb-0" type="password" required name="nhap_lai_mat_khau_moi" 
                                onKeyUp={func.KiemTraDuLieuNhapLaMatKhau}   
                                onFocus={func.KiemTraNhapDuLieu}
                                                onChange={(value) => this.onChange(value)} value={this.state.nhap_lai_mat_khau_moi} />
                                <p className="mess-err none" id="nhap_lai_mat_khau_moi">Mật khẩu phải lớn hơn 7 ký tự</p>
                            </div>
                            <br/>
                            <br/> 
                            <button style={{float:"right"}}  onClick={() => this.CapNhatMatKhau()} className="button-login--style" type="button"  >Gửi
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
const mapStateToProps = state => ({
    isLogin: state.DangNhapAdmin.isLogin, 
    admin: state.DangNhapAdmin.admin, 
    XacThucTK:state.LayDLKiemTraTaiKhoan.XacThucTK,
    XacThucOTP: state.LayOTPKiemTraTaiKhoan.XacThucOTP,
    // statusCNMK: state.LayOTPKiemTraTaiKhoan.statusCNMK,
    a: console.log(state.LayDLKiemTraTaiKhoan.XacThucTK),
    b: console.log(state.LayOTPKiemTraTaiKhoan.XacThucOTP),
    
});
dangnhap.propTypes = {
    DangNhapAdmin: propTypes.func.isRequired,
    XacThucTaiKhoan: propTypes.func.isRequired,
    KiemTraMaOTP: propTypes.func.isRequired,
    CapNhatMatKhau: propTypes.func.isRequired
}
export default connect(mapStateToProps, { DangNhapAdmin, XacThucTaiKhoan, KiemTraMaOTP,CapNhatMatKhau })(withRouter(dangnhap));
