import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';  
import khung from "../assets/img/khung.png";
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import DSGiaoVien from './DanhSach/ds-giao-vien';
import GiaoVienItem from './Item/giao-vien-item';
import propTypes from 'prop-types';
import {DanhSachGiaoVien, TimGiaoVienTheoTen} from './../actions/giao_vien';
import Pagination from "react-js-pagination";
class giaovien extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            trangthai:1,
            DSGiaoVien:[], 
            giaoVienItem:[],
            activePage: '', 
            current_page:'',
            total:'',
            per_page:'', 
        }  
        this.HienThiGiaoVienItem = this.HienThiGiaoVienItem.bind(this);
    }
    componentDidMount() {
        this.props.DanhSachGiaoVien(0);
    }
    componentWillReceiveProps(nextProps) {
        this.setState({ DSGiaoVien: nextProps.DSGiaoVien.data,
                        current_page:nextProps.DSGiaoVien.current_page,
                        total:nextProps.DSGiaoVien.total,
                        per_page:nextProps.DSGiaoVien.per_page}); 
    } 
    handlePageChange=(pageNumber)=> {
        this.state.current_page = pageNumber;
        console.log(`active page is ${pageNumber}`);
        this.setState({current_page: this.state.current_page});
        this.props.DanhSachGiaoVien(this.state.current_page);
    } 
    TimGiaoVienTheoTen=(e)=>{
        var target = e.target;
        console.log(target.value)
        this.props.TimGiaoVienTheoTen(target.value);
        // alert('1')
    }
    render() {   
        return (
            <div id="body-content">
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-md-12">
                            <div className="content-title"> 
                                <h1 className="text-center h1-title-style">Giáo Viên</h1>
                                <span className="kitu-dacbiet">&#9884;</span>
                            </div>
                        </div>
                        <div className="col-md-12">
                            <Link to="/giaovien/them" className="button-them--style button-default--style button-thaotac--style">Thêm Giáo Viên</Link>
                        </div>
                        <div className="col-md-12"> 
                            <div className="tableTKB">
                                <div className="chuc-nang-them" background={khung}>
                                    <input className="search-data-table pl-1" name="seart-data-table" placeholder="Search..." onChange={this.TimGiaoVienTheoTen}></input>
                                </div>
                            <DSGiaoVien>
                                {()=>this.HienThiGiaoVienItem(this.state.DSGiaoVien)}
                            </DSGiaoVien>
                            <div className="locDLTheoTrang">
                                    <Pagination
                                        activePage={this.state.current_page}
                                        itemsCountPerPage={this.state.per_page}
                                        totalItemsCount={this.state.total}
                                        pageRangeDisplayed={8}
                                        onChange={this.handlePageChange}
                                        itemClass="page-item"
                                        linkClass='page-link'
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
    HienThiGiaoVienItem=(giaoVienItem)=>{
        var result = null;
        if(giaoVienItem.length>0){
            result = giaoVienItem.map((item, key)=>{
                return <GiaoVienItem 
                key={key}
                giaoVienItem={item}
                index = {key}
                />
            })
        } 
        return result;
    }
}
const mapStateToProps=state=>({
    DSGiaoVien: state.LayDSGiaoVien.DSGiaoVien,
    a:console.log(state.LayDSGiaoVien.DSGiaoVien)
}); 
giaovien.propTypes = {
    DanhSachGiaoVien: propTypes.func.isRequired,
    TimGiaoVienTheoTen:  propTypes.func.isRequired,
}
export default connect(mapStateToProps, {DanhSachGiaoVien, TimGiaoVienTheoTen})(giaovien);