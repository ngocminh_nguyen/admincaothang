import * as types from '../constants/constants'; 
import axios from "axios";
export function DanhSachLichThi() {
    return dispatch => {
        return axios({
            method: 'GET',
            url: types.server + `lich_thi/lay-danh-sach`,
            data:  null,  
        }).then(result =>{
            dispatch({
                type: types.DANH_SACH_LICH_THI, 
                payload: result.data,
            })
        })
        .catch(err => {
            // if (err.response.status===500){
            //     types.ThongBaoThatBai(); 
            // }
            // else if (err.response.status===401){
            //     types.ThongBaoKhongTimThay()
            // }
            // else{
            //     types.ThongBaoDuLieuKhongTonTai() 
            // }
        }) 
    }
}
export function ChiTietLichThiTheoMaLichThi(maLT, loai) {
    return dispatch => {
        return axios({
            method: 'GET',
            url: types.server + `lich_thi/${maLT}/chi-tiet?loai=${loai}`,
            data:  null,
        }).then(result => { 
                dispatch({
                    type: types.CHI_TIET_LICH_THI_THEO_MA_LICH_THI,
                    payload:  result.data,  
                })  
            }
        ).catch(err => {
            // if (err.response.status===500){
            //     // types.ThongBaoThatBai(); 
            // }
            // else if (err.response.status===401){
            //     types.ThongBaoKhongTimThay()
            // }
            // else{
            //     types.ThongBaoDuLieuKhongTonTai() 
            // }
        }) 
    }
}; 
export function ThemLichThi(LT) { 
    console.log(LT.phong_thi)
    return dispatch => {
        return axios({
            method: 'POST',
            url: types.server+'lich_thi/them-moi',
            data: {  
                ma_lop  :LT.ma_lop  , 
                ngay_thi :LT.ngay_thi , 
                gio_thi :LT.gio_thi , 
                phong_thi :JSON.stringify(LT.phong_thi),
                loai  :LT.loai , 
            }
        })
        .then(result => {
            if (result.data != null) {
                dispatch({
                    type: types.THEM_LICH_THI,
                    payload: true
                }) 
            }
            types.ThongBaoThanhCong();
        }).catch(err => {
            if (err.response.status===500){
                types.ThongBaoThatBai(); 
            }
            else if (err.response.status===401){
                types.ThongBaoKhongTimThay()
            }
            else{
                types.ThongBaoDuLieuKhongTonTai() 
            }
        }) 
    }
};
export function CapNhatLichThi(LT) { 
    console.log(LT)
    return dispatch => {
        return axios({
            method: 'POST',
            url: types.server + `lich_thi/${LT.ma_lich_thi}/cap-nhat`,
            data: {  
                ma_lop  :LT.ma_lop  ,
                ngay_thi :LT.ngay_thi , 
                gio_thi :LT.gio_thi , 
                phong_thi :JSON.stringify(LT.phong_thi) ,
                loai  :LT.loai , 
            }
        }).then(result => { 
                dispatch({
                    type: types.CAP_NHAT_LICH_THI,
                    payload: result
                })  
            types.ThongBaoThanhCong()

            }
        ).catch(err => {
            if (err.response.status===500){
                types.ThongBaoThatBai(); 
            }
            else if (err.response.status===401){
                types.ThongBaoKhongTimThay()
            }
            else{
                types.ThongBaoDuLieuKhongTonTai() 
            }
        }) 
    }
};
export function XoaLichThi(maLT,trangthai){
    return dispatch => {
        return axios({
            method: 'POST',
            url: types.server + `lich_thi/${maLT}/xoa?trang_thai=${trangthai}`,
            data: {
                ma_lich_thi : maLT,
                xoa: trangthai,
            }
        }).then(result => {
            if (result.data != null) {
                dispatch({
                    type: types.XOA_LICH_THI,
                    payload: true
                }) 
            }  
        }).catch(err => {
            if (err.response.status===500){
                types.ThongBaoThatBai(); 
            }
            else if (err.response.status===401){
                types.ThongBaoKhongTimThay()
            }
            else{
                types.ThongBaoDuLieuKhongTonTai() 
            }
        }) 
    }
};