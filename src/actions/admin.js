import * as types from '../constants/constants';
import axios from "axios";
import $ from 'jquery'
import { showLoading, hideLoading } from 'react-redux-loading-bar'
export function DanhSachAdmin(pageNumber) {
    if (pageNumber < 0) {
    return dispatch => {
        return axios({
            method: 'GET',
            url: types.server + `admin/lay-danh-sach`,
            data: null,
        }).then(result => {
            dispatch({
                type: types.DANH_SACH_ADMIN,
                payload: result.data
            })
            showLoading('sectionBar')
        }).catch(err => {
            hideLoading('sectionBar')
        })
    }}else{
        return dispatch => {
            return axios({
                method: 'GET',
                url: types.server + `admin/lay-danh-sach?page=${pageNumber}`,
                data: null
            })
                .then(result => {
                    dispatch({
                        type: types.DANH_SACH_ADMIN,
                        payload: result.data
                    });
                }).catch(err => {
                })
        }
    }
}
export function ChiTietAdminTheoMaAdmin(admin) { 
    return dispatch => {
        return axios({
            method: 'GET',
            url: types.server + `admin/${admin}/chi-tiet`,
            data: null,
        }).then(result => {
            dispatch({
                type: types.CHI_TIET_ADMIN_THEO_MA_ADMIN,
                payload: result.data
            })
        }
        ).catch(err => { 
        }) 
    }
};
export function ChiTietUserTheoMaUser(admin) { 
    return dispatch => {
        return axios({
            method: 'GET',
            url: types.server + `admin/${admin}/chi-tiet`,
            data: null,
        }).then(result => {
            dispatch({
                type: types.CHI_TIET_USER_THEO_MA_USER,
                payload: result.data
            })
        }
        ).catch(err => { 
        }) 
    }
};
export function ThemAdmin(Admin) {
    return dispatch => { 
        let url_hinh_anh = '';
        if (Admin.hinh_dai_dien != '') {
            const data = new FormData()
            var x = Math.floor((Math.random() * 10) + 1);
            var y = Math.floor((Math.random() * 100) + 1);
            let ten_hinh_anh = x + y.toString() + '_Admin.jpg';
            data.append('hinh_dai_dien', Admin.hinh_dai_dien, ten_hinh_anh);
            url_hinh_anh = types.serverUploadHinh + ten_hinh_anh;
            axios.post(types.server + "sinh_vien/upload-anh", data);
        }
        else{
            url_hinh_anh = 'none';
        }
        return axios({
            method: 'POST',
            url: types.server + 'admin/them-moi',
            data: {
                ma_admin: Admin.ma_admin,
                ten_dang_nhap: Admin.ten_dang_nhap,
                mat_khau: Admin.mat_khau,
                hinh_dai_dien: url_hinh_anh,
                ho_ten: Admin.ho_ten,
                email: Admin.email,
                sdt: Admin.sdt,
                fcmToken : Admin.token
            }
        })
            .then(result => {
                if (result.data != null) {
                    dispatch({
                        type: types.THEM_ADMIN,
                        payload: true
                    })
                }
                types.ThongBaoThanhCong();
            }).catch(err => {
                if (err.response.status === 500){
                    types.ThongBaoThatBai(); 
                }
                else if (err.response.status === 400){
                    types.ThongBaoMaLoi(err.response.data)
                }
                else if (err.response.status === 403){
                    types.ThongBaoMaLoi(err.response.data)
                }
                else{
                    types.ThongBaoDuLieuKhongTonTai() 
                }
            }) 
    }
};
export function CapNhatAdmin(Admin) {
    return dispatch => { 
        let url_hinh_anh = '';
        if (Admin.dachonhinh) {
            const data = new FormData()
            var x = Math.floor((Math.random() * 10) + 1);
            var y = Math.floor((Math.random() * 100) + 1);
            let ten_hinh_anh = x + y.toString() + '_Admin.jpg';
            data.append('hinh_dai_dien', Admin.hinh_anh, ten_hinh_anh);
            url_hinh_anh = types.serverUploadHinh+ ten_hinh_anh;
            axios.post(types.server + "sinh_vien/upload-anh", data); 
        }
        else{
            url_hinh_anh = Admin.hinh_dai_dien;
        } 
        return axios({
            method: 'POST',
            url: types.server + `admin/${Admin.ma_admin}/cap-nhat`,
            data: {
                ma_admin: Admin.ma_admin,
                ten_dang_nhap: Admin.ten_dang_nhap, 
                hinh_dai_dien: url_hinh_anh,
                ho_ten: Admin.ho_ten,
                email: Admin.email,
                sdt: Admin.sdt
            }
        }).then(result => {
            dispatch({
                type: types.CAP_NHAT_ADMIN,
                payload: {
                    status: result,
                    admin:{
                        ma_admin: Admin.ma_admin,
                        ten_dang_nhap: Admin.ten_dang_nhap, 
                        hinh_dai_dien: url_hinh_anh,
                        ho_ten: Admin.ho_ten,
                        email: Admin.email,
                        sdt: Admin.sdt
                    }
                }
            }) 
        }
        ).catch(err => {
            if (err.response.status === 500){
                types.ThongBaoThatBai(); 
            }
            else if (err.response.status === 400){
                types.ThongBaoMaLoi(err.response.data)
            }
            else if (err.response.status === 403){
                types.ThongBaoMaLoi(err.response.data)
            }
            else{
                types.ThongBaoDuLieuKhongTonTai() 
            }
        }) 
    }
};
export function XoaAdmin(admin, trangthai) {
    return dispatch => {
        return axios({
            method: 'POST',
            url: types.server + `admin/${admin}/xoa?trang_thai=${trangthai}`,
            data: {
                ma_admin: admin,
                xoa: trangthai,
            }
        }).then(result => {
            if (result.data != null) {
                dispatch({
                    type: types.XOA_ADMIN,
                    payload: true
                })
            }
        }).catch(err => {
            if (err.response.status === 500){
                types.ThongBaoThatBai(); 
            }
            else if (err.response.status === 401){
                types.ThongBaoKhongTimThay()
            }
            else{
                types.ThongBaoDuLieuKhongTonTai() 
            }
        }) 
    }
};
export function DangNhapAdmin(admin, matkhau, token) {
    return dispatch => {
        return axios({
            method: 'POST',
            url: types.server + `admin/dang-nhap`,
            data: {
                ten_dang_nhap: admin,
                mat_khau: matkhau,
                fcmToken: token
            }
        }).then(result => {
            sessionStorage.setItem('token',result.data.jwt );
            sessionStorage.setItem('admin', JSON.stringify(result.data));
                dispatch({
                type: types.DANG_NHAP_ADMIN,
                payload: result.data
            })
            // types.ThongBaoThanhCong(); 
        }).catch(err => {
            if (err.response.status === 500){
                types.ThongBaoThatBai(); 
            }
            else if (err.response.status === 401){
                types.ThongBaoKhongTimThay()
            }
            else if (err.response.status === 404){
                types.ThongBaoMaLoi(err.response.data)
            }
            else{
                types.ThongBaoDuLieuKhongTonTai() 
            }
        }) 
    }
}; 
export function DangXuatAdmin() {
    return dispatch => {
        sessionStorage.removeItem('admin'); 
        sessionStorage.removeItem('token');
        dispatch({
            type: types.DANG_XUAT_ADMIN,
        })
    }
}
export function XacThucTaiKhoan(tendangnhap){
    return dispatch => {
        return axios({
            method: 'POST',
            url: types.server + `admin/gui-ma-otp-email`,
            data: {
                ten_dang_nhap: tendangnhap,
            }
        }).then(result => {  
                dispatch({
                type: types.XAC_THUC_TAI_KHOAN,
                payload: result.data,
            }) 
            types.ThongBaoThanhCong();
            
        }).catch(err => {
            if (err.response.status === 500){
                types.ThongBaoThatBai(); 
            }
            else if (err.response.status === 401){
                types.ThongBaoKhongTimThay()
            }
            else if (err.response.status === 404){
                types.ThongBaoMaLoi(err.response.data)
            }
            else{
                types.ThongBaoDuLieuKhongTonTai() 
            }
        }) 
    }
}
export function KiemTraMaOTP(tendangnhap, otp){
    return dispatch => {
        console.log(tendangnhap, "otp:" ,otp)
        return axios({
            method: 'POST',
            url: types.server + `admin/kiem-tra-ma-otp`,
            data: {
                ten_dang_nhap: tendangnhap,
                otp: otp, 
            }
        }).then(result => {  
                dispatch({
                type: types.KIEM_TRA_MA_OTP,
                payload: result.data,
            }) 
            types.ThongBaoThanhCong(); 
            
        }).catch(err => {
            if (err.response.status === 500){
                types.ThongBaoThatBai(); 
            }
            else if (err.response.status === 401){
                types.ThongBaoKhongTimThay()
            }
            else if (err.response.status === 403){
                types.ThongBaoMaLoi(err.response.data)
            }
            else if (err.response.status === 404){
                types.ThongBaoMaLoi(err.response.data)
            }
        }) 
    }
}
export function CapNhatMatKhau(tendangnhap, otp, mk){
    return dispatch => {
        return axios({
            method: 'POST',
            url: types.server + `admin/cap-nhat-mat-khau`,
            data: {
                ten_dang_nhap: tendangnhap,
                otp: otp, 
                mat_khau: mk,
                trang_thai_otp:'0'
            }
        }).then(result => {  
                dispatch({
                type: types.CAP_NHAT_MAT_KHAU,
                payload: result
            })  
        }).catch(err => {
            if (err.response.status === 500){
                types.ThongBaoThatBai(); 
            }
            else if (err.response.status === 401){
                types.ThongBaoKhongTimThay()
            }
            else if (err.response.status === 400){
                types.ThongBaoMaLoi(err.response.data)
            }
            else{
                types.ThongBaoDuLieuKhongTonTai() 
            }
        }) 
    }
}
export function DoiAvatarAdmin(ma_admin, hinh_dai_dien ) {
    return dispatch => {
        let url_hinh_anh="";
        const data = new FormData();
        data.append('hinh_dai_dien',hinh_dai_dien, hinh_dai_dien.name);
        url_hinh_anh = types.serverUploadHinh + hinh_dai_dien.name;
        axios.post(types.server + "sinh_vien/upload-anh", data); 
        return axios({
            method: 'POST',
            url: types.server + `admin/${ma_admin}/cap-nhat-avatar`,
            data: {
                ma_admin : ma_admin ,
                hinh_dai_dien : url_hinh_anh ,  
            }
        }).then(result => {  
            let admin = JSON.parse(sessionStorage.getItem("admin"));
            admin.hinh_dai_dien = url_hinh_anh;
                dispatch({
                type: types.CAP_NHAT_AVATAR_ADMIN,
                payload: admin
            }) 
            types.ThongBaoThanhCong();
        }).catch(err => {
            if (err.response.status === 500){
                types.ThongBaoThatBai(); 
            }
            else if (err.response.status === 401){
                types.ThongBaoKhongTimThay()
            }
            else{
                types.ThongBaoDuLieuKhongTonTai() 
            }
        }) 
    }
};
export function DoiMatKhau(ma_admin , mat_khau_cu, mat_khau_moi){
    return dispatch => {
        return axios({
            method: 'POST',
            url: types.server + `admin/${ma_admin}/doi-mat-khau`,
            data: {
                ma_admin : ma_admin,
                mat_khau_cu : mat_khau_cu , 
                mat_khau_moi : mat_khau_moi , 
            }
        }).then(result => {  
                dispatch({
                type: types.DOI_MAT_KHAU,
                payload: result.data
            }) 
            types.ThongBaoThanhCong();
        }).catch(err => {
            if (err.response.status === 500){
                types.ThongBaoMaLoi("Mật Khẩu Cũ Sai!")
            }
            else if (err.response.status === 401){
                types.ThongBaoKhongTimThay()
            }
            else if (err.response.status === 404){
                types.ThongBaoMaLoi("Mật Khẩu Cũ Sai!")
            }
            else{
                types.ThongBaoDuLieuKhongTonTai() 
            }
        }) 
    }
}