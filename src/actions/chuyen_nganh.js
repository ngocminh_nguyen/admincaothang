import * as types from '../constants/constants'; 
import axios from "axios";
export function DanhSachChuyenNganh() {
    return dispatch => {
        return axios({
            method: 'GET',
            url: types.server + 'chuyen_nganh/lay-danh-sach',
            data: null
        }).then(result => {
            dispatch({
                type: types.DANH_SACH_CHUYEN_NGANH,
                payload: result.data
            });
        })
    }
};
export function ChiTietChuyenNganhTheoMaSo(maso) {
    return dispatch => {
        return axios({
            method: 'GET',
            url: types.server + `chuyen_nganh/${maso}/chi-tiet`,
            data:  null,
        }).then(result => { 
                dispatch({
                    type: types.CHI_TIET_CHUYEN_NGANH_THEO_MA_SO,
                    payload:  result.data
                })  
            }
        ).catch(err => {
        }) 
    }
};
export function ThemChuyenNganhVaoDS(item) {
    return dispatch => {
        return axios({
            method: 'POST',
            url: types.server + 'chuyen_nganh/them-moi',
            data: {
                ma_so:item.ma_so,
                ma_chu: item.ma_chu,
                ten_chuyen_nganh: item.ten_chuyen_nganh,
                ma_khoa: item.ma_khoa, 
            },
        }).then(result => {
            dispatch({
                type: types.THEM_CHUYEN_NGANH,
                payload: true,
            });
            types.ThongBaoThanhCong()
        }).catch(err => {
                types.ThongBaoMaLoi(err.response.data);
            })
    }
};
export function CapNhatChuyenNganh(item) {
    return dispatch => {
        return axios({
            method: 'POST',
            url: types.server + `chuyen_nganh/cap-nhat`,
            data: {
                ma_chu: item.ma_chu,
                ma_so: item.ma_so,
                ten_chuyen_nganh: item.ten_chuyen_nganh,
                ma_khoa: item.ma_khoa
            }
        }).then(result => { 
                dispatch({
                    type: types.CAP_NHAT_CHUYEN_NGANH,
                    payload: result
                })  
            }
        ).catch(err => {
            types.ThongBaoMaLoi(err.response.data);
        }) 
    }
};
export function XoaChuyenNganh(maCN, trangthai) {
    return dispatch => {
        return axios({
            method: 'POST',
            url: types.server + `chuyen_nganh/${maCN}/xoa?trang_thai=${trangthai}`,
            data: {
                ma_so: maCN,
                xoa: trangthai,
            }
        }).then(result => { 
            dispatch({
                type: types.XOA_CHUYEN_NGANH,
                payload: true
            }) 
        })
        .catch(err => {
            types.ThongBaoMaLoi(err.response.data);
        }) 
    }
};
