import * as types from '../constants/constants'; 
import axios from "axios";

export function DanhSachGiaoVien(pageNumber) {
    if (pageNumber < 0) {
        return dispatch => {
            return axios({
                method: 'GET',
                url: types.server + `giao_vien/lay-danh-sach`,
                data: null,
            }).then(result => {
                dispatch({
                    type: types.DANH_SACH_GIAO_VIEN,
                    payload: result.data
                })
            })
                .catch(err => { 
            })
        }
    }else{
        return dispatch => {
            return axios({
                method: 'GET',
                url: types.server + `giao_vien/lay-danh-sach?page=${pageNumber}`,
                data: null,
            }).then(result => {
                dispatch({
                    type: types.DANH_SACH_GIAO_VIEN,
                    payload: result.data
                })
            })
                .catch(err => { 
            })
        }
    }
}
export function DanhSachGiaoVienChoOption() {
    return dispatch => {
        return axios({
            method: 'GET',
            url: types.server + 'giao_vien/lay-danh-sach-gv',
            data: null
        })
            .then(result => {
                dispatch({
                    type: types.DANH_SACH_GIAO_VIEN_CHO_OPTION,
                    payload: result.data
                });
            }).catch(err => {
        })
    }
} 
export function ChiTietGiaoVienTheoMAGV(magv) {
    return dispatch => {
        return axios({
            method: 'GET',
            url: types.server + `giao_vien/${magv}/chi-tiet`,
            data:  null,
        }).then(result => { 
                dispatch({
                    type: types.CHI_TIET_GIAO_VIEN_THEO_MA_GV,
                    payload:  result.data
                })  
            }
        ).catch(err => {
            if (err.response.status === 500){
                types.ThongBaoThatBai(); 
            }
            else if (err.response.status === 401){
                types.ThongBaoKhongTimThay()
            }
            else{
                types.ThongBaoDuLieuKhongTonTai() 
            }
        }) 
    }
};
export function ThemGiaoVien(giaovien) {
    return dispatch => {
        return axios({
            method: 'POST',
            url: types.server+'giao_vien/them-moi',
            data: { 
                ma_khoa: giaovien.ma_khoa,
                ma_giao_vien:giaovien.ma_giao_vien,
                ho_ten:giaovien.ho_ten,
                gioi_tinh:giaovien.rdGioiTinh,
                ngay_sinh: giaovien.ngay_sinh,
                dien_thoai: giaovien.dien_thoai,
                email: giaovien.email,
                dia_chi: giaovien.dia_chi
            }
        })
        .then(result => {
            if (result.data != null) {
                dispatch({
                    type: types.THEM_GIAO_VIEN,
                    payload: true
                }) 
            }
            types.ThongBaoThanhCong();
        }).catch(err => {
            if (err.response.status === 500){
                types.ThongBaoThatBai(); 
            }
            else if (err.response.status === 401){
                types.ThongBaoKhongTimThay()
            }
            else{
                types.ThongBaoDuLieuKhongTonTai() 
            }
        }) 
    }
};
export function CapNhatGiaoVien(giaovien) {
    return dispatch => {
        return axios({
            method: 'POST',
            url: types.server + `giao_vien/${giaovien.ma_giao_vien}/cap-nhat`,
            data: {
                ma_khoa: giaovien.ma_khoa,
                ma_giao_vien:giaovien.ma_giao_vien,
                ho_ten:giaovien.ho_ten,
                gioi_tinh:giaovien.rdGioiTinh,
                ngay_sinh: giaovien.ngay_sinh,
                dien_thoai: giaovien.dien_thoai,
                email: giaovien.email,
                dia_chi: giaovien.dia_chi
            }
        }).then(result => { 
                dispatch({
                    type: types.CAP_NHAT_GIAO_VIEN,
                    payload: result
                })  
            }
        ).catch(err => {
            if (err.response.status === 500){
                types.ThongBaoThatBai(); 
            }
            else if (err.response.status === 401){
                types.ThongBaoKhongTimThay()
            }
            else{
                types.ThongBaoDuLieuKhongTonTai() 
            }
        }) 
    }
};
export function XoaGiaoVien(maGV,trangthai){
    return dispatch => {
        return axios({
            method: 'POST',
            url: types.server + `giao_vien/${maGV}/xoa?trang_thai=${trangthai}`,
            data: {
                ma_so: maGV,
                xoa: trangthai,
            }
        }).then(result => {
            if (result.data != null) {
                dispatch({
                    type: types.XOA_GIAO_VIEN,
                    payload: true
                }) 
            } 
        }).catch(err => {
            if (err.response.status === 500){
                types.ThongBaoThatBai(); 
            }
            else if (err.response.status === 401){
                types.ThongBaoKhongTimThay()
            }
            else{
                types.ThongBaoDuLieuKhongTonTai() 
            }
        }) 
    }
};
export function TimGiaoVienTheoTen(ten) {
    return dispatch => {
        return axios({
            method: 'GET',
            url: types.server + `giao_vien/tim-kiem?ho_ten=${ten}`,
            data: {
                ho_ten: ten
            },
        }).then(result => {
            dispatch({
                type: types.LAY_DSGV_THEO_TEN,
                payload: result.data, 
            })
        })
    }
} 