import * as types from '../constants/constants'; 
import axios from "axios";
export function DanhSachCTLop() {
    return dispatch => {
        return axios({
            method: 'GET',
            url: types.server + `chi_tiet_lop/lay-danh-sach`,
            data:  null,  
        }).then(result =>{
            dispatch({
                type: types.DANH_SACH_CTLOP, 
                payload: result.data
            })
        }).catch(err => {
        }) 
    }
}
export function ChiTietCTLopTheoMaLop(maLop) {
    return dispatch => {
        return axios({
            method: 'GET',
            url: types.server+`chi_tiet_lop/${maLop}/chi-tiet`,
            data:  null,
        }).then(result => { 
                dispatch({
                    type: types.CHI_TIET_CTLOP_THEO_MA_CTLOP,
                    payload:  result.data
                })  
            }
        ).catch(err => {
        }) 
    }
}; 
export function ThemCTLop(Lop) {
    return dispatch => {
        return axios({
            method: 'POST',
            url: types.server+'chi_tiet_lop/them-moi',
            data: { 
                ma_lop:Lop.ma_lop,
                masv :Lop.masv ,   
            }
        })
        .then(result => {
            if (result.data != null) {
                dispatch({
                    type: types.THEM_CTLOP,
                    payload: true
                }) 
            }
            types.ThongBaoThanhCong();
        }).catch(err=>{
            types.ThongBaoThatBai();
        })
    }
};
export function CapNhatCTLop(Lop) {
    return dispatch => {
        return axios({
            method: 'POST',
            url: types.server + `chi_tiet_lop/${Lop.ma_lop}/cap-nhat`,
            data: {
                ma_lop:Lop.ma_lop,
                masv :Lop.masv ,  
            }
        }).then(result => { 
                dispatch({
                    type: types.CAP_NHAT_CTLOP,
                    payload: result
                })  
            }
        ).catch(err => {
            types.ThongBaoThatBai();
        }) 
    }
};
export function XoaCTLop(maLop,masv){  
    return dispatch => {
        return axios({
            method: 'POST',
            url: types.server + `chi_tiet_lop/${maLop}/xoa`,
            data: {
                ma_lop: maLop,
                masv: masv,
            }
        }).then(result => {
            if (result.data != null) {
                dispatch({
                    type: types.XOA_CTLOP,
                    payload: "thanh cong"
                }) 
            } 
            types.ThongBaoThanhCong();
        }).catch(err => {
            types.ThongBaoThatBai();
        }) 
    }
};