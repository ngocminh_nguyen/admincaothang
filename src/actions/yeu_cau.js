import * as types from '../constants/constants'; 
import axios from "axios";
export function DanhSachYeuCau() {
    return dispatch => {
        return axios({
            method: 'GET',
            url: types.server + 'yeu_cau/lay-danh-sach',
            data: null
        }).then(result => {
            dispatch({
                type: types.DANH_SACH_YEU_CAU,
                payload: result.data
            });
        })
    }
}; 
export function ChiTietViPhamTheoMaViPham(maso) {
    return dispatch => {
        return axios({
            method: 'GET',
            url: types.server + `vi_pham/${maso}/chi-tiet`,
            data:  null,  
        }).then(result => { 

                dispatch({
                    type: types.CHI_TIET_VI_PHAM_THEO_MA_VP,
                    payload:  result.data
                })  
            }
        ).catch(err => {
            console.log(err);
        }) 
    }
};
export function ChiTietYeuCauTheoMaYeuCau(maso) {
    return dispatch => {
        return axios({
            method: 'GET',
            url: types.server + `yeu_cau/${maso}/chi-tiet`,
            data:  null,  
        }).then(result => {  
                dispatch({
                    type: types.CHI_TIET_YEU_CAU_THEO_MA_YC,
                    payload:  result.data,
                    a:console.log("action-yeucau----", result.data)
                })  
            }
        ).catch(err => {
            console.log(err);
        }) 
    }
};
export function CapNhatYeuCau(ma_yeu_cau ,mssv, ho_ten, ngay_sinh, gioi_tinh, noi_sinh, sdt){
    return dispatch => {
        return axios({
            method: 'POST',
            url: types.server + `yeu_cau/${ma_yeu_cau}/cap-nhat`,
            data: {
                ma_yeu_cau : ma_yeu_cau,
                mssv : mssv ,
                ho_ten :ho_ten ,
                ngay_sinh: ngay_sinh,
                gioi_tinh:gioi_tinh,
                noi_sinh: noi_sinh,
                sdt:sdt
            }
        }).then(result => {
            if (result.data != null) {
                dispatch({
                    type: types.CAP_NHAT_YEU_CAU,
                    payload: true
                }) 
            } 
            types.ThongBaoThanhCong();
        }).catch(err => {
            console.log(err);
            types.ThongBaoThatBai();
        }) 
    }
};
export function XoaYeuCau(maYC,trangthai){
    return dispatch => {
        return axios({
            method: 'POST',
            url: types.server + `yeu_cau/${maYC}/xoa?trang_thai=${trangthai}`,
            data: {
                ma_yeu_cau : maYC,
                trang_thai: trangthai,
            }
        }).then(result => {
            if (result.data != null) {
                dispatch({
                    type: types.XOA_YEU_CAU,
                    payload: true
                }) 
            } 
            types.ThongBaoThanhCong();
        }).catch(err => {
            console.log(err);
            types.ThongBaoThatBai();
        }) 
    }
};