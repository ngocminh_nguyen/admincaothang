import * as types from '../constants/constants'; 
import axios from "axios";
export function DanhSachLop() {
    return dispatch => {
        return axios({
            method: 'GET',
            url: types.server + `lop/lay-danh-sach`,
            data:  null,  
        }).then(result =>{
            dispatch({
                type: types.DANH_SACH_LOP, 
                payload: result.data
            })
        }).catch(err => {
            // console.log(err);
            // types.ThongBaoThatBai();
        }) 
    }
}
export function ChiTietLopTheoMaLop(maLop) {
    return dispatch => {
        return axios({
            method: 'GET',
            url: types.server + `lop/${maLop}/chi-tiet`,
            data:  null,
        }).then(result => { 
                dispatch({
                    type: types.CHI_TIET_LOP_THEO_MA_LOP,
                    payload:  result.data
                })  
            }
        ).catch(err => { 
        }) 
    }
};
export function DanhSachSinhVienTheoLop(maLop) {
    return dispatch => {
        return axios({
            method: 'POST',
            url: types.server + `chi_tiet_lop/${maLop}/lay-danh-sach-theo-ma-lop`,
            data:  null,
        }).then(result => { 
                dispatch({
                    type: types.DANH_SACH_SV_THEO_LOP,
                    payload:  result.data
                })  
            }
        ).catch(err => { 
        }) 
    }
};
export function ThemLop(Lop) {
    return dispatch => {
        return axios({
            method: 'POST',
            url: types.server+'lop/them-moi',
            data: { 
                bac_dao_tao:Lop.bac_dao_tao,
                chuyen_nganh:Lop.chuyen_nganh, 
                khoa:Lop.khoa,
                ky_tu_phan_biet :Lop.ky_tu_phan_biet, 
                ten_lop :Lop.ten_lop, 
            }
        })
        .then(result => {
            if (result.data != null) {
                dispatch({
                    type: types.THEM_LOP,
                    payload: true
                }) 
            }
            types.ThongBaoThanhCong();
        }).catch(err => {
            if (err.response.status === 500){
                types.ThongBaoThatBai(); 
            }
            else if (err.response.status === 401){
                types.ThongBaoMaLoi(err.response.data)
            }
            else if (err.response.status === 400){
                types.ThongBaoMaLoi(err.response.data)
            }
            else{
                types.ThongBaoDuLieuKhongTonTai() 
            }
        }) 
    }
};
export function CapNhatLop(Lop) {
    return dispatch => {
        return axios({
            method: 'POST',
            url: types.server + `lop/${Lop.ma_lop}/cap-nhat`,
            data: {
                bac_dao_tao:Lop.bac_dao_tao,
                chuyen_nganh:Lop.chuyen_nganh, 
                khoa:Lop.khoa,
                ky_tu_phan_biet :Lop.ky_tu_phan_biet, 
                ten_lop :Lop.ten_lop, 
            }
        }).then(result => { 
                dispatch({
                    type: types.CAP_NHAT_LOP,
                    payload: result
                })  
            }
        ).catch(err => {
            if (err.response.status === 500){
                types.ThongBaoThatBai(); 
            }
            else if (err.response.status === 401){
                types.ThongBaoKhongTimThay()
            }
            else{
                types.ThongBaoDuLieuKhongTonTai() 
            }
        }) 
    }
};
export function XoaLop(maLop,trangthai){
    return dispatch => {
        return axios({
            method: 'POST',
            url: types.server + `lop/${maLop}/xoa?trang_thai=${trangthai}`,
            data: {
                ma_lop: maLop,
                xoa: trangthai,
            }
        }).then(result => {
            if (result.data != null) {
                dispatch({
                    type: types.XOA_LOP,
                    payload: true
                }) 
            } 
        }).catch(err => {
            if (err.response.status === 500){
                types.ThongBaoThatBai(); 
            }
            else if (err.response.status === 401){
                types.ThongBaoKhongTimThay()
            }
            else{
                types.ThongBaoDuLieuKhongTonTai() 
            }
        }) 
    }
};