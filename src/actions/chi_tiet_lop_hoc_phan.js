import * as types from '../constants/constants'; 
import axios from "axios";
export function DanhSachCTLopHocPhan() {
    return dispatch => {
        return axios({
            method: 'GET',
            url: types.server + `chi_tiet_lop_hoc_phan/lay-danh-sach`,
            data:  null,  
        }).then(result =>{
            dispatch({
                type: types.DANH_SACH_CTLOP_HOC_PHAN, 
                payload: result.data
            })
        }).catch(err => {
        }) 
    }
}
export function ChiTietCTLopHocPhanTheoMaCTLopHocPhan(maLop, masv) {
    return dispatch => {
        return axios({
            method: 'GET',
            url: types.server + `chi_tiet_lop_hoc_phan/${maLop}/chi-tiet?masv=${masv}`,
            data:null,
        }).then(result => { 
                dispatch({
                    type: types.CHI_TIET_CTLOP_HOC_PHAN_THEO_MA_CTLOP_HOC_PHAN,
                    payload:  result.data
                })  
            }
        ).catch(err => {
        }) 
    }
}; 
export function ThemCTLopHocPhan(Lop) {
    return dispatch => {
        return axios({
            method: 'POST',
            url: types.server+'chi_tiet_lop_hoc_phan/them-moi',
            data: { 
                ma_lop:Lop.ma_lop,
                masv :Lop.masv , 
                diem :Lop.diem , 
            }
        })
        .then(result => {
            if (result.data != null) {
                dispatch({
                    type: types.THEM_CTLOP_HOC_PHAN,
                    payload: true
                }) 
            }
            types.ThongBaoThanhCong();
        }).catch(err=>{
            types.ThongBaoThatBai();
        })
    }
};
export function CapNhatCTLopHocPhan(Lop) {
    return dispatch => {
        return axios({
            method: 'POST',
            url: types.server + `chi_tiet_lop_hoc_phan/${Lop.ma_lop}/cap-nhat`,
            data: {
                ma_lop:Lop.ma_lop,
                masv :Lop.masv , 
                diem:Lop.diem
            }
        }).then(result => { 
                dispatch({
                    type: types.CAP_NHAT_CTLOP_HOC_PHAN,
                    payload: result 
                })  
            }
        ).catch(err => {
            types.ThongBaoThatBai();
        }) 
    }
};
export function XoaCTLopHocPhan(maLop,masv){
    return dispatch => {
        return axios({
            method: 'POST',
            url: types.server + `chi_tiet_lop_hoc_phan/${maLop}/xoa`,
            data: {
                ma_lop: maLop,
                masv: masv,
            }
        }).then(result => {
            if (result.data != null) {
                dispatch({
                    type: types.XOA_CTLOP_HOC_PHAN,
                    payload: "thanh cong"
                }) 
            } 
            types.ThongBaoThanhCong();
        }).catch(err => {
            types.ThongBaoThatBai();
        }) 
    }
};