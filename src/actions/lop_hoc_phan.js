import * as types from '../constants/constants'; 
import axios from "axios";
export function DanhSachLopHocPhan() {
    return dispatch => {
        return axios({
            method: 'GET',
            url: types.server + `lop_hoc_phan/lay-danh-sach`,
            data:  null,  
        }).then(result =>{
            dispatch({
                type: types.DANH_SACH_LOP_HOC_PHAN, 
                payload: result.data
            })
        })
        // .catch(err => {
        //     if (err.response.status===500){
        //         types.ThongBaoThatBai(); 
        //     }
        //     else if (err.response.status===401){
        //         types.ThongBaoKhongTimThay()
        //     }
        //     else{
        //         types.ThongBaoDuLieuKhongTonTai() 
        //     }
        // }) 
    }
}
export function ChiTietLopHocPhanTheoMaLopHocPhan(maLop) {
    return dispatch => {
        return axios({
            method: 'GET',
            url: types.server + `lop_hoc_phan/${maLop}/chi-tiet`,
            data:  null,
        }).then(result => { 
                dispatch({
                    type: types.CHI_TIET_LOP_HOC_PHAN_THEO_MA_LOP_HOC_PHAN,
                    payload:  result.data
                })  
            }
        ).catch(err => { 
        }) 
    }
};
export function DanhSachSinhVienTheoLopHocPhan(maLop) {
    return dispatch => {
        return axios({
            method: 'POST',
            url: types.server + `chi_tiet_lop_hoc_phan/${maLop}/lay-danh-sach-theo-ma-lop/`,
            data:  null,
        }).then(result => { 
                dispatch({
                    type: types.DANH_SACH_SINH_VIEN_THEO_LOP_HOC_PHAN,
                    payload:  result.data
                })  
            }
        ).catch(err => { 
        }) 
    }
};
export function ThoiKhoaBieuLopHocPhanTheoMaSinhVien(masv) {
    return dispatch => {
        return axios({
            method: 'GET',
            url: types.server + `lop_hoc_phan/${masv}/chi-tiet`,
            data:  null,
        }).then(result => { 
                dispatch({
                    type: types.THOI_KHOA_BIEU_LOP_HOC_PHAN_THEO_MSSV,
                    payload:  result.data
                })  
            }
        ).catch(err => {
            if (err.response.status===500){
                types.ThongBaoThatBai(); 
            }
            else if (err.response.status===401){
                types.ThongBaoKhongTimThay()
            }
            else{
                types.ThongBaoDuLieuKhongTonTai() 
            }
        }) 
    }
};
export function ThemLopHocPhan(Lop) { 
    return dispatch => {
        return axios({
            method: 'POST',
            url: types.server+'lop_hoc_phan/them-moi',
            data: {  
                ma_giao_vien:Lop.ma_giao_vien, 
                ma_mon_hoc:Lop.ma_mon_hoc,
                hoc_ky:Lop.hoc_ky, 
                thoi_gian:JSON.stringify(Lop.tableTKB),  
                thoi_gian_ket_thuc: Lop.thoi_gian_ket_thuc
            }
        })
        .then(result => {
            if (result.data != null) {
                dispatch({
                    type: types.THEM_LOP_HOC_PHAN,
                    payload: true
                }) 
            }
            types.ThongBaoThanhCong();
        }).catch(err => {
            if (err.response.status===500){
                types.ThongBaoThatBai(); 
            }
            else if (err.response.status===401){
                types.ThongBaoKhongTimThay()
            }
            else if (err.response.status===400){
                types.ThongBaoMaLoi(err.response.data)
            }
            else{
                types.ThongBaoDuLieuKhongTonTai() 
            }
        }) 
    }
};
export function CapNhatLopHocPhan(Lop) {
    return dispatch => {
        return axios({
            method: 'POST',
            url: types.server + `lop_hoc_phan/${Lop.ma_lop}/cap-nhat`,
            data: {
                ma_lop:Lop.ma_lop,
                ma_giao_vien:Lop.ma_giao_vien, 
                ma_mon_hoc:Lop.ma_mon_hoc,
                hoc_ky:Lop.hoc_ky, 
                thoi_gian:JSON.stringify(Lop.tableTKB),  
                thoi_gian_ket_thuc: Lop.thoi_gian_ket_thuc
            }
        }).then(result => { 
                dispatch({
                    type: types.CAP_NHAT_LOP_HOC_PHAN,
                    payload: result
                })  
            }
        ).catch(err => {
            if (err.response.status===500){
                types.ThongBaoThatBai(); 
            }
            else if (err.response.status===401){
                types.ThongBaoKhongTimThay()
            }
            else if (err.response.status===400){
                types.ThongBaoMaLoi(err.response.data)
            }
            else{
                types.ThongBaoDuLieuKhongTonTai() 
            }
        }) 
    }
};
export function XoaLopHocPhan(maLop,trangthai){
    return dispatch => {
        return axios({
            method: 'POST',
            url: types.server + `lop_hoc_phan/${maLop}/xoa?trang_thai=${trangthai}`,
            data: {
                ma_lop: maLop,
                xoa: trangthai,
            }
        }).then(result => {
            if (result.data != null) {
                dispatch({
                    type: types.XOA_LOP_HOC_PHAN,
                    payload: true
                }) 
            } 
        }).catch(err => {
            if (err.response.status===500){
                types.ThongBaoThatBai(); 
            }
            else if (err.response.status===401){
                types.ThongBaoKhongTimThay()
            }
            else if (err.response.status===400){
                types.ThongBaoMaLoi(err.response.data)
            }
            else{
                types.ThongBaoDuLieuKhongTonTai() 
            }
        }) 
    }
};