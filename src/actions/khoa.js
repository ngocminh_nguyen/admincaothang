import * as types from '../constants/constants'; 
import * as  func from '../constants/FunctionsKTDuLieu_ThongBao';
import axios from "axios";
export function DanhSachKhoa() {
    return dispatch => {
        return axios({
            method: 'GET',
            url: types.server + `khoa/lay-danh-sach`,
            data:  null,  
        }).then(result =>{
            dispatch({
                type: types.DANH_SACH_KHOA, 
                payload: result.data
            })
        }).catch(err => { 
        }) 
    }
}
export function ChiTietKhoaTheoMaKhoa(makhoa) {
    return dispatch => {
        return axios({
            method: 'GET',
            url: types.server + `khoa/${makhoa}/chi-tiet`,
            data:  null,
        }).then(result => { 
                dispatch({
                    type: types.CHI_TIET_KHOA_THEO_MA_KHOA,
                    payload:  result.data
                })  
            }
        ).catch(err => {  
        }) 
    }
};
export function ThemKhoa(Khoa) {
    return dispatch => {
        return axios({
            method: 'POST',
            url: types.server+'khoa/them-moi',
            data: { 
                ma_khoa:Khoa.ma_khoa,
                ten_khoa:Khoa.ten_khoa, 
            }
        })
        .then(result => {
            if (result.data != null) {
                dispatch({
                    type: types.THEM_KHOA,
                    payload: true
                }) 
            }
            types.ThongBaoThanhCong();
        }).catch(err=>{
            if (err.response.status === 500){
                types.ThongBaoThatBai(); 
            }
            else if (err.response.status === 401){
                types.ThongBaoKhongTimThay()
            } 
            else{
                types.ThongBaoDuLieuKhongTonTai() 
            }
        })
    }
};
export function CapNhatKhoa(Khoa) {
    return dispatch => {
        return axios({
            method: 'POST',
            url: types.server + `khoa/${Khoa.ma_khoa}/cap-nhat`,
            data: {
                ma_khoa:Khoa.ma_khoa,
                ten_khoa:Khoa.ten_khoa, 
            }
        }).then(result => { 
                dispatch({
                    type: types.CAP_NHAT_KHOA,
                    payload: result
                })  
            }
        ).catch(err => {
            if (err.response.status === 500){
                types.ThongBaoThatBai(); 
            }
            else if (err.response.status === 401){
                types.ThongBaoKhongTimThay()
            }
            else{
                types.ThongBaoDuLieuKhongTonTai() 
            }
        }) 
    }
};
export function XoaKhoa(makhoa,trangthai){
    return dispatch => {
        return axios({
            method: 'POST',
            url: types.server + `khoa/${makhoa}/xoa?trang_thai=${trangthai}`,
            data: { 
            }
        }).then(result => {
            if (result.data != null) {
                dispatch({
                    type: types.XOA_KHOA,
                    payload: true
                }) 
            } 
        }).catch(err => {
            if (err.response.status === 500){
                types.ThongBaoThatBai(); 
            }
            else if (err.response.status === 401){
                types.ThongBaoKhongTimThay()
            }
            else{
                types.ThongBaoDuLieuKhongTonTai() 
            }
        }) 
    }
}; 