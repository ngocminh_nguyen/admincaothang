import * as types from '../constants/constants'; 
import axios from "axios";

export function DanhSachMonHoc(pageNumber) {
    if (pageNumber < 0) {
        return dispatch => {
            return axios({
                method: 'GET',
                url: types.server + 'mon_hoc/lay-danh-sach',
                data: null
            })
                .then(result => {
                    dispatch({
                        type: types.DANH_SACH_MON_HOC,
                        payload: result.data
                    });
                }).catch(err => {
                })
        }
    }else{
        return dispatch => {
            return axios({
                method: 'GET',
                url: types.server + `mon_hoc/lay-danh-sach?page=${pageNumber}`,
                data: null
            })
                .then(result => {
                    dispatch({
                        type: types.DANH_SACH_MON_HOC,
                        payload: result.data
                    });
                }).catch(err => {
                })
        }
    }
};
export function DanhSachMonHocChoOption() {
    return dispatch => {
        return axios({
            method: 'GET',
            url: types.server + 'mon_hoc/lay-danh-sach-mh',
            data: null
        })
            .then(result => {
                dispatch({
                    type: types.DANH_SACH_MON_HOC_CHO_OPTION,
                    payload: result.data
                });
            }).catch(err => {
            })
    }
} 
export function ChiTietMonHocTheoMaMH(mamh) {
    return dispatch => {
        return axios({
            method: 'GET',
            url: types.server + `mon_hoc/${mamh}/chi-tiet`,
            data:  null,
        }).then(result => { 
                dispatch({
                    type: types.CHI_TIET_MON_HOC_THEO_MA_MH,
                    payload:  result.data
                })  
            }
        ).catch(err => { 
        }) 
    }
};
export function ThemMonHoc(monhoc) {
    return dispatch => {
        return axios({
            method: 'POST',
            url: types.server+'mon_hoc/them-moi',
            data: { 
                ma_mon_hoc:monhoc.ma_mon_hoc,
                ten_mon_hoc:monhoc.ten_mon_hoc,
                dv_hoc_phan:monhoc.dv_hoc_phan,
                ma_chuyen_nganh:monhoc.ma_chuyen_nganh,
            }
        }).then(result => {
            if (result.data != null) {
                dispatch({
                    type: types.THEM_MON_HOC,
                    payload: true
                })  
            }
            types.ThongBaoThanhCong()
        }).catch(err => {
            if (err.response.status === 500){
                types.ThongBaoThatBai(); 
            }
            else if (err.response.status === 401){
                types.ThongBaoKhongTimThay()
            }
            else{
                types.ThongBaoDuLieuKhongTonTai() 
            }
        }) 
    }
};
export function CapNhatMonHoc(monhoc) {
    return dispatch => {
        return axios({
            method: 'POST',
            url: types.server + `mon_hoc/${monhoc.ma_mon_hoc}/cap-nhat`,
            data: {
                ma_mon_hoc:monhoc.ma_mon_hoc,
                ten_mon_hoc:monhoc.ten_mon_hoc,
                dv_hoc_phan:monhoc.dv_hoc_phan,
                ma_chuyen_nganh:monhoc.ma_chuyen_nganh,
            }
        }).then(result => { 
                dispatch({
                    type: types.CAP_NHAT_MON_HOC,
                    payload: result
                })  
            }
        ).catch(err => {
            if (err.response.status === 500){
                types.ThongBaoThatBai(); 
            }
            else if (err.response.status === 401){
                types.ThongBaoKhongTimThay()
            }
            else{
                types.ThongBaoDuLieuKhongTonTai() 
            }
        })  
    }
};
export function XoaMonHoc(maMH, xoa){
    return dispatch => {
        return axios({
            method: 'POST',
            url:types.server+`mon_hoc/${maMH}/xoa?trang_thai=${xoa}`,
            data: {
                ma_mon_hoc:maMH,
                xoa: xoa,
            }, 
        }).then(result => {
            if (result.data != null) {
                dispatch({
                    type: types.XOA_MON_HOC,
                    payload: true
                }) 
            }
            else{
                dispatch({
                    type: types.XOA_MON_HOC,
                    payload: false
                }) 
            }
        }).catch(err => {
            if (err.response.status === 500){
                types.ThongBaoThatBai(); 
            }
            else if (err.response.status === 401){
                types.ThongBaoKhongTimThay()
            }
            else{
                types.ThongBaoDuLieuKhongTonTai() 
            }
        }) 
    }
};
export function TimMonHocTheoTen(ten) { 
    return dispatch => {
        return axios({
            method: 'GET',
            url: types.server + `mon_hoc/tim-kiem?ten_mon_hoc=${ten}`,
            data: {
                ten_moc_hoc: ten
            },
        }).then(result => {
            dispatch({
                type: types.LAY_DSMH_THEO_TEN,
                payload: result.data, 
            })
        }) 
    }
} 