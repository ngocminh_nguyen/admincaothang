import * as types from '../constants/constants'; 
import axios from "axios";
export function DanhSachSinhVien(pageNumber) {
    console.log(pageNumber)
    if(pageNumber<0){ 
        return dispatch => { 
            return axios({
                method: 'GET',
                url: types.server + `sinh_vien/lay-danh-sach`,
                data:  null,  
            }).then(result =>{
                dispatch({
                    type: types.DANH_SACH_SINH_VIEN, 
                    payload: result.data
                })
            }).catch(err => {
                console.log(err);
            }) 
        }   
    }else{ 
        return dispatch => { 
            return axios({
                method: 'GET',
                url: types.server + `sinh_vien/lay-danh-sach?page=${pageNumber}`,
                data:  null,  
            }).then(result =>{
                dispatch({
                    type: types.DANH_SACH_SINH_VIEN, 
                    payload: result.data
                })
            }).catch(err => {
                console.log(err);
            }) 
        }   
    }
} 
export function DanhSachSinhVienChoOption() {
    return dispatch => {
        return axios({
            method: 'GET',
            url: types.server + 'sinh_vien/lay-danh-sach-sv',
            data: null
        })
            .then(result => {
                dispatch({
                    type: types.DANH_SACH_SINH_VIEN_CHO_OPTION,
                    payload: result.data
                });
            }).catch(err => {
        })
    }
} 
export function LayDanhSachSinhVienTheoMSSV(mssv) {
    console.log(mssv)
    if(mssv<1){ 
        return dispatch => { 
            return axios({
                method: 'GET',
                url: types.server + `sinh_vien/lay-danh-sach`,
                data:  null,  
            }).then(result =>{
                dispatch({
                    type: types.DANH_SACH_SINH_VIEN, 
                    payload: result.data
                })
            }).catch(err => {
                console.log(err);
            }) 
        }   
    }else{ 
        return dispatch => { 
            return axios({
                method: 'GET',
                url: types.server + `sinh_vien/tim-kiem?mssv=${mssv}`,
                data:  {
                    mssv :mssv 
                },  
            }).then(result =>{
                dispatch({
                    type: types.LAY_DSSV_THEO_MA_SINHVIEN, 
                    payload: result.data
                })
            }) 
        }   
    }
} 
export function ChiTietSinhVienTheoMaSV(masv) {
    return dispatch => {
        return axios({
            method: 'GET',
            url: types.server + `sinh_vien/${masv}/chi-tiet-sv`,
            data:  null,
        }).then(result => { 
                dispatch({
                    type: types.CHI_TIET_SINHVIEN_THEO_MA_SINHVIEN,
                    payload:  result.data,
                    a:console.log(result.data)
                })  
            }
        ).catch(err => {
            console.log(err);
        }) 
    }
};
export function ThemSinhVien(SinhVien) {   
    let url_hinh_anh = '';
    if (SinhVien.hinh_dai_dien != '') {
        const data = new FormData() 
        let ten_hinh_anh = SinhVien.hinh_dai_dien.name
        data.append('hinh_dai_dien', SinhVien.hinh_dai_dien, ten_hinh_anh);
        url_hinh_anh = types.serverUploadHinh + ten_hinh_anh;
        axios.post(types.server + "sinh_vien/upload-anh", data);
    }
    else{
        url_hinh_anh="none" 

    }
    return dispatch => {
        return axios({
            method: 'POST',
            url: types.server+'sinh_vien/them-moi',
            data: { 
                bac_dao_tao :SinhVien.bac_dao_tao,
                ma_chuyen_nganh :SinhVien.ma_chuyen_nganh,
                khoa :SinhVien.khoa,
                loai_hinh_dao_tao : SinhVien.loai_hinh_dao_tao,
                ho_ten : SinhVien.ho_ten,
                ngay_sinh : SinhVien.ngay_sinh,
                gioi_tinh : SinhVien.rdGioiTinh,
                so_dien_thoai : SinhVien.so_dien_thoai ,
                hinh_dai_dien  : url_hinh_anh,
                noi_sinh  : SinhVien.noi_sinh,
                ma_phu_huynh : SinhVien.ma_phu_huynh
            }
        })
        .then(result => {
            if (result.data != null) {
                dispatch({
                    type: types.THEM_SINH_VIEN,
                    payload: true
                }) 
            } 
            types.ThongBaoThanhCong();
        }).catch(err=>{
            console.log(err);
            types.ThongBaoThatBai();
        })
    }
};

export function CapNhatSinhVien(SinhVien) {
    if(SinhVien.gioi_tinh){
        SinhVien.rdGioiTinh =SinhVien.gioi_tinh 
        alert(1)
    }
    console.log(SinhVien)
    console.log(SinhVien.rdGioiTinh)
    return dispatch => {
     
        let url_hinh_anh = '';
        if (SinhVien.dachonhinh) {
            const data = new FormData() 
            let ten_hinh_anh = SinhVien.mssv+'.png';
            data.append('hinh_dai_dien', SinhVien.hinh_anh, ten_hinh_anh);
            url_hinh_anh = types.serverUploadHinh + ten_hinh_anh;
            axios.post(types.server + "sinh_vien/upload-anh", data); 
        }
        else{
            url_hinh_anh = SinhVien.hinh_dai_dien;
        }
        console.log(url_hinh_anh)
        return axios({
            method: 'POST',
            url: types.server + `sinh_vien/${SinhVien.mssv}/cap-nhat`,
            data: {  
                mssv: SinhVien.mssv,
                ho_ten : SinhVien.ho_ten,
                ngay_sinh : SinhVien.ngay_sinh,
                gioi_tinh : SinhVien.rdGioiTinh,
                so_dien_thoai : SinhVien.so_dien_thoai ,
                hinh_dai_dien  : url_hinh_anh,
                noi_sinh  : SinhVien.noi_sinh,
                ma_phu_huynh : SinhVien.ma_phu_huynh,
            }
        }).then(result => { 
                dispatch({
                    type: types.CAP_NHAT_SINH_VIEN,
                    payload:  "Thành Công",
                })   
            }
        ).catch(err => {
            types.ThongBaoThatBai();
        }) 
    }
};
export function XoaSinhVien(masv,trangthai){
    return dispatch => {
        return axios({
            method: 'POST',
            url: types.server + `sinh_vien/${masv}/xoa?trang_thai=${trangthai}`,
            data: {
                mssv: masv,
                xoa: trangthai,
            }
        }).then(result => {
            if (result.data != null) {
                dispatch({
                    type: types.XOA_SINH_VIEN,
                    payload: true
                }) 
            } 
        }).catch(err => {
            types.ThongBaoThatBai();
        }) 
    }
};
export function UpLoadAnh(SinhVien) {
    return dispatch => {
        return axios({
            method: 'POST',
            url: types.server+'sinh_vien/them-moi',
            data: { 
                bac_dao_tao :SinhVien.bac_dao_tao,
                ma_chuyen_nganh :SinhVien.ma_chuyen_nganh,
                khoa :SinhVien.khoa,
                loai_hinh_dao_tao : SinhVien.loai_hinh_dao_tao,
                ho_ten : SinhVien.ho_ten,
                ngay_sinh : SinhVien.ngay_sinh,
                gioi_tinh : SinhVien.rdGioiTinh,
                hinh_dai_dien  : SinhVien.hinh_dai_dien,
                noi_sinh  : SinhVien.noi_sinh
            }
        })
        .then(result => {
            if (result.data != null) {
                dispatch({
                    type: types.THEM_SINH_VIEN,
                    payload: true
                }) 
            }
            types.ThongBaoThanhCong();
        }).catch(err=>{
            types.ThongBaoThatBai();
        })
    }
};
export function ThemHinhSinhVien(tenfile) { 
    return dispatch => {
        const data = new FormData()
        data.append('up_load_file_anh', tenfile, tenfile.name);
        let filenen = '';
        filenen = types.serverUploadHinh + tenfile.name;
        axios.post(types.server + "sinh_vien/upload-file-anh", data)
        types.ThongBaoThanhCong();
    }   
};
export function UpLoadFileNen(tenfile) {  
    return dispatch => {
    const data = new FormData()  
    data.append('up_load_file',tenfile , tenfile.name);
    let filenen = '';
    filenen = types.serverUploadFile + tenfile.name; 
    axios.post(types.server +"sinh_vien/upload-file", data)  
    }    
};
export function ThemSinhVienTuFileNen(tenfile, lop) {  
    return dispatch => {
        return axios({
            method: 'POST',
            url: types.server+'sinh_vien/them-moi-nhieu',
            data: { 
                tenfile : tenfile,
                ma_lop:lop,
            }
        })
        .then(result => {
            if (result.data != null) {
                dispatch({
                    type: types.THEM_SINH_VIEN_TU_EXCEL_FILE,
                    payload: true
                }) 
            }
            types.ThongBaoThanhCong();
        }).catch(err=>{
            types.ThongBaoThatBai();
        })
    }
};
export function SinhVienDongKinhPhi(mssv, hk,loai, trangthai){
    return dispatch => {
        return axios({
            method: 'POST',
            url: types.server + `sinh_vien/cap-nhat-kinh-phi`,
            data: {
                mssv: mssv,
                hoc_ky : hk,
                loai: loai,
            }
        }).then(result => {
            if (result.data != null) {
                dispatch({
                    type: types.SINH_VIEN_CAP_NHAT_KINH_PHI,
                    payload: true
                }) 
            }  
        }).catch(err => {
        }) 
    }
}; 
export function ThemViPhamSinhVien(mssv, thongbao, ngay){
    return dispatch => {
        return axios({
            method: 'POST',
            url: types.server + `sinh_vien/them-vi-pham`,
            data: {
                mssv: mssv,
                thong_bao  : thongbao, 
                thoi_gian_vi_pham:ngay
            }
        }).then(result => {
            if (result.data != null) {
                dispatch({
                    type: types.THEM_VI_PHAM_SINH_VIEN,
                    payload: true
                }) 
            } 
            types.ThongBaoThanhCong();
        }).catch(err => {
            types.ThongBaoThatBai();
        }) 
    }
};