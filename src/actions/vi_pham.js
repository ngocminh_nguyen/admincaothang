import * as types from '../constants/constants'; 
import axios from "axios";
export function DanhSachViPham() {
    return dispatch => {
        return axios({
            method: 'GET',
            url: types.server + 'vi_pham/lay-danh-sach',
            data: null
        }).then(result => {
            dispatch({
                type: types.DANH_SACH_VI_PHAM,
                payload: result.data
            });
        })
    }
};
export function ChiTietViPhamTheoMaViPham(maso) {
    return dispatch => {
        return axios({
            method: 'GET',
            url: types.server + `vi_pham/${maso}/chi-tiet`,
            data:  null,  
        }).then(result => { 

                dispatch({
                    type: types.CHI_TIET_VI_PHAM_THEO_MA_VP,
                    payload:  result.data
                })  
            }
        ).catch(err => {
            console.log(err);
        }) 
    }
};
export function ThemViPham(vipham) {
    return dispatch => {
        return axios({
            method: 'POST',
            url: types.server + 'vi_pham/them-moi',
            data: {
                ma_vi_pham:vipham.ma_vi_pham,
                noi_dung:vipham.noi_dung,
                hinh_thuc_ky_luat:vipham.hinh_thuc_ky_luat,
            },
        }).then(result => {
            dispatch({
                type: types.THEM_VI_PHAM,
                payload: true,
            });
            types.ThongBaoThanhCong()
        }).catch(err => {
                console.log(err);
                types.ThongBaoThatBai();
        })
    }
};
export function CapNhatViPham(item) {
    return dispatch => {
        return axios({
            method: 'POST',
            url: types.server + `vi_pham/${item.ma_vi_pham}/cap-nhat`,
            data: {
                ma_vi_pham:item.ma_vi_pham,
                noi_dung:item.noi_dung,
                hinh_thuc_ky_luat:item.hinh_thuc_ky_luat,
            }
        }).then(result => { 
                dispatch({
                    type: types.CAP_NHAT_VI_PHAM,
                    payload: result
                })  
            }
        ).catch(err => {
            console.log(err);
            types.ThongBaoThatBai();
        }) 
    }
};
export function XoaViPham(maVP,trangthai){
    return dispatch => {
        return axios({
            method: 'POST',
            url: types.server + `vi_pham/${maVP}/xoa?trang_thai=${trangthai}`,
            data: {
                ma_so: maVP,
                xoa: trangthai,
            }
        }).then(result => {
            if (result.data != null) {
                dispatch({
                    type: types.XOA_VI_PHAM,
                    payload: true
                }) 
            } 
        }).catch(err => {
            console.log(err);
            types.ThongBaoThatBai();
        }) 
    }
};