import * as types from '../constants/constants'; 
import axios from "axios";

export function DanhSachPhuHuynh(pageNumber) {
    if (pageNumber < 0) {
        return dispatch => {
            return axios({
                method: 'GET',
                url: types.server + 'phu_huynh/lay-danh-sach',
                data: null
            })
                .then(result => {
                    dispatch({
                        type: types.DANH_SACH_PHU_HUYNH,
                        payload: result.data
                    });
                }).catch(err => {
                })
        }
    } else {
        return dispatch => {
            return axios({
                method: 'GET',
                url: types.server + `phu_huynh/lay-danh-sach?page=${pageNumber}`,
                data: null
            })
                .then(result => {
                    dispatch({
                        type: types.DANH_SACH_PHU_HUYNH,
                        payload: result.data
                    });
                }).catch(err => {
                })
        }
    }
};
export function DanhSachPhuHuynhChoOption() {
    return dispatch => {
        return axios({
            method: 'GET',
            url: types.server + 'phu_huynh/lay-danh-sach-ph',
            data: null
        })
            .then(result => {
                dispatch({
                    type: types.DANH_SACH_PHU_HUYNH_CHO_OPTION,
                    payload: result.data
                });
            }).catch(err => {
            })
    }
} 
export function ChiTietPhuHuynhTheoMAPH(maph) {
    return dispatch => {
        return axios({
            method: 'GET',
            url: types.server + `phu_huynh/${maph}/chi-tiet`,
            data:  null,
        }).then(result => { 
                dispatch({
                    type: types.CHI_TIET_PHU_HUYNH_THEO_MA_PHU_HUYNH,
                    payload:  result.data,
                    a:console.log( result.data , "--Action")
                })  
            }
        ).catch(err => { 
        }) 
    }
};
export function ThemPhuHuynh(phuhuynh) {
    return dispatch => {
        return axios({
            method: 'POST',
            url: types.server+'phu_huynh/them-moi',
            data: { 
                ho_ten_cha  :phuhuynh.ho_ten_cha  ,
                ho_ten_me :phuhuynh.ho_ten_me ,
                so_dien_thoai :phuhuynh.so_dien_thoai ,
                dia_chi: phuhuynh.dia_chi,
            }
        }).then(result => {
            if (result.data != null) {
                dispatch({
                    type: types.THEM_PHU_HUYNH,
                    payload: true
                })  
            }
            types.ThongBaoThanhCong()
        }).catch(err => {
            if (err.response.status === 500){
                types.ThongBaoThatBai(); 
            }
            else if (err.response.status === 401){
                types.ThongBaoKhongTimThay()
            }
            else{
                types.ThongBaoDuLieuKhongTonTai() 
            }
        }) 
    }
};
export function CapNhatPhuHuynh(phuhuynh) {
    return dispatch => {
        return axios({
            method: 'POST',
            url: types.server + `phu_huynh/${phuhuynh.ma_phu_huynh}/cap-nhat`,
            data: {
                ho_ten_me :phuhuynh.ho_ten_me ,
                ho_ten_cha :phuhuynh.ho_ten_cha ,
                so_dien_thoai :phuhuynh.so_dien_thoai ,
                dia_chi: phuhuynh.dia_chi,
            }
        }).then(result => { 
                dispatch({
                    type: types.CAP_NHAT_PHU_HUYNH,
                    payload: "Thành Công"
                })  
            }
        ).catch(err => {
            if (err.response.status === 500){
                types.ThongBaoThatBai(); 
            }
            else if (err.response.status === 401){
                types.ThongBaoKhongTimThay()
            }
            else{
                types.ThongBaoDuLieuKhongTonTai() 
            }
        })  
    }
};
export function XoaPhuHuynh(maph, xoa){
    return dispatch => {
        return axios({
            method: 'POST',
            url:types.server+`phu_huynh/${maph}/xoa?trang_thai=${xoa}`,
            data: {
                ma_phu_huynh :maph,
                xoa: xoa,
            }, 
        }).then(result => {
            if (result.data != null) {
                dispatch({
                    type: types.XOA_PHU_HUYNH,
                    payload: true
                }) 
            }
            else{
                dispatch({
                    type: types.XOA_PHU_HUYNH,
                    payload: false
                }) 
            }
        }).catch(err => {
            if (err.response.status === 500){
                types.ThongBaoThatBai(); 
            }
            else if (err.response.status === 401){
                types.ThongBaoKhongTimThay()
            }
            else{
                types.ThongBaoDuLieuKhongTonTai() 
            }
        }) 
    }
};
export function TimPhuHuynhTheoTen(ten) {
    console.log(ten)
    return dispatch => {
        return axios({
            method: 'GET',
            url: types.server + `phu_huynh/tim-kiem?hoten=${ten}`,
            data: {
                hoten : ten
            },
        }).then(result => {
            dispatch({
                type: types.LAY_DSPH_THEO_TEN,
                payload: result.data, 
            })
        }).catch(err => { 
            }
        )
    }
} 