import * as types from '../constants/constants'; 
import axios from "axios";

export function DanhSachKinhPhi() {
    return dispatch => {
        return axios({
            method: 'GET',
            url: types.server+'kinh_phi/lay-danh-sach',
            data:null
        })
        .then(result => {
            dispatch({
                type: types.DANH_SACH_KINH_PHI,
                payload: result.data
            });
        }).catch(err => { 
        }) 
    }
};
export function ChiTietKinhPhiTheoMaKhoa(makhoa) {
    return dispatch => {
        return axios({
            method: 'GET',
            url: types.server + `kinh_phi/${makhoa}/chi-tiet`,
            data:  null,
        }).then(result => { 
                dispatch({
                    type: types.CHI_TIET_KINH_PHI_THEO_MA_KHOA,
                    payload:  result.data
                })  
            }
        ).catch(err => { 
        }) 
    }
};
export function ThemKinhPhi(kinhphi) { 
    return dispatch => {  
    return  axios({
            method: 'POST',
            url: types.server+'sinh_vien/kinh-phi',
            data: { 
                khoa:kinhphi.khoa,
                hoc_ky:kinhphi.hoc_ky,
                loai:kinhphi.loai,
                so_tien:kinhphi.so_tien,
                thoi_gian_bat_dau: kinhphi.thoi_gian_bat_dau,
                thoi_gian_ket_thuc:kinhphi.thoi_gian_ket_thuc
            }
        }).then(result => {
            if (result.data != null) {
                dispatch({
                    type: types.THEM_KINH_PHI,
                    payload: true
                }) 
            }
            types.ThongBaoThanhCong();
        }).catch(err => {
            if (err.response.status === 500){
                types.ThongBaoThatBai(); 
            }
            else if (err.response.status === 401){
                types.ThongBaoKhongTimThay()
            }
            else if (err.response.status === 404){
                types.ThongBaoMaLoi(err.response.data)
            }
            else{
                types.ThongBaoDuLieuKhongTonTai() 
            }
        }) 
    }
};
export function CapNhatKinhPhi(kinhphi) {
    return dispatch => {
        return axios({
            method: 'POST',
            url: types.server + `kinh_phi/${kinhphi.khoa}/cap-nhat`,
            data: {
                khoa:kinhphi.khoa,
                hoc_ky:kinhphi.hoc_ky,
                loai:kinhphi.loai,
                so_tien:kinhphi.so_tien,
                thoi_gian_bat_dau: kinhphi.thoi_gian_bat_dau,
                thoi_gian_ket_thuc:kinhphi.thoi_gian_ket_thuc
            }
        }).then(result => { 
                dispatch({
                    type: types.CAP_NHAT_KINH_PHI,
                    payload: result
                })  
            }
        ).catch(err => {
            if (err.response.status === 500){
                types.ThongBaoThatBai(); 
            }
            else if (err.response.status === 401){
                types.ThongBaoKhongTimThay()
            }
            else{
                types.ThongBaoDuLieuKhongTonTai() 
            }
        })  
    }
};
export function XoaKinhPhi(makhoa, xoa){
    return dispatch => {
        return axios({
            method: 'POST',
            url:types.server+`kinh_phi/${makhoa}/xoa?trang_thai=${xoa}`,
            data: {
                khoa:makhoa,
                xoa:xoa,
            }, 
        }).then(result => {
            if (result.data != null) {
                dispatch({
                    type: types.XOA_KINH_PHI,
                    payload: true
                }) 
            } 
        }).catch(err => {
            if (err.response.status === 500){
                types.ThongBaoThatBai(); 
            }
            else if (err.response.status === 401){
                types.ThongBaoKhongTimThay()
            }
            else{
                types.ThongBaoDuLieuKhongTonTai() 
            }
        }) 
    }
};