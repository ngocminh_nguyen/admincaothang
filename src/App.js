import React, { Component } from 'react';
// import './App.css';
// import routes from './router';
import { Switch, Route,Redirect, BrowserRouter as Router } from 'react-router-dom';

import LichThi from './pages/LichThi';
import ChuyenNganh from './pages/ChuyenNganh'; 
import store from './store/store';

import MonHoc from './pages/MonHoc';
import ViPham from './pages/ViPham';
import GiaoVien from './pages/GiaoVien';
import KinhPhi from './pages/KinhPhi';
import Lop from './pages/Lop';
import CTLop from './pages/CTLop';
import LopHocPhan from './pages/LopHocPhan';
import CTLopHocPhan from './pages/CTLopHocPhan';
import Khoa from './pages/Khoa';
import Admin from './pages/Admin';
import SinhVien from './pages/SinhVien';
import YeuCau from './pages/YeuCau';
import XLYeuCau from './pages/XulyYeuCau';

import ThemKinhPhi from './pages/ThemKinhPhi';
import ThemMonHoc from './pages/ThemMonHoc';
import ThemGiaoVien from './pages/ThemGiaoVien';
import ThemViPham from './pages/ThemViPham';
import ThemViPhamSinhVien from './pages/ThemViPhamSinhVien';
import ThemLop from './pages/ThemLop';
import ThemCTLop from './pages/ThemChiTietLop';
import ThemLopHocPhan from './pages/ThemLopHocPhan';
import CapNhatLopHocPhan from './pages/CapNhatLopHocPhan'
import ThemCTLopHocPhan from './pages/ThemChiTietLopHocPhan';
import ThemKhoa from './pages/ThemKhoa';
import ThemAdmin from './pages/ThemAdmin';
import ThemSinhVien from './pages/ThemSinhVien'; 
import ThemLichThi from './pages/ThemLichThi';
import DangNhap from './pages/DangNhap';

import {Provider} from 'react-redux'; 
import ThemChuyenNganh from './pages/ThemChuyenNganh';
import ThemPhuHuynh from './pages/ThemPhuHuynh';
import PhuHuynh from './pages/PhuHuynh';
import ProfileAdmin from './pages/ProfileAdmin'
const PrivateAdmin = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={props =>
      store.getState().DangNhapAdmin.isLogin ? (
        <Component {...props} />
      ) : (
          <Redirect
            to={{
              pathname: "/dangnhap"
            }}
          />
        )
    }
  />
);
const IsPrivateAdmins = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={props =>
      store.getState().DangNhapAdmin.isLogin ? (
        <Redirect
          to={{
            pathname: "/khoa"
          }}
        />
      ) : (
          <Component {...props} />
        )
    }
  />
);
class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <Router>
          <Switch>
            {/* <Route exact path="/dangnhap" component={DangNhap}/> */}
            <IsPrivateAdmins exact path="/dangnhap" component={DangNhap} />
            <PrivateAdmin exact path="/chuyennganh" component={ChuyenNganh} />
            <PrivateAdmin exact path="/chuyennganh/them" component={ThemChuyenNganh} />
            <PrivateAdmin exact path="/chuyennganh/:id/capnhat" component={ThemChuyenNganh} />

            <PrivateAdmin exact path="/lichthi" component={LichThi} />
            <PrivateAdmin exact path="/lichthi" component={LichThi} />
            <PrivateAdmin exact path="/lichthi/them" component={ThemLichThi} />
            <PrivateAdmin exact path="/lichthi/:id/them" component={ThemLichThi} />
            <PrivateAdmin exact path="/lichthi/:id/:loai/capnhat" component={ThemLichThi} />

            <PrivateAdmin exact path="/monhoc" component={MonHoc} />
            <PrivateAdmin exact path="/monhoc/them" component={ThemMonHoc} />
            <PrivateAdmin exact path="/monhoc/:id/capnhat" component={ThemMonHoc} />

            <PrivateAdmin exact path="/vipham" component={ViPham} />
            <PrivateAdmin exact path="/vipham/them" component={ThemViPham} />
            <PrivateAdmin exact path="/vipham/sinhvien" component={ThemViPhamSinhVien} />
            <PrivateAdmin exact path="/vipham/:id/capnhat" component={ThemViPham} />

            <PrivateAdmin exact path="/giaovien" component={GiaoVien} />
            <PrivateAdmin exact path="/giaovien/them" component={ThemGiaoVien} />
            <PrivateAdmin exact path="/giaovien/:id/capnhat" component={ThemGiaoVien} />

            <PrivateAdmin exact path="/kinhphi"  component={KinhPhi} />
            <PrivateAdmin exact path="/kinhphi/them" component={ThemKinhPhi} />
            <PrivateAdmin exact path="/kinhphi/:id/capnhat" component={ThemKinhPhi} />

            <PrivateAdmin exact path="/khoa" component={Khoa} />
            <PrivateAdmin exact path="/khoa/them" component={ThemKhoa} />
            <PrivateAdmin exact path="/khoa/:id/capnhat" component={ThemKhoa} />
            
            <PrivateAdmin exact path="/lop" component={Lop} />
            <PrivateAdmin exact path="/lop/them" component={ThemLop} />
            <PrivateAdmin exact path="/lop/:id/capnhat" component={ThemLop} />
            <PrivateAdmin exact path="/lop/:id/danhsach" component={CTLop} />

            <PrivateAdmin exact path="/ctlop" component={CTLop} />
            <PrivateAdmin exact path="/lop/:id/themsinhvien" component={ThemCTLop} />
            <PrivateAdmin exact path="/ctlop/:id/capnhat" component={ThemCTLop} />

            <PrivateAdmin exact path="/lophocphan" component={LopHocPhan} />
            <PrivateAdmin exact path="/lophocphan/them" component={ThemLopHocPhan} />
            <PrivateAdmin exact path="/lophocphan/:id/capnhat" component={CapNhatLopHocPhan} />

            <PrivateAdmin exact path="/ctlophocphan" component={CTLopHocPhan} />
            <PrivateAdmin exact path="/lophocphan/:id/themsinhvien" component={ThemCTLopHocPhan} />
            <PrivateAdmin exact path="/ctlophocphan/:id/:mssv" component={ThemCTLopHocPhan} />
            <PrivateAdmin exact path="/lophocphan/:id/danhsach" component={CTLopHocPhan} />
            
            <PrivateAdmin exact path="/admin" component={Admin} />
            <PrivateAdmin exact path="/admin/them" component={ThemAdmin} />
            <PrivateAdmin exact path="/admin/:id/capnhat" component={ThemAdmin} />
            <PrivateAdmin exact path="/admin/:id/profile" component={ProfileAdmin} />
            
            <PrivateAdmin exact path="/phuhuynh" component={PhuHuynh} />
            <PrivateAdmin exact path="/phuhuynh/them" component={ThemPhuHuynh} />
            <PrivateAdmin exact path="/phuhuynh/:id/capnhat" component={ThemPhuHuynh} />

            <PrivateAdmin exact path="/sinhvien" component={SinhVien} />
            <PrivateAdmin exact path="/sinhvien/them" component={ThemSinhVien} />
            <PrivateAdmin exact path="/sinhvien/:id/capnhat" component={ThemSinhVien} />

            <PrivateAdmin exact path="/yeucau" component={YeuCau} />
            <PrivateAdmin exact path="/yeucau/:id/:loai/chitiet" component={XLYeuCau} />
          </Switch>
        </Router>
      </Provider>
    );
  }
}


export default App;
