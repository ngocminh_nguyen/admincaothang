import * as types from '../constants/constants';

const initialState = {
    DSKinhPhi: [],
    CTKinhPhi: [],
    status:''
};
export default function (state = initialState, action) {
    switch (action.type) {
        case types.FETCH_KINH_PHI:
            return {
                ...state,
                DSKinhPhi: action.payload,
            }
        case types.DANH_SACH_KINH_PHI:
            state = action.kinhphi;
            return {
                ...state,
                DSKinhPhi: action.payload
            };
        case types.THEM_KINH_PHI:
            state = action.kinhphi;
            return {
                ...state,
                DSKinhPhi: action.payload
            };
        case types.CAP_NHAT_KINH_PHI: 
            return {
                ...state,
                status:action.payload.data, 
            };
        case types.CHI_TIET_KINH_PHI_THEO_MA_KHOA:
            return {
                ...state,
                CTKinhPhi: action.payload
            };
        default: return state;
    }
}
