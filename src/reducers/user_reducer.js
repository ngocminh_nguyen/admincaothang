import * as types from '../constants/constants';

const initialState = {  
    isLogin: sessionStorage.getItem("token") ? true:false,
    admin: sessionStorage.getItem("token") ? JSON.parse(sessionStorage.getItem("admin")) : {}, 
    user:'',
};
export default function (state = initialState, action) {
    switch (action.type) { 
        case types.DANG_NHAP_ADMIN:
            return {
                ...state,
                isLogin: true, 
                admin: action.payload, 
            }
        case types.DANG_XUAT_ADMIN:
            return {
                ...state,
                isLogin: false,
                admin: ""
            } 
        case types.CHI_TIET_USER_THEO_MA_USER:
            return {
                ...state,
                user: action.payload, 
            }; 
        default: return state;
    }
}
