import * as types from '../constants/constants';

const initialState = {
    DSPhuHuynh: [],
    CTPhuHuynh: [],
    DSPhuHuynhChoOption:[],
    status:''
};
export default function (state = initialState, action) {
    switch (action.type) {
        case types.FETCH_PHU_HUYNH:
            return {
                ...state,
                DSPhuHuynh: action.payload,
            }
        case types.DANH_SACH_PHU_HUYNH:
            state = action.phuhuynh;
            return {
                ...state,
                DSPhuHuynh: action.payload
            };
        case types.DANH_SACH_PHU_HUYNH_CHO_OPTION:
            return {
                ...state,
                DSPhuHuynhChoOption: action.payload
            }
        case types.CAP_NHAT_PHU_HUYNH: 
            return {
                ...state,
                status:action.payload, 
            };
        case types.CHI_TIET_PHU_HUYNH_THEO_MA_PHU_HUYNH:
            return {
                ...state,
                CTPhuHuynh: action.payload,
                status:''
            };
        case types.LAY_DSPH_THEO_TEN:
            state = action.phuhuynh;
            return {
                ...state,
                DSPhuHuynh: action.payload
            };
        default: return state;
    }
}
