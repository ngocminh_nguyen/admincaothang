import * as types from '../constants/constants';

const initialState = {
    DSKhoa: [],
    CTKhoa: [],
    status:''
};
export default function (state = initialState, action) {
    switch (action.type) {
        case types.FETCH_KHOA:
            return {
                ...state,
                DSKhoa: action.payload,
            }
        case types.DANH_SACH_KHOA:
            state = action.payload;
            return {
                ...state,
                DSKhoa: action.payload
            };
        case types.CAP_NHAT_KHOA:
            const khoa = action.payload;
            return {
                ...state,
                status:action.payload.data,
            };
        case types.CHI_TIET_KHOA_THEO_MA_KHOA:
            return {
                ...state,
                CTKhoa: action.payload
            };
        default: return state;
    }
}
