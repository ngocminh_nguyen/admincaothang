import * as types from '../constants/constants';

const initialState = {
    DSGiaoVien: [],
    CTGiaoVien: [],
    DSGiaoVienChoOption:[],
    status:''
};
export default function (state = initialState, action) {
    switch (action.type) {
        case types.FETCH_GIAO_VIEN:
            return {
                ...state,
                DSGiaoVien: action.payload,
            }
        case types.DANH_SACH_GIAO_VIEN:
            state = action.payload;
            return {
                ...state,
                DSGiaoVien: action.payload
            };
        case types.DANH_SACH_GIAO_VIEN_CHO_OPTION:
            state = action.payload;
            return {
                ...state,
                DSGiaoVienChoOption: action.payload
            };
        case types.CAP_NHAT_GIAO_VIEN: 
            return {
                ...state,
                status:action.payload.data, 
            };
        case types.CHI_TIET_GIAO_VIEN_THEO_MA_GV:
            return {
                ...state,
                CTGiaoVien: action.payload
            };
        case types.LAY_DSGV_THEO_TEN:
            return{
                DSGiaoVien: action.payload
            }
        default: return state;
    }
}
