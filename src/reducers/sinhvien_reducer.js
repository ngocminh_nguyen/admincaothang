import * as types from '../constants/constants';
import { type } from 'os';

const initialState = {
    DSSinhVien: [],
    CTSinhVien: [],
    DSSinhVienChoOption: [],
    status:"",
};
export default function (state = initialState, action) {
    switch (action.type) {
        case types.FETCH_SINH_VIEN:
            var a = action.payload.data;
            return {
                ...state,
                DSSinhVien: action.payload,
            }
        case types.DANH_SACH_SINH_VIEN:
            state = action.payload;
            return {
                ...state,
                DSSinhVien: action.payload
            };
        case types.DANH_SACH_SINH_VIEN_CHO_OPTION:
            state = action.payload;
            return {
                ...state,
                DSSinhVienChoOption: action.payload
            };
        case types.THEM_SINH_VIEN_TU_EXCEL_FILE:
            state = action.payload;
            return {
                ...state,
                DSSinhVien: action.payload
        };
        case types.CAP_NHAT_SINH_VIEN:
            const sinhvien = action.payload; 
            return {
                ...state, 
                status:action.payload,
            };
        case types.CHI_TIET_SINHVIEN_THEO_MA_SINHVIEN:
            return {
                ...state,
                CTSinhVien: action.payload
            };
        case types.LAY_DSSV_THEO_MA_SINHVIEN:
            return{
                ...state,
                DSSinhVien: action.payload
            }
        default: return state;
    }
}
