import * as types from '../constants/constants';

const initialState = {
    DSViPham: [],
    CTViPham:[],
    status:""
};
export default function (state = initialState, action) {
    switch (action.type) {
        case types.FETCH_VI_PHAM:
            return {
                ...state,
                DSViPham: action.payload,
            }
        case types.DANH_SACH_VI_PHAM:
            state = action.vipham;
            return {
                ...state,
                DSViPham: action.payload
            };
        case types.CAP_NHAT_VI_PHAM: 
            return {
                ...state,
                status: action.payload.data
            };
        case types.CHI_TIET_VI_PHAM_THEO_MA_VP:
            return {
                ...state,
                CTViPham: action.payload
            };
        default: return state;
    }
}
