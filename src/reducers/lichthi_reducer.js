import * as types from '../constants/constants';

const initialState = {
    DSLichThi: [],
    CTLichThi: [],
    status:''
};
export default function (state = initialState, action) {
    switch (action.type) {
        case types.FETCH_LICH_THI:
            return {
                ...state,
                DSLichThi: action.payload,
            }
        case types.DANH_SACH_LICH_THI:
            state = action.payload;
            return {
                ...state,
                DSLichThi: action.payload, 
            };
        case types.CAP_NHAT_LICH_THI: 
            return {
                ...state,
                status:action.payload.data, 
            };
        case types.CHI_TIET_LICH_THI_THEO_MA_LICH_THI:
            return {
                ...state,
                CTLichThi: action.payload
            };
        default: return state;
    }
}
