import * as types from '../constants/constants';

const initialState = {
    DSLopHocPhan: [],
    CTLopHocPhan: [],
    status:''
};
export default function (state = initialState, action) {
    switch (action.type) {
        case types.FETCH_LOP_HOC_PHAN:
            return {
                ...state,
                DSLopHocPhan: action.payload,
            }
        case types.DANH_SACH_SINH_VIEN_THEO_LOP_HOC_PHAN:
            state = action.payload;
            return {
                ...state,
                DSLopHocPhan: action.payload
            };
        case types.DANH_SACH_LOP_HOC_PHAN:
            state = action.payload;
            return {
                ...state,
                DSLopHocPhan: action.payload
            };
        case types.CAP_NHAT_LOP_HOC_PHAN: 
            return {
                ...state,
                status:action.payload.data,  
            };
        case types.CHI_TIET_LOP_HOC_PHAN_THEO_MA_LOP_HOC_PHAN:
            return {
                ...state,
                CTLopHocPhan: action.payload
            };
        default: return state;
    }
}
