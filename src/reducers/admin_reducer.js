import * as types from '../constants/constants';

const initialState = {
    DSAdmin: [],
    CTAdmin: [], 
    XacThucTK:'',
    XacThucOTP:'',
    status:'',
    statusCNMK:''
};
export default function (state = initialState, action) {
    switch (action.type) {
        case types.FETCH_ADMIN:
            return {
                ...state,
                DSAdmin: action.payload,
            }
        case types.DANH_SACH_ADMIN:
            state = action.payload;
            return {
                ...state,
                DSAdmin: action.payload
            };
        case types.CAP_NHAT_ADMIN: 
            return {
                ...state, 
                CTAdmin: action.payload.admin,
                status:action.payload.status.data, 
            };
        case types.CHI_TIET_ADMIN_THEO_MA_ADMIN:
            return {
                ...state,
                CTAdmin: action.payload
            }; 
        case types.XAC_THUC_TAI_KHOAN: 
            return {
                ...state, 
                XacThucTK: action.payload,
            };
        case types.KIEM_TRA_MA_OTP:{
            return{
                ...state,
                XacThucOTP: action.payload,
            }
        }
        case types.CAP_NHAT_MAT_KHAU:{
            return{
                ...state,
                statusCNMK: action.payload.data
            }
        }
        case types.CAP_NHAT_AVATAR_ADMIN:{
            const admin = action.payload; 
            return {
                ...state,
                CTAdmin: admin, 
            };
        }

        default: return state;
    }
}
