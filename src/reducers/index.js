import {combineReducers} from 'redux';
import { loadingBarReducer } from 'react-redux-loading-bar'
import dsChuyenNganh_re from './chuyennganh_reducer';
import dsMonHoc_re from './monhoc_reducer';
import dsViPham_re from './vipham_reducer';
import dsGiaoVien_re from './giaovien_reducer';
import dsKinhPhi_re from './kinhphi_reducer';

import ctChuyenNganh_re from './chuyennganh_reducer'; 
import ctMonHoc_re from './monhoc_reducer';
import ctViPham_re from './vipham_reducer';
import ctGiaoVien_re from './giaovien_reducer';
import ctKinhPhi_re from './kinhphi_reducer'; 
//Tại vì có bản lớp học và bản chi tiết lớp học nên có ct lớp và ct của chi tiết lớp.
import dsAdmin_re from './admin_reducer';
import dsSinhVien_re from './sinhvien_reducer';
import dsLop_re from './lop_reducer';
import dsCuaChiTietLop_re from './ctlop_reducer';
import dsLopHocPhan_re from './lophocphan_reducer';
import dsCuaChiTietLopHocPhan_re from './ctlophocphan_reducer'
import dsKhoa_re from './khoa_reducer';
import dsLichThi_re from './lichthi_reducer';

import ctAdmin_re from './admin_reducer';
import ctSinhVien_re from './sinhvien_reducer';
import ctLop_re from './lop_reducer';
import ctCuaChiTietLop_re from './ctlop_reducer';
import ctLopHocPhan_re from './lophocphan_reducer';
import ctCuaChiTietLopHocPhan_re from './ctlophocphan_reducer';
import ctKhoa_re from './khoa_reducer';
import ctLichThi_re from './lichthi_reducer';
import ctUser_re from './user_reducer';

import dangnhap_re from './user_reducer';
import dangxuat_re from './user_reducer';

import xtTaiKhoan_re from './admin_reducer';
import xtOTP_re from './admin_reducer';
import dsPhuHuynh_re from './phuhuynh_reducer';
import ctPhuHuynh_re from './phuhuynh_reducer';
import dsYeuCau_re from './yeucau_reducer'
export default combineReducers({
    loadingBar: loadingBarReducer,
    LayDSChuyenNganh: dsChuyenNganh_re,
    LayDSMonHoc: dsMonHoc_re,
    LayDSViPham: dsViPham_re,
    LayDSGiaoVien: dsGiaoVien_re,
    LayDSKinhPhi: dsKinhPhi_re,
    
    LayCTChuyenNganh: ctChuyenNganh_re,
    LayCTMonHoc: ctMonHoc_re,
    LayCTViPham: ctViPham_re,
    LayCTGiaoVien: ctGiaoVien_re,
    LayCTKinhPhi: ctKinhPhi_re,

    LayDSAdmin : dsAdmin_re,
    LayDSSinhVien : dsSinhVien_re,
    LayDSLop : dsLop_re,
    LayDSCuaChiTietLop : dsCuaChiTietLop_re,
    LayDSLopHocPhan : dsLopHocPhan_re,
    LayDSCuaChiTietLopHocPhan : dsCuaChiTietLopHocPhan_re,
    LayDSKhoa: dsKhoa_re,

    LayCTAdmin : ctAdmin_re,
    LayCTSinhVien : ctSinhVien_re,
    LayCTLop : ctLop_re,
    LayCTCuaChiTietLop : ctCuaChiTietLop_re,
    LayCTLopHocPhan : ctLopHocPhan_re,
    LayCTCuaChiTietLopHocPhan : ctCuaChiTietLopHocPhan_re,
    LayCTKhoa: ctKhoa_re,

    DangNhapAdmin: dangnhap_re,
    DangXuatAdmin: dangxuat_re,

    LayDLKiemTraTaiKhoan: xtTaiKhoan_re,
    LayOTPKiemTraTaiKhoan: xtOTP_re,

    LayDSLichThi: dsLichThi_re,
    LayCTLichThi: ctLichThi_re,
    LayDSPhuHuynh: dsPhuHuynh_re,
    LayCTPhuHuynh: ctPhuHuynh_re,
    LayCTUser: ctUser_re,
    LayDSYeuCau: dsYeuCau_re,
});