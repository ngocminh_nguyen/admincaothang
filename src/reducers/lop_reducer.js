import * as types from '../constants/constants';

const initialState = {
    DSLop: [],
    CTLop: [],
    status:'',
};
export default function (state = initialState, action) {
    switch (action.type) {
        case types.FETCH_LOP:
            return {
                ...state,
                DSLop: action.payload,
            }
        case types.DANH_SACH_LOP:
            state = action.payload;
            return {
                ...state,
                DSLop: action.payload
            };
        case types.CAP_NHAT_LOP: 
            return {
                ...state,
                status:action.payload.data, 
            };
        case types.CHI_TIET_LOP_THEO_MA_LOP:
            return {
                ...state,
                CTLop: action.payload
            }; 
        
        case types.DANH_SACH_SV_THEO_LOP:
            return {
                ...state,
                DSLop: action.payload
            };
        default: return state;
    }
}
