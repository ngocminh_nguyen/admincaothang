import * as types from '../constants/constants';

const initialState = {
    DSMonHoc: [],
    DSMonHocChoOption:[],
    CTMonHoc: [],
    status:''
};
export default function (state = initialState, action) {
    switch (action.type) {
        case types.FETCH_MON_HOC:
            return {
                ...state,
                DSMonHoc: action.payload,
            }
        case types.DANH_SACH_MON_HOC:
            return {
                ...state,
                DSMonHoc: action.payload,
                status:"that bai",
            };
        case types.DANH_SACH_MON_HOC_CHO_OPTION:
            return {
                ...state,
                DSMonHocChoOption: action.payload, 
            };
        case types.CAP_NHAT_MON_HOC:
            return {
                ...state,
                status:action.payload.data, 
            };
        case types.CHI_TIET_MON_HOC_THEO_MA_MH:
            return {
                ...state,
                CTMonHoc: action.payload
            };
        case types.LAY_DSMH_THEO_TEN:
            return {
                ...state,
                DSMonHoc: action.payload
            };
        default: return state;
    }
}
