import * as types from '../constants/constants';

const initialState = {
    DSChiTietLop: [],
    CTCuaChiTietLop: [],
    status:''
};
export default function (state = initialState, action) {
    switch (action.type) {
        case types.FETCH_CTLOP:
            return {
                ...state,
                DSChiTietLop: action.payload,
            }
        case types.DANH_SACH_CTLOP:
            state = action.payload;
            return {
                ...state,
                DSChiTietLop: action.payload
            };
        case types.CAP_NHAT_CTLOP:
            const ctlop = action.payload;
            return {
                ...state,
                status:action.payload.data, 
            };
        case types.CHI_TIET_CTLOP_THEO_MA_CTLOP:
            return {
                ...state,
                CTCuaChiTietLop: action.payload
            };
        case types.XOA_CTLOP:
            var a = action.payload; 
            return {
                ...state,
                CTCuaChiTietLop: action.payload
            };
        default: return state;
    }
}
