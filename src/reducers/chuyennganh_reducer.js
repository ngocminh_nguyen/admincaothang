import * as types from '../constants/constants';

const initialState = {
    DSChuyenNganh: [],
    CTChuyenNganh:[],
    status:''
};
export default function( state = initialState, action){ 
    switch (action.type){
        case types.FETCH_CHUYEN_NGANH:
            return {
                ...state,
                DSChuyenNganh: action.payload,
            };
        case types.DANH_SACH_CHUYEN_NGANH: 
            return{
                ...state,
                DSChuyenNganh: action.payload,
                status:"that bai",
            };
        case types.CAP_NHAT_CHUYEN_NGANH: 
            return{
                ...state,
                status:action.payload.data, 
            };
        case types.CHI_TIET_CHUYEN_NGANH_THEO_MA_SO:
            return{
                ...state,
                CTChuyenNganh: action.payload
            };
        default: return state;
    }
}
