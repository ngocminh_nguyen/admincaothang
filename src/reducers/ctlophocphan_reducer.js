import * as types from '../constants/constants';

const initialState = {
    DSChiTietLopHocPhan: [],
    CTCuaChiTietLopHocPhan: [],
    status:''
};
export default function (state = initialState, action) {
    switch (action.type) {
        case types.FETCH_CTLOP_HOC_PHAN:
            return {
                ...state,
                DSChiTietLopHocPhan: action.payload,
            }
        case types.DANH_SACH_CTLOP_HOC_PHAN:
            state = action.payload;
            return {
                ...state,
                DSChiTietLopHocPhan: action.payload, 
            };
        case types.CAP_NHAT_CTLOP_HOC_PHAN: 
            return {
                ...state,
                status:action.payload.data,  
            };
        case types.CHI_TIET_CTLOP_HOC_PHAN_THEO_MA_CTLOP_HOC_PHAN:
            return {
                ...state,
                CTCuaChiTietLopHocPhan: action.payload,
                status:"Thất bại",
                a:console.log(state.status,"31")

            };
        default: return state;
    }
}
