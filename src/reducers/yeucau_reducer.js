import * as types from '../constants/constants';

const initialState = {
    DSYeuCau: [], 
    CTYeuCau:[],
};
export default function (state = initialState, action) {
    switch (action.type) { 
        case types.DANH_SACH_YEU_CAU: 
            return {
                ...state,
                DSYeuCau: action.payload
            };
        case types.CHI_TIET_YEU_CAU_THEO_MA_YC: 
            return {
                ...state,
                CTYeuCau: action.payload,
            }; 
        default: return state;
    }
}
